import { MatListModule } from '@angular/material/list';
import { CauHinhHeThongRoutingModule } from "./cauhinhhethong-routing.module";
// import { DanhmucphancapComponent } from "./danhmucphancap/danhmucphancap.component";
// import { CauhinhphancapapiComponent } from "./cauhinhphancapapi/cauhinhphancapapi.component";
import { SharedModule } from "src/app/shared/shared.module";
import { MatSelectModule } from "@angular/material/select";
import { DefaultModule } from "./../../layouts/default/default.module";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatMenuModule } from "@angular/material/menu";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCardModule } from "@angular/material/card";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatButtonModule } from "@angular/material/button";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatIconModule } from "@angular/material/icon";
import { MatTreeModule } from "@angular/material/tree";
import { MatCheckbox, MatCheckboxModule } from "@angular/material/checkbox";

import {
  MatNativeDateModule,
  MatOptionModule,
  MAT_DATE_LOCALE,
} from "@angular/material/core";
import { LogdangnhapComponent } from "./logdangnhap/logdangnhap.component";
import { LogchucnangComponent } from "./logchucnang/logchucnang.component";
@NgModule({
  declarations: [
    // CauhinhphancapapiComponent,
    // DanhmucphancapComponent,
    LogdangnhapComponent,
    LogchucnangComponent,
  ],
  imports: [
    DefaultModule,
    CommonModule,
    CauHinhHeThongRoutingModule,
    MatToolbarModule,
    MatCardModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule,
    MatTooltipModule,
    MatTreeModule,
    MatCheckboxModule,
    MatOptionModule,
    MatSelectModule,
    SharedModule,
    MatNativeDateModule,
    MatListModule
  ],
  exports: [
    // CauhinhphancapapiComponent,
    // DanhmucphancapComponent,
    LogdangnhapComponent,
    LogchucnangComponent,
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: "vn-VI" }],
})
export class CauHinhHeThongModule {}
