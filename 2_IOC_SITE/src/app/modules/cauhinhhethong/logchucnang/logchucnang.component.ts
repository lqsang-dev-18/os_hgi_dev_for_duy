import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MESSAGE_COMMON, MESSAGE_TYPE } from "src/app/constant/system-constant";
import { AggService } from "src/app/services/agg.service";
import { pageSizeOptions } from "src/app/services/config.service";
import { PaginatorService } from "src/app/services/paginator.service";
import { SnackbarService } from "src/app/services/snackbar.service";
import { Spinner } from "src/app/services/spinner.service";

@Component({
  selector: "app-logchucnang",
  templateUrl: "./logchucnang.component.html",
  styleUrls: ['../cauhinhhethong.component.scss']
})
export class LogchucnangComponent implements OnInit {
  private subscription: Subscription[] = [];
  private modalRefs: any[] = [];
  public formSearch = new FormGroup({
    keyword: new FormControl(null),
  });

  public ipClient: string = "";
  public serverName: string = "";
  public ipServer: string = "";
  public ghiChu: string = "";
  public moTa: string = "";
  public hoTen: string = "";
  public tenApi: string = "";
  public tenDonVi: string = "";
  public thoiGian: string = "";
  public username: string = "";
  public duongDan: string = "";

  public pageEvent: PageEvent;
  public pageIndex: number = 0;
  public pageSize: number = 10;
  public length: number;
  public pageOption = pageSizeOptions;
  public totalPage: number;
  public ELEMENT_DATA: any[] = [];
  tungay = new FormControl(new Date());
  denngay = new FormControl(new Date());
  dataSource: MatTableDataSource<any>;
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  public displayedColumns: string[] = [
    "stt",
    "username",
    "hoten",
    "tendonvi",
    "tenapi",
    "ghichu",
    "thoidiem",
  ];
  constructor(
    private aggService: AggService,
    private spinner: Spinner,
    private snackbar: SnackbarService,
    private translator: PaginatorService,
    private cdRef: ChangeDetectorRef,
    private modalService: NgbModal
  ) {
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }

  ngOnInit(): void {
    this.search();
  }

  ngOnDestroy() {
    this.subscription.forEach((subscription) => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.translator.translatePaginator(this.paginator);
    this.paginator.nextPage = () =>
      this.paginatorChange(this.pageIndex + 1, this.paginator.pageSize, 1);
    this.paginator.previousPage = () =>
      this.paginatorChange(this.pageIndex - 1, this.paginator.pageSize, 2);
    this.paginator.lastPage = () =>
      this.paginatorChange(this.length - 1, this.paginator.pageSize, 3);
    this.paginator.firstPage = () =>
      this.paginatorChange(0, this.paginator.pageSize, 4);
    this.cdRef.detectChanges();
  }

  paginatorChange(page, pageSize, type) {
    switch (type) {
      case 1:
        this.pageIndex++;
        this.search();
        this.resetPageSize();
        break;
      case 2:
        this.pageIndex--;
        this.search();
        this.resetPageSize();
        break;
      case 3:
        this.pageIndex = this.totalPage - 1;
        this.search();
        this.resetPageSize();
        break;
      case 4:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 5:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 6:
        this.search();
        this.resetPageSize();
    }
  }

  resetPageSize() {
    setTimeout(() => {
      this.paginator.pageIndex = this.pageIndex;
      this.dataSource.paginator.length = this.length;
    }, 500);
  }


  search(event?: PageEvent) {
    let keyword = this.formSearch.controls.keyword.value;
    let page = this.pageIndex;
    let size = event != undefined ? event.pageSize : this.pageSize;
    var obj = {
      gia_tri: keyword ?? "",
      page: page + 1,
      size: size,
      tungay : this.dateToStringVN(this.tungay.value),
      denngay: this.dateToStringVN(this.denngay.value)
    };
    this.spinner.show();
    this.subscription.push(
      this.aggService.readLogChucNang(obj).subscribe(
        (rs) => {
          this.ELEMENT_DATA = rs.data;
          this.dataSource.data = this.ELEMENT_DATA;
          this.pageIndex = page;
          this.pageSize = size;
          this.length = rs.total_row;
          this.totalPage = rs.total_page;
          this.resetPageSize();
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
    return event;
  }
  setDataUpdate(idLogin, idAction) {
    var obj = {
      id_login: idLogin,
      id_action: idAction,
    };
    this.subscription.push(
      this.aggService.readCTLogChucNang(obj).subscribe(
        (rs) => {
          let data = rs.data[0];
          this.ipClient = data.IP_CLIENT;
          this.ipServer = data.IP_SERVER;
          this.serverName = data.SERVER_NAME;
          this.ghiChu =  data.GHI_CHU;
          this.moTa =  data.MOTA;
          this.duongDan =  data.DUONG_DAN;
          this.hoTen =  data.HO_TEN;
          this.tenApi =  data.TEN_API;
          this.tenDonVi =  data.TEN_DON_VI;
          this.thoiGian =  data.THOI_DIEM;
          this.username =  data.USERNAME;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
    return;
  }

  dateToStringVN(date) {
    let month = '' + (date.getMonth() + 1).toString();
    let day = '' + date.getDate().toString();
    const year = date.getFullYear().toString();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join("/");
  }

  // Common function
  openModal(content, id, size?) {
    let modalRef = {};
    modalRef[id] = this.modalService.open(content, {
      ariaLabelledBy: id,
      size: size ?? "md",
    });
    modalRef[id].result.then(
      (result) => {
        console.log(`Closed with: ${result}`);
      },
      (reason) => {
        console.log(`with: ${reason}`);
      }
    );
    this.modalRefs.push(modalRef);
  }

  closeModal(id) {
    let modalRef = null;
    this.modalRefs.forEach((element) => {
      Object.keys(element).forEach((key, index) => {
        if (id == key) {
          modalRef = element[key];
          return true;
        }
      });
    });
    if (modalRef != null) {
      modalRef.close();
    }
  }
  // End: Common function
}
