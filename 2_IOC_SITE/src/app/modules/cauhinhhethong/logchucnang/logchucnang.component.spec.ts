import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogchucnangComponent } from './logchucnang.component';

describe('LogchucnangComponent', () => {
  let component: LogchucnangComponent;
  let fixture: ComponentFixture<LogchucnangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogchucnangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogchucnangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
