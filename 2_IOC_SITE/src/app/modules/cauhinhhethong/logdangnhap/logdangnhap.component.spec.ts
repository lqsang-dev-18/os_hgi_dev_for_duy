import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogdangnhapComponent } from './logdangnhap.component';

describe('LogdangnhapComponent', () => {
  let component: LogdangnhapComponent;
  let fixture: ComponentFixture<LogdangnhapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogdangnhapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogdangnhapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
