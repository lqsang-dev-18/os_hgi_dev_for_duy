import { LogdangnhapComponent } from "./logdangnhap/logdangnhap.component";
import { LogchucnangComponent } from "./logchucnang/logchucnang.component";
// import { CauhinhphancapapiComponent } from "./cauhinhphancapapi/cauhinhphancapapi.component";
// import { DanhmucphancapComponent } from "./danhmucphancap/danhmucphancap.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
  // { path: "cau-hinh/phan-cap-api", component: CauhinhphancapapiComponent },
  // {
  //   path: "danh-muc/phan-cap",
  //   component: DanhmucphancapComponent,
  // },
  {
    path: "log/chuc-nang",
    component: LogchucnangComponent,
  },
  {
    path: "log/dang-nhap",
    component: LogdangnhapComponent,
  },
];

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CauHinhHeThongRoutingModule {}
