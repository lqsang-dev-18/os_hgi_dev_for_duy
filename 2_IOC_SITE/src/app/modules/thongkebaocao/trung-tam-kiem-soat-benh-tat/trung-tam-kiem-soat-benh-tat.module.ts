import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from "@angular/material-moment-adapter";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from "@angular/material/core";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSelectModule } from "@angular/material/select";
import { MatTableModule } from "@angular/material/table";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSelectInfiniteScrollModule } from "ng-mat-select-infinite-scroll";
import { SharedModule } from "./../../../shared/shared.module";
import { TrungTamKiemSoatBenhTatRoutingModule } from "./trung-tam-kiem-soat-benh-tat-routing.module";
import { BaoCaoPhongChongUngThuComponent } from "./phong-chong-ung-thu/bao-cao-phong-chong-ung-thu.component";
import { FilterTypePipe } from "./filterType.pipe";
import { BaoCaoPhongChongHivAidsComponent } from './bao-cao-phong-chong-hiv-aids/bao-cao-phong-chong-hiv-aids.component';

@NgModule({
  declarations: [FilterTypePipe, BaoCaoPhongChongUngThuComponent, BaoCaoPhongChongHivAidsComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatSelectInfiniteScrollModule,
    MatDatepickerModule,
    SharedModule,
    TrungTamKiemSoatBenhTatRoutingModule
  ],
  exports: [
    MatFormFieldModule,
    MatToolbarModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatSelectInfiniteScrollModule,
  ],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: "en-GB" },
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
  ],
})
export class TrungTamKiemSoatBenhTatModule {}
