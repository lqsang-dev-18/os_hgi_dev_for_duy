import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaoCaoPhongChongHivAidsComponent } from './bao-cao-phong-chong-hiv-aids.component';

describe('BaoCaoPhongChongHivAidsComponent', () => {
  let component: BaoCaoPhongChongHivAidsComponent;
  let fixture: ComponentFixture<BaoCaoPhongChongHivAidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaoCaoPhongChongHivAidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaoCaoPhongChongHivAidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
