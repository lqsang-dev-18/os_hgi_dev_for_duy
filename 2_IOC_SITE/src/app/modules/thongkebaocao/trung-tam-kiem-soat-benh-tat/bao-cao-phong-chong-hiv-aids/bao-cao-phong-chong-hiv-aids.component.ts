import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Subscription } from 'rxjs';
import { PICK_FORMATS, pageSizeOptions } from 'src/app/services/config.service';
import { PaginatorService } from 'src/app/services/paginator.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Spinner } from 'src/app/services/spinner.service';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { DatePipe } from '@angular/common';
import { VNDateAdapter } from 'src/environments/datepicker-apdapter';
import { MatTableDataSource } from '@angular/material/table';
import { AggService } from 'src/app/services/agg.service';
import { MESSAGE_TYPE } from 'src/app/constant/system-constant';
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import * as moment from 'moment';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';
import { CommonFunctionService } from 'src/app/services/common-function.service';

@Component({
  selector: 'app-bao-cao-phong-chong-hiv-aids',
  templateUrl: './bao-cao-phong-chong-hiv-aids.component.html',
  styleUrls: ['./bao-cao-phong-chong-hiv-aids.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: VNDateAdapter },
    { provide: MAT_DATE_LOCALE, useValue: PICK_FORMATS },
    DatePipe]
})
export class BaoCaoPhongChongHivAidsComponent implements OnInit, OnDestroy {
  private subscription: Subscription[] = [];
  private currentDate: Date = new Date();

  public LIST_NAM: any[] = [];
  public LIST_THANG: any[] = [];
  public formSearch = new FormGroup({
    loai: new FormControl(0, [Validators.required]),
    nam: new FormControl(this.currentDate.getFullYear()),
    thang: new FormControl(this.currentDate.getMonth()),
    tuNgay: new FormControl(this.currentDate),
    denNgay: new FormControl(this.currentDate)
  });

  public pageOption = pageSizeOptions;
  public pageSizeDefault: number = this.pageOption[0];
  public ELEMENT_DATA: any[] = [];
  dataSource!: MatTableDataSource<any>;
  @ViewChild('paginator', { static: true }) paginator!: MatPaginator;
  public displayedColumns: string[] = [
    'stt',
    'nam',
    'thang',
    'tenHuyen',
    'tyLeNguoiNhiem',
    'soMauPhatHien',
    'soNguoiDieuTri',
    'tyLeNguoiLon',
    'soTreEm',
    'soPhuNuMangThai',
    'tyLePhuNuMangThai',
    'tyLeNguoiBiLaoHiv'
  ];

  constructor(private aggService: AggService,
    private spinner: Spinner,
    private snackbar: SnackbarService,
    private translator: PaginatorService,
    private cdRef: ChangeDetectorRef,
    private dateAdapter: DateAdapter<Date>,
    private cmFunction: CommonFunctionService
  ) {
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
    this.dateAdapter.setLocale("vi-VN");
  }

  ngOnInit(): void {
    for (let i = 1; i <= 12; i++) {
      this.LIST_THANG.push({ key: i, value: "Tháng " + i });
    }
    for (let i = this.currentDate.getFullYear(); i >= 1970; i--) {
      this.LIST_NAM.push({ key: i, value: "Năm " + i });
    }
    this.search();
  }

  ngOnDestroy(): void {
    this.subscription.forEach(subscription => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.translator.translatePaginator(this.paginator);
    this.cdRef.detectChanges();
  }

  public resetPaginator(index: number, length: number) {
    setTimeout(() => { // need timeout
      this.paginator.pageIndex = index;
      this.paginator.length = length;
    });
  }

  public search(event?: PageEvent) {
    if (!event) { this.paginator.pageIndex = 0; }
    const formData = this.formSearch.getRawValue();
    var obj: any = {
      page: this.paginator.pageIndex + 1,
      size: this.paginator.pageSize ?? this.pageSizeDefault
    };
    if (formData.loai == 0) {
      obj.nam = formData.nam;
    } else if (formData.loai == 1) {
      obj.nam = formData.nam;
      obj.thang = formData.thang;
    } else {
      obj.tuNgay = moment(formData.tuNgay).format('DD/MM/YYYY');
      obj.denNgay = moment(formData.denNgay).format('DD/MM/YYYY');
    }
    this.spinner.show();
    this.subscription.push(this.aggService.getBaoCaoPhongChongHivAids(obj).subscribe({
      next: (rs) => {
        this.ELEMENT_DATA = rs.data;
        this.dataSource.data = this.ELEMENT_DATA;
        this.resetPaginator(obj.page - 1, rs.total_row);
        this.spinner.hide();
      }, error: (error) => {
        this.spinner.hide();
        this.snackbar.showError(error.message, MESSAGE_TYPE.ERROR);
      }
    }));
    return event;
  }

  exportToExcel(): void {
    const sheetName = 'Sheet 1';
    const columnNames = JSON.parse(`[{"NAM":"Năm"}, {"THANG":"Tháng"}, {"TEN_HUYEN":"Tên huyện"}, 
      {"TY_LE_NHIEM_HIV_BHYT":"Tỷ lệ người nhiễm HIV có thẻ bảo hiểm y tế được chi trả theo quy định"},
      {"SO_MAU_PHAT_HIEN":"Số mẫu giám sát phát hiện"}, {"SO_DIEU_TRI_METHADONE":"Số người điều trị Methadone"},
      {"TY_LE_NGUOI_LON_ARV":"Tỷ lệ bệnh nhân người lớn điều trị ARV/Số BN đủ tiêu chuẩn điều trị"},
      {"SO_TRE_EM_ARV":"Số trẻ em điều trị ARV"}, {"SO_PHU_NU_MANG_THAI_XN_HIV":"Số phụ nữ mang thai được xét nghiệm HIV"},
      {"TY_LE_PHU_NU_MANG_THAI_DPLTMC":"Tỷ lệ Phụ nữ mang thai HIV (+) được điều trị DPLTMC"}, 
      {"TY_LE_HIV_LAO_ARV":"Tỷ lệ người nhiễm HIV mắc lao được điều trị đồng thời Lao và ARV"}]`);
    let maxWidths = [];
    const mappedData = this.dataSource.data.map((item) => {
      const mappedItem = {};
      columnNames.forEach((columnName, index) => {
        const key = Object.keys(columnName)[0];
        mappedItem[columnName[key]] = item[key];
        const value = item[key] ?? 0;
        let colWidth = Math.ceil(value.toString().length + 2)
        if (Math.ceil(columnName[key].toString().length + 2) > Math.ceil(value.toString().length + 2)) {
          colWidth = Math.ceil(columnName[key].toString().length + 2);
        }
        if (!maxWidths[index] || colWidth > maxWidths[index]) {
          maxWidths[index] = { width: colWidth };
        }
      });
      return mappedItem;
    });
    const worksheet = XLSX.utils.json_to_sheet(mappedData);

    worksheet["!autofilter"] = { ref: `A1:B${mappedData.length + 1}` };

    worksheet["!cols"] = maxWidths;
    const workbook = {
      Sheets: { [sheetName]: worksheet },
      SheetNames: [sheetName],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    const excelFile: Blob = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    saveAs(excelFile, this.cmFunction.getStringDateTime() + ".xlsx");
  }

  exportToPDF() {
    // Demo export to PDF via html2canvas
    const element = document.getElementById("reportView");
    if (!element) {
      return;
    }
    const doc = new jsPDF('l', 'mm', 'a4'); // landscape mode, millimeters unit, A4 size
    const margin = 0;
    const width = doc.internal.pageSize.getWidth() - 2 * margin;
    const height = doc.internal.pageSize.getHeight() - 2 * margin;
    const scale = 3; // larger = more quality
    html2canvas(element, { scale: scale, onclone: (clonedDoc) => {
      let element = clonedDoc.getElementById('reportView');
      if (element) { element.style.display = 'block'; }
    }}).then((canvas) => {
      const imgData = canvas.toDataURL('image/jpeg'); // png, jpeg
      doc.addImage(imgData, 'JPEG', margin, margin, width, height); // PNG, JPEG
      doc.save('report.pdf');
    });
  }
}
