import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BaoCaoPhongChongUngThuComponent } from "./phong-chong-ung-thu/bao-cao-phong-chong-ung-thu.component";
import { BaoCaoPhongChongHivAidsComponent } from './bao-cao-phong-chong-hiv-aids/bao-cao-phong-chong-hiv-aids.component';

const routes: Routes = [
  { path: "bao-cao-phong-chong-ung-thu", component: BaoCaoPhongChongUngThuComponent },
  { path: 'bao-cao-phong-chong-hiv-aids', component: BaoCaoPhongChongHivAidsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TrungTamKiemSoatBenhTatRoutingModule {}
