import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KyThuatKiemNghiemRoutingModule } from './ky-thuat-kiem-nghiem-routing.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { SharedModule } from 'src/app/shared/shared.module';
import { ChiTieuHoatDongComponent } from './chi-tieu-hoat-dong/chi-tieu-hoat-dong.component';

@NgModule({
  declarations: [ChiTieuHoatDongComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    SharedModule,
    KyThuatKiemNghiemRoutingModule
  ]
})
export class KyThuatKiemNghiemModule { }
