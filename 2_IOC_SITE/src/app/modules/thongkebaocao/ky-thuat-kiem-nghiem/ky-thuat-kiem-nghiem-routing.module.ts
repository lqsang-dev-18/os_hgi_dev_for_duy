import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChiTieuHoatDongComponent } from './chi-tieu-hoat-dong/chi-tieu-hoat-dong.component';

const routes: Routes = [
  { path: 'chi-tieu-hoat-dong', component: ChiTieuHoatDongComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KyThuatKiemNghiemRoutingModule { }
