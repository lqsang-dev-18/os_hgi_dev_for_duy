import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChiTieuHoatDongComponent } from './chi-tieu-hoat-dong.component';

describe('ChiTieuHoatDongComponent', () => {
  let component: ChiTieuHoatDongComponent;
  let fixture: ComponentFixture<ChiTieuHoatDongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChiTieuHoatDongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChiTieuHoatDongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
