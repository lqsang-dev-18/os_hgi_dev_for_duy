import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChuakhoiondinhTTDKComponent } from './chua-khoi-on-dinh-tt-dk.component';

describe('ChuakhoiondinhTTDKComponent', () => {
  let component: ChuakhoiondinhTTDKComponent;
  let fixture: ComponentFixture<BChuakhoiondinhTTDKComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChuakhoiondinhTTDKComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChuakhoiondinhTTDKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
