import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhnhanlauComponent } from './benh-nhan-lau.component';

describe('BenhnhanlauComponent', () => {
  let component: BenhnhanlauComponent;
  let fixture: ComponentFixture<BenhnhanlauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhnhanlauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhnhanlauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
