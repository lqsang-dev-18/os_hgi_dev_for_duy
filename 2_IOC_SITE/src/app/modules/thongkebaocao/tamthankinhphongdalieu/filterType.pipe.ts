import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "filterType" })
export class FilterTypePipe implements PipeTransform {
  transform(value: number): string {
    if (value == 1) {
      return "Lọc từ ngày đến ngày";
    } else if (value == 2) {
      return "Lọc theo Tháng";
    } else if (value == 3) {
      return "Lọc theo Quý";
    } else if (value == 4) {
      return "Lọc theo Năm";
    }
    return "";
  }
}
