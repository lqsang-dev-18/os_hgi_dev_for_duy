import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChitieuttkpdlComponent } from './chitieuttkpdl/chitieuttkpdl.component';
import { TtkpdlbenhmoiComponent } from './ttkpdlbenhmoi/ttkpdlbenhmoi.component';
import { TtkpdlbenhquanlyComponent } from './ttkpdlbenhquanly/ttkpdlbenhquanly.component';
import { BenhnhandhtlComponent } from './benhnhandhtl/benhnhandhtl.component';
import { BenhnhanhoanthanhdhtlComponent } from './benhnhanhoanthanhdhtl/benhnhanhoanthanhdhtl.component';
import { BenhnhangiamsatComponent } from './benhnhangiamsat/benhnhangiamsat.component';
import { NguoitxbenhnhanphongComponent } from './nguoitxbenhnhanphong/nguoitxbenhnhanphong.component';
import { BenhnhanstiComponent } from './benhnhansti/benhnhansti.component';
import { BenhnhanlauComponent } from './benh-nhan-lau/benh-nhan-lau.component';
import { BenhnhangiangmaiComponent } from './benh-nhan-giang-mai/benh-nhan-giang-mai.component';
import { ChuakhoiondinhTTDKComponent } from './chua-khoi-on-dinh-tt-dk/chua-khoi-on-dinh-tt-dk.component';


const routes: Routes = [
  { path: 'chi-tieu-tam-than-kinh-phong-da-lieu', component: ChitieuttkpdlComponent },
  { path: 'tam-than-kinh-phong-da-lieu-benh-moi', component: TtkpdlbenhmoiComponent },
  { path: 'tam-than-kinh-phong-da-lieu-benh-quan-ly', component: TtkpdlbenhquanlyComponent },
  { path: 'benh-nhan-DHTL-trong-nam', component: BenhnhandhtlComponent },
  { path: 'benh-nhan-hoan-thanh-DHTL', component: BenhnhanhoanthanhdhtlComponent },
  { path: 'benh-nhan-giam-sat', component: BenhnhangiamsatComponent },
  { path: 'nguoi-tiep-xuc-voi-BN-phong', component: NguoitxbenhnhanphongComponent },
  { path: 'benh-nhan-STI', component: BenhnhanstiComponent },
  { path: 'benh-nhan-giang-mai', component: BenhnhangiangmaiComponent },
  { path: 'chua-khoi-on-dinh-tt-dk', component: ChuakhoiondinhTTDKComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TamThanKinhPhongDaLieuRoutingModule { }
