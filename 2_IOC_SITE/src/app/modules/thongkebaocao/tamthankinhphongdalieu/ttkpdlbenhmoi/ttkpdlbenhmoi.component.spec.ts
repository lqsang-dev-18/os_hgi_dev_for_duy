import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtkpdlbenhmoiComponent } from './ttkpdlbenhmoi.component';

describe('TtkpdlbenhmoiComponent', () => {
  let component: TtkpdlbenhmoiComponent;
  let fixture: ComponentFixture<TtkpdlbenhmoiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TtkpdlbenhmoiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtkpdlbenhmoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
