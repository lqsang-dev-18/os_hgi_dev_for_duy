import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhnhangiamsatComponent } from './benhnhangiamsat.component';

describe('BenhnhangiamsatComponent', () => {
  let component: BenhnhangiamsatComponent;
  let fixture: ComponentFixture<BenhnhangiamsatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhnhangiamsatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhnhangiamsatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
