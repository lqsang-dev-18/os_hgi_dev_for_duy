import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChitieuttkpdlComponent } from './chitieuttkpdl.component';

describe('ChitieuttkpdlComponent', () => {
  let component: ChitieuttkpdlComponent;
  let fixture: ComponentFixture<ChitieuttkpdlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChitieuttkpdlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChitieuttkpdlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
