import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NguoitxbenhnhanphongComponent } from './nguoitxbenhnhanphong.component';

describe('NguoitxbenhnhanphongComponent', () => {
  let component: NguoitxbenhnhanphongComponent;
  let fixture: ComponentFixture<NguoitxbenhnhanphongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NguoitxbenhnhanphongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NguoitxbenhnhanphongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
