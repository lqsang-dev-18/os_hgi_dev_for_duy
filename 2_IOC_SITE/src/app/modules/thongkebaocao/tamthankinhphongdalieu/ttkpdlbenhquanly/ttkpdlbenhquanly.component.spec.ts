import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtkpdlbenhquanlyComponent } from './ttkpdlbenhquanly.component';

describe('TtkpdlbenhquanlyComponent', () => {
  let component: TtkpdlbenhquanlyComponent;
  let fixture: ComponentFixture<TtkpdlbenhquanlyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TtkpdlbenhquanlyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtkpdlbenhquanlyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
