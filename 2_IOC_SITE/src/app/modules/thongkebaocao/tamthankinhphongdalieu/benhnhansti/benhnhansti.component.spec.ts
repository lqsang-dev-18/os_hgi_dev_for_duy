import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhnhanstiComponent } from './benhnhansti.component';

describe('BenhnhanstiComponent', () => {
  let component: BenhnhanstiComponent;
  let fixture: ComponentFixture<BenhnhanstiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhnhanstiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhnhanstiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
