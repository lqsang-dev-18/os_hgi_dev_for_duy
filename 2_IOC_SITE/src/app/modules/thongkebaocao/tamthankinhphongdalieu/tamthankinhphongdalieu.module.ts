import { SharedModule } from './../../../shared/shared.module';
import { NgxDropdownComponent } from './../../../shared/components/ngx-dropdown/ngx-dropdown.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectInfiniteScrollModule } from 'ng-mat-select-infinite-scroll';

import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { TtkpdlbenhquanlyComponent } from './ttkpdlbenhquanly/ttkpdlbenhquanly.component';
import { ChitieuttkpdlComponent } from './chitieuttkpdl/chitieuttkpdl.component';
import { TtkpdlbenhmoiComponent } from './ttkpdlbenhmoi/ttkpdlbenhmoi.component';
import { TamThanKinhPhongDaLieuRoutingModule } from './tamthankinhphongdalieu-routing.module';
import { FilterTypePipe } from './filterType.pipe';
import { BenhnhandhtlComponent } from './benhnhandhtl/benhnhandhtl.component';
import { BenhnhanhoanthanhdhtlComponent } from './benhnhanhoanthanhdhtl/benhnhanhoanthanhdhtl.component';
import { BenhnhangiamsatComponent } from './benhnhangiamsat/benhnhangiamsat.component';
import { NguoitxbenhnhanphongComponent } from './nguoitxbenhnhanphong/nguoitxbenhnhanphong.component';
import { BenhnhanstiComponent } from './benhnhansti/benhnhansti.component';
import { BenhnhanlauComponent } from './benh-nhan-lau/benh-nhan-lau.component';
import { BenhnhangiangmaiComponent } from './benh-nhan-giang-mai/benh-nhan-giang-mai.component';
import { ChuakhoiondinhTTDKComponent } from './chua-khoi-on-dinh-tt-dk/chua-khoi-on-dinh-tt-dk.component';


@NgModule({
  declarations: [
    TtkpdlbenhquanlyComponent,
    ChitieuttkpdlComponent,
    TtkpdlbenhmoiComponent,
    FilterTypePipe, BenhnhandhtlComponent,
    BenhnhanhoanthanhdhtlComponent,
    BenhnhangiamsatComponent,
    NguoitxbenhnhanphongComponent,
    BenhnhanstiComponent,
    BenhnhanlauComponent,
    BenhnhangiangmaiComponent,
    ChuakhoiondinhTTDKComponent],
  imports: [
    CommonModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatSelectInfiniteScrollModule,
    MatDatepickerModule,
    SharedModule,
    TamThanKinhPhongDaLieuRoutingModule
  ],
  exports: [
    MatFormFieldModule,
    MatToolbarModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatSelectInfiniteScrollModule,
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class TamThanKinhPhongDaLieuModule { }
