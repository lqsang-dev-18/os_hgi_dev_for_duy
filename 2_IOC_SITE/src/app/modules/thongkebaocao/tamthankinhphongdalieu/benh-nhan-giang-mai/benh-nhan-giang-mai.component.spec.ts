import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhnhangiangmaiComponent } from './benh-nhan-giang-mai.component';

describe('BenhnhangiangmaiComponent', () => {
  let component: BenhnhangiangmaiComponent;
  let fixture: ComponentFixture<BenhnhangiangmaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhnhangiangmaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhnhangiangmaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
