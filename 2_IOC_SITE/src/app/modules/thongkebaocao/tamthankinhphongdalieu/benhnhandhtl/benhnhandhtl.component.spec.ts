import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhnhandhtlComponent } from './benhnhandhtl.component';

describe('BenhnhandhtlComponent', () => {
  let component: BenhnhandhtlComponent;
  let fixture: ComponentFixture<BenhnhandhtlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhnhandhtlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhnhandhtlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
