import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { MESSAGE_COMMON, MESSAGE_TYPE, DATE_PATTERN } from 'src/app/constant/system-constant';
import { BCTKService } from 'src/app/services/baocaothongke.service';
import { CommonFunctionService } from 'src/app/services/common-function.service';
import { pageSizeOptions } from 'src/app/services/config.service';
import { PaginatorService } from 'src/app/services/paginator.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Spinner } from 'src/app/services/spinner.service';
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import { apiUrl } from 'src/app/constant/api-url';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-benhnhanhoanthanhdhtl',
  templateUrl: './benhnhanhoanthanhdhtl.component.html',
  styleUrls: ['../tamthankinhphongdalieu.component.scss']
})
export class BenhnhanhoanthanhdhtlComponent implements OnInit {

  dateFormat = DATE_PATTERN.NORMAL;
  constructor(
    private bctkService: BCTKService,
    private spinner: Spinner,
    private snackbar: SnackbarService,
    private translator: PaginatorService,
    private cdRef: ChangeDetectorRef,
    private _adapter: DateAdapter<any>,
    private cmFunction: CommonFunctionService
  ) {
    this._adapter.setLocale("vi");
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }
  public ELEMENT_DATA: any[] = [];
  public formSearch = new FormGroup({
    keyword: new FormControl(""),
    tungay: new FormControl(moment()),
    denngay: new FormControl(moment()),
    thang: new FormControl(moment().get("month") + 1),
    nam: new FormControl(moment().get("year")),
    quy: new FormControl(moment().get("quarter")),
    filter: new FormControl(1)
  });
  public displayedColumns: string[] = ['NGAY', 'HO_TEN', 'DIA_CHI'];
  private subscription: Subscription[] = [];
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  public years: number[] = [];
  public months = [
    { name: 'Tháng 1', value: 1 },
    { name: 'Tháng 2', value: 2 },
    { name: 'Tháng 3', value: 3 },
    { name: 'Tháng 4', value: 4 },
    { name: 'Tháng 5', value: 5 },
    { name: 'Tháng 6', value: 6 },
    { name: 'Tháng 7', value: 7 },
    { name: 'Tháng 8', value: 8 },
    { name: 'Tháng 9', value: 9 },
    { name: 'Tháng 10', value: 10 },
    { name: 'Tháng 11', value: 11 },
    { name: 'Tháng 12', value: 12 }
  ];
  public quarters = [
    { name: 'Quý 1', value: 1 },
    { name: 'Quý 2', value: 2 },
    { name: 'Quý 3', value: 3 },
    { name: 'Quý 4', value: 4 },
  ];
  public pageEvent: PageEvent;
  public pageIndex: number = 0;
  public pageSize: number = 10;
  public length: number;
  public pageOption = pageSizeOptions;
  public totalPage: number;
  dataSource: MatTableDataSource<any>;
  hideDate = 0;
  hideMonth = 0;
  hideQuater = 0;
  hideYear = 0;
  ngOnInit(): void {
    this.initializeYears();
    this.hideFilter(this.formSearch.controls.filter.value);
    this.search();
  }
  initializeYears() {
    this.years = [];
    for (let i = moment().get("year") - 10; i <= moment().get("year") + 10; i++) {
      this.years.push(i);
    }
  }
  hideFilter(value) {
    switch (value) {
      case 1:
        this.hideDate = 0;
        this.hideMonth = 1;
        this.hideQuater = 1;
        this.hideYear = 1;
        break;
      case 2:
        this.hideDate = 1;
        this.hideMonth = 0;
        this.hideQuater = 1;
        this.hideYear = 0;
        break;
      case 3:
        this.hideDate = 1;
        this.hideMonth = 1;
        this.hideQuater = 0;
        this.hideYear = 0;
        break;
      case 4:
        this.hideDate = 1;
        this.hideMonth = 1;
        this.hideQuater = 1;
        this.hideYear = 0;
        break;
      default:
        this.hideDate = 1;
        this.hideMonth = 1;
        this.hideQuater = 1;
        this.hideYear = 0;
        break;
    }
  }
  generateObj(value, page, size) {
    var obj = {
      page: page + 1,
      size: size
    };

    if (value == 1) {
      obj["tungay"] = this.cmFunction.dateToStringVN(this.formSearch.controls.tungay.value);
      obj["denngay"] = this.cmFunction.dateToStringVN(this.formSearch.controls.denngay.value);
    } else if (value == 2) {
      obj["thang"] = this.formSearch.controls.thang.value;
      obj["nam"] = this.formSearch.controls.nam.value;
    } else if (value == 3) {
      obj["quy"] = this.formSearch.controls.quy.value;
      obj["nam"] = this.formSearch.controls.nam.value;
    } else if (value == 4) {
      obj["nam"] = this.formSearch.controls.nam.value;
    }
    return obj;
  }
  search(event?: PageEvent) {
    let page = this.pageIndex;
    let size = event != undefined ? event.pageSize : this.pageSize;
    var obj = this.generateObj(this.formSearch.controls.filter.value, page, size)
    this.spinner.show();
    this.subscription.push(
      this.bctkService.loadBaoCao(apiUrl.TAMTHANKINH_PHONG_DALIEU.VIEW_BENHNHAN_HOANTHANH_DHTL, obj).subscribe(
        (rs) => {
          this.ELEMENT_DATA = rs.data;
          this.dataSource.data = this.ELEMENT_DATA;
          this.pageIndex = page;
          this.pageSize = size;
          this.length = rs.total_row;
          this.totalPage = rs.total_page;
          this.resetPageSize();
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
    return event;
  }
  paginatorChange(page, pageSize, type) {
    switch (type) {
      case 1:
        this.pageIndex++;
        this.search();
        this.resetPageSize();
        break;
      case 2:
        this.pageIndex--;
        this.search();
        this.resetPageSize();
        break;
      case 3:
        this.pageIndex = this.totalPage - 1;
        this.search();
        this.resetPageSize();
        break;
      case 4:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 5:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 6:
        this.search();
        this.resetPageSize();
    }
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.translator.translatePaginator(this.paginator);
    this.paginator.nextPage = () =>
      this.paginatorChange(this.pageIndex + 1, this.paginator.pageSize, 1);
    this.paginator.previousPage = () =>
      this.paginatorChange(this.pageIndex - 1, this.paginator.pageSize, 2);
    this.paginator.lastPage = () =>
      this.paginatorChange(this.length - 1, this.paginator.pageSize, 3);
    this.paginator.firstPage = () =>
      this.paginatorChange(0, this.paginator.pageSize, 4);
    this.cdRef.detectChanges();
  }
  resetPageSize() {
    setTimeout(() => {
      this.paginator.pageIndex = this.pageIndex;
      this.dataSource.paginator.length = this.length;
    }, 500);
  }
  exportToExcel(): void {
    const sheetName = 'Sheet 1';
    const columnNames = JSON.parse(`[{"NGAY":"Ngày"}, {"HO_TEN":"Họ tên"}, {"DIA_CHI":"Địa chỉ"}]`);
    let maxWidths = [];
    const mappedData = this.dataSource.data.map((item) => {
      const mappedItem = {};

      columnNames.forEach((columnName, index) => {
        const key = Object.keys(columnName)[0];
        mappedItem[columnName[key]] = item[key];
        const value = item[key] ?? 0;
        let colWidth = Math.ceil(value.toString().length + 2)
        if (Math.ceil(columnName[key].toString().length + 2) > Math.ceil(value.toString().length + 2))
          colWidth = Math.ceil(columnName[key].toString().length + 2);
        if (!maxWidths[index] || colWidth > maxWidths[index]) {
          maxWidths[index] = { width: colWidth };
        }
      });

      return mappedItem;
    });
    const worksheet = XLSX.utils.json_to_sheet(mappedData);

    worksheet["!autofilter"] = {
      ref: `A1:B${mappedData.length + 1}`,
    };

    worksheet["!cols"] = maxWidths;
    const workbook = {
      Sheets: { [sheetName]: worksheet },
      SheetNames: [sheetName],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    const excelFile: Blob = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    saveAs(
      excelFile,
      this.cmFunction.getStringDateTime() +
      ".xlsx"
    );
  }
  exportToPDF() {
    // Demo export to PDF via html2canvas
    const element = document.getElementById("reportView");
    if (!element) {
      return;
    }
    const doc = new jsPDF('p', 'mm', 'a4'); // portrait mode, millimeters unit, A4 size
    const margin = 0;
    const width = doc.internal.pageSize.getWidth() - 2 * margin;
    const height = doc.internal.pageSize.getHeight() - 2 * margin;
    const scale = 3; // larger = more quality
    html2canvas(element, { scale: scale, onclone: (clonedDoc) => {
      let element = clonedDoc.getElementById('reportView');
      if (element) { element.style.display = 'block'; }
    }}).then((canvas) => {
      const imgData = canvas.toDataURL('image/jpeg'); // png, jpeg
      doc.addImage(imgData, 'JPEG', margin, margin, width, height); // PNG, JPEG
      doc.save('report.pdf');
    });
  }
}
