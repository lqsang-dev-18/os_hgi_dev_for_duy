import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhnhanhoanthanhdhtlComponent } from './benhnhanhoanthanhdhtl.component';

describe('BenhnhanhoanthanhdhtlComponent', () => {
  let component: BenhnhanhoanthanhdhtlComponent;
  let fixture: ComponentFixture<BenhnhanhoanthanhdhtlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhnhanhoanthanhdhtlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhnhanhoanthanhdhtlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
