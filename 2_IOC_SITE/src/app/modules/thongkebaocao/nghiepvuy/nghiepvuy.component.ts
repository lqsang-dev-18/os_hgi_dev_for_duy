import { element } from "protractor";
import { CommonFunctionService } from "./../../../services/common-function.service";
import { BCTKService } from "./../../../services/baocaothongke.service";
import { ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { Subscription } from "rxjs";
import { MESSAGE_COMMON, MESSAGE_TYPE } from "src/app/constant/system-constant";
import { pageSizeOptions } from "src/app/services/config.service";
import { PaginatorService } from "src/app/services/paginator.service";
import { SnackbarService } from "src/app/services/snackbar.service";
import { Spinner } from "src/app/services/spinner.service";
import { DateAdapter } from "@angular/material/core";
import * as moment from "moment";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";

@Component({
  selector: "app-nghiepvuy",
  templateUrl: "./nghiepvuy.component.html",
  styleUrls: ["./nghiepvuy.component.scss"],
})
export class NghiepvuyComponent implements OnInit {
  constructor(
    private bctkService: BCTKService,
    private spinner: Spinner,
    private snackbar: SnackbarService,
    private translator: PaginatorService,
    private cdRef: ChangeDetectorRef,
    private _adapter: DateAdapter<any>,
    private cmFunction: CommonFunctionService
  ) {
    this._adapter.setLocale("vi");
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }
  public displayedColumns: string[] = [];
  private subscription: Subscription[] = [];
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;

  public ELEMENT_DATA: any[] = [];
  public formSearch = new FormGroup({
    maubc: new FormControl(0),
    huyen: new FormControl(-1),
    xa: new FormControl(-1),
    cskcb: new FormControl(-1),
    hinhthuc: new FormControl(1),
    tungay: new FormControl(moment()),
    denngay: new FormControl(moment()),
    thang: new FormControl(-1),
    nam: new FormControl(-1),
  });
  public years: number[] = [];
  public months = [
    { name: 'Tất cả', value: -1 },
    { name: 'Tháng 1', value: 1 },
    { name: 'Tháng 2', value: 2 },
    { name: 'Tháng 3', value: 3 },
    { name: 'Tháng 4', value: 4 },
    { name: 'Tháng 5', value: 5 },
    { name: 'Tháng 6', value: 6 },
    { name: 'Tháng 7', value: 7 },
    { name: 'Tháng 8', value: 8 },
    { name: 'Tháng 9', value: 9 },
    { name: 'Tháng 10', value: 10 },
    { name: 'Tháng 11', value: 11 },
    { name: 'Tháng 12', value: 12 }
  ];
  public pageEvent: PageEvent;
  public pageIndex: number = 0;
  public pageSize: number = 10;
  public length: number;
  public pageOption = pageSizeOptions;
  public totalPage: number;
  dataSource: MatTableDataSource<any>;
  public listHuyen: any[] = [];
  listXa = [];
  listCSKCB = [];
  listMauBC = [];
  mauBC: any;
  timeOut: any;
  ngOnInit(): void {
    this.initializeYears();
    this.getDsHuyen();
    this.getCSKCB();
    this.getMauBC(4); //lĩnh vực Nghiệp vụ Y ID = 4
  }
  initializeYears() {
    this.years = [];
    let tungay = this.formSearch.controls.tungay.value.toDate().getFullYear().toString();
    let denngay = this.formSearch.controls.denngay.value.toDate().getFullYear().toString();
    for (let i = tungay; i <= denngay; i++) {
      this.years.push(i);
    }
  }
  getCSKCB(keyword?) {
    this.subscription.push(
      this.bctkService.layDSCSKCB({ gia_tri: keyword ?? "" }).subscribe(
        (rs) => {
          this.listCSKCB = rs.data;
          this.listCSKCB.unshift({ MA_BV: -1, TEN_CSKCB: "Tất cả" });
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  setThongTinMauBaoCao() {
    this.mauBC = this.listMauBC.find(
      (element) => element.ID === this.formSearch.controls.maubc.value
    );
    this.displayedColumns = this.mauBC.DISPLAY_COLUMN?.split(",");
    this.search();
  }
  getMauBC(id) {
    this.subscription.push(
      this.bctkService.layDSMauBC({ idLinhVuc: id }).subscribe(
        (rs) => {
          this.listMauBC = rs.data;
          this.formSearch.controls.maubc.setValue(this.listMauBC[0]?.ID);
          this.setThongTinMauBaoCao();
          this.search();
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getDsHuyen(keyword?) {
    this.subscription.push(
      this.bctkService.layDSHuyen({ gia_tri: keyword ?? "" }).subscribe(
        (rs) => {
          this.listHuyen = rs.data;
          this.listHuyen.unshift({ MA_HUYEN: -1, TEN_HUYEN: "Tất cả" });
          this.getDsXa();
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getDsXa(keyword?) {
    this.formSearch.controls.xa.setValue(-1);
    this.subscription.push(
      this.bctkService
        .layDSXa({
          ma_huyen: this.formSearch.controls.huyen.value,
          gia_tri: keyword ?? "",
        })
        .subscribe(
          (rs) => {
            this.listXa = rs.data;
            this.listXa.unshift({ MA_XA: -1, TEN_XA: "Tất cả" });
          },
          (err) => {
            this.snackbar.showError(
              MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
              MESSAGE_TYPE.ERROR
            );
          }
        )
    );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.translator.translatePaginator(this.paginator);
    this.paginator.nextPage = () =>
      this.paginatorChange(this.pageIndex + 1, this.paginator.pageSize, 1);
    this.paginator.previousPage = () =>
      this.paginatorChange(this.pageIndex - 1, this.paginator.pageSize, 2);
    this.paginator.lastPage = () =>
      this.paginatorChange(this.length - 1, this.paginator.pageSize, 3);
    this.paginator.firstPage = () =>
      this.paginatorChange(0, this.paginator.pageSize, 4);
    this.cdRef.detectChanges();
  }

  paginatorChange(page, pageSize, type) {
    switch (type) {
      case 1:
        this.pageIndex++;
        this.search();
        this.resetPageSize();
        break;
      case 2:
        this.pageIndex--;
        this.search();
        this.resetPageSize();
        break;
      case 3:
        this.pageIndex = this.totalPage - 1;
        this.search();
        this.resetPageSize();
        break;
      case 4:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 5:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 6:
        this.search();
        this.resetPageSize();
    }
  }

  resetPageSize() {
    setTimeout(() => {
      this.paginator.pageIndex = this.pageIndex;
      this.dataSource.paginator.length = this.length;
    }, 500);
  }

  search(event?: PageEvent) {
    let linkAPI = this.mauBC.LINK_API;
    let page = this.pageIndex;
    let size = event != undefined ? event.pageSize : this.pageSize;
    var obj = {
      page: page + 1,
      size: size,
      tungay: this.cmFunction.dateToStringVN(
        this.formSearch.controls.tungay.value
      ),
      denngay: this.cmFunction.dateToStringVN(
        this.formSearch.controls.denngay.value
      ),
      ma_huyen:
        this.formSearch.controls.huyen.value == -1
          ? ""
          : this.formSearch.controls.huyen.value,
      ma_xa:
        this.formSearch.controls.xa.value == -1
          ? ""
          : this.formSearch.controls.xa.value,
      macsyt:
        this.formSearch.controls.cskcb.value == -1
          ? ""
          : this.formSearch.controls.cskcb.value,
      nam:
        this.formSearch.controls.nam.value == -1
          ? ""
          : this.formSearch.controls.nam.value,
      thang:
        this.formSearch.controls.thang.value == -1
          ? ""
          : this.formSearch.controls.thang.value,
    };
    this.spinner.show();
    this.subscription.push(
      this.bctkService.loadBaoCao(linkAPI, obj).subscribe(
        (rs) => {
          this.ELEMENT_DATA = rs.data;
          this.dataSource.data = this.ELEMENT_DATA;
          this.pageIndex = page;
          this.pageSize = size;
          this.length = rs.total_row;
          this.totalPage = rs.total_page;
          this.resetPageSize();
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
    return event;
  }
  exportToPDF(){}
  // exportToPDsF() {
  //   const source = document.getElementById("pdfTable");

  //   html2canvas(source).then(canvas => {
  //     const imgWidth = 210;
  //     const imgHeight = canvas.height * imgWidth / canvas.width;
  //     const contentDataURL = canvas.toDataURL('image/png');
  //     const pdf = new jsPDF();
  //     const position = 0;
  //     pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
  //     pdf.save('table.pdf');
  //   });
  // }

  // exportToPdDF() {
  //   const doc = new jsPDF();

  //   const columns = ["Column 1", "Column 2", "Column 3"]; // Adjust with your column headers
  //   const rows = this.dataSource.data.map(d => [d.TEN_CSYT, d.MA_DON_VI, d.TONGSO]); // Adjust with your data properties

  //   (doc as any).autoTable({
  //     head: [columns],
  //     body: rows
  //   });

  //   doc.save('table.pdf');
  // }

  // exportToPDF() {
  //   const documentDefinition = {
  //     content: [
  //       { text: 'Table Exported to PDF', style: 'header' },
  //       {
  //         table: {
  //           headerRows: 1,
  //           widths: ['*', '*', '*'],
  //           body: [
  //             ['Column 1', 'Column 2', 'Column 3'], // Adjust with your column headers
  //             ...this.dataSource.data.map(d => [d.TEN_CSYT, d.MA_DON_VI, d.TONGSO]) // Adjust with your data properties
  //           ]
  //         }
  //       }
  //     ],
  //     styles: {
  //       header: {
  //         fontSize: 18,
  //         bold: true,
  //         margin: [0, 0, 0, 10]
  //       }
  //     }
  //   };

  //   pdfMake.createPdf(documentDefinition).download('table.pdf');
  // }


  exportToExcel(): void {
    const sheetName = this.mauBC.DEFAULT_SHEETNAME;
    const columnNames = JSON.parse(this.mauBC.DEFAULT_COLUMNNAME);
    let maxWidths = [];
    const mappedData = this.dataSource.data.map((item) => {
      const mappedItem = {};

      columnNames.forEach((columnName, index) => {
        const key = Object.keys(columnName)[0];
        mappedItem[columnName[key]] = item[key];
        const value = item[key] ?? 0;
        let colWidth = Math.ceil(value.toString().length + 2)
        if (Math.ceil(columnName[key].toString().length + 2) > Math.ceil(value.toString().length + 2))
          colWidth = Math.ceil(columnName[key].toString().length + 2);
        if (!maxWidths[index] || colWidth > maxWidths[index]) {
          maxWidths[index] = { width: colWidth };
        }
      });

      return mappedItem;
    });
    const worksheet = XLSX.utils.json_to_sheet(mappedData);

    worksheet["!autofilter"] = {
      ref: `A1:B${mappedData.length + 1}`,
    };

    worksheet["!cols"] = maxWidths;
    const workbook = {
      Sheets: { [sheetName]: worksheet },
      SheetNames: [sheetName],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    const excelFile: Blob = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    saveAs(
      excelFile,
      this.mauBC.DEFAULT_SHEETNAME +
      " " +
      this.cmFunction.getStringDateTime() +
      ".xlsx"
    );
  }
}
