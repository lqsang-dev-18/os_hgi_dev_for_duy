import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "hinhthucType" })
export class HinhThucTypePipe implements PipeTransform {
  transform(value: number): string {
    if (value == 1) {
      return "Công lập";
    } else if (value == 2) {
      return "Tư nhân";
    } else if (value == 3) {
      return "Khác";
    } 
    return "";
  }
}
