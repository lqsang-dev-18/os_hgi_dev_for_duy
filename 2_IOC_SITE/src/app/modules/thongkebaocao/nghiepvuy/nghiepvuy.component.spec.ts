import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NghiepvuyComponent } from './nghiepvuy.component';

describe('NghiepvuyComponent', () => {
  let component: NghiepvuyComponent;
  let fixture: ComponentFixture<NghiepvuyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NghiepvuyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NghiepvuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
