import { Component, OnInit, Input } from '@angular/core';
import * as PDFObject from 'pdfobject'

@Component({
  selector: 'list-reports',
  templateUrl: './listreports.component.html',
  styleUrls: ['./listreports.component.scss']
})
export class ListReportsComponent implements OnInit {
  oldReport = {};
  params: any[] = [];
  @Input() reportList: any[];
  constructor() { }

  ngOnInit(): void {
    
  }

  isShow(report) {
    if (report.isShow === undefined) {
      report["isShow"] = true;
    } else {
      report["isShow"] = !report["isShow"];
    }
  }
  loadReport(url, report) {
    if (report.isSelect === undefined) {
      report["isSelect"] = true;
      this.oldReport["isSelect"] = false;
    } 
    
    this.oldReport = report;
    this.handleRenderPdf("https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf")
  }
  handleRenderPdf(linkPdf) {
    const pdfObject = PDFObject.embed(linkPdf, '#pdfContainer');
  }
  
}
