import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { FunctionService } from './FunctionService';
import { log } from 'console';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Subscription } from "rxjs";
// import { ReportsService } from 'src/app/services/reports.service';
@Component({
  selector: 'app-viewreports',
  templateUrl: './viewreports.component.html',
  styleUrls: ['./viewreports.component.scss']
})
export class ViewreportsComponent implements OnInit {
  constructor(private functionService: FunctionService,
    // private reportsService: ReportsService,

    private fb: FormBuilder
  ) { }
  private subscription: Subscription[] = [];
  params: any[] = [];
  reportList: any[] = [];
  form: FormGroup;
  typeReport = [
    { "id": 0, "val": "pdf" },
    { "id": 1, "val": "excel" },
    { "id": 2, "val": "word" }
  ]

  isLoading = false;
  pdfData;

  ngOnInit(): void {
    this.loadListReport()
    this.params = this.loadParam();
    this.createForm()
  }

  ngAfterViewInit() {
    this.functionService.toggleSideBar();
  }

  createForm() {
    let formGroup = {};
    this.params.forEach(field => {
      formGroup[field.fieldproc] = new FormControl('');
    });
    formGroup['p_reportype'] = new FormControl('');
    this.form = this.fb.group(formGroup);
  }

 
  loadListReport() {
    //this.reportList = [{ "reportid": 22, "reportname": "Nghiệp vụ Dược", "active": 0, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": null, "type_web": 0, "position": 1, "levlereport": 0, "reportcha": 0, "urlreport": null, "chilReport": [{ "reportid": 23, "reportname": "Báo cáo dược con", "active": 0, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": null, "type_web": 0, "position": 1, "levlereport": 1, "reportcha": 22, "urlreport": null, "chilReport": null }] }, { "reportid": 1, "reportname": "Nghiệp vụ y", "active": 0, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": null, "type_web": 0, "position": 1, "levlereport": 0, "reportcha": 0, "urlreport": null, "chilReport": [{ "reportid": 21, "reportname": "Báo cáo Y", "active": 0, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": "PROC_TEST", "type_web": 0, "position": 2, "levlereport": 1, "reportcha": 1, "urlreport": "src/main/resources/templates/NGUOIDUNG.jasper", "chilReport": null }, { "reportid": 43, "reportname": "Phát hiện lao", "active": 1, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": "Insser", "type_web": 1, "position": 1, "levlereport": 1, "reportcha": 1, "urlreport": "PROC_GET_PAGE_LIST_DS_BENH_NHAN_DHTL.jasper", "chilReport": null }, { "reportid": 2, "reportname": "Báo cáo biểu 02 Hoạt động khám chữa bệnh", "active": 0, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": "PROC_TEST", "type_web": 0, "position": 1, "levlereport": 1, "reportcha": 1, "urlreport": "src/main/resources/templates/NGUOIDUNG.jasper", "chilReport": null }, { "reportid": 41, "reportname": "Phát hiện lao", "active": 1, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": "Insser", "type_web": 1, "position": 1, "levlereport": 1, "reportcha": 1, "urlreport": "PROC_GET_PAGE_LIST_DS_BENH_NHAN_DHTL.jasper", "chilReport": null }, { "reportid": 42, "reportname": "Phát hiện lao", "active": 1, "createday": "2024-01-01", "updatedate": "2024-01-01", "userupdate": "Tạ Văn Hoàng Hên", "procname": "Insser", "type_web": 1, "position": 1, "levlereport": 1, "reportcha": 1, "urlreport": "PROC_GET_PAGE_LIST_DS_BENH_NHAN_DHTL.jasper", "chilReport": null }] }]
    this.subscription.push(
      // this.reportsService.getDanhSachReports({}).subscribe(
      //   (rs) => {
      //     this.reportList = rs;
      //   },
      //   (err) => {
      //     console.log(err);
      //   }
      // )
    );
  }
  loadParam() {
    const params =
      [
        {
          "fieldid": 1,
          "fieldname": "Từ ngày",
          "fieldproc": "p_tungay",
          "typeid": null,
          "active": 0,
          "reportid": 2,
          "position": 1,
          "parasql": null,
          "typename": "date"
        },
        {
          "fieldid": 2,
          "fieldname": "Đến ngày",
          "fieldproc": "p_denngay",
          "typeid": null,
          "active": 0,
          "reportid": 2,
          "position": 2,
          "parasql": null,
          "typename": "date"
        },
        {
          "fieldid": 4,
          "fieldname": "Loại đối tượng",
          "fieldproc": "p_loaidoituong",
          "typeid": null,
          "active": 0,
          "reportid": 2,
          "position": 3,
          "parasql": "SELECT 1 AS ID, 'Tư lập' as NAME FROM DUAL\r\nUNION\r\nSELECT 2 AS ID, 'Công lập' as NAME FROM DUAL",
          "typename": "select"
        },
        {
          "fieldid": 5,
          "fieldname": "Số đối tượng",
          "fieldproc": "p_sodoituong",
          "typeid": null,
          "active": 0,
          "reportid": 2,
          "position": 4,
          "parasql": null,
          "typename": "number"
        }
      ]
    params.forEach(field => {
      if (field.typename == "select") {
        field["data"] = [
          {
            "id": 1,
            "val": "Tư lập"
          },
          {
            "id": 2,
            "val": "Công lập"
          }
        ]
      }
    });
    return params
  }
}
