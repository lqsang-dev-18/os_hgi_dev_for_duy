import { NghiepvuyComponent } from './nghiepvuy/nghiepvuy.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewreportsComponent } from './viewreports/viewreports.component';
import { QlskHogiadinhComponent } from './qlsk-hogiadinh/qlsk-hogiadinh.component';


const routes: Routes = [
  { path: 'nghiep-vu-y', component: NghiepvuyComponent },
  { path: 'view-reports', component: ViewreportsComponent },
  { path: 'tam-than-kinh-phong-da-lieu', loadChildren: () => import('./tamthankinhphongdalieu/tamthankinhphongdalieu.module').then((m) => m.TamThanKinhPhongDaLieuModule) },
  { path: 'qlsk-hogiadinh', component: QlskHogiadinhComponent},
  { path: 'bao-cao-dan-so-khhgd', loadChildren: () => import('./bao-cao-dan-so-khhgd/bao-cao-dan-so-khhgd.module').then((m) => m.BaoCaoDanSoKHHGDModule) },
  { path: 'bao-cao-trung-tam-kiem-soat-benh-tat', loadChildren: () => import('./trung-tam-kiem-soat-benh-tat/trung-tam-kiem-soat-benh-tat.module').then((m) => m.TrungTamKiemSoatBenhTatModule) },
  { path: 'bao-cao-trung-tam-kiem-soat-benh-tat', loadChildren: () => import('./trung-tam-kiem-soat-benh-tat/trung-tam-kiem-soat-benh-tat.module').then((m) => m.TrungTamKiemSoatBenhTatModule) },
  { path: 'ky-thuat-kiem-nghiem', loadChildren: () => import('./ky-thuat-kiem-nghiem/ky-thuat-kiem-nghiem.module').then((m) => m.KyThuatKiemNghiemModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThongKeBaoCaoRoutingModule { }
