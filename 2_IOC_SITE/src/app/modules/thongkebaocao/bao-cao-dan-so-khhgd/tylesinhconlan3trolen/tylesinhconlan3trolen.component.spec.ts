import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tylesinhconlan3trolenComponent } from './tylesinhconlan3trolen.component';

describe('Tylesinhconlan3trolenComponent', () => {
  let component: Tylesinhconlan3trolenComponent;
  let fixture: ComponentFixture<Tylesinhconlan3trolenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Tylesinhconlan3trolenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Tylesinhconlan3trolenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
