import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { DateAdapter } from "@angular/material/core";
import { MatPaginator, PageEvent } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { saveAs } from "file-saver";
import * as moment from "moment";
import { Subscription } from "rxjs";
import { apiUrl } from "src/app/constant/api-url";
import { MA_TINH, MESSAGE_COMMON, MESSAGE_TYPE } from "src/app/constant/system-constant";
import { BCTKService } from "src/app/services/baocaothongke.service";
import { CommonFunctionService } from "src/app/services/common-function.service";
import { pageSizeOptions } from "src/app/services/config.service";
import { PaginatorService } from "src/app/services/paginator.service";
import { SnackbarService } from "src/app/services/snackbar.service";
import { Spinner } from "src/app/services/spinner.service";
import * as XLSX from "xlsx";

@Component({
  selector: "app-bao-cao-sang-loc-so-sinh",
  templateUrl: "./bao-cao-sang-loc-so-sinh.component.html",
  styleUrls: ["../bao-cao-dan-so-khhgd.component.scss"],
})
export class BaoCaoSangLocSoSinhComponent implements OnInit {
  constructor(
    private bctkService: BCTKService,
    private spinner: Spinner,
    private snackbar: SnackbarService,
    private translator: PaginatorService,
    private cdRef: ChangeDetectorRef,
    private _adapter: DateAdapter<any>,
    private cmFunction: CommonFunctionService
  ) {
    this._adapter.setLocale("vi");
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  private subscription: Subscription[] = [];
  public ELEMENT_DATA: any[] = [];
  public pageEvent: PageEvent;
  public pageIndex: number = 0;
  public pageSize: number = 100;
  public pageOption = pageSizeOptions;
  public totalPage: number;
  public length: number;
  dataSource: MatTableDataSource<any>;
  public displayedColumns: string[] = ["NAM", "THANG", "NGAY_BAO_CAO", "TEN_TINH", "TEN_HUYEN", "TEN_XA", "DUOC_SLSS"];
  public formSearch = new FormGroup({
    keyword: new FormControl(""),
    tungay: new FormControl(moment().subtract(1, "months")),
    denngay: new FormControl(moment()),
    thang: new FormControl(moment().get("month") + 1),
    nam: new FormControl(moment().get("year")),
    filter: new FormControl(1),
  });
  public years: number[] = Array.from({ length: 10 }, (_, i) => new Date().getFullYear() - i);
  filterType: 1 | 2 | 3 = 1;
  ngOnInit(): void {
    this.search();
  }
  getFormData(page, size) {
    var formData = {
      matinh: MA_TINH,
      page: page + 1,
      size: size,
    };

    const { value } = this.formSearch.controls.filter;

    if (value === 1) {
      formData["tungay"] = this.cmFunction.dateToStringVN(this.formSearch.controls.tungay.value);
      formData["denngay"] = this.cmFunction.dateToStringVN(this.formSearch.controls.denngay.value);
    }
    if (value === 2) {
      formData["thang"] = this.formSearch.controls.thang.value;
      formData["nam"] = this.formSearch.controls.nam.value;
    }
    if (value === 3) {
      formData["nam"] = this.formSearch.controls.nam.value;
    }
    return formData;
  }
  search(event?: PageEvent) {
    let page = this.pageIndex;
    let size = event != undefined ? event.pageSize : this.pageSize;
    var formData = this.getFormData(page, size);
    this.spinner.show();
    this.subscription.push(
      this.bctkService.loadBaoCao(apiUrl.BAO_CAO_DAN_SO_KHHGD.BAO_CAO_SANG_LOC_SO_SINH, formData).subscribe(
        (rs) => {
          this.ELEMENT_DATA = rs.data.map((row) => ({ ...row, NGAY_BAO_CAO: moment(row.NGAY_BAO_CAO).format("DD/MM/yyyy") }));
          this.dataSource.data = this.ELEMENT_DATA;
          this.pageIndex = page;
          this.pageSize = size;
          this.length = rs.total_row;
          this.totalPage = rs.total_page;
          this.resetPageSize();
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        }
      )
    );
    return event;
  }
  paginatorChange(page, pageSize, type) {
    switch (type) {
      case 1:
        this.pageIndex++;
        this.search();
        this.resetPageSize();
        break;
      case 2:
        this.pageIndex--;
        this.search();
        this.resetPageSize();
        break;
      case 3:
        this.pageIndex = this.totalPage - 1;
        this.search();
        this.resetPageSize();
        break;
      case 4:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 5:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 6:
        this.search();
        this.resetPageSize();
    }
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.translator.translatePaginator(this.paginator);
    this.paginator.nextPage = () => this.paginatorChange(this.pageIndex + 1, this.paginator.pageSize, 1);
    this.paginator.previousPage = () => this.paginatorChange(this.pageIndex - 1, this.paginator.pageSize, 2);
    this.paginator.lastPage = () => this.paginatorChange(this.length - 1, this.paginator.pageSize, 3);
    this.paginator.firstPage = () => this.paginatorChange(0, this.paginator.pageSize, 4);
    this.cdRef.detectChanges();
  }
  resetPageSize() {
    setTimeout(() => {
      this.paginator.pageIndex = this.pageIndex;
      this.dataSource.paginator.length = this.length;
    }, 500);
  }
  exportToExcel(): void {
    const sheetName = "Sheet 1";
    const columnNames = JSON.parse(
      `[{"NAM":"Năm"}, {"THANG":"Tháng"}, {"NGAY_BAO_CAO":"Ngày báo cáo"}, {"TEN_TINH":"Tên tỉnh"}, {"TEN_HUYEN":"Tên huyện"}, {"TEN_XA":"Tên xã"}, {"DUOC_SLSS":"Số lượng được sàng lọc sau sinh"}]`
    );
    let maxWidths = [];
    const mappedData = this.dataSource.data.map((item) => {
      const mappedItem = {};

      columnNames.forEach((columnName, index) => {
        const key = Object.keys(columnName)[0];
        mappedItem[columnName[key]] = item[key];
        const value = item[key] ?? 0;
        let colWidth = Math.ceil(value.toString().length + 2);
        if (Math.ceil(columnName[key].toString().length + 2) > Math.ceil(value.toString().length + 2))
          colWidth = Math.ceil(columnName[key].toString().length + 2);
        if (!maxWidths[index] || colWidth > maxWidths[index]) {
          maxWidths[index] = { width: colWidth };
        }
      });

      return mappedItem;
    });
    const worksheet = XLSX.utils.json_to_sheet(mappedData);

    worksheet["!autofilter"] = {
      ref: `A1:B${mappedData.length + 1}`,
    };

    worksheet["!cols"] = maxWidths;
    const workbook = {
      Sheets: { [sheetName]: worksheet },
      SheetNames: [sheetName],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    const excelFile: Blob = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    saveAs(excelFile, this.cmFunction.getStringDateTime() + ".xlsx");
  }
  exportToPDF() {
    //
  }
}
