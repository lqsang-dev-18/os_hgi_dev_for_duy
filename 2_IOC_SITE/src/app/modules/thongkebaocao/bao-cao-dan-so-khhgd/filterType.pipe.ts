import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "filterType" })
export class FilterTypePipe implements PipeTransform {
  transform(value: number): string {
    const filter = {
      1: "Lọc từ ngày đến ngày",
      2: "Lọc theo tháng",
      3: "Lọc theo năm",
    };
    
    return filter[value] ?? "";
  }
}
