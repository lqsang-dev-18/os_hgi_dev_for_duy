import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KhdansoKhhgiadinhComponent } from './khdanso-khhgiadinh.component';

describe('KhdansoKhhgiadinhComponent', () => {
  let component: KhdansoKhhgiadinhComponent;
  let fixture: ComponentFixture<KhdansoKhhgiadinhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KhdansoKhhgiadinhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KhdansoKhhgiadinhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
