import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { MESSAGE_COMMON, MESSAGE_TYPE } from 'src/app/constant/system-constant';
import { BCTKService } from 'src/app/services/baocaothongke.service';
import { CommonFunctionService } from 'src/app/services/common-function.service';
import { pageSizeOptions } from 'src/app/services/config.service';
import { PaginatorService } from 'src/app/services/paginator.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Spinner } from 'src/app/services/spinner.service';
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import { apiUrl } from 'src/app/constant/api-url';

@Component({
  selector: 'app-khdanso-khhgiadinh',
  templateUrl: './khdanso-khhgiadinh.component.html',
  styleUrls: ['../bao-cao-dan-so-khhgd.component.scss']
})
export class KhdansoKhhgiadinhComponent implements OnInit {

  constructor(
    private bctkService: BCTKService,
    private spinner: Spinner,
    private snackbar: SnackbarService,
    private translator: PaginatorService,
    private cdRef: ChangeDetectorRef,
    private _adapter: DateAdapter<any>,
    private cmFunction: CommonFunctionService
  ) {
    this._adapter.setLocale("vi");
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }
  public ELEMENT_DATA: any[] = [];
  public formSearch = new FormGroup({
    tungay: new FormControl(moment()),
    denngay: new FormControl(moment()),
    thang: new FormControl(moment().get("month") + 1),
    nam: new FormControl(moment().get("year")),
    filter: new FormControl(1)
  });
  public displayedColumns: string[] = ['STT', 'NGAY', 'THANG', 'NAM', 'TEN_CHI_TIEU', 'TEN_DON_VI', 'CHI_TIEU'];
  private subscription: Subscription[] = [];
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  public years: number[] = [];
  public months = [
    { name: 'Tháng 1', value: 1 },
    { name: 'Tháng 2', value: 2 },
    { name: 'Tháng 3', value: 3 },
    { name: 'Tháng 4', value: 4 },
    { name: 'Tháng 5', value: 5 },
    { name: 'Tháng 6', value: 6 },
    { name: 'Tháng 7', value: 7 },
    { name: 'Tháng 8', value: 8 },
    { name: 'Tháng 9', value: 9 },
    { name: 'Tháng 10', value: 10 },
    { name: 'Tháng 11', value: 11 },
    { name: 'Tháng 12', value: 12 }
  ];
  public pageEvent: PageEvent;
  public pageIndex: number = 0;
  public pageSize: number = 10;
  public length: number;
  public pageOption = pageSizeOptions;
  public totalPage: number;
  dataSource: MatTableDataSource<any>;
  hideDate = 0;
  hideMonth = 0;
  hideYear = 0;
  ngOnInit(): void {
    this.initializeYears();
    this.hideFilter(this.formSearch.controls.filter.value);
    this.search();
  }
  initializeYears() {
    this.years = [];
    for (let i = moment().get("year") - 10; i <= moment().get("year") + 10; i++) {
      this.years.push(i);
    }
  }
  hideFilter(value) {
    switch (value) {
      case 1:
        this.hideDate = 0;
        this.hideMonth = 1;
        this.hideYear = 1;
        break;
      case 2:
        this.hideDate = 1;
        this.hideMonth = 0;
        this.hideYear = 0;
        break;
      case 3:
        this.hideDate = 1;
        this.hideMonth = 1;
        this.hideYear = 0;
        break;
      default:
        this.hideDate = 1;
        this.hideMonth = 1;
        this.hideYear = 0;
        break;
    }
  }
  generateObj(value, page, size) {
    var obj = {
      page: page + 1,
      size: size
    };

    if (value == 1) {
      obj["tungay"] = this.cmFunction.dateToStringVN(this.formSearch.controls.tungay.value);
      obj["denngay"] = this.cmFunction.dateToStringVN(this.formSearch.controls.denngay.value);
    } else if (value == 2) {
      obj["thang"] = this.formSearch.controls.thang.value;
      obj["nam"] = this.formSearch.controls.nam.value;
    } else if (value == 3) {
      obj["nam"] = this.formSearch.controls.nam.value;
    }
    return obj;
  }
  search(event?: PageEvent) {
    let page = this.pageIndex;
    let size = event != undefined ? event.pageSize : this.pageSize;
    var obj = this.generateObj(this.formSearch.controls.filter.value, page, size)
    this.spinner.show();
    this.subscription.push(
      this.bctkService.loadBaoCao(apiUrl.BAO_CAO_DAN_SO_KHHGD.KH_DANSO_KHH_GIADINH, obj).subscribe(
        (rs) => {
          this.ELEMENT_DATA = rs.data;
          this.dataSource.data = this.ELEMENT_DATA;
          this.pageIndex = page;
          this.pageSize = size;
          this.length = rs.total_row;
          this.totalPage = rs.total_page;
          this.resetPageSize();
          this.spinner.hide();
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
    return event;
  }
  paginatorChange(page, pageSize, type) {
    switch (type) {
      case 1:
        this.pageIndex++;
        this.search();
        this.resetPageSize();
        break;
      case 2:
        this.pageIndex--;
        this.search();
        this.resetPageSize();
        break;
      case 3:
        this.pageIndex = this.totalPage - 1;
        this.search();
        this.resetPageSize();
        break;
      case 4:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 5:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 6:
        this.search();
        this.resetPageSize();
    }
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.translator.translatePaginator(this.paginator);
    this.paginator.nextPage = () =>
      this.paginatorChange(this.pageIndex + 1, this.paginator.pageSize, 1);
    this.paginator.previousPage = () =>
      this.paginatorChange(this.pageIndex - 1, this.paginator.pageSize, 2);
    this.paginator.lastPage = () =>
      this.paginatorChange(this.length - 1, this.paginator.pageSize, 3);
    this.paginator.firstPage = () =>
      this.paginatorChange(0, this.paginator.pageSize, 4);
    this.cdRef.detectChanges();
  }
  resetPageSize() {
    setTimeout(() => {
      this.paginator.pageIndex = this.pageIndex;
      this.dataSource.paginator.length = this.length;
    }, 500);
  }
  exportToExcel(): void {
    const sheetName = 'Sheet 1';
    const columnNames = JSON.parse(`[{"NGAY_FORMAT":"Ngày"}, {"THANG":"Tháng"}, {"NAM":"Năm"}, {"TEN_CHI_TIEU":"Tên chỉ tiêu"}, {"TEN_DON_VI":"Tên đơn vị"}, {"CHI_TIEU":"Chỉ tiêu"}]`);
    let maxWidths = [];
    const mappedData = this.dataSource.data.map((item) => {
      const mappedItem = {};

      columnNames.forEach((columnName, index) => {
        const key = Object.keys(columnName)[0];
        mappedItem[columnName[key]] = item[key];
        const value = item[key] ?? 0;
        let colWidth = Math.ceil(value.toString().length + 2)
        if (Math.ceil(columnName[key].toString().length + 2) > Math.ceil(value.toString().length + 2))
          colWidth = Math.ceil(columnName[key].toString().length + 2);
        if (!maxWidths[index] || colWidth > maxWidths[index]) {
          maxWidths[index] = { width: colWidth };
        }
      });

      return mappedItem;
    });
    const worksheet = XLSX.utils.json_to_sheet(mappedData);

    worksheet["!autofilter"] = {
      ref: `A1:B${mappedData.length + 1}`,
    };

    worksheet["!cols"] = maxWidths;
    const workbook = {
      Sheets: { [sheetName]: worksheet },
      SheetNames: [sheetName],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    const excelFile: Blob = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    saveAs(
      excelFile,
     'BCThucHienKeHoach-' + this.cmFunction.getStringDateTime() +
      ".xlsx"
    );
  }
  exportToPDF() {
    //
  }
}
