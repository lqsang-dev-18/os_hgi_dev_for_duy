import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BaoCaoSangLocSoSinhComponent } from "./bao-cao-sang-loc-so-sinh/bao-cao-sang-loc-so-sinh.component";
import { KhdansoKhhgiadinhComponent } from "./khdanso-khhgiadinh/khdanso-khhgiadinh.component";
import { Tylesinhconlan3trolenComponent } from "./tylesinhconlan3trolen/tylesinhconlan3trolen.component";

const routes: Routes = [
  { path: "bao-cao-sang-loc-so-sinh", component: BaoCaoSangLocSoSinhComponent },
  { path: "bao-cao-thuc-hien-ke-hoach", component: KhdansoKhhgiadinhComponent },
  { path: "bao-cao-sinh-con-lan3-tro-len", component: Tylesinhconlan3trolenComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BaoCaoDanSoKHHGDRoutingModule {}
