import { SharedModule } from './../../shared/shared.module';
import { NgxDropdownComponent } from './../../shared/components/ngx-dropdown/ngx-dropdown.component';
import { HinhThucTypePipe } from './nghiepvuy/hinhthucType.pipe';
import { NghiepvuyComponent } from './nghiepvuy/nghiepvuy.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectInfiniteScrollModule } from 'ng-mat-select-infinite-scroll';
import { ThongKeBaoCaoRoutingModule } from './thongkebaocao-routing.module';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ViewreportsComponent } from './viewreports/viewreports.component';
import { ListReportsComponent } from './viewreports/listreports/listreports.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatList, MatListModule } from '@angular/material/list';
import { QlskHogiadinhComponent } from './qlsk-hogiadinh/qlsk-hogiadinh.component';


@NgModule({
  declarations: [NghiepvuyComponent, HinhThucTypePipe, ViewreportsComponent, ListReportsComponent, QlskHogiadinhComponent],
  imports: [
    CommonModule,
    ThongKeBaoCaoRoutingModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatSelectInfiniteScrollModule,
    MatDatepickerModule,
    SharedModule,
    MatListModule
  ],
  exports: [
    MatFormFieldModule,
    MatToolbarModule,
    MatCardModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatPaginatorModule,
    MatIconModule,
    MatTableModule,
    MatInputModule,
    MatSelectModule,
    MatSelectInfiniteScrollModule,
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
    },
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
  ],
})
export class ThongKeBaoCaoModule { }
