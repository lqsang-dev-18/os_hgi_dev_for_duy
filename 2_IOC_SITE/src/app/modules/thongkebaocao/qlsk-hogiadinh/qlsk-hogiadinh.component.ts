import { MA_TINH } from './../../../constant/system-constant';
import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DateAdapter } from '@angular/material/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { MESSAGE_COMMON, MESSAGE_TYPE, DATE_PATTERN } from 'src/app/constant/system-constant';
import { CommonFunctionService } from 'src/app/services/common-function.service';
import { pageSizeOptions } from 'src/app/services/config.service';
import { PaginatorService } from 'src/app/services/paginator.service';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { Spinner } from 'src/app/services/spinner.service';
import * as moment from "moment";
import { BCTKService } from 'src/app/services/baocaothongke.service';
import { apiUrl } from 'src/app/constant/api-url';
import { DmChungService } from 'src/app/services/dm-chung.service';
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-qlsk-hogiadinh',
  templateUrl: './qlsk-hogiadinh.component.html',
  styleUrls: ['./qlsk-hogiadinh.component.scss']
})
export class QlskHogiadinhComponent implements OnInit {

  dateFormat = DATE_PATTERN.NORMAL;
  constructor(
    private bctkService: BCTKService,
    private spinner: Spinner,
    private snackbar: SnackbarService,
    private translator: PaginatorService,
    private cdRef: ChangeDetectorRef,
    private _adapter: DateAdapter<any>,
    private cmFunction: CommonFunctionService,
    private dmchungService: DmChungService,
  ) {
    this._adapter.setLocale("vi");
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }

  public displayedColumns: string[] = ['STT', 
                                        'DANSO',
                                        'TENDIAPHUONG',
                                        'KHAMDUOC',
                                        'TYLE',
                                        'TANGHUYETAP',
                                        'UNGTHU',
                                        'DAITHAODUONG',
                                        'TIMMACH',
                                        'PHOIMANTINH',
                                        'HENSUYEN',
                                        'LAO',
                                        'TAMTHAN',
                                        'TUKY',
                                        'DONGKINH'];
  public displayedColumns2: string[] = ['TANGHUYETAP',
                                        'UNGTHU',
                                        'DAITHAODUONG',
                                        'TIMMACH',
                                        'PHOIMANTINH',
                                        'HENSUYEN',
                                        'LAO',
                                        'TAMTHAN',
                                        'TUKY',
                                        'DONGKINH'];
  private subscription: Subscription[] = [];
  @ViewChild("paginator", { static: true }) paginator: MatPaginator;
  public years: number[] = [];
  public months = [
    { name: 'Tháng 1', value: 1 },
    { name: 'Tháng 2', value: 2 },
    { name: 'Tháng 3', value: 3 },
    { name: 'Tháng 4', value: 4 },
    { name: 'Tháng 5', value: 5 },
    { name: 'Tháng 6', value: 6 },
    { name: 'Tháng 7', value: 7 },
    { name: 'Tháng 8', value: 8 },
    { name: 'Tháng 9', value: 9 },
    { name: 'Tháng 10', value: 10 },
    { name: 'Tháng 11', value: 11 },
    { name: 'Tháng 12', value: 12 }
  ];
  public ELEMENT_DATA: any[] = [];
  public formSearch = new FormGroup({
    tungay: new FormControl(moment()),
    denngay: new FormControl(moment()),
    huyen: new FormControl(-1),
    tinh: new FormControl('93'),
  })
  public pageEvent: PageEvent;
  public pageIndex: number = 0;
  public pageSize: number = 10;
  public length: number;
  public pageOption = pageSizeOptions;
  public totalPage: number;
  dataSource: MatTableDataSource<any>;
  hideDate = 0;
  hideMonth = 0;
  hideQuater = 0;
  hideYear = 0;
  public listHuyen: any[] = [];
  public listTinh: any[] = [];

  ngOnInit(): void {
    this.initializeYears();
    this.getTinhHuyen();
    this.search();
  }

  ngOnDestroy() {
    this.subscription.forEach((subscription) => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  initializeYears() {
    this.years = [];
    for (let i = moment().get("year") - 10; i <= moment().get("year") + 10; i++) {
      this.years.push(i);
    }
  }

  async getTinhHuyen(): Promise<void>{
    await this.getDsTinh();
    this.getDsHuyen('93'); 
  }

  async getDsTinh(keyword?){
    try{
      var response = await this.dmchungService.getListTinh({ keyword: keyword ?? "", size: 100}).toPromise();
      this.listTinh = response.data;
    }catch(error){
      this.snackbar.showError(
        MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
        MESSAGE_TYPE.ERROR
      );
    }
  }

  getDsHuyen(iDTinh?) {
    let id_tinh = this.listTinh.find((tinh) => tinh.MA_TINH == (iDTinh ?? this.formSearch.controls.tinh.value)).ID;
    this.subscription.push(
      this.dmchungService.getListHuyen({ size: 100, idTinh: id_tinh }).subscribe(
        (rs) => {
          this.listHuyen = rs.data;
          this.listHuyen.unshift({MA_HUYEN: -1, TEN_HUYEN: "Tất cả"});   
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    )
  }

  search(event?: PageEvent){
    let page = event != undefined ? event.pageIndex : this.pageIndex;
    let size = event != undefined ? event.pageSize : this.pageSize;
    var obj = {
      page: page + 1,
      size: size,
      tungay: this.cmFunction.dateToStringVN(
        this.formSearch.controls.tungay.value
      ),
      denngay: this.cmFunction.dateToStringVN(
        this.formSearch.controls.denngay.value
      ),
      ma_huyen:
        this.formSearch.controls.huyen.value == -1
          ? "all"
          : this.formSearch.controls.huyen.value,
      ma_tinh: 
        this.formSearch.controls.tinh.value,
    };
    this.spinner.show();
    this.subscription.push(
      this.bctkService.loadBaoCao(apiUrl.BCTK.BC_TINH_HINH_QLSK_HOGIADINH, obj).subscribe(
        (rs) => {
          this.ELEMENT_DATA = rs.data;
          
          this.ELEMENT_DATA.forEach(element => {
            if(element.DAN_SO !== 0) {
              element.TY_LE = Math.round((element.KHAM_DUOC / element.DAN_SO)* 1e5) / 1e5;
            } else {
              element.TY_LE = 0;
            }
          });
    
          this.dataSource.data = this.ELEMENT_DATA;
          this.pageIndex = page;
          this.pageSize = size;
          this.length = rs.total_row;
          this.totalPage = rs.total_page;
          this.resetPageSize();
          this.spinner.hide();          
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    )
    return event;
  }

  paginatorChange(page, pageSize, type) {
    switch (type) {
      case 1:
        this.pageIndex++;
        this.search();
        this.resetPageSize();
        break;
      case 2:
        this.pageIndex--;
        this.search();
        this.resetPageSize();
        break;
      case 3:
        this.pageIndex = this.totalPage - 1;
        this.search();
        this.resetPageSize();
        break;
      case 4:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 5:
        this.pageIndex = 0;
        this.search();
        this.resetPageSize();
        break;
      case 6:
        this.search();
        this.resetPageSize();
    }
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.translator.translatePaginator(this.paginator);
    this.paginator.nextPage = () =>
      this.paginatorChange(this.pageIndex + 1, this.paginator.pageSize, 1);
    this.paginator.previousPage = () =>
      this.paginatorChange(this.pageIndex - 1, this.paginator.pageSize, 2);
    this.paginator.lastPage = () =>
      this.paginatorChange(this.length - 1, this.paginator.pageSize, 3);
    this.paginator.firstPage = () =>
      this.paginatorChange(0, this.paginator.pageSize, 4);
    this.cdRef.detectChanges();
  }
  resetPageSize() {
    setTimeout(() => {
      this.paginator.pageIndex = this.pageIndex;
      this.dataSource.paginator.length = this.length;
    }, 500);
  }

  exportToExcel(): void {
    const sheetName = 'Sheet 1';
    const columnNames = JSON.parse(`[{"DAN_SO":"Dân số"}, 
    {"DIA_PHUONG":"Địa phương"}, 
    {"KHAM_DUOC":"Khám được"},
    {"TY_LE":"Tỷ lệ"},
    {"SL_THA":"Tăng huyết áp"}, 
    {"SL_UT":"Ung thư"}, 
    {"SL_DTD":"Đái tháo đường"},
    {"SL_TIMMACH":"Tim mạch"},
    {"SL_PHOI":"Phổi mãn tính"},
    {"SL_HENSUYEN":"Hen suyễn"},
    {"SL_LAO":"Lao"},
    {"SL_TAMTHAN":"Tâm thần"},
    {"SL_TUKY":"Tự kỷ"},
    {"SL_DK":"Động kinh"}]`);
    let maxWidths = [];
    const mappedData = this.dataSource.data.map((item) => {
      const mappedItem = {};
      columnNames.forEach((columnName, index) => {
        const key = Object.keys(columnName)[0];
        mappedItem[columnName[key]] = item[key];
        const value = item[key] ?? 0;
        let colWidth = Math.ceil(value.toString().length + 2)
        if (Math.ceil(columnName[key].toString().length + 2) > Math.ceil(value.toString().length + 2))
          colWidth = Math.ceil(columnName[key].toString().length + 2);
        if (!maxWidths[index] || colWidth > maxWidths[index]) {
          maxWidths[index] = { width: colWidth };
        }
      });

      return mappedItem;
    });
    const worksheet = XLSX.utils.json_to_sheet(mappedData);

    worksheet["!autofilter"] = {
      ref: `A1:B${mappedData.length + 1}`,
    };

    worksheet["!cols"] = maxWidths;
    const workbook = {
      Sheets: { [sheetName]: worksheet },
      SheetNames: [sheetName],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    const excelFile: Blob = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    saveAs(
      excelFile,
      this.cmFunction.getStringDateTime() +
      ".xlsx"
    );
  }
  exportToPDF() {
    // Demo export to PDF via html2canvas
    const element = document.getElementById("reportView");
    if (!element) {
      return;
    }
    const doc = new jsPDF('l', 'mm', 'a4'); // portrait mode, millimeters unit, A4 size
    const margin = 0;
    const width = doc.internal.pageSize.getWidth() - 2 * margin;
    const height = doc.internal.pageSize.getHeight() - 2 * margin;
    const scale = 3; // larger = more quality
    html2canvas(element, { scale: scale, onclone: (clonedDoc) => {
      let element = clonedDoc.getElementById('reportView');
      if (element) { element.style.display = 'block'; }
    }}).then((canvas) => {
      const imgData = canvas.toDataURL('image/jpeg'); // png, jpeg
      doc.addImage(imgData, 'JPEG', margin, margin, width, height); // PNG, JPEG
      doc.save('report.pdf');
    });
  }

}
