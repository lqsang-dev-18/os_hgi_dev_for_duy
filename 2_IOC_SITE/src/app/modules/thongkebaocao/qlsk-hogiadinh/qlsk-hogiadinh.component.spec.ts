import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QlskHogiadinhComponent } from './qlsk-hogiadinh.component';

describe('QlskHogiadinhComponent', () => {
  let component: QlskHogiadinhComponent;
  let fixture: ComponentFixture<QlskHogiadinhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QlskHogiadinhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QlskHogiadinhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
