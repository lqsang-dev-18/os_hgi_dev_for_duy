import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GiamDinhPhapYComponent } from './giam-dinh-phap-y/giam-dinh-phap-y.component';

const routes: Routes = [
  { path: '', component: GiamDinhPhapYComponent },
  { path: 'tt-giam-dinh-y-khoa', component: GiamDinhPhapYComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GiamDinhYKhoaRoutingModule { }
