import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiamDinhPhapYComponent } from './giam-dinh-phap-y.component';

describe('GiamDinhPhapYComponent', () => {
  let component: GiamDinhPhapYComponent;
  let fixture: ComponentFixture<GiamDinhPhapYComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiamDinhPhapYComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiamDinhPhapYComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
