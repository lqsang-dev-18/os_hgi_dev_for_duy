import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GiamDinhYKhoaRoutingModule } from './giam-dinh-y-khoa-routing.module';
import { GiamDinhPhapYComponent } from './giam-dinh-phap-y/giam-dinh-phap-y.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [GiamDinhPhapYComponent],
  imports: [
    CommonModule,
    GiamDinhYKhoaRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSlideToggleModule,
    SharedModule
  ]
})
export class GiamDinhYKhoaModule { }
