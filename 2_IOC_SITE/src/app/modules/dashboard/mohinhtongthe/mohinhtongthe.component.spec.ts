import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MohinhtongtheComponent } from './mohinhtongthe.component';

describe('MohinhtongtheComponent', () => {
  let component: MohinhtongtheComponent;
  let fixture: ComponentFixture<MohinhtongtheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MohinhtongtheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MohinhtongtheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
