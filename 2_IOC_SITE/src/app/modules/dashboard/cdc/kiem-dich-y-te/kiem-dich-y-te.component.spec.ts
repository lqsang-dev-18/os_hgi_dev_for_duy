import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KiemDichYTeComponent } from './kiem-dich-y-te.component';

describe('KiemDichYTeComponent', () => {
  let component: KiemDichYTeComponent;
  let fixture: ComponentFixture<KiemDichYTeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KiemDichYTeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KiemDichYTeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
