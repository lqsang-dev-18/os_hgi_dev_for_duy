import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhKhongLayHgiComponent } from './benh-khong-lay-hgi.component';

describe('BenhKhongLayHgiComponent', () => {
  let component: BenhKhongLayHgiComponent;
  let fixture: ComponentFixture<BenhKhongLayHgiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhKhongLayHgiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhKhongLayHgiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
