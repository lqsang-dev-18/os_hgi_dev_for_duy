import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SucKhoeSinhSanComponent } from './suc-khoe-sinh-san.component';

describe('SucKhoeSinhSanComponent', () => {
  let component: SucKhoeSinhSanComponent;
  let fixture: ComponentFixture<SucKhoeSinhSanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SucKhoeSinhSanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SucKhoeSinhSanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
