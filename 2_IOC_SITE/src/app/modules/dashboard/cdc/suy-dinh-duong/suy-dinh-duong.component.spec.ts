import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuyDinhDuongComponent } from './suy-dinh-duong.component';

describe('SuyDinhDuongComponent', () => {
  let component: SuyDinhDuongComponent;
  let fixture: ComponentFixture<SuyDinhDuongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuyDinhDuongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuyDinhDuongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
