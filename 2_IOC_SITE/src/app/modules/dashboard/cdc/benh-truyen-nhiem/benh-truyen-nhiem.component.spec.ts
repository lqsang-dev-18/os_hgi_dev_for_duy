import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhTruyenNhiemComponent } from './benh-truyen-nhiem.component';

describe('BenhTruyenNhiemComponent', () => {
  let component: BenhTruyenNhiemComponent;
  let fixture: ComponentFixture<BenhTruyenNhiemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhTruyenNhiemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhTruyenNhiemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
