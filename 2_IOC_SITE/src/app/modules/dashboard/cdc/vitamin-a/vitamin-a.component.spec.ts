import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VitaminAComponent } from './vitamin-a.component';

describe('VitaminAComponent', () => {
  let component: VitaminAComponent;
  let fixture: ComponentFixture<VitaminAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VitaminAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitaminAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
