import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiemChungComponent } from './tiem-chung.component';

describe('TiemChungComponent', () => {
  let component: TiemChungComponent;
  let fixture: ComponentFixture<TiemChungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiemChungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiemChungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
