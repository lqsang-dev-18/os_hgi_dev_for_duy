import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VitaminAHgiComponent } from './vitamin-a-hgi.component';

describe('VitaminAHgiComponent', () => {
  let component: VitaminAHgiComponent;
  let fixture: ComponentFixture<VitaminAHgiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VitaminAHgiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitaminAHgiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
