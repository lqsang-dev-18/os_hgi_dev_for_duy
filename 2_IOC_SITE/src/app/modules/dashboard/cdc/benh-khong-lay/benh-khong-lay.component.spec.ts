import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenhKhongLayComponent } from './benh-khong-lay.component';

describe('BenhKhongLayComponent', () => {
  let component: BenhKhongLayComponent;
  let fixture: ComponentFixture<BenhKhongLayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenhKhongLayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenhKhongLayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
