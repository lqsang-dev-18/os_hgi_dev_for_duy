import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TiemChungHgiComponent } from './tiem-chung-hgi.component';

describe('TiemChungHgiComponent', () => {
  let component: TiemChungHgiComponent;
  let fixture: ComponentFixture<TiemChungHgiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TiemChungHgiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TiemChungHgiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
