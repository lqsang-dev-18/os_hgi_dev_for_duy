import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TinhHinhPhongChongLaoComponent } from './tinh-hinh-phong-chong-lao.component';

describe('ThongTinChungComponent', () => {
  let component: TinhHinhPhongChongLaoComponent;
  let fixture: ComponentFixture<TinhHinhPhongChongLaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinhHinhPhongChongLaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinhHinhPhongChongLaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
