import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XetNghiemPkdkComponent } from './xet-nghiem-pkdk.component';

describe('XetNghiemPkdkComponent', () => {
  let component: XetNghiemPkdkComponent;
  let fixture: ComponentFixture<XetNghiemPkdkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XetNghiemPkdkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XetNghiemPkdkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
