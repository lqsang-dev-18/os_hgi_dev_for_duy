import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HivAidsComponent } from './hiv-aids.component'

describe('HivAidsComponent', () => {
  let component: HivAidsComponent;
  let fixture: ComponentFixture<HivAidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HivAidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HivAidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
