import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KythuatkiemnghiemComponent } from './kythuatkiemnghiem.component';

describe('KythuatkiemnghiemComponent', () => {
  let component: KythuatkiemnghiemComponent;
  let fixture: ComponentFixture<KythuatkiemnghiemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KythuatkiemnghiemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KythuatkiemnghiemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
