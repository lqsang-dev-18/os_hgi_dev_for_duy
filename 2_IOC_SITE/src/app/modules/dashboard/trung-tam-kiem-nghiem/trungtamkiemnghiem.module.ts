import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrungTamKiemNghiemRoutingModule } from './trungtamkiemnghiem-routing.module';
import { KythuatkiemnghiemComponent } from './kythuatkiemnghiem/kythuatkiemnghiem.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [KythuatkiemnghiemComponent],
  imports: [
    CommonModule,
    TrungTamKiemNghiemRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSlideToggleModule,
    SharedModule
  ]
})
export class TrungTamKiemNghiemModule { }
