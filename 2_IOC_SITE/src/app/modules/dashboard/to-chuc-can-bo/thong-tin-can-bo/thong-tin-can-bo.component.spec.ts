import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongTinCanBoComponent } from './thong-tin-can-bo.component';

describe('ThongTinCanBoComponent', () => {
  let component: ThongTinCanBoComponent;
  let fixture: ComponentFixture<ThongTinCanBoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongTinCanBoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongTinCanBoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
