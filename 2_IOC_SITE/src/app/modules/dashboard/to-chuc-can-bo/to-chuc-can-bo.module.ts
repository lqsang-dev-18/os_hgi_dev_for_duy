import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ToChucCanBoRoutingModule } from './to-chuc-can-bo-routing.module';
import { ThongTinCanBoComponent } from './thong-tin-can-bo/thong-tin-can-bo.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [ThongTinCanBoComponent],
  imports: [
    CommonModule,
    ToChucCanBoRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSlideToggleModule,
    SharedModule
  ]
})
export class ToChucCanBoModule { }
