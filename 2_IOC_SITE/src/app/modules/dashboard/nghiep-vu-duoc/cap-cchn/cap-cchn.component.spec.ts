import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapCchnComponent } from './cap-cchn.component';

describe('CapCchnComponent', () => {
  let component: CapCchnComponent;
  let fixture: ComponentFixture<CapCchnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapCchnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapCchnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
