import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongBoMyPhamComponent } from './cong-bo-my-pham.component';

describe('ThongTinChungComponent', () => {
  let component: CongBoMyPhamComponent;
  let fixture: ComponentFixture<CongBoMyPhamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongBoMyPhamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongBoMyPhamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
