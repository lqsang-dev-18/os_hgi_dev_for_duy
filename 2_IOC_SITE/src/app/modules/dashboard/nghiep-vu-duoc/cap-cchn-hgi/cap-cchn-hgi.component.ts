import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { BarChartComponent } from 'src/app/shared/components/chart-js/bar-chart/bar-chart.component';
import { COLOR_PALETTES, MESSAGE_COMMON, MESSAGE_TYPE } from 'src/app/constant/system-constant';
import { AggService } from 'src/app/services/agg.service';
import { MatPaginator } from "@angular/material/paginator";
import { pageSizeOptions } from "src/app/services/config.service";
import { Spinner } from "src/app/services/spinner.service";
import { MatTableDataSource } from "@angular/material/table";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ID_TINH, MA_TINH } from 'src/app/constant/system-constant';
import { DmChungService } from 'src/app/services/dm-chung.service';
@Component({
  selector: 'app-cap-cchn-hgi',
  templateUrl: './cap-cchn-hgi.component.html',
  styleUrls: ['./cap-cchn-hgi.component.scss']
})
export class CapCchnHgiComponent implements OnInit, OnDestroy {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};

  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public QUATER: any[] = [];
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    loai: new FormControl(0),
    year: new FormControl("", [Validators.required]),
    quater: new FormControl(),
    month: new FormControl("", [Validators.required]),
    fromDate:  new FormControl(new Date().toISOString()),
    toDate:  new FormControl(new Date().toISOString()),
    huyen: new FormControl(),
    xa: new FormControl(),
  });

  public CARD_COLORS: any[] = COLOR_PALETTES.RIVER_NIGHTS;
  public sumraryInfo = { CAP_MOI: 0, CAP_LAI: 0, THU_HOI: 0 };
  public LIST_CCHN: any[] = [];
  @ViewChild("loaiVanBang", { static: true }) loaiVanBang: BarChartComponent;

  dataSource!: MatTableDataSource<any>;
  pageIndex: number = 0;
  index: number = 0;
  length: number;
  pageOption = pageSizeOptions;
  pageSizeDefault: number = this.pageOption[0];
  ELEMENT_DATA: any[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  modalRefs: any[] = [];
  listHeaderColumnCCHN: any[] = [
    {
      key: "HO_TEN",
      label: "HỌ TÊN",
    },
    {
      key: "NGAY_SINH",
      label: "NGÀY SINH",
    },
    {
      key: "LOAI_VAN_BANG",
      label: "LOẠi VĂN BẰNG",
    },
    {
      key: "LOAI_CAP",
      label: "LOẠI",
    },
    {
      key: "SO_CCHN",
      label: "SỐ",
    },
  ];

  loai: string;
  showNam = true;
  showQuy = true;
  showThang = true;
  showTuNgay = false;
  showDenNgay = false;
  listHuyen: any = [];
  listXa: any = [];


  constructor(
    private aggService: AggService,
    private snackbar: SnackbarService,
    private spinner: Spinner,
    private dmChungService:DmChungService,
    private modalService: NgbModal
  ) { 
    this.dataSource = new MatTableDataSource(this.ELEMENT_DATA);
  }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    let currentMonth = currentDate.getMonth() + 1; // 0 to 11, January = 0, February = 1, ...
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }
    this.formSearch.controls["month"].setValue(currentMonth);
    this.QUATER.push({ id: 0, text: "Tất cả" })
    for(let i = 1; i <= 4;i++){
      this.QUATER.push({ id: i, text: "Quý " + i })
    }
    this.formSearch.controls["quater"].setValue((Math.ceil(currentMonth / 3)));
    this.detectMode();

    this.loaiVanBang.barChartOptions.legend.display = false;
    this.onFilterChangeCombobox(false);
    this.getListHuyen(true);
  }

  ngOnDestroy() {
    this.subscription.forEach(subscription => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.loaiVanBang.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.loaiVanBang.barChartOptions.plugins.datalabels.anchor = "end";
    this.loaiVanBang.barChartOptions.plugins.datalabels.align = "end";
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = (16 // padding top
        + 43.75 + 16 // form height and its margin bottom
        + 0); // no row spacing
      let rowHeight = 'calc((100% - ' + others + 'px) / 1)';
      this.rowStyles = { 'height': rowHeight, 'margin-bottom': '1rem' };
    }
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }

  public getFilter() {
    let loai_cbx = this.formSearch.get("loai").value;
    let year = this.formSearch.controls.year.value ?? 0;
    let quater = this.formSearch.controls.quater.value ?? 0;
    let month = this.formSearch.controls.month.value ?? 0;
    let tungay = this.formatDate(this.formSearch.get("fromDate").value);
    let denngay = this.formatDate(this.formSearch.get("toDate").value);
    let ma_huyen = this.formSearch.get("huyen").value ?? 0;
    let ma_xa  = this.formSearch.get("xa").value ?? 0;
    let ma_tinh  = MA_TINH;
    return { loai_cbx: loai_cbx, nam: year, thang: month, tungay: tungay, denngay: denngay,
        ma_tinh: ma_tinh,ma_huyen: ma_huyen,ma_xa:ma_xa,quy: quater };
  }

  public getData(): void {
    this.thongKeTongQuan();
    this.thongKeLoaiVanBang();
    this.danhSachCCHNMoiCap();
  }

  getListHuyen(init: boolean = false) {
    var obj = {
      idTinh: ID_TINH,
    };

    this.subscription.push(
      this.dmChungService.getListHuyen(obj).subscribe(
        (rs) => {
          this.listHuyen = rs.data;
          console.log(rs.data)
          if (init) {
            this.formSearch.controls["huyen"].setValue(this.listHuyen[0].MA_HUYEN);
            this.getListXa(this.listHuyen[0].MA_HUYEN, init);
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getListXa(maHuyen: number, init: boolean = false) {
    let index = this.listHuyen.findIndex(x => x.MA_HUYEN === maHuyen);
    var obj = { idHuyen: this.listHuyen[index].ID };

    this.subscription.push(
      this.dmChungService.getListXa(obj).subscribe(
        (rs) => {
          this.listXa = rs.data;
          this.formSearch.controls["xa"].setValue(this.listXa[0].MA_XA);
          console.log(rs.data)
          if (init) {
            this.getData();
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  public thongKeTongQuan(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.aggService.soLieuTongQuanChungChiHanhNgheHgi(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;
      }
      this.sumraryInfo = rs.data[0];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using PALETTE_3[1]
  public thongKeLoaiVanBang(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.aggService.loaiVanBangChungChiHanhNgheHgi(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.loaiVanBang.barChartLabels = rs.data.map(x => this.convertToArray(' ', x.LOAI_VAN_BANG, 2));
      let data = rs.data.map(x => x.SO_LUONG);
      this.loaiVanBang.barChartData = [
        { data: data, label: 'Số lượng', backgroundColor: COLOR_PALETTES.PALETTE_3[1] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  public danhSachCCHNMoiCap(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.aggService.danhSachChungChiHanhNgheHgi(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;
      }
      this.LIST_CCHN = rs.data;
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  public getColor(element: any) {
    let style = "color: ";
    return style + (element.MA_LOAI_CAP == "M" ? this.CARD_COLORS[6] : this.CARD_COLORS[3]);
  }

  public getBorderColor(element: any) {
    let style = "border: 1px solid ";
    return style + (element.MA_LOAI_CAP == "M" ? this.CARD_COLORS[6] : this.CARD_COLORS[3]);
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }

  onClicked(data: any, content: any, id: string, size: string) {
    switch (id) {
      case "CAP_MOI": {
        this.loai = 'M';
        break;
      }
      case "CAP_LAI": {
        this.loai = 'L';
        break;
      }
      case "THU_HOI": {
        this.loai = 'T';
        break;
      }
    }
    this.getListCCHNByTrangThai();
    this.openModal(content, id, size);
  }

  openModal(content, id, size?) {
    let modalRef = {};
    modalRef[id] = this.modalService.open(content, {
      ariaLabelledBy: id,
      size: size ?? "md",
    });
    modalRef[id].result.then(
      (result) => {
        console.log(`Closed with: ${result}`);
      },
      (reason) => {
        console.log(`with: ${reason}`);
      }
    );
    this.modalRefs.push(modalRef);
  }

  getListCCHNByTrangThai(event?: any) {
    if (!event && this.pageIndex != 0) {
      this.paginator.pageIndex = 0;
    } else if (event) {
      this.paginator = event;
    }
    let loai_cbx = this.formSearch.get("loai").value;
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    let tungay = this.formatDate(this.formSearch.get("fromDate").value);
    let denngay = this.formatDate(this.formSearch.get("toDate").value);
    let page = event ? event.pageIndex : this.pageIndex;
    var obj = {
      loai_cbx: loai_cbx,
      nam: year,
      thang: month,
      tungay: tungay,
      denngay: denngay,
      loai: this.loai,
      page: page + 1,
      size: this.paginator ? this.paginator.pageSize : this.pageSizeDefault,
    };
    this.spinner.show();
    this.subscription.push(
      this.aggService.danhSachChungChiHanhNgheTheoLoai(obj).subscribe(
        (rs) => {
          this.ELEMENT_DATA = rs.data;
          this.dataSource.data = this.ELEMENT_DATA;
          this.spinner.hide();

          this.index = obj.page - 1;
          this.length = rs.total_row;
        },
        (err) => {
          this.spinner.hide();
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );

    return event;
  }

  onFilterChangeCombobox(getData = true){
    let loai = this.formSearch.get("loai").value
    
    if(loai == 0){
      this.showNam = true
      this.showQuy = false
      this.showThang = false
      this.showTuNgay = false
      this.showDenNgay = false
    }else if(loai == 1){
      this.showNam = true
      this.showQuy = false
      this.showThang = true
      this.showTuNgay = false
      this.showDenNgay = false
    }else if(loai == 2){
      this.showNam = true
      this.showQuy = true
      this.showThang = false
      this.showTuNgay = false
      this.showDenNgay = false
    }else{
      this.showNam = false
      this.showQuy = false
      this.showThang = false
      this.showTuNgay = true
      this.showDenNgay = true
    }
    if(getData)
      this.getData()
  }

  onFilterChange (){
    this.detectMode
    this.getData();
  }
}
