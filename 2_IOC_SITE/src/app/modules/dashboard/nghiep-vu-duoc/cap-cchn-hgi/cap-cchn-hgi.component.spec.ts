import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapCchnHgiComponent } from './cap-cchn-hgi.component';

describe('CapCchnHgiComponent', () => {
  let component: CapCchnHgiComponent;
  let fixture: ComponentFixture<CapCchnHgiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapCchnHgiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapCchnHgiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
