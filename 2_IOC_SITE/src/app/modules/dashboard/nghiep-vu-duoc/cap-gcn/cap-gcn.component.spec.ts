import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CapGcnComponent } from './cap-gcn.component';

describe('CapGcnComponent', () => {
  let component: CapGcnComponent;
  let fixture: ComponentFixture<CapGcnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CapGcnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CapGcnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
