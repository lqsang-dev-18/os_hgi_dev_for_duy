import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongTinChungP2Component } from './thong-tin-chung-p2.component';

describe('ThongTinChungP2Component', () => {
  let component: ThongTinChungP2Component;
  let fixture: ComponentFixture<ThongTinChungP2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongTinChungP2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongTinChungP2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
