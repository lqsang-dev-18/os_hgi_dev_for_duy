import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeAn06Component } from './de-an06.component';

describe('DeAn06Component', () => {
  let component: DeAn06Component;
  let fixture: ComponentFixture<DeAn06Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeAn06Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeAn06Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
