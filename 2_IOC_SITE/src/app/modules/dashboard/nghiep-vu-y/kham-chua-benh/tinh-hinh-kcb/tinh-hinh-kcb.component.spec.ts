import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TinhHinhKcbComponent } from './tinh-hinh-kcb.component';

describe('TinhHinhKcbComponent', () => {
  let component: TinhHinhKcbComponent;
  let fixture: ComponentFixture<TinhHinhKcbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinhHinhKcbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinhHinhKcbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
