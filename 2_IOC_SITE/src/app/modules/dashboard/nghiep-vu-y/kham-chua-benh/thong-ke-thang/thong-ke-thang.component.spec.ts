import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongKeThangComponent } from './thong-ke-thang.component';

describe('ThongKeThangComponent', () => {
  let component: ThongKeThangComponent;
  let fixture: ComponentFixture<ThongKeThangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongKeThangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongKeThangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
