import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongKeNgayComponent } from './thong-ke-ngay.component';

describe('ThongKeNgayComponent', () => {
  let component: ThongKeNgayComponent;
  let fixture: ComponentFixture<ThongKeNgayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongKeNgayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongKeNgayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
