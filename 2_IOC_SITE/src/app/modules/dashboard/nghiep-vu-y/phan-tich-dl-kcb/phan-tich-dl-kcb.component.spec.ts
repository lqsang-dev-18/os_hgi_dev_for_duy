import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhanTichDlKcbComponent } from './phan-tich-dl-kcb.component';

describe('PhanTichDlKcbComponent', () => {
  let component: PhanTichDlKcbComponent;
  let fixture: ComponentFixture<PhanTichDlKcbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhanTichDlKcbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhanTichDlKcbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
