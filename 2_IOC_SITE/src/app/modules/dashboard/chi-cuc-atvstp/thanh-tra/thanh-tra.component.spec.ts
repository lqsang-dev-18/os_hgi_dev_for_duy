import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThanhTraComponent } from './thanh-tra.component';

describe('ThanhTraComponent', () => {
  let component: ThanhTraComponent;
  let fixture: ComponentFixture<ThanhTraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThanhTraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThanhTraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
