import { ChiCucAtvstpService } from './../../../../services/chi-cuc-atvstp.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { COLOR_PALETTES, MESSAGE_COMMON, MESSAGE_TYPE } from 'src/app/constant/system-constant';
import { Subscription } from "rxjs";
import { SnackbarService } from "src/app/services/snackbar.service";
import { LineChartComponent } from "src/app/shared/components/chart-js/line-chart/line-chart.component";
import { BarChartComponent } from 'src/app/shared/components/chart-js/bar-chart/bar-chart.component';
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";

@Component({
  selector: 'app-thanh-tra',
  templateUrl: './thanh-tra.component.html',
  styleUrls: ['./thanh-tra.component.scss']
})
export class ThanhTraComponent implements OnInit {

  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};

  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public DON_VI_LIST: any = [];
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl("", [Validators.required]),
    madonvi: new FormControl([""]),
  });
  
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
    COLOR_PALETTES.DUTCH_FIELD[4]
  ];
  public summaryInfo;

  @ViewChild("chuyenNganhTheoDot", { static: true })
  chuyenNganhTheoDot: BarChartComponent;
  @ViewChild("chuyenNganhTheoLoaiHinh", { static: true })
  chuyenNganhTheoLoaiHinh: BarChartComponent;
  @ViewChild("lienNganhTheoLoaiHinh", { static: true })
  lienNganhTheoLoaiHinh: BarChartComponent;
  @ViewChild("lienNganhTheoDot", { static: true })
  lienNganhTheoDot: BarChartComponent;

  isWithChartCT : any;
  lableDonVi = true;
  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private atvstpService: ChiCucAtvstpService,
    private snackbar: SnackbarService
  ) {
    this.summaryInfo = { CHUYENNGANH: 0, LIENNGANH: 0, VIPHAM: 0, XUPHAT: 0 };
   }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    //this.formSearch.controls["month"].setValue([1,2,3,4,5,6,7,8,9,10,11,12]);
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }
    this.lableMonth = true;

    this.chuyenNganhTheoDot.barChartOptions.legend.display = false;
    this.chuyenNganhTheoLoaiHinh.barChartOptions.legend.display = false;
    this.lienNganhTheoLoaiHinh.barChartOptions.legend.display = false;
    this.lienNganhTheoDot.barChartOptions.legend.display = false;

    this.detectMode();

    this.getData();
    this.getListDonVi();
  }

  ngOnDestroy() {
    this.subscription.forEach((subscription) => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.chuyenNganhTheoDot.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.chuyenNganhTheoLoaiHinh.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.lienNganhTheoLoaiHinh.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.lienNganhTheoDot.barChartOptions.maintainAspectRatio = !this.isFitPage;
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = (16 // padding top
        + 43.75 + 16 // form height and its margin bottom
        + 16); // 1 row spacing
      let rowHeight = 'calc((100% - ' + others + 'px) / 2)';
      this.rowStyles = { 'height': rowHeight, 'margin-bottom': '1rem' };
    }
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let result = { nam: year, };
    return result;
  }

  public getData(): void {
    this.ttTong();
    this.ttChuyenNganhTheoLoaiHinh();
    this.ttChuyenNganhTheoDot();
    this.ttLienNganhTheoLoaiHinh();
    this.ttLienNganhTheoDot();
  }

  getListDonVi() {
    //this.formSearch.controls.madonvi.reset();
    this.subscription.push(
      this.vanbandieuhanhService.getListDonVi().subscribe(
        (rs) => {
          this.DON_VI_LIST = rs.data;
          //this.formSearch.controls["madonvi"].setValue(rs.data.map(x => x.MA_DON_VI_CON));
          this.lableDonVi = true;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  public ttTong(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.atvstpService.getThanhTraTong(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;
      }
      console.log(rs);
      
      this.summaryInfo = rs.data[0];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  ttChuyenNganhTheoLoaiHinh() {
    let obj: any = this.getFilter();
    obj.doan_thanh_tra = 'CN';
    this.subscription.push(this.atvstpService.getThanhTraTheoLoaiHinh(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data;
      this.chuyenNganhTheoLoaiHinh.barChartLabels = [];
      this.chuyenNganhTheoLoaiHinh.barChartData = [];
      if (!(result && result.length > 0)) {  
        return;
      }
      result = result.filter(x => x.TONG > 0 || x.TONG > 0 || x.TONG > 0);
      this.chuyenNganhTheoLoaiHinh.barChartLabels = result.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_LOAI_HINH), 2));
      this.chuyenNganhTheoLoaiHinh.barChartData.push({
        data: result.map(x => x.TONG),
        label: "Tổng",
        backgroundColor: COLOR_PALETTES.PALETTE_2[3],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.chuyenNganhTheoLoaiHinh.barChartData.push({
        data: result.map(x => x.DAT),
        label: "Đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[5],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.chuyenNganhTheoLoaiHinh.barChartData.push({
        data: result.map(x => x.KHONGDAT),
        label: "Không đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[6],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  ttLienNganhTheoLoaiHinh() {
    let obj: any = this.getFilter();
    obj.doan_thanh_tra = 'LN';
    this.subscription.push(this.atvstpService.getThanhTraTheoLoaiHinh(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data;
      this.lienNganhTheoLoaiHinh.barChartLabels = [];
      this.lienNganhTheoLoaiHinh.barChartData = [];
      if (!(result && result.length > 0)) {  
        return;
      }
      result = result.filter(x => x.TONG > 0 || x.TONG > 0 || x.TONG > 0);
      this.lienNganhTheoLoaiHinh.barChartLabels = result.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_LOAI_HINH), 2));
      this.lienNganhTheoLoaiHinh.barChartData.push({
        data: result.map(x => x.TONG),
        label: "Tổng",
        backgroundColor: COLOR_PALETTES.RIVER_NIGHTS[6],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.lienNganhTheoLoaiHinh.barChartData.push({
        data: result.map(x => x.DAT),
        label: "Đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[3],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.lienNganhTheoLoaiHinh.barChartData.push({
        data: result.map(x => x.KHONGDAT),
        label: "Không đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[4],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  ttLienNganhTheoDot() {
    let obj: any = this.getFilter();
    obj.doan_thanh_tra = 'LN';
    this.subscription.push(this.atvstpService.getThanhTraTheoDot(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data;
      this.lienNganhTheoDot.barChartLabels = [];
      this.lienNganhTheoDot.barChartData = [];
      if (!(result && result.length > 0)) {  
        return;
      }
      result = result.filter(x => x.TONG > 0 || x.TONG > 0 || x.TONG > 0);
      this.lienNganhTheoDot.barChartLabels = result.map(x => this.convertToArray(' ', this.reduceLabel(x.DOT), 2));
      this.lienNganhTheoDot.barChartData.push({
        data: result.map(x => x.TONG),
        label: "Tổng",
        backgroundColor: COLOR_PALETTES.RIVER_NIGHTS[6],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.lienNganhTheoDot.barChartData.push({
        data: result.map(x => x.DAT),
        label: "Đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[3],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.lienNganhTheoDot.barChartData.push({
        data: result.map(x => x.KHONGDAT),
        label: "Không đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[4],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  ttChuyenNganhTheoDot() {
    let obj: any = this.getFilter();
    obj.doan_thanh_tra = 'CN';
    this.subscription.push(this.atvstpService.getThanhTraTheoDot(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data;
      this.chuyenNganhTheoDot.barChartLabels = [];
      this.chuyenNganhTheoDot.barChartData = [];
      if (!(result && result.length > 0)) {  
        return;
      }
      result = result.filter(x => x.TONG > 0 || x.TONG > 0 || x.TONG > 0);
      this.chuyenNganhTheoDot.barChartLabels = result.map(x => this.convertToArray(' ', this.reduceLabel(x.DOT), 2));
      this.chuyenNganhTheoDot.barChartData.push({
        data: result.map(x => x.TONG),
        label: "Tổng",
        backgroundColor: COLOR_PALETTES.PALETTE_2[3],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.chuyenNganhTheoDot.barChartData.push({
        data: result.map(x => x.DAT),
        label: "Đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[5],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.chuyenNganhTheoDot.barChartData.push({
        data: result.map(x => x.KHONGDAT),
        label: "Không đạt",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[6],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + ((length * percent) < 100 ? 95 : (length * percent)) + "%";
  }

  private reduceLabel(label: string) {
    return label.replace(/trung tâm y tế/gi, "TTYT")
                .replace(/bệnh viện/gi, "BV")
                .replace(/phòng khám/gi, "PK")
                .replace(/trạm y tế/gi, "TYT");
  }

}
