import { ChiCucAtvstpService } from "./../../../../services/chi-cuc-atvstp.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { CommonFunctionService } from "src/app/services/common-function.service";

@Component({
  selector: "app-tinh-hinh-cap-gcn",
  templateUrl: "./tinh-hinh-cap-gcn.component.html",
  styleUrls: ["./tinh-hinh-cap-gcn.component.scss"],
})
export class TinhHinhCapGcnComponent implements OnInit {
  subscription: Subscription[] = [];

  isFitPage = true;
  formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    timeType: new FormControl(0, [Validators.required]),
    year: new FormControl("", [Validators.required]),
    // month: new FormControl("", [Validators.required]),
    quarter: new FormControl(""),
  });

  YEARS: any[] = [];
  // MONTHS: any[] = [];
  QUARTERS: any[] = [];
  rowStyles: any = {};
  SL_THE_TONG:  any = {};
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.GREY_TO_RED[6],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_3[0],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
  ];

  @ViewChild("DVAUHetHan", { static: true })
  DVAUHetHan: BarChartComponent;
  @ViewChild("NhomLoaiSPHetHan", { static: true })
  NhomLoaiSPHetHan: BarChartComponent;
  @ViewChild("DVAUCapGCN", { static: true })
  DVAUCapGCN: BarChartComponent;
  @ViewChild("NhomLoaiSPCapGCN", { static: true })
  NhomLoaiSPCapGCN: BarChartComponent;
  @ViewChild("TuCongBo", { static: true })
  TuCongBo: BarChartComponent;
  @ViewChild("LoaiCoSo", { static: true })
  LoaiCoSo: BarChartComponent;



  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private chiCucAtvstpService: ChiCucAtvstpService,
    private commonService: CommonFunctionService
  ) {}

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2016; i <= currentYear+1; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    // let currentMonth = currentDate.getMonth() + 1; // 0 to 11, January = 0, February = 1, ...
    // this.formSearch.controls["month"].setValue(currentMonth);
    // for (let i = 1; i <= 12; i++) {
    //   this.MONTHS.push({ id: i, text: "Tháng " + i });
    // }
    this.formSearch.controls["quarter"].setValue(1);
    for (let i = 1; i <= 4; i++) {
      this.QUARTERS.push({ id: i, text: "Quý " + i });
    }

    this.detectMode();
    this.getData();
  }

  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== "responsive";
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.LoaiCoSo.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.LoaiCoSo.barChartOptions.legend.position = "bottom";

    this.DVAUHetHan.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.DVAUHetHan.barChartOptions.legend.position = "bottom";

    this.NhomLoaiSPHetHan.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.NhomLoaiSPHetHan.barChartOptions.legend.position = "bottom";

    this.DVAUCapGCN.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.DVAUCapGCN.barChartOptions.legend.position = "bottom";

    this.NhomLoaiSPCapGCN.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.NhomLoaiSPCapGCN.barChartOptions.legend.position = "bottom";

    this.TuCongBo.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.TuCongBo.barChartOptions.legend.position = "bottom";

  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 2 rows spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }

  getFilter() {
    let params = {};
    if (this.formSearch.controls.year.value) {
      params["nam"] = this.formSearch.controls.year.value;
    }
    // if (
    //   this.formSearch.controls.month.value &&
    //   this.formSearch.controls["timeType"].value == 1
    // ) {
    //   params["thang"] = this.formSearch.controls.month.value;
    // }
    if (
      this.formSearch.controls.quarter.value &&
      this.formSearch.controls["timeType"].value == 2
    ) {
      params["quy"] = this.formSearch.controls.quarter.value;
    }

    return params;
  }

  getData(): void {
    this.getLoaiCoSo();
    this.getDVAUHetHan();
    this.getNhomLoaiSPHetHan();
    this.getDVAUCapGCN();
    this.getNhomLoaiSPCapGCN();
    this.getTuCongBo();
  }
  getLoaiCoSo(): void {
    let obj: any = this.getFilter();
    // obj.rowNum = 10;
    this.subscription.push(
      this.chiCucAtvstpService.getLoaiCoSo(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.LoaiCoSo.barChartLabels = rs.data.map((x) =>
            this.commonService.convertStringToArray(" ", x.LABEL, 3)
          );
          let arrGiaTri = rs.data.map((x) => Math.round(x.VALUE));
          this.LoaiCoSo.barChartData = [
            {
              data: arrGiaTri,
              label: "Số lượng cơ sở",
              backgroundColor: "#e60073",
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getDVAUHetHan(): void {
    let obj: any = this.getFilter();
    // obj.rowNum = 10;
    this.subscription.push(
      this.chiCucAtvstpService.getDVAUHetHan(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.DVAUHetHan.barChartLabels = rs.data.map((x) =>
            this.commonService.convertStringToArray(" ", x.LABEL, 2)
          );
          let arrGiaTri = rs.data.map((x) => Math.round(x.VALUE));
          this.DVAUHetHan.barChartData = [
            {
              data: arrGiaTri,
              label: "Số lượng cơ sở",
              backgroundColor: this.CONST_COLOR[2],
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getNhomLoaiSPHetHan(): void {
    let obj: any = this.getFilter();
    // obj.rowNum = 10;
    this.subscription.push(
      this.chiCucAtvstpService.getNhomLoaiSPHetHan(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.NhomLoaiSPHetHan.barChartLabels = rs.data.map((x) =>
            this.commonService.convertStringToArray(" ", x.LABEL, 2)
          );
          let arrGiaTri = rs.data.map((x) => Math.round(x.VALUE));
          this.NhomLoaiSPHetHan.barChartData = [
            {
              data: arrGiaTri,
              label: "Số lượng cơ sở",
              backgroundColor: this.CONST_COLOR[1],
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getDVAUCapGCN(): void {
    let obj: any = this.getFilter();
    // obj.rowNum = 10;
    this.subscription.push(
      this.chiCucAtvstpService.getDVAUCapGCN(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.DVAUCapGCN.barChartLabels = rs.data.map((x) =>
            this.commonService.convertStringToArray(" ", x.LABEL, 3)
          );
          let arrGiaTri = rs.data.map((x) => Math.round(x.VALUE));
          this.DVAUCapGCN.barChartData = [
            {
              data: arrGiaTri,
              label: "Số lượng cơ sở",
              backgroundColor: this.CONST_COLOR[3],
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getNhomLoaiSPCapGCN(): void {
    let obj: any = this.getFilter();
    // obj.rowNum = 10;
    this.subscription.push(
      this.chiCucAtvstpService.getNhomLoaiSPCapGCN(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.NhomLoaiSPCapGCN.barChartLabels = rs.data.map((x) =>
            this.commonService.convertStringToArray(" ", x.LABEL, 2)
          );
          let arrGiaTri = rs.data.map((x) => Math.round(x.VALUE));
          this.NhomLoaiSPCapGCN.barChartData = [
            {
              data: arrGiaTri,
              label: "Số lượng cơ sở",
              backgroundColor: this.CONST_COLOR[6],
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getTuCongBo(): void {
    let obj: any = this.getFilter();
    // obj.rowNum = 10;
    this.subscription.push(
      this.chiCucAtvstpService.getNhomLoaiSPTuCongBo(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.TuCongBo.barChartLabels = rs.data.map((x) =>
            this.commonService.convertStringToArray(" ", x.LABEL, 2)
          );
          let arrGiaTri = rs.data.map((x) => Math.round(x.VALUE));
          this.TuCongBo.barChartData = [
            {
              data: arrGiaTri,
              label: "Số lượng cơ sở",
              backgroundColor: this.CONST_COLOR[7],
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }


  getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }
}
