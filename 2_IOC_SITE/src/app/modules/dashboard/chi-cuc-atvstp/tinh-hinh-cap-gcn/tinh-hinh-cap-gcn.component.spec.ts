import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TinhHinhCapGcnComponent } from './tinh-hinh-cap-gcn.component';

describe('TinhHinhCapGcnComponent', () => {
  let component: TinhHinhCapGcnComponent;
  let fixture: ComponentFixture<TinhHinhCapGcnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TinhHinhCapGcnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TinhHinhCapGcnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
