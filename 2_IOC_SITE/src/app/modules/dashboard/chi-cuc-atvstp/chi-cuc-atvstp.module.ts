import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChiCucAtvstpRoutingModule } from './chi-cuc-atvstp-routing.module';
import { TinhHinhCapGcnComponent } from './tinh-hinh-cap-gcn/tinh-hinh-cap-gcn.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/app/shared/shared.module';
import { ThanhTraComponent } from './thanh-tra/thanh-tra.component';


@NgModule({
  declarations: [TinhHinhCapGcnComponent, ThanhTraComponent],
  imports: [
    CommonModule,
    ChiCucAtvstpRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSlideToggleModule,
    SharedModule
  ]
})
export class ChiCucAtvstpModule { }
