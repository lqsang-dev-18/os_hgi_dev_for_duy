import { ThanhTraComponent } from './thanh-tra/thanh-tra.component';
import { TinhHinhCapGcnComponent } from './tinh-hinh-cap-gcn/tinh-hinh-cap-gcn.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: "", component: TinhHinhCapGcnComponent },
  { path: "tinh-hinh-cap-gcn", component: TinhHinhCapGcnComponent },
  { path: "thanh-tra-attp", component: ThanhTraComponent },];
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChiCucAtvstpRoutingModule { }
