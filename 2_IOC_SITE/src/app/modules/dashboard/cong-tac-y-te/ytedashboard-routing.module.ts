import { CongTacYTeComponent } from './cong-tac-y-te/cong-tac-y-te.component';
import { HoatDongTongHopComponent } from './hoat-dong-tong-hop-tckt/hoat-dong-tong-hop-tckt.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CongTacYTeHgiComponent } from './cong-tac-y-te-hgi/cong-tac-y-te-hgi.component';

const routes: Routes = [
  // { path: "", component: CongTacYTeComponent },
  { path: "cong-tac-y-te", component: CongTacYTeComponent },
  { path: "hoat-dong-tong-hop-tckt", component: HoatDongTongHopComponent },
  { path: "cong-tac-y-te-hgi", component: CongTacYTeHgiComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class YTeDashboardRoutingModule {}
