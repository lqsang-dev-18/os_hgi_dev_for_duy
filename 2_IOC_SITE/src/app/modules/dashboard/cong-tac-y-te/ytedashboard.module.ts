import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { YTeDashboardRoutingModule } from './ytedashboard-routing.module';
import { CongTacYTeComponent } from './cong-tac-y-te/cong-tac-y-te.component';
import { HoatDongTongHopComponent } from './hoat-dong-tong-hop-tckt/hoat-dong-tong-hop-tckt.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/app/shared/shared.module';
import { CongTacYTeHgiComponent } from './cong-tac-y-te-hgi/cong-tac-y-te-hgi.component';


@NgModule({
  declarations: [CongTacYTeComponent, HoatDongTongHopComponent, CongTacYTeHgiComponent],
  imports: [
    CommonModule,
    YTeDashboardRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSlideToggleModule,
    SharedModule
  ]
})
export class YTeDashboardModule { }
