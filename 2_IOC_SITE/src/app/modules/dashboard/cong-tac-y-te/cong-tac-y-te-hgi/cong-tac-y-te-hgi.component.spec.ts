import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongTacYTeHgiComponent } from './cong-tac-y-te-hgi.component';

describe('CongTacYTeHgiComponent', () => {
  let component: CongTacYTeHgiComponent;
  let fixture: ComponentFixture<CongTacYTeHgiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongTacYTeHgiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongTacYTeHgiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
