import { TaiChinhKeHoachService } from './../../../../services/tai-chinh-ke-hoach.service';
import { ToChucCanBoService } from "../../../../services/to-chuc-can-bo.service";
import { DmChungService } from 'src/app/services/dm-chung.service';
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
  MA_DV_SO_Y_TE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { CommonFunctionService } from './../../../../services/common-function.service';

@Component({
  selector: 'app-cong-tac-y-te-hgi',
  templateUrl: './cong-tac-y-te-hgi.component.html',
  styleUrls: ['./cong-tac-y-te-hgi.component.scss']
})
export class CongTacYTeHgiComponent implements OnInit {

  subscription: Subscription[] = [];
  isFitPage = false;
  formSearch = new FormGroup({
    nam: new FormControl(new Date().getFullYear()),
    thang: new FormControl((new Date().getMonth() + 1).toString()),
  });
  documentElement: any;
  isFullScreen = false;
  nam: any[] = [];
  MONTHS: any[] = [];
  QUARTERS: any[] = [];
  rowStyles: any = {};
  mauBC: any;
  showNam = true;
  showThang = false;
  listThang = [];
  listNam = [];
  public isNoneChart = false;
  public isNoneChartNN = false;
  public isNoneChartHD = false;
  public isNoneChartCT = false;
  public isNoneChartBS = false;
  userStorage = JSON.parse(sessionStorage.user);
  @ViewChild("tinhHinhDichBenh", { static: true })
  tinhHinhDichBenh: BarChartComponent;
  @ViewChild("tiemChungMoRong", { static: true })
  tiemChungMoRong: BarChartComponent;
  @ViewChild("luyKeNamNamTruoc", { static: true })
  luyKeNamNamTruoc: BarChartComponent;
  @ViewChild("luotKhamBenh", { static: true })
  luotKhamBenh: BarChartComponent;
  @ViewChild("caDieuTriNoiTru", { static: true })
  caDieuTriNoiTru: BarChartComponent;
  @ViewChild("tinhHinhATVSTP", { static: true })
  tinhHinhATVSTP: BarChartComponent;
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_6[1],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
  ];
  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private dmchungService: DmChungService,
    private dateAdapter: DateAdapter<Date>,
    private taiChinhKeHoachService: TaiChinhKeHoachService,
    private commonService: CommonFunctionService,
  ) {
    this.dateAdapter.setLocale("vi-VN")
  }
  ngOnInit(): void {
    this.documentElement = document.documentElement;
    this.detectMode();
    this.getListNam();
    this.getListThang();
    this.getData();
  }
  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== 'responsive';
    this.buildStyles();
    this.tinhHinhDichBenh.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tiemChungMoRong.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.luyKeNamNamTruoc.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.luotKhamBenh.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.caDieuTriNoiTru.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tinhHinhATVSTP.barChartOptions.maintainAspectRatio = !this.isFitPage;
  }
  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 2 rows spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }
  toggleFullScreen() {
    if (!this.isFullScreen) {

      this.documentElement.requestFullscreen();
    } else {
      document.exitFullscreen();
    }
    this.isFullScreen = !this.isFullScreen;
  }
  onFullscreenChange(event) {
    let mainView = document.getElementsByClassName("main-wrapper")[0];
    if (!document.fullscreenElement) {
      // Xử lý khi thoát khỏi chế độ toàn màn hình
      mainView.classList.add("vh-100");
    }
  }
  onFilterChange (){
    this.getFilter()
    this.getData();
     this.clearAndFetchData();
  }
  clearAndFetchData() {
    // Xóa dữ liệu cũ trong biểu đồ hoặc thiết lập lại các biến dữ liệu
    this.tinhHinhDichBenh.barChartData = [];
    this.tiemChungMoRong.barChartData = [];
    this.luyKeNamNamTruoc.barChartData = [];
    this.luotKhamBenh.barChartData = [];
    this.caDieuTriNoiTru.barChartData = [];
    this.tinhHinhATVSTP.barChartData = [];

    // Gọi hàm lấy dữ liệu mới
    this.getData();
  }

  getData(): void {
    this.getTinhHinhDichBenh();
    this.timChungMoRong();
    this.getLuyKeNamNamTruoc();
    this.getLuotKhamBenh();
    this.getCaDieuTriNoiTru();
    this.gettinhHinhATVSTP();
  }
  getListNam(){
    this.subscription.push(
      this.dmchungService.getListNam().subscribe(
        (rs) => {
          this.listNam = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListThang(){
    this.subscription.push(
      this.dmchungService.getListThang().subscribe(
        (rs) => {
          this.listThang = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getFilter(){
    let nam  = this.formSearch.get("nam").value.toString();
    let thang  = this.formSearch.get("thang").value == "Tất cả" ? 0 : this.formSearch.get("thang").value;
    let result  =  {
      nam: nam,
      thang:thang
    };
    return result;
}
  getTinhHinhDichBenh(){
    let obj: any = this.getFilter();
    // this.isNoneChart = true
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTTinhHinhDichBenh(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.tinhHinhDichBenh.barChartLabels = rs.data.map(x => x.LOAI);
          let arrHienTai = rs.data.map(x => x.TONG_CA_MAC);
          let arrCungKy = rs.data.map(x => x.LUY_KE_CUNG_KY);
          let arrTB5Nam = rs.data.map(x => x.TB_CUNG_KY_5_NAM);
          let arrTuVong = rs.data.map(x => x.TU_VONG);
          this.tinhHinhDichBenh.barChartData = [
            {
              data: arrHienTai,
              label: 'Hiện tại',
              backgroundColor: "#38761d",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrCungKy,
              label: 'Cùng kỳ',
              backgroundColor: "#93c47d",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrTB5Nam,
              label: 'Trung bình cùng kỳ 5 năm',
              backgroundColor: "#ffca18",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrTuVong,
              label: 'Tử vong',
              backgroundColor: "#EA0808",
              datalabels: {align: "end", anchor:"end", clamp: true},
            }
          ];

        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  timChungMoRong(): void {
      let arrLableName = [{"name":'Tiêm đủ cho TE dưới 01 tuổi'},{"name":'Tiêm UV cho PN mang thai'}
                         ,{"name":'Tiêm MR cho trẻ 18 tháng'},{"name":'Tiêm BPT cho trẻ 18 tháng'}]
      //"Biểu đồ Tiêm chủng mở rộng, Tiêm đủ cho TE dưới 01 tuổi, Tiêm UV cho PN mang thai, Tiêm MR cho trẻ 18 tháng, Tiêm BPT cho trẻ 18 tháng"
      let arrKeHoach = [95, 85, 90, 80]
      let arrThucHien = [1.8, 10.7, 0.3, 0.3];
      this.tiemChungMoRong.barChartLabels = [];
      this.tiemChungMoRong.barChartLabels = arrLableName.map(x => this.convertToArray(' ', x.name, 3));
      this.tiemChungMoRong.barChartData.push({
        data: arrThucHien,
        label: "Thực hiện",
        backgroundColor: "#5b9bd5",
        order: 2
      });
      this.tiemChungMoRong.barChartData.push({
        type: "line",
        datalabels: { align: "top" },
        fill: false,
        data: arrKeHoach,
        label: "Kế hoạch",
        borderColor: "#ff7f27",
        backgroundColor: COLOR_PALETTES.PALETTE_3[1],
        order: 1
      });

  }
  getLuyKeNamNamTruoc(){
    let obj: any = this.getFilter();
    this.isNoneChart = true
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTTinhHinhNhiemBenhHIVAIDSTV(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.luyKeNamNamTruoc.barChartLabels = rs.data.map(x => x.LOAI);
          let arrHienTai = rs.data.map(x => x.LUY_KE_HIEN_TAI);
          let arrCungKy = rs.data.map(x => x.SS_CUNG_KY_5_NAM);
          this.luyKeNamNamTruoc.barChartData = [
            {
              data: arrHienTai,
              label: 'Luỹ kế hiện tại',
              backgroundColor: "#741b47",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrCungKy,
              label: 'So sánh cùng kỳ năm trước',
              backgroundColor: "#c27ba0",
              datalabels: {align: "end", anchor:"end", clamp: true},
            }
          ];

        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  getLuotKhamBenh(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTLuotKhamBenh(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.luotKhamBenh.barChartLabels = rs.data.map(x => x.TUYEN);
          let SLHienTai = rs.data.map(x => x.SL_HT);
          let SLCungKy = rs.data.map(x => x.SL_CK);
          this.luotKhamBenh.barChartData = [
            {
              data: SLHienTai,
              label: 'Hiện tại',
              backgroundColor: COLOR_PALETTES.PALETTE_1[2],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:15,
              },
            },
            {
              data: SLCungKy,
              label: 'Cùng kỳ',
              backgroundColor: COLOR_PALETTES.PALETTE_3[1],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:0,
              },
            }
          ];

        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  getCaDieuTriNoiTru(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTDieuTriNoiTru(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.caDieuTriNoiTru.barChartLabels = rs.data.map(x => x.TUYEN);
          let SLHienTai = rs.data.map(x => x.SL_HT);
          let SLCungKy = rs.data.map(x => x.SL_CK);
          this.caDieuTriNoiTru.barChartData = [
            {
              data: SLHienTai,
              label: 'Hiện tại',
              backgroundColor: COLOR_PALETTES.PALETTE_4[2],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:15,
              },
            },
            {
              data: SLCungKy,
              label: 'Cùng kỳ',
              backgroundColor: COLOR_PALETTES.PALETTE_3[3],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:0,
              },
            }
          ];

        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  gettinhHinhATVSTP(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.taiChinhKeHoachService.getATVSTP(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.tinhHinhATVSTP.barChartLabels = rs.data.map(
            (x) => this.commonService.convertStringToArray(" ", x.LOAI, 2)
          );
          let SLHienTai = rs.data.map(x => x.SL_HT);
          let SLCungKy = rs.data.map(x => x.SL_CK);
          this.tinhHinhATVSTP.barChartData = [
            {
              data: SLHienTai,
              label: 'Hiện tại',
              backgroundColor: "#8e7cc3",
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:15,
              },
            },
            {
              data: SLCungKy,
              label: 'Cùng kỳ',
              backgroundColor: "#a5a5a5",
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:0,
              },
            }
          ];
        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }

  getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }
}
