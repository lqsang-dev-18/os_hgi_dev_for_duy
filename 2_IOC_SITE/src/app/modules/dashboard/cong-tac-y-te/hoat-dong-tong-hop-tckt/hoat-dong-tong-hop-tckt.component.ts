import { PieChartComponent } from '../../../../shared/components/chart-js/pie-chart/pie-chart.component';
import { ToChucCanBoService } from "../../../../services/to-chuc-can-bo.service";
import { DmChungService } from 'src/app/services/dm-chung.service';
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { arrayBuffer } from 'stream/consumers';
import { NumberFormatService } from 'src/app/services/number-format.service';


@Component({
  selector: "app-hoat-dong-tong-hop-tckt",
  templateUrl: "./hoat-dong-tong-hop-tckt.component.html",
  styleUrls: ["./hoat-dong-tong-hop-tckt.component.scss"],
})
export class HoatDongTongHopComponent implements OnInit {
  subscription: Subscription[] = [];

  isFitPage = false;
  formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    nam: new FormControl(new Date().getFullYear()),
    thang: new FormControl((new Date().getMonth() + 1).toString()),
    
    
  });

  nam: any[] = [];
  MONTHS: any[] = [];
  QUARTERS: any[] = [];
  rowStyles: any = {};
  SL_4_THE_TONG:  any = {};
  public DON_VI_LIST: any = [];
  DOANHTHU: any;
  showNam = true;
  showThang = false;
  listThang = [];
  listNam = [];
  public isNoneChart = false;
  public isNoneChartNN = false;
  public isNoneChartHD = false;
  public isNoneChartCT = false;
  public isNoneChartBS = false;
  userStorage = JSON.parse(sessionStorage.user);

  @ViewChild("thongKeThangDuTheoLoai", { static: true }) thongKeThangDuTheoLoai: PieChartComponent;
  @ViewChild("thangDuThamHut", { static: true }) thangDuThamHut: BarChartComponent;
  @ViewChild("hoatDongSanXuat", { static: true }) hoatDongSanXuat: BarChartComponent;
  
  @ViewChild("hanhChinhSuNghiep", { static: true }) hanhChinhSuNghiep: BarChartComponent;
  @ViewChild("hoatDongTaiChinh", { static: true }) hoatDongTaiChinh: BarChartComponent;
  @ViewChild("hoatDongKhac", { static: true }) hoatDongKhac: BarChartComponent;
 

  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_6[1],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private dmchungService: DmChungService,
    private toChucCanBoService: ToChucCanBoService,
    private dateAdapter: DateAdapter<Date>,
    private numberFormatService: NumberFormatService
  ) {
    this.dateAdapter.setLocale("vi-VN")
    this.DOANHTHU = {
      TONGDOANHTHU: 0,
      TONGCHIPHI: 0,
      TONGTHANGDUTHAMHUT:0,
      TONGCHIPHITHUTNDN:0
    }
  }

  ngOnInit(): void {
    this.detectMode();
    this.getListNam()
    this.getListThang()
    this.getData();

  }

  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== "responsive";
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();
    this.thongKeThangDuTheoLoai.pieChartType = "doughnut";
    this.thongKeThangDuTheoLoai.pieChartOptions.maintainAspectRatio = !this.isFitPage;
    this.thongKeThangDuTheoLoai.pieChartOptions.legend.position = "right";

    this.thangDuThamHut.barChartOptions.maintainAspectRatio = !this.isFitPage;

    this.hoatDongSanXuat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.hanhChinhSuNghiep.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.hoatDongTaiChinh.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.hoatDongKhac.barChartOptions.maintainAspectRatio = !this.isFitPage;



  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 2 rows spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }
  onFilterChange(){
    
  }
 

  getData(): void {
    this.getDoanhThu()
    this.getThongKeThangDuTheoLoai()
    this.getThangDuThamHut()
    this.getHoatDongSanXuat()
    this.getHanhChinhSuNghiep()
    this.getHoatDongTaiChinh()
    this.getHoatDongKhac()
  }

  
  getListNam(){
    this.subscription.push(
      this.dmchungService.getListNam().subscribe(
        (rs) => {
          this.listNam = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
    
  }
  getListThang(){
    this.subscription.push(
      this.dmchungService.getListThang().subscribe(
        (rs) => {
          this.listThang = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  // getFilter() {
  //   let params = {};
  //   console.log(this.formSearch.controls.coSoKCB.value);
  //   if (this.formSearch.controls.year.value) {
  //     params["nam"] = this.formSearch.controls.year.value;
  //   }
  //   if (this.formSearch.controls.coSoKCB.value) {
  //     params["maDonVi"] = this.formSearch.controls.coSoKCB.value.toString();
  //   }
  //   else if(this.userStorage?.MA_DON_VI.toString() !== MA_DV_SO_Y_TE.toString()) {
  //     params["maDonVi"] = this.userStorage?.MA_DON_VI 
  //   }
  //   else{
  //     params["maDonVi"] = -1
  //   }

  //   // console.log(params)
  //   return params;

  // }

  getDoanhThu(){
    let objDoangthu =[{"TONGDOANHTHU":2599700180122},{"TONGCHIPHI":2244856965980}, {"TONGTHANGDUTHAMHUT":354843184142},{"TONGCHIPHITHUTNDN":423463323}]
    this.DOANHTHU = {
      TONGDOANHTHU: objDoangthu[0].TONGDOANHTHU,
      TONGCHIPHI: objDoangthu[1].TONGCHIPHI,
      TONGTHANGDUTHAMHUT: objDoangthu[2].TONGTHANGDUTHAMHUT,
      TONGCHIPHITHUTNDN: objDoangthu[3].TONGCHIPHITHUTNDN,
    }
    // this.DOANHTHU = {
    //   TONGDOANHTHU: new Intl.NumberFormat("vi-VN").format(objDoangthu[0].TONGDOANHTHU),
    //   TONGCHIPHI: new Intl.NumberFormat("vi-VN").format(objDoangthu[1].TONGCHIPHI),
    //   TONGTHANGDUTHAMHUT:new Intl.NumberFormat("vi-VN").format(objDoangthu[2].TONGTHANGDUTHAMHUT),
    //   TONGCHIPHITHUTNDN:new Intl.NumberFormat("vi-VN").format(objDoangthu[3].TONGCHIPHITHUTNDN),
    // }
  }
  getThongKeThangDuTheoLoai(){
    this.isNoneChart =true;
    let arrLableName = [{"name":"HĐ SX-KD-DV"}, {"name":"HĐ tài chính"}, {"name":"HĐ Hành chính, sự nghiệp"}, {"name":"HĐ khác"}]
    let arrThongKeThangDuTheoLoai = [33374128357,2108001645,15462537186,3531361741]
    
    this.thongKeThangDuTheoLoai.pieChartData = arrThongKeThangDuTheoLoai
    this.thongKeThangDuTheoLoai.pieChartLabels =  arrLableName.map(x => this.convertToArray(' ', x.name, 3));
    this.thongKeThangDuTheoLoai.pieChartColor = [
      { backgroundColor: COLOR_PALETTES.PALETTE_1 },
    ];
  }
  getThangDuThamHut(): void {
    this.isNoneChart = true
    let arrLableName =  [{"name":"SD kinh phí tiết kiệm"}, {"name":"PP các quỹ"}, {"name":"KP cải cách tiền lương"},{"name":"PP khác"}]
    let arrHienTai =[918655845,316249496363,38772009367, 0]
    let arrCungKy =  [844186612, 202670923493, 32392945688, 4047378]
    this.thangDuThamHut.barChartLabels =  arrLableName.map(x => this.convertToArray(' ', x.name, 3));
      this.thangDuThamHut.barChartData.push({
        data: arrHienTai,
        label: "Hiện tại",
        backgroundColor: "#38761d",
        datalabels: {align: "top", anchor:"end", clamp: true}
      });
      this.thangDuThamHut.barChartData.push({
        data: arrCungKy,
        label: "Cùng kỳ",
        backgroundColor: "#93c47d",
        datalabels: {align: "top", anchor:"center", clamp: true}
      });
  }
  

  getHoatDongSanXuat(): void {
    this.isNoneChart = true
    let arrLableName = [{"name":"Doanh thu"}, {"name":"Chi phí"}, {"name":"Thặng dư/ thâm hụt"}]
    let arrHienTai = [1846285393521,1512544109951,333741283570]
    let arrCungKy = [1610928174424, 1403515068180, 207413106244]
    this.hoatDongSanXuat.barChartLabels = arrLableName.map(x => this.convertToArray(' ', x.name, 3));
      this.hoatDongSanXuat.barChartData.push({
        data: arrHienTai,
        label: "Hiện tại",
        backgroundColor: "#0b5394",
        datalabels: {align: "top", anchor:"end", clamp: true}
      });
      this.hoatDongSanXuat.barChartData.push({
        data: arrCungKy,
        label: "Cùng kỳ",
        backgroundColor: "#8e7cc3",
        datalabels: {align: "top", anchor:"center", clamp: true}
      });
  }
  getHanhChinhSuNghiep(): void {
    this.isNoneChart = true
    let arrLableName =  [{"name":"Doanh thu"}, {"name":"Chi phí"}, {"name":"Thặng dư/ thâm hụt"}]
    let arrHienTai = [746268950521, 730806413335, 15462537186]
    let arrCungKy = [945449669161,  940238549170, 5210970034]
    this.hanhChinhSuNghiep.barChartLabels = arrLableName.map(x => this.convertToArray(' ', x.name, 3));
      this.hanhChinhSuNghiep.barChartData.push({
        data: arrHienTai,
        label: "Hiện tại",
        backgroundColor: "#134f5c",
        datalabels: {align: "top", anchor:"end", clamp: true}
      });
      this.hanhChinhSuNghiep.barChartData.push({
        data: arrCungKy,
        label: "Cùng kỳ",
        backgroundColor: "#93c47d",
        datalabels: {align: "top", anchor:"center", clamp: true}
      });
  }
  getHoatDongTaiChinh(): void {
    this.isNoneChart = true
    let arrLableName =  [{"name":"Doanh thu"}, {"name":"Chi phí"}, {"name":"Thặng dư/ thâm hụt"}]
    let arrHienTai = [2137255793,29224148,2108001645]
    let arrCungKy = [2831003498,356264412, 2474739086]
    this.hoatDongTaiChinh.barChartLabels = arrLableName.map(x => this.convertToArray(' ', x.name, 3));
      this.hoatDongTaiChinh.barChartData.push({
        data: arrHienTai,
        label: "Hiện tại",
        backgroundColor: "#741b47",
        datalabels: {align: "top", anchor:"end", clamp: true}
      });
      this.hoatDongTaiChinh.barChartData.push({
        data: arrCungKy,
        label: "Cùng kỳ",
        backgroundColor: "#dd7e6b",
        datalabels: {align: "top", anchor:"center", clamp: true}
      });
  }
  getHoatDongKhac(): void {
    this.isNoneChart = true
    let arrLableName = [{"name":"Doanh thu"}, {"name":"Chi phí"}, {"name":"Thặng dư/ thâm hụt"}]
    let arrHienTai = [5008580287,1477218546, 3531361741]
    let arrCungKy = [5688457999,2347273373, 3341184626]
    this.hoatDongKhac.barChartLabels = arrLableName.map(x => this.convertToArray(' ', x.name, 3));
      this.hoatDongKhac.barChartData.push({
        data: arrHienTai,
        label: "Hiện tại",
        backgroundColor: "#351c75",
        datalabels: {align: "top", anchor:"end", clamp: true}
      });
      this.hoatDongKhac.barChartData.push({
        data: arrCungKy,
        label: "Cùng kỳ",
        backgroundColor: "#6d9eeb",
        datalabels: {align: "top", anchor:"center", clamp: true}
      });
  }
    
  
  getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }
  private reduceLabel(label: string) {
    return label.replace(/huyện/gi, "")
                .replace(/thị xã/gi, "TX. ")
                .replace(/thành phố/gi, "TP. ");
  }
  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }

  nFormatter(num: any, digits: number) {
    if (isNaN(num)) {
      return num;
    }
    const lookup = [
      { value: 1, symbol: "" },
      { value: 1e3, symbol: " nghìn đồng" },
      { value: 1e6, symbol: " triệu đồng" },
      // { value: 1e9, symbol: " tỷ" },
      // { value: 1e12, symbol: "T" },
      // { value: 1e15, symbol: "P" },
      // { value: 1e18, symbol: "E" },
    ];
    const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i: number;
    for (i = lookup.length - 1; i > 0; i--) {
      if (num >= lookup[i].value) {
        break;
      }
    }
    if (i >= 3) {
      return (
        Number(
          (num / lookup[i].value).toFixed(digits).replace(rx, "$1")
        ).toLocaleString("vi") + lookup[i].symbol
      );
    } else {
      return (
        Number(
          (num / lookup[i].value).toFixed(0).replace(rx, "$1")
        ).toLocaleString("vi") + lookup[i].symbol
      );
    }
  }

}
