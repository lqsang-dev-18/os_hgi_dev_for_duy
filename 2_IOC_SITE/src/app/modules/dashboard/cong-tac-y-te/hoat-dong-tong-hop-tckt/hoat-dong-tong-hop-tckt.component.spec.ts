import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoatDongTongHopComponent } from './hoat-dong-tong-hop-tckt.component';

describe('HoatDongTongHopComponent', () => {
  let component: HoatDongTongHopComponent;
  let fixture: ComponentFixture<HoatDongTongHopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoatDongTongHopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoatDongTongHopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
