import { TaiChinhKeHoachService } from './../../../../services/tai-chinh-ke-hoach.service';
import { ToChucCanBoService } from "../../../../services/to-chuc-can-bo.service";
import { DmChungService } from 'src/app/services/dm-chung.service';
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { DateAdapter } from '@angular/material/core';
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import * as XLSX from "xlsx";
import { saveAs } from "file-saver";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
  MA_DV_SO_Y_TE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";

@Component({
  selector: "app-cong-tac-y-te",
  templateUrl: "./cong-tac-y-te.component.html",
  styleUrls: ["./cong-tac-y-te.component.scss"],
})
export class CongTacYTeComponent implements OnInit {
  subscription: Subscription[] = [];
  isFitPage = false;
  formSearch = new FormGroup({
    nam: new FormControl(new Date().getFullYear()),
    thang: new FormControl((new Date().getMonth() + 1).toString()),    
  });
  documentElement: any;
  isFullScreen = false;
  nam: any[] = [];
  MONTHS: any[] = [];
  QUARTERS: any[] = [];
  rowStyles: any = {};
  mauBC: any;
  showNam = true;
  showThang = false;
  listThang = [];
  listNam = [];
  DichBenh_Excel: any[] = [];
  TiemChung_Excel: any[] = [];
  LuyKeTrongNamVaNamTruoc_Excel: any[] = [];
  LuotKhamBenh_Excel: any[] = [];
  DieuTriNoiTru_Excel: any[] = [];
  TNGT_Excel: any[] = [];
  public isNoneChart = false;
  public isNoneChartNN = false;
  public isNoneChartHD = false;
  public isNoneChartCT = false;
  public isNoneChartBS = false;
  userStorage = JSON.parse(sessionStorage.user);
  @ViewChild("tinhHinhDichBenh", { static: true })
  tinhHinhDichBenh: BarChartComponent;
  @ViewChild("tiemChungMoRong", { static: true })
  tiemChungMoRong: BarChartComponent;
  @ViewChild("luyKeNamNamTruoc", { static: true })
  luyKeNamNamTruoc: BarChartComponent;
  @ViewChild("luotKhamBenh", { static: true })
  luotKhamBenh: BarChartComponent;
  @ViewChild("caDieuTriNoiTru", { static: true })
  caDieuTriNoiTru: BarChartComponent;
  @ViewChild("tinhHinhTaiNanGT", { static: true })
  tinhHinhTaiNanGT: BarChartComponent;
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_6[1],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
  ];
  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private dmchungService: DmChungService,
    private toChucCanBoService: ToChucCanBoService,
    private dateAdapter: DateAdapter<Date>,
    private taiChinhKeHoachService: TaiChinhKeHoachService,
  ) {
    this.dateAdapter.setLocale("vi-VN")
  }
  ngOnInit(): void {
    this.documentElement = document.documentElement;
    this.detectMode();
    this.getListNam();
    this.getListThang();
    this.mauBC = {
      DEFAULT_SHEETNAME: 'CongTacYTe',
      DEFAULT_COLUMNNAME: 'Tên Cột Mặc Định'
    };
    this.getData();
  }
  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== 'responsive';
    this.buildStyles();
    this.tinhHinhDichBenh.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tiemChungMoRong.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.luyKeNamNamTruoc.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.luotKhamBenh.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.caDieuTriNoiTru.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tinhHinhTaiNanGT.barChartOptions.maintainAspectRatio = !this.isFitPage;
  }
  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 2 rows spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }
  toggleFullScreen() {
    if (!this.isFullScreen) {
      
      this.documentElement.requestFullscreen();
    } else {
      document.exitFullscreen();
    }
    this.isFullScreen = !this.isFullScreen;
  }
  onFullscreenChange(event) {
    let mainView = document.getElementsByClassName("main-wrapper")[0];
    if (!document.fullscreenElement) {
      // Xử lý khi thoát khỏi chế độ toàn màn hình
      mainView.classList.add("vh-100");
    }
  }
  onFilterChange (){
    this.getFilter()
    this.getData();
    this.clearAndFetchData();
  }
  onThangChange() {
    // Gọi hàm để xóa dữ liệu cũ và lấy dữ liệu mới
    this.clearAndFetchData();
  }
  clearAndFetchData() {
    // Xóa dữ liệu cũ trong biểu đồ hoặc thiết lập lại các biến dữ liệu
    this.tinhHinhDichBenh.barChartData = [];
    this.tiemChungMoRong.barChartData = [];
    this.luyKeNamNamTruoc.barChartData = [];
    this.luotKhamBenh.barChartData = [];
    this.caDieuTriNoiTru.barChartData = [];
    this.tinhHinhTaiNanGT.barChartData = [];
    this.DichBenh_Excel=[];
    this.TiemChung_Excel=[];
    this.LuyKeTrongNamVaNamTruoc_Excel=[];
    this.LuotKhamBenh_Excel=[];
    this.DieuTriNoiTru_Excel=[];
    this.TNGT_Excel=[];
    // Gọi hàm lấy dữ liệu mới
    this.getData();
  }
  getData(): void {
    this.getTinhHinhDichBenh();
    this.timChungMoRong();
    this.getLuyKeNamNamTruoc();
    this.getLuotKhamBenh();
    this.getCaDieuTriNoiTru();
    this.getTinhHinhTaiNanGT();
  }
  getListNam(){
    this.subscription.push(
      this.dmchungService.getListNam().subscribe(
        (rs) => {
          this.listNam = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListThang(){
    this.subscription.push(
      this.dmchungService.getListThang().subscribe(
        (rs) => {
          this.listThang = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getFilter(){
    let nam  = this.formSearch.get("nam").value;
    let thang  = this.formSearch.get("thang").value == "Tất cả" ? 0 : this.formSearch.get("thang").value;
    let result  =  { 
      nam: nam,
      thang:thang
    };
    return result;
}
  getTinhHinhDichBenh(){
    let obj: any = this.getFilter();
    // this.isNoneChart = true
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTTinhHinhDichBenh(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.tinhHinhDichBenh.barChartLabels = rs.data.map(x => x.LOAI);
          let arrHienTai = rs.data.map(x => x.TONG_CA_MAC);
          let arrCungKy = rs.data.map(x => x.LUY_KE_CUNG_KY);
          let arrTB5Nam = rs.data.map(x => x.TB_CUNG_KY_5_NAM);
          let arrTuVong = rs.data.map(x => x.TU_VONG);
          this.tinhHinhDichBenh.barChartData = [
            { 
              data: arrHienTai, 
              label: 'Hiện tại', 
              backgroundColor: "#38761d",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrCungKy, 
              label: 'Cùng kỳ', 
              backgroundColor: "#93c47d",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrTB5Nam, 
              label: 'Trung bình cùng kỳ 5 năm', 
              backgroundColor: "#ffca18",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrTuVong, 
              label: 'Tử vong', 
              backgroundColor: "#EA0808",
              datalabels: {align: "end", anchor:"end", clamp: true},
            }
          ];
          this.DichBenh_Excel.push("Sốt xuất huyết");
          this.DichBenh_Excel.push(arrHienTai[0]);
          this.DichBenh_Excel.push(arrCungKy[0]);
          this.DichBenh_Excel.push(arrTB5Nam[0]);
          this.DichBenh_Excel.push(arrTuVong[0]);
          this.DichBenh_Excel.push("Tay chân miệng");
          this.DichBenh_Excel.push(arrHienTai[1]);
          this.DichBenh_Excel.push(arrCungKy[1]);
          this.DichBenh_Excel.push(arrTB5Nam[1]);
          this.DichBenh_Excel.push(arrTuVong[1]);
        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  timChungMoRong(): void {
      let arrLableName = [{"name":'Tiêm đủ cho TE dưới 01 tuổi'},{"name":'Tiêm UV cho PN mang thai'}
                         ,{"name":'Tiêm MR cho trẻ 18 tháng'},{"name":'Tiêm BPT cho trẻ 18 tháng'}]
      //"Biểu đồ Tiêm chủng mở rộng, Tiêm đủ cho TE dưới 01 tuổi, Tiêm UV cho PN mang thai, Tiêm MR cho trẻ 18 tháng, Tiêm BPT cho trẻ 18 tháng"
      let arrKeHoach = [95, 85, 90, 80]
      let arrThucHien = [1.8, 10.7, 0.3, 0.3];
      this.tiemChungMoRong.barChartLabels = [];
      this.tiemChungMoRong.barChartLabels = arrLableName.map(x => this.convertToArray(' ', x.name, 3));
      this.tiemChungMoRong.barChartData.push({
        data: arrThucHien,
        label: "Thực hiện",
        backgroundColor: "#5b9bd5",
        order: 2
      });
      this.tiemChungMoRong.barChartData.push({
        type: "line",
        datalabels: { align: "top" },
        fill: false,
        data: arrKeHoach,
        label: "Kế hoạch",
        borderColor: "#ff7f27",
        backgroundColor: COLOR_PALETTES.PALETTE_3[1],
        order: 1
      });
      this.TiemChung_Excel.push("Tiêm chủng cho trẻ em dưới 1 tuổi");
      this.TiemChung_Excel.push(arrKeHoach[0]);
      this.TiemChung_Excel.push(arrThucHien[0]);
      this.TiemChung_Excel.push("Tiêm UV cho PN mang thai");
      this.TiemChung_Excel.push(arrKeHoach[1]);
      this.TiemChung_Excel.push(arrThucHien[1]);
      this.TiemChung_Excel.push("Tiêm MR cho trẻ 18 tháng");
      this.TiemChung_Excel.push(arrKeHoach[2]);
      this.TiemChung_Excel.push(arrThucHien[2]);
      this.TiemChung_Excel.push("Tiêm BPT cho trẻ 18 tháng");
      this.TiemChung_Excel.push(arrKeHoach[3]);
      this.TiemChung_Excel.push(arrThucHien[3]);
  }
  getLuyKeNamNamTruoc(){
    let obj: any = this.getFilter();
    this.isNoneChart = true
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTTinhHinhNhiemBenhHIVAIDSTV(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.luyKeNamNamTruoc.barChartLabels = rs.data.map(x => x.LOAI);
          let arrHienTai = rs.data.map(x => x.LUY_KE_HIEN_TAI);
          let arrCungKy = rs.data.map(x => x.SS_CUNG_KY_5_NAM);
          this.luyKeNamNamTruoc.barChartData = [
            { 
              data: arrHienTai, 
              label: 'Luỹ kế hiện tại', 
              backgroundColor: "#741b47",
              datalabels: {align: "end", anchor:"end", clamp: true},
            },
            {
              data: arrCungKy, 
              label: 'So sánh cùng kỳ năm trước', 
              backgroundColor: "#c27ba0",
              datalabels: {align: "end", anchor:"end", clamp: true},
            }
          ];
          this.LuyKeTrongNamVaNamTruoc_Excel.push("HIV");
          this.LuyKeTrongNamVaNamTruoc_Excel.push(arrHienTai[0]);
          this.LuyKeTrongNamVaNamTruoc_Excel.push(arrCungKy[0]);
          this.LuyKeTrongNamVaNamTruoc_Excel.push("AIDS");
          this.LuyKeTrongNamVaNamTruoc_Excel.push(arrHienTai[1]);
          this.LuyKeTrongNamVaNamTruoc_Excel.push(arrCungKy[1]);
          this.LuyKeTrongNamVaNamTruoc_Excel.push("Tử vong");
          this.LuyKeTrongNamVaNamTruoc_Excel.push(arrHienTai[2]);
          this.LuyKeTrongNamVaNamTruoc_Excel.push(arrCungKy[2]);
        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  getLuotKhamBenh(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTLuotKhamBenh(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.luotKhamBenh.barChartLabels = rs.data.map(x => x.TUYEN);
          let SLHienTai = rs.data.map(x => x.SL_HT);
          let SLCungKy = rs.data.map(x => x.SL_CK);
          this.luotKhamBenh.barChartData = [
            { 
              data: SLHienTai, 
              label: 'Hiện tại', 
              backgroundColor: COLOR_PALETTES.PALETTE_1[2],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:15,
              },
            },
            {
              data: SLCungKy, 
              label: 'Cùng kỳ', 
              backgroundColor: COLOR_PALETTES.PALETTE_3[1],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:0,
              },
            }
          ];
          this.LuotKhamBenh_Excel.push("Tuyến tỉnh");
          this.LuotKhamBenh_Excel.push(SLHienTai[0]);
          this.LuotKhamBenh_Excel.push(SLCungKy[0]);
          this.LuotKhamBenh_Excel.push("Tuyến huyện");
          this.LuotKhamBenh_Excel.push(SLHienTai[1]);
          this.LuotKhamBenh_Excel.push(SLCungKy[1]);
          this.LuotKhamBenh_Excel.push("Tuyến xã");
          this.LuotKhamBenh_Excel.push(SLHienTai[2]);
          this.LuotKhamBenh_Excel.push(SLCungKy[2]);
        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  getCaDieuTriNoiTru(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTDieuTriNoiTru(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.caDieuTriNoiTru.barChartLabels = rs.data.map(x => x.TUYEN);
          let SLHienTai = rs.data.map(x => x.SL_HT);
          let SLCungKy = rs.data.map(x => x.SL_CK);
          this.caDieuTriNoiTru.barChartData = [
            { 
              data: SLHienTai, 
              label: 'Hiện tại', 
              backgroundColor: COLOR_PALETTES.PALETTE_4[2],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:15,
              },
            },
            {
              data: SLCungKy, 
              label: 'Cùng kỳ', 
              backgroundColor: COLOR_PALETTES.PALETTE_3[3],
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:0,
              },
            }
          ];
          this.DieuTriNoiTru_Excel.push("Tuyến tỉnh");
          this.DieuTriNoiTru_Excel.push(SLHienTai[0]);
          this.DieuTriNoiTru_Excel.push(SLCungKy[0]);
          this.DieuTriNoiTru_Excel.push("Tuyến huyện");
          this.DieuTriNoiTru_Excel.push(SLHienTai[1]);
          this.DieuTriNoiTru_Excel.push(SLCungKy[1]);
        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  getTinhHinhTaiNanGT(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.taiChinhKeHoachService.getCTYTTaiNanGiaoThong(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.tinhHinhTaiNanGT.barChartLabels = rs.data.map(x => x.LOAI);
          let SLHienTai = rs.data.map(x => x.SL_HT);
          let SLCungKy = rs.data.map(x => x.SL_CK);
          this.tinhHinhTaiNanGT.barChartData = [
            { 
              data: SLHienTai, 
              label: 'Hiện tại', 
              backgroundColor: "#8e7cc3",
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:15,
              },
            },
            {
              data: SLCungKy, 
              label: 'Cùng kỳ', 
              backgroundColor: "#a5a5a5",
              datalabels: {
                align: 'top',
                anchor: 'end',
                padding:0,
              },
            }
          ];
          this.TNGT_Excel.push("Số bệnh nhân");
          this.TNGT_Excel.push(SLHienTai[0]);
          this.TNGT_Excel.push(SLCungKy[0]);
          this.TNGT_Excel.push("Số tử vong");
          this.TNGT_Excel.push(SLHienTai[1]);
          this.TNGT_Excel.push(SLCungKy[1]);
        }, err => {
          this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
        })
    );
  }
  getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }
  private reduceLabel(label: string) {
    return label.replace(/huyện/gi, "")
                .replace(/thị xã/gi, "TX. ")
                .replace(/thành phố/gi, "TP. ");
  }
  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }
  // cột dữ liệu - dữ liệu - mapdata - 1 mảng bao nhiêu cột thì xuống dòng - data có bao nhiêu dòng
  xuatdulieu(columnNames, columnName_excel, mappedData, dongxuatdata, dong) {
    // const mappedData = [];
    columnName_excel.splice(dongxuatdata, columnName_excel.length - dongxuatdata);
    if(dong<columnNames.length){
      columnNames.splice(dong, columnNames.length - dong);
    }
    for (let i = 0; i < columnNames.length; i += columnName_excel.length) {
        const rowData = {};
        columnName_excel.forEach((columnName, index) => {
            const value = columnNames[i + index] ?? "";
            rowData[columnName] = value;
        });
        mappedData.push(rowData);
    }
    return mappedData;
  }
  exportToExcel(): void {
    const sheetName = "Công tác y tế"; // Tên sheet
    const columnName_excel=["A", "B", "C", "D", "E", "F", "G"];
    const columnNames_dichbenh = [" ", "Hiện tại", "Cùng kỳ", "Trung bình cùng kỳ 5 năm", "Tử vong"];
    const columnNames_tiemchung = [" ","Kế hoạch", "Thực hiện"];
    const columnNames_LuyKeTrongNamVaNamTruoc = [" ","Lũy kế hiện tại", "So với cùng kỳ năm trước"];
    const columnNames_luotkham = [" ","Hiện tại", "Cùng kỳ"];
    const mappedData = [];
    // Tạo các dòng dữ liệu trong bảng Excel
    mappedData.push({ "A": "Tình hình dịch bệnh" });
    // Dữ liệu - cột - bao nhiêu xuống dòng - bao nhiêu dòng
    this.xuatdulieu(columnNames_dichbenh, columnName_excel, mappedData,5,9);
    this.xuatdulieu(this.DichBenh_Excel, columnName_excel, mappedData,5,9);

    mappedData.push({ });
    mappedData.push({ "A": "Tiêm chủng mở rộng lũy kế" });
    this.xuatdulieu(columnNames_tiemchung, columnName_excel, mappedData,3,3);
    this.xuatdulieu(this.TiemChung_Excel, columnName_excel, mappedData,3,12);

    mappedData.push({ });
    mappedData.push({ "A": "Lũy kế trong năm và năm trước" });
    this.xuatdulieu(columnNames_LuyKeTrongNamVaNamTruoc, columnName_excel, mappedData,3,3);
    this.xuatdulieu(this.LuyKeTrongNamVaNamTruoc_Excel, columnName_excel, mappedData,3,9);

    mappedData.push({ });
    mappedData.push({ "A": "Lượt khám bệnh" });
    this.xuatdulieu(columnNames_luotkham, columnName_excel, mappedData,3,3);
    this.xuatdulieu(this.LuotKhamBenh_Excel, columnName_excel, mappedData,3,9);

    mappedData.push({ });
    mappedData.push({ "A": "Điều trị nội trú" });
    this.xuatdulieu(columnNames_luotkham, columnName_excel, mappedData,3,3);
    this.xuatdulieu(this.DieuTriNoiTru_Excel, columnName_excel, mappedData,3,6);

    mappedData.push({ });
    mappedData.push({ "A": "Tình hình tai nạn giao thông" });
    this.xuatdulieu(columnNames_luotkham, columnName_excel, mappedData,3,3);
    this.xuatdulieu(this.TNGT_Excel, columnName_excel, mappedData,3,6);

    const worksheet = XLSX.utils.json_to_sheet(mappedData);
    worksheet['!cols'] = [{ width: 30 }];  
    const workbook = {
      Sheets: { [sheetName]: worksheet },
      SheetNames: [sheetName],
    };
    const excelBuffer: any = XLSX.write(workbook, {
      bookType: "xlsx",
      type: "array",
    });
    const excelFile: Blob = new Blob([excelBuffer], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
    });
    saveAs(
      excelFile,
      this.mauBC.DEFAULT_SHEETNAME +
      " " +
      ".xlsx"
    );
  }
}
