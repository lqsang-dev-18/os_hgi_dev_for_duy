import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CongTacYTeComponent } from './cong-tac-y-te.component';

describe('CongTacYTeComponent', () => {
  let component: CongTacYTeComponent;
  let fixture: ComponentFixture<CongTacYTeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CongTacYTeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CongTacYTeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
