import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TTPhapYComponent } from './tt-phap-y.component';

describe('TTPhapYComponent', () => {
  let component: TTPhapYComponent;
  let fixture: ComponentFixture<TTPhapYComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TTPhapYComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TTPhapYComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
