import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTPhapYComponent } from './tt-phap-y/tt-phap-y.component';

const routes: Routes = [
  { path: '', component: TTPhapYComponent },
  { path: 'tt-phap-y', component: TTPhapYComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrungTamGiamDinhPhapYRoutingModule { }
