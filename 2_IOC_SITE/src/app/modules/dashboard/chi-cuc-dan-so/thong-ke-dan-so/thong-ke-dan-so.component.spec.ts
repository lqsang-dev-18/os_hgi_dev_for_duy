import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongKeDanSoComponent } from './thong-ke-dan-so.component';

describe('ThongKeDanSoComponent', () => {
  let component: ThongKeDanSoComponent;
  let fixture: ComponentFixture<ThongKeDanSoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongKeDanSoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongKeDanSoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
