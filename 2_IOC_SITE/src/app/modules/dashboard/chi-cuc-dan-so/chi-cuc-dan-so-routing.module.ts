import { ThongKeDanSoComponent } from './thong-ke-dan-so/thong-ke-dan-so.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: "", component: ThongKeDanSoComponent },
  { path: "thong-ke-dan-so", component: ThongKeDanSoComponent },];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChiCucDanSoRoutingModule { }
