import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DauTuCongComponent } from './dau-tu-cong.component';

describe('DauTuCongComponent', () => {
  let component: DauTuCongComponent;
  let fixture: ComponentFixture<DauTuCongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DauTuCongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DauTuCongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
