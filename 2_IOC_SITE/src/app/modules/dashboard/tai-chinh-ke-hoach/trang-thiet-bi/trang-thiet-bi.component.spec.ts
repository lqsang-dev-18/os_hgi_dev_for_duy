import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrangThietBiComponent } from './trang-thiet-bi.component';

describe('TrangThietBiComponent', () => {
  let component: TrangThietBiComponent;
  let fixture: ComponentFixture<TrangThietBiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrangThietBiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrangThietBiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
