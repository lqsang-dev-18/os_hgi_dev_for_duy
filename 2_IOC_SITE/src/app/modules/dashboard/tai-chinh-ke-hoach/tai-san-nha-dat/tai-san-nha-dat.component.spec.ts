/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { TaiSanNhaDatComponent } from './tai-san-nha-dat.component';

describe('TaiSanNhaDatComponent', () => {
  let component: TaiSanNhaDatComponent;
  let fixture: ComponentFixture<TaiSanNhaDatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaiSanNhaDatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaiSanNhaDatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
