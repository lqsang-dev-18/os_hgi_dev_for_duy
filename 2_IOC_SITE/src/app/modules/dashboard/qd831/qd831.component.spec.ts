import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QD831Component } from './qd831.component';

describe('QD831Component', () => {
  let component: QD831Component;
  let fixture: ComponentFixture<QD831Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QD831Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QD831Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
