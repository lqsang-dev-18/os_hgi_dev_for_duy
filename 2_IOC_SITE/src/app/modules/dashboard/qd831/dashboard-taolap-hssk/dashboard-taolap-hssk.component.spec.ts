import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTaolapHsskComponent } from './dashboard-taolap-hssk.component';

describe('DashboardTaolapHsskComponent', () => {
  let component: DashboardTaolapHsskComponent;
  let fixture: ComponentFixture<DashboardTaolapHsskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTaolapHsskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTaolapHsskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
