
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TTKiemNghiemDuocPhamMyPhamComponent } from './tt-kiem-nghiem-duoc-pham-my-pham/tt-kiem-nghiem-duoc-pham-my-pham.component'

const routes: Routes = [
  { path: '', component: TTKiemNghiemDuocPhamMyPhamComponent },
  { path: 'tt-kiem-nghiem-duoc-pham-my-pham', component: TTKiemNghiemDuocPhamMyPhamComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TTKiemNghiemDuocPhamMyPhamRoutingModule { }
