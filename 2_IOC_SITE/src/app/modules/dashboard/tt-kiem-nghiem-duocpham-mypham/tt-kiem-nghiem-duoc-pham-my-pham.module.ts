import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TTKiemNghiemDuocPhamMyPhamRoutingModule } from './tt-kiem-nghiem-duoc-pham-my-pham-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatButtonModule } from '@angular/material/button';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatInputModule } from '@angular/material/input';
import { TTKiemNghiemDuocPhamMyPhamComponent } from './tt-kiem-nghiem-duoc-pham-my-pham/tt-kiem-nghiem-duoc-pham-my-pham.component';

@NgModule({
  declarations: [ TTKiemNghiemDuocPhamMyPhamComponent],
  imports: [
    CommonModule,
    TTKiemNghiemDuocPhamMyPhamRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSlideToggleModule,
    SharedModule
  ]
})
export class TTKiemNghiemDuocPhamMyPhamModule { }
