import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TTKiemNghiemDuocPhamMyPhamComponent } from './tt-kiem-nghiem-duoc-pham-my-pham.component';

describe('TTKiemNghiemDuocPhamMyPhamComponent', () => {
  let component: TTKiemNghiemDuocPhamMyPhamComponent;
  let fixture: ComponentFixture<TTKiemNghiemDuocPhamMyPhamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TTKiemNghiemDuocPhamMyPhamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TTKiemNghiemDuocPhamMyPhamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
