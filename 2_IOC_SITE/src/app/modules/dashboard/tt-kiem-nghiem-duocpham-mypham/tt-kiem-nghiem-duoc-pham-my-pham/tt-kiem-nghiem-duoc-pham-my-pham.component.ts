
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import {
  ID_TINH,
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { TTKiemNghiemDuocPhamMyPhamService } from "src/app/services/tt-kiem-nghiem-duoc-pham-my-pham.service";
import { DmChungService } from 'src/app/services/dm-chung.service';
import { CommonFunctionService } from "src/app/services/common-function.service";

@Component({
  selector: 'app-tt-kiem-nghiem-duoc-pham-my-pham',
  templateUrl: './tt-kiem-nghiem-duoc-pham-my-pham.component.html',
  styleUrls: ['./tt-kiem-nghiem-duoc-pham-my-pham.component.scss']
})
export class TTKiemNghiemDuocPhamMyPhamComponent implements OnInit {
  subscription: Subscription[] = [];
  isNoneChart = false;
  isFitPage = false;
  formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    coSoKCB: new FormControl(-1),
    huyen: new FormControl(""),
    tenhuyen: new FormControl(null),
    loai: new FormControl(0),
    quy: new FormControl("Tất cả"),
    thang: new FormControl((new Date().getMonth() + 1).toString()),
  });

  YEARS: any[] = [];
  MONTHS: any[] = [];
  QUARTERS: any[] = [];
  rowStyles: any = {};
  isHuyen= false;
  listHuyen = [];
  listQuy = [];
  listThang = [];
  listNam = [];
  showNam = true;
  showThang = false;
  showQuy = false;
  SL_4_THE_TONG:  any = {};
  public DON_VI_LIST: any = [];
  public isLayMau = false;
  public isLayGui = false;
  public isMauLayKhongDat = false;
  public isMauGuiKhongDat = false;
  userStorage = JSON.parse(sessionStorage.user);
  @ViewChild("slLayMau", { static: true })
  slLayMau: BarChartComponent;
  @ViewChild("slLayGui", { static: true })
  slLayGui: BarChartComponent;
  @ViewChild("slMauLayKhongDat", { static: true })
  slMauLayKhongDat: BarChartComponent;
  @ViewChild("slMauGuiKhongDat", { static: true })
  slMauGuiKhongDat: BarChartComponent;
  @ViewChild("slTheoCS", { static: true })
  slTheoCS: BarChartComponent;


  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_6[1],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
  ];

  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private dmchungService: DmChungService,
    private TTKiemNghiemDPMP: TTKiemNghiemDuocPhamMyPhamService,
    private commonService: CommonFunctionService
  ) {
    this.SL_4_THE_TONG = {
      MAU_LAY:0,
      MAU_GUI:0,
      MAULAY_BC_VIEN:0,
      MAUGUI_BC_VIEN:0
    }

  }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear-1);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }

    this.onFilterChangeLoai();
    this.getDsHuyen();
    this.getListThang();
    this.getListQuy();
    this.getListNam();
    this.detectMode();
  }

  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== "responsive";
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.slLayMau.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.slLayMau.barChartOptions.legend.display = false;
    this.slLayMau.barChartOptions.scales.yAxes[0].display = false;

    this.slLayGui.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.slLayGui.barChartOptions.legend.display = false;
    this.slLayGui.barChartOptions.scales.yAxes[0].display = false;

    this.slMauLayKhongDat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.slMauLayKhongDat.barChartOptions.legend.display = false;
    this.slMauLayKhongDat.barChartOptions.scales.yAxes[0].display = false;

    this.slMauGuiKhongDat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.slMauGuiKhongDat.barChartOptions.legend.display = false;
    this.slMauGuiKhongDat.barChartOptions.scales.yAxes[0].display = false;

    this.slTheoCS.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.slTheoCS.barChartOptions.legend.display = false;
    this.slTheoCS.barChartOptions.scales.yAxes[0].display = false;



  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 2 rows spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }
  getDsHuyen() {
    var obj = {
      keyword: "",
      page: 1,
      size: 20,
      idTinh: ID_TINH,
    };
    this.subscription.push(
      this.dmchungService.getListHuyen(obj).subscribe(
        (rs) => {
          this.listHuyen = rs.data;
          if (this.userStorage?.LEVEL_USER_GIA_TRI == "Admin" || this.userStorage?.MA_DON_VI == 89000) {
            this.isHuyen= false
            this.listHuyen.unshift({ MA_HUYEN: "", TEN_HUYEN: "Tất cả" });
          } else {
            this.formSearch.controls.huyen.setValue(
              this.userStorage?.MA_HUYEN
            );
            this.formSearch.controls.tenhuyen.setValue(
              this.listHuyen.find(element => element.MA_HUYEN == this.userStorage?.MA_HUYEN)?.TEN_HUYEN
            );
            this.formSearch.controls.coSoKCB.setValue(
              [this.userStorage?.MA_DON_VI]
            );
            this.isHuyen= true
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListThang(){
    this.subscription.push(
      this.dmchungService.getListThang().subscribe(
        (rs) => {
          this.listThang = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListQuy(){
    this.subscription.push(
      this.dmchungService.getListQuy().subscribe(
        (rs) => {
          this.listQuy = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListNam(){
    this.subscription.push(
      this.dmchungService.getListNam().subscribe(
        (rs) => {
          this.listNam = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );

  }

  getData(): void {
    this.get4TheTong();
    this.getLauMau();
    this.getMauGui();
    this.getMauLayKhongDat()
    this.getMauGuiKhongDat()
    this.getMauGuiTheoCSKB()
    this.getTheoLoaiCS()
  }

  onFilterChangeLoai(){
    let loai = this.formSearch.get("loai").value

    if(loai == 0){
      this.showNam = true
      this.showQuy = false
      this.showThang = false
    }else if(loai == 1 ){
      this.showNam = true
      this.showQuy = true
      this.showThang = false
    }else{
      this.showNam = true
      this.showQuy = false
      this.showThang = true
    }
    this.getData()
  }
  getFilter() {
    let loai = this.formSearch.get("loai").value;
    let nam = "";
    let thang = "";
    let huyen = "";
    let quy ="";
    if (this.userStorage?.LEVEL_USER_GIA_TRI == "Admin" || this.userStorage?.MA_DON_VI == 89000) {
      huyen = this.formSearch.controls.huyen.value == ""?"":this.formSearch.controls.huyen.value;
    }else{
      huyen = this.userStorage?.MA_HUYEN
    }
    if(loai == 0){
      nam  = this.formSearch.get("year").value;
    }
    else if(loai == 1){
      nam  = this.formSearch.get("year").value;
      quy = this.formSearch.get("quy").value == "Tất cả"?"": this.formSearch.get("quy").value
    }else{
      nam  = this.formSearch.get("year").value;
      thang  = this.formSearch.get("thang").value == "Tất cả"?"": this.formSearch.get("thang").value;
    }
      let result  =  {
        nam: nam,
        thang:thang,
        quy: quy,
        huyen: huyen,
      };
      return result;

  }
  get4TheTong(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.TTKiemNghiemDPMP.get4TheTong(obj).subscribe(
        (rs) => {
          this.SL_4_THE_TONG = {
            MAU_LAY: new Intl.NumberFormat("vi-VN").format(rs.data[0].MAU_LAY),
            MAU_GUI: new Intl.NumberFormat("vi-VN").format(rs.data[0].MAU_GUI),
            MAULAY_BC_VIEN: new Intl.NumberFormat("vi-VN").format(rs.data[0].MAULAY_BC_VIEN),
            MAUGUI_BC_VIEN: new Intl.NumberFormat("vi-VN").format(rs.data[0].MAUGUI_BC_VIEN)
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getLauMau(): void {
    let obj: any = this.getFilter();
    this.isLayMau= true;
    // obj.rowNum = 10;
    this.subscription.push(
      this.TTKiemNghiemDPMP.getMauLay(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          let ten = Object.keys(rs.data[0]);
          let names = {
            HOADUOC_TRONGNUOC:"Hóa dược trong nước",
            HOADUOC_NGOAINHAP: "Hóa dược ngoại nhập",
            VI_THUOC_CO_TRUYEN:"Vị thuốc cổ truyền",
            MY_PHAM:"Mỹ phẩm",
            DUOC_LIEU:"Dược liệu",
            KHAC:"Khác"

          }
          if (Object.values(rs.data[0]).includes(null)){
            this.isLayMau= false;
            this.slLayMau.barChartLabels = []
          }
          else{
            let lableName = ten.map(x => this.commonService.convertStringToArray(" ", names[x], 2));
            this.slLayMau.barChartLabels = lableName
          }
          let soLuong = Object.values(rs.data[0]);

          this.slLayMau.barChartData = [
            {
              data: soLuong,
              backgroundColor: "#741b47",
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getTheoLoaiCS(): void {
    let obj: any = this.getFilter();
    this.isNoneChart= true;
    // obj.rowNum = 10;
    this.subscription.push(
      this.TTKiemNghiemDPMP.getMauGuiTheoCSKB(obj).subscribe(
        (rs: any) => {
          if(rs.data.length == 0){
            this.isNoneChart= false;
          }
          if (!rs.success) {
            return false;
          }
          this.slTheoCS.barChartLabels = rs.data.map(x => this.commonService.convertStringToArray(" ", x.TEN_LOAI_CO_SO, 3));
          let data = rs.data.map((x) => Math.round(x.SOLUONG));

          this.slTheoCS.barChartData = [
            {
              data: data,
              backgroundColor: COLOR_PALETTES.PALETTE_1[2],
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getMauGui(): void {
    let obj: any = this.getFilter();
    this.isLayGui= true;
    this.subscription.push(
      this.TTKiemNghiemDPMP.getMauGui(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          let nameLable = Object.keys(rs.data[0]);
          let labelMap = {
            HOADUOC_TRONGNUOC:"Hóa dược trong nước",
            HOADUOC_NGOAINHAP: "Hóa dược ngoại nhập",
            VI_THUOC_CO_TRUYEN:"Vị thuốc cổ truyền",
            MY_PHAM:"Mỹ phẩm",
            DUOC_LIEU:"Dược liệu",
            KHAC:"Khác"

          }
          if (Object.values(rs.data[0]).includes(null)){
            this.isLayGui= false;
            this.slLayGui.barChartLabels = []
          }
          else{
            let lableName = nameLable.map(x => this.commonService.convertStringToArray(" ", labelMap[x], 1));
            this.slLayGui.barChartLabels = lableName
          }

          let soLuong = Object.values(rs.data[0]);

          this.slLayGui.barChartData = [
            {
              data: soLuong,
              backgroundColor: COLOR_PALETTES.RIVER_NIGHTS[3],
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getMauLayKhongDat(): void {
    let obj: any = this.getFilter();
    this.isMauLayKhongDat= true;
    this.subscription.push(
      this.TTKiemNghiemDPMP.getMauLayKhongDat(obj).subscribe(
        (rs: any) => {

          if (!rs.success) {
            return false;
          }
          let nameLable = Object.keys(rs.data[0]);
          let labelMap = {
            HOADUOC_TRONGNUOC:"Hóa dược trong nước",
            HOADUOC_NGOAINHAP: "Hóa dược ngoại nhập",
            VI_THUOC_CO_TRUYEN:"Vị thuốc cổ truyền",
            MY_PHAM:"Mỹ phẩm",
            DUOC_LIEU:"Dược liệu",
            KHAC:"Khác"

          }
          if (Object.values(rs.data[0]).includes(null)){
            this.isMauLayKhongDat= false;
            this.slMauLayKhongDat.barChartLabels = []
          }
          else{
            let lableName = nameLable.map(x => this.commonService.convertStringToArray(" ", labelMap[x], 1));
            this.slMauLayKhongDat.barChartLabels = lableName
          }
          let soLuong = Object.values(rs.data[0]);
          this.slMauLayKhongDat.barChartData = [
            {
              data: soLuong,
              backgroundColor: "#0b5394",
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getMauGuiKhongDat(): void {
    let obj: any = this.getFilter();
    this.isMauGuiKhongDat= true;
    this.subscription.push(
      this.TTKiemNghiemDPMP.getMauGuiKhongDat(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          let nameLable = Object.keys(rs.data[0]);
          let labelMap = {
            HOADUOC_TRONGNUOC:"Hóa dược trong nước",
            HOADUOC_NGOAINHAP: "Hóa dược ngoại nhập",
            VI_THUOC_CO_TRUYEN:"Vị thuốc cổ truyền",
            MY_PHAM:"Mỹ phẩm",
            DUOC_LIEU:"Dược liệu",
            KHAC:"Khác"

          }
          if (Object.values(rs.data[0]).includes(null)){
            this.isMauGuiKhongDat= false;
            this.slMauGuiKhongDat.barChartLabels = []
          }
          else{
            let lableName = nameLable.map(x => this.commonService.convertStringToArray(" ", labelMap[x], 1));
            this.slMauGuiKhongDat.barChartLabels = lableName
          }

          let soLuong = Object.values(rs.data[0]);

          this.slMauGuiKhongDat.barChartData = [
            {
              data: soLuong,
              backgroundColor: "#b45f06",
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getMauGuiTheoCSKB(): void {
    // let obj: any = this.getFilter();
    // this.isNoneChart= true;
    // this.subscription.push(
    //   this.TTKiemNghiemDPMP.getMauGuiTheoCSKB(obj).subscribe(
    //     (rs: any) => {
    //       if(rs.data.length == 0){
    //         this.isNoneChart= false;
    //       }
    //       if (!rs.success) {
    //         return false;
    //       }
    //       let nameLable = Object.keys(rs.data[0]);
    //       let labelMap = {
    //         HOADUOC_TRONGNUOC:"Hóa dược trong nước",
    //         HOADUOC_NGOAINHAP: "Hóa dược ngoại nhập",
    //         VI_THUOC_CO_TRUYEN:"Vị thuốc cổ truyền",
    //         MY_PHAM:"Mỹ phẩm",
    //         DUOC_LIEU:"Dược liệu",
    //         KHAC:"Khác"

    //       }
    //       let lableName = nameLable.map(x => this.commonService.convertStringToArray(" ", labelMap[x], 1));
    //       let soLuong = Object.values(rs.data[0]);
    //       this.slMauGuiTheoCSKB.barChartLabels = lableName
    //       this.slMauGuiTheoCSKB.barChartData = [
    //         {
    //           data: soLuong,
    //           backgroundColor: COLOR_PALETTES.RIVER_NIGHTS[3],
    //         },
    //       ];
    //     },
    //     (err) => {
    //       this.snackbar.showError(
    //         MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
    //         MESSAGE_TYPE.ERROR
    //       );
    //     }
    //   )
    // );
  }

  getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }
}
