import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent } from "./thong-ke-cac-phong-ban-chuyen-mon-truc-thuoc-so-y-te.componet"

describe("ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent", () => {
  let component: ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent;
  let fixture: ComponentFixture<ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(
      ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent
    );
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
