import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XuLyVanBanTheoDonViComponent } from './xu-ly-van-ban-theo-don-vi.component';

describe('XuLyVanBanTheoDonViComponent', () => {
  let component: XuLyVanBanTheoDonViComponent;
  let fixture: ComponentFixture<XuLyVanBanTheoDonViComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XuLyVanBanTheoDonViComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XuLyVanBanTheoDonViComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
