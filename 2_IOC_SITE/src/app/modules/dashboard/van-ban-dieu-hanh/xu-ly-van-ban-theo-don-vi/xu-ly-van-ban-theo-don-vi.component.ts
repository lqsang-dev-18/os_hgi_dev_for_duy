import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { COLOR_PALETTES, MESSAGE_COMMON, MESSAGE_TYPE } from 'src/app/constant/system-constant';
import { Subscription } from "rxjs";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from 'src/app/shared/components/chart-js/bar-chart/bar-chart.component';
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";

@Component({
  selector: 'app-xu-ly-van-ban-theo-don-vi',
  templateUrl: './xu-ly-van-ban-theo-don-vi.component.html',
  styleUrls: ['./xu-ly-van-ban-theo-don-vi.component.scss']
})
export class XuLyVanBanTheoDonViComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};

  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public DON_VI_LIST: any = [];
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl([""], [Validators.required]),
    madonvi: new FormControl([""]),
  });

  @ViewChild("top5DonViCoVBDiDenNhieuNhat", { static: true }) top5DonViCoVBDiDenNhieuNhat: BarChartComponent;
  @ViewChild("top5DonViCoVBDenNhieuNhat", { static: true }) top5DonViCoVBDenNhieuNhat: BarChartComponent;
  @ViewChild("top5DonViCoVBDiNhieuNhat", { static: true }) top5DonViCoVBDiNhieuNhat: BarChartComponent;
  @ViewChild("top5DonViCoVBDenTreHanNhieuNhat", { static: true }) top5DonViCoVBDenTreHanNhieuNhat: BarChartComponent;
  @ViewChild("top5DonViCoSNTruyCapNhieuNhat", { static: true }) top5DonViCoSNTruyCapNhieuNhat: BarChartComponent;
  @ViewChild("top5DonViCoNhieuVBDenChuaTiepNhan", { static: true }) top5DonViCoNhieuVBDenChuaTiepNhan: BarChartComponent;

  lableDonVi = true;
  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService,
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    // this.formSearch.controls["month"].setValue([1,2,3,4,5,6,7,8,9,10,11,12]);
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }
    this.lableMonth = true;

    this.detectMode();

    this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.legend.display = false;
    this.top5DonViCoVBDenNhieuNhat.barChartOptions.legend.display = false;
    this.top5DonViCoVBDiNhieuNhat.barChartOptions.legend.display = false;
    this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.legend.display = false;
    this.top5DonViCoVBDenTreHanNhieuNhat.barChartType = "horizontalBar";
    this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.legend.display = false;
    this.top5DonViCoSNTruyCapNhieuNhat.barChartType = "horizontalBar";
    this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.legend.display = false;
    this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartType = "horizontalBar";

    this.getData();
    this.getListDonVi();
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.legend.position = "top";
    this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.legend.align = "start";
    this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    this.top5DonViCoVBDenNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top5DonViCoVBDenNhieuNhat.barChartOptions.legend.position = "top";
    this.top5DonViCoVBDenNhieuNhat.barChartOptions.legend.align = "start";
    this.top5DonViCoVBDenNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    this.top5DonViCoVBDenNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    this.top5DonViCoVBDiNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top5DonViCoVBDiNhieuNhat.barChartOptions.legend.position = "top";
    this.top5DonViCoVBDiNhieuNhat.barChartOptions.legend.align = "start";
    this.top5DonViCoVBDiNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    this.top5DonViCoVBDiNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.legend.position = "top";
    this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.legend.align = "start";
    this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.legend.position = "top";
    this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.legend.align = "start";
    this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.legend.position = "top";
    this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.legend.align = "start";
    this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.scales.xAxes[0].stacked = true;
    this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.scales.yAxes[0].stacked = true;
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = (16 // padding top
        + 43.75 + 16 // form height and its margin bottom
        + 16); // 1 row spacing
      let rowHeight = 'calc((100% - ' + others + 'px) / 2)';
      this.rowStyles = { 'height': rowHeight, 'margin-bottom': '1rem' };
    }
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    let madonvi = this.formSearch.controls.madonvi.value;
    let result = { nam: year, thang: month.toString(), madonvi: madonvi == null ? "" : madonvi.toString() };

    return result;
  }

  public getData(): void {
    this.tktop5DonViCoVBDiDenNhieuNhat();
    this.tktop5DonViCoVBDenTreHanNhieuNhat();
    this.tktop5DonViCoSNTruyCapNhieuNhat();
    this.tktop5DonViCoNhieuVBDenChuaTiepNhan();
  }

  public getListDonVi(): void {
    this.formSearch.controls.madonvi.reset();
    this.subscription.push(
      this.vanbandieuhanhService.getListDonVi().subscribe(
        (rs) => {
          this.DON_VI_LIST = rs.data;
          //this.formSearch.controls["madonvi"].setValue(rs.data.map(x => x.MA_DON_VI_CON));
          this.lableDonVi = true;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  // using PALETTE_3[2]
  public tktop5DonViCoVBDiDenNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTop5DonViCoVBDiDenNhieuNhat(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.top5DonViCoVBDiDenNhieuNhat.barChartLabels = rs.data.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_DON_VI), 1));
      let arrTongSoDiDen = rs.data.map(x => x.TONG_SO_DI_DEN);
      this.top5DonViCoVBDiDenNhieuNhat.barChartData = [
        { data: arrTongSoDiDen, label: 'Tổng số VB đi đến', backgroundColor: COLOR_PALETTES.PALETTE_3[1], datalabels: {align: "end", anchor:"end", clamp: true} }
      ];

      this.top5DonViCoVBDenNhieuNhat.barChartLabels = rs.data.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_DON_VI), 1));
      let arrTongSoDen = rs.data.map(x => x.TONG_SO_DEN);
      this.top5DonViCoVBDenNhieuNhat.barChartData = [
        { data: arrTongSoDen, label: 'Tổng số VB đến', backgroundColor: COLOR_PALETTES.PALETTE_2[3], datalabels: {align: "end", anchor:"end", clamp: true} }
      ];
      
      this.top5DonViCoVBDiNhieuNhat.barChartLabels = rs.data.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_DON_VI), 1));
      let arrTongSoDi = rs.data.map(x => x.TONG_SO_DI);
      this.top5DonViCoVBDiNhieuNhat.barChartData = [
        { data: arrTongSoDi, label: 'Tổng số VB đi', backgroundColor: COLOR_PALETTES.PALETTE_4[2], datalabels: {align: "end", anchor:"end", clamp: true} }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using PALETTE_3[2]
  public tktop5DonViCoVBDenTreHanNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTop5DonViCoVBDenTreHanNhieuNhat(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      console.log( rs.data)
      this.top5DonViCoVBDenTreHanNhieuNhat.barChartLabels = rs.data.map(x => this.reduceLabel(x.TEN_DON_VI));
      let arrTongSoTreHan = rs.data.map(x => x.TS_VBDE_TRE_HAN);
      this.top5DonViCoVBDenTreHanNhieuNhat.barChartData = [
        { data: arrTongSoTreHan, label: 'Tổng số VB đến trễ hạn', backgroundColor: COLOR_PALETTES.PALETTE_6[1] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using PALETTE_3[2]
  public tktop5DonViCoSNTruyCapNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTop5DonViCoSoNguoiTruyCapNhieuNhat(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.top5DonViCoSNTruyCapNhieuNhat.barChartLabels = rs.data.map(x => this.reduceLabel(x.TEN_DON_VI));
      let arrSoNguoiTruyCap = rs.data.map(x => x.SO_NGUOI_TRUY_CAP);
      this.top5DonViCoSNTruyCapNhieuNhat.barChartData = [
        { data: arrSoNguoiTruyCap, label: 'Số người truy cập', backgroundColor: COLOR_PALETTES.PALETTE_6[2] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using PALETTE_3[2]
  public tktop5DonViCoNhieuVBDenChuaTiepNhan(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTop5DonViCoNhieuVBDenChuaTiepNhan(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartLabels = rs.data.map(x => this.reduceLabel(x.TEN_DON_VI));
      let arrSoVBDenChuaTiepNhan = rs.data.map(x => x.TONG_CHUAXL);
      this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartData = [
        { data: arrSoVBDenChuaTiepNhan, label: 'Tổng số VB chưa tiếp', backgroundColor: COLOR_PALETTES.PALETTE_3[3] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }

  private reduceLabel(label: string) {
    return label.replace(/trung tâm y tế/gi, "TTYT")
                .replace(/bệnh viện/gi, "BV")
                .replace(/phòng khám/gi, "PK")
                .replace(/trạm y tế/gi, "TYT");
  }

}
