import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XuLyVanBanTheoPhongBanComponent } from './xu-ly-van-ban-theo-phong-ban.component';

describe('XuLyVanBanTheoPhongBanComponent', () => {
  let component: XuLyVanBanTheoPhongBanComponent;
  let fixture: ComponentFixture<XuLyVanBanTheoPhongBanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XuLyVanBanTheoPhongBanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XuLyVanBanTheoPhongBanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
