import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SnackbarService } from "src/app/services/snackbar.service";
import { COLOR_PALETTES, MESSAGE_COMMON, MESSAGE_TYPE } from 'src/app/constant/system-constant';
import { Subscription } from "rxjs";
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";
import { LineChartComponent } from "src/app/shared/components/chart-js/line-chart/line-chart.component";
import { BarChartComponent } from 'src/app/shared/components/chart-js/bar-chart/bar-chart.component';

@Component({
  selector: 'app-xu-ly-van-ban-theo-phong-ban',
  templateUrl: './xu-ly-van-ban-theo-phong-ban.component.html',
  styleUrls: ['./xu-ly-van-ban-theo-phong-ban.component.scss']
})
export class XuLyVanBanTheoPhongBanComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};

  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public PHONG_BAN_LIST: any = [];
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl([""], [Validators.required]),
    maphongban: new FormControl([""]),
  });

  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
  ];
  public sumraryInfo = { TONG_SO_CHUA_DUYET: 0 };

  @ViewChild("tongVBDiDenTheoPhongBanChart", { static: true })
  tongVBDiDenTheoPhongBanChart: LineChartComponent;
  @ViewChild("top10CaNhanNhanVBDenNhieuNhat", { static: true }) top10CaNhanNhanVBDenNhieuNhat: BarChartComponent;
  @ViewChild("top10CaNhanThamMuuVBDiNhieuNhat", { static: true }) top10CaNhanThamMuuVBDiNhieuNhat: BarChartComponent;
  @ViewChild("tkVBDenTreHanTheoPhongBanChart", { static: true })
  tkVBDenTreHanTheoPhongBanChart: BarChartComponent;

  isWithChartCT : any;
  lablePhongBan = true;
  lableMonth = true;

  isFullScreen = false;
  documentElement: any;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService,
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    // this.formSearch.controls["month"].setValue([1,2,3,4,5,6,7,8,9,10,11,12]);
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }
    this.lableMonth = true;

    this.documentElement = document.documentElement;

    this.detectMode();

    this.tongVBDiDenTheoPhongBanChart.lineChartOptions.legend.display = false;
    this.tkVBDenTreHanTheoPhongBanChart.barChartOptions.legend.display = false;
    this.top10CaNhanNhanVBDenNhieuNhat.barChartOptions.legend.display = false;
    this.top10CaNhanThamMuuVBDiNhieuNhat.barChartOptions.legend.display = false;

    this.getData();
    this.getListPhongBan();
  }

  toggleFullScreen() {
    if (!this.isFullScreen) {
      
      this.documentElement.requestFullscreen();
    } else {
      document.exitFullscreen();
    }
    this.isFullScreen = !this.isFullScreen;
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.tongVBDiDenTheoPhongBanChart.lineChartOptions.maintainAspectRatio = !this.isFitPage;

    this.top10CaNhanNhanVBDenNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top10CaNhanNhanVBDenNhieuNhat.barChartOptions.legend.position = "top";
    this.top10CaNhanNhanVBDenNhieuNhat.barChartOptions.legend.align = "start";
    this.top10CaNhanNhanVBDenNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    this.top10CaNhanNhanVBDenNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;

    this.top10CaNhanThamMuuVBDiNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.top10CaNhanThamMuuVBDiNhieuNhat.barChartOptions.legend.position = "top";
    this.top10CaNhanThamMuuVBDiNhieuNhat.barChartOptions.legend.align = "start";
    this.top10CaNhanThamMuuVBDiNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    this.top10CaNhanThamMuuVBDiNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;

    this.tkVBDenTreHanTheoPhongBanChart.barChartOptions.maintainAspectRatio = !this.isFitPage;

    // this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    // this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.legend.position = "top";
    // this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.legend.align = "start";
    // this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    // this.top5DonViCoVBDiDenNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    // this.top5DonViCoVBDenNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    // this.top5DonViCoVBDenNhieuNhat.barChartOptions.legend.position = "top";
    // this.top5DonViCoVBDenNhieuNhat.barChartOptions.legend.align = "start";
    // this.top5DonViCoVBDenNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    // this.top5DonViCoVBDenNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    // this.top5DonViCoVBDiNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    // this.top5DonViCoVBDiNhieuNhat.barChartOptions.legend.position = "top";
    // this.top5DonViCoVBDiNhieuNhat.barChartOptions.legend.align = "start";
    // this.top5DonViCoVBDiNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    // this.top5DonViCoVBDiNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    // this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    // this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.legend.position = "top";
    // this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.legend.align = "start";
    // this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    // this.top5DonViCoVBDenTreHanNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    // this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.maintainAspectRatio = !this.isFitPage;
    // this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.legend.position = "top";
    // this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.legend.align = "start";
    // this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.scales.xAxes[0].stacked = true;
    // this.top5DonViCoSNTruyCapNhieuNhat.barChartOptions.scales.yAxes[0].stacked = true;
    // this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.maintainAspectRatio = !this.isFitPage;
    // this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.legend.position = "top";
    // this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.legend.align = "start";
    // this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.scales.xAxes[0].stacked = true;
    // this.top5DonViCoNhieuVBDenChuaTiepNhan.barChartOptions.scales.yAxes[0].stacked = true;
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = (16 // padding top
        + 43.75 + 16 // form height and its margin bottom
        + 16); // 1 row spacing
      let rowHeight = 'calc((100% - ' + others + 'px) / 2)';
      this.rowStyles = { 'height': rowHeight, 'margin-bottom': '1rem' };
    }
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    let maphongban = this.formSearch.controls.maphongban.value;
    let result = { nam: year, thang: month.toString(), maphongban: maphongban == null ? "" : maphongban.toString() };

    return result;
  }

  public getData(): void {
    this.tkVBDiDenTheoDonVi();
    this.tktop10CaNhanNhanVBDenNhieuNhat();
    this.tktop10CaNhanThamMuuVBDiNhieuNhat();
    this.tkVBDenTreHanTheoPhongBan();
    this.tkTinhHinhPheDuyetVBDen();
    // this.tktop5DonViCoVBDiDenNhieuNhat();
    // this.tktop5DonViCoVBDenTreHanNhieuNhat();
    // this.tktop5DonViCoSNTruyCapNhieuNhat();
    // this.tktop5DonViCoNhieuVBDenChuaTiepNhan();
  }

  public getListPhongBan(): void {
    this.formSearch.controls.maphongban.reset();
    this.subscription.push(
      this.vanbandieuhanhService.getListPhongBan().subscribe(
        (rs) => {
          this.PHONG_BAN_LIST = rs.data;
          //this.formSearch.controls["madonvi"].setValue(rs.data.map(x => x.MA_DON_VI_CON));
          this.lablePhongBan = true;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  tkVBDiDenTheoDonVi() {
    let obj: any = this.getFilter();
    let listData = [];
    let arrVBDen = [];
    let arrVBDi = [];
    let arrDonVi = [];

    this.subscription.push(
      this.vanbandieuhanhService.GetTKVBDiDenTheoPhongBan(obj).subscribe(
        (rs) => {
          listData = rs.data;
          if (listData && listData.length > 0) {
            listData.forEach((e) => {
              arrVBDen.push(e.TONG_SO_DEN == null ? 0 : e.TONG_SO_DEN);
              arrVBDi.push(e.TONG_SO_DI == null ? 0 : e.TONG_SO_DI);
              arrDonVi.push(this.convertToArray(' ', this.reduceLabel(e.TEN_PHONG_BAN), 2));
            });
          }
          // nếu lơn hơn 10 thì show scroll
          if(arrDonVi.length <= 10){
            this.isWithChartCT = "10"
          }else if(arrDonVi.length <= 50){
            this.isWithChartCT = "50"
          } else{
            this.isWithChartCT = "100"
          }
          //Set label for chart
          this.tongVBDiDenTheoPhongBanChart.lineChartLabels = arrDonVi
          //Set data for chart
          this.tongVBDiDenTheoPhongBanChart.lineChartData = [
            {
              data: arrVBDen,
              label: "Tổng số VB đến",
              fill: false,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              backgroundColor: "#118AB2",
            },
            {
              data: arrVBDi,
              label: "Tổng số VB đi",
              fill: false,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              backgroundColor: "#FF9770"
            },
          ];

          this.tongVBDiDenTheoPhongBanChart.lineChartColors = [
            { borderColor: "#118AB2" },
            { borderColor: "#FF9770" },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  tkVBDenTreHanTheoPhongBan() {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKVBDenTreHanTheoPhongBan(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data;
      this.tkVBDenTreHanTheoPhongBanChart.barChartLabels = [];
      this.tkVBDenTreHanTheoPhongBanChart.barChartData = [];
      if (!(result && result.length > 0)) {  
        return;
      }
      result = result.filter(x => x.TS_TREHAN_DXL > 0 || x.TS_TREHAN_CXL > 0 || x.TS_SAPDENHAN > 0);
      this.tkVBDenTreHanTheoPhongBanChart.barChartLabels = result.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_PHONG_BAN), 2));
      this.tkVBDenTreHanTheoPhongBanChart.barChartData.push({
        data: result.map(x => x.TS_TREHAN_DXL),
        label: "Trễ hạn (đã xử lý)",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[2],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.tkVBDenTreHanTheoPhongBanChart.barChartData.push({
        data: result.map(x => x.TS_TREHAN_CXL),
        label: "Trễ hạn (chưa xử lý)",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[3],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.tkVBDenTreHanTheoPhongBanChart.barChartData.push({
        data: result.map(x => x.TS_SAPDENHAN),
        label: "Sắp đến hạn (3 ngày)",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[4],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using PALETTE_3[2]
  public tktop10CaNhanNhanVBDenNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTop10CaNhanNhanVBDenNhieuNhat(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }

      this.top10CaNhanNhanVBDenNhieuNhat.barChartLabels = rs.data.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_NGUOI_NHAN), 1));
      let arrTongSoDen = rs.data.map(x => x.TONG_SO_DEN);
      this.top10CaNhanNhanVBDenNhieuNhat.barChartData = [
        { data: arrTongSoDen, label: 'Tổng số VB đến', backgroundColor: COLOR_PALETTES.PALETTE_2[3], datalabels: {align: "end", anchor:"end", clamp: true} }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using PALETTE_3[2]
  public tktop10CaNhanThamMuuVBDiNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTop10CaNhanThamMuuVBDiNhieuNhat(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      
      this.top10CaNhanThamMuuVBDiNhieuNhat.barChartLabels = rs.data.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_CAN_BO_SOAN), 1));
      let arrTongSoDi = rs.data.map(x => x.TONG_SO_DI);
      this.top10CaNhanThamMuuVBDiNhieuNhat.barChartData = [
        { data: arrTongSoDi, label: 'Tổng số VB đi', backgroundColor: COLOR_PALETTES.PALETTE_4[2], datalabels: {align: "end", anchor:"end", clamp: true} }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  tkTinhHinhPheDuyetVBDen() {
    // let obj: any = this.getFilter();
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    let madonvi = null;
    let obj : any = { nam: year, thang: month.toString(), madonvi: madonvi == null ? "" : madonvi.toString() };
    this.subscription.push(this.vanbandieuhanhService.GetTKTinhHinhPheDuyetVBDen(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data.filter(x => x.TEN_DON_VI_CON == "Sở Y tế - Tỉnh An Giang");
      this.sumraryInfo.TONG_SO_CHUA_DUYET = result[0].VB_CHUA_DUYET;
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }

  private reduceLabel(label: string) {
    return label.replace(/trung tâm y tế/gi, "TTYT")
                .replace(/bệnh viện/gi, "BV")
                .replace(/phòng khám/gi, "PK")
                .replace(/trạm y tế/gi, "TYT");
  }

}
