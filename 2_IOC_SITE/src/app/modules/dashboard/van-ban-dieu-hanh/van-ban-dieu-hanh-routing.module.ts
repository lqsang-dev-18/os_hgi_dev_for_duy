import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ThongTinChungComponent } from "./thong-tin-chung/thong-tin-chung.component";
import { XuLyVanBanTheoThangComponent } from "./xu-ly-van-ban-theo-thang/xu-ly-van-ban-theo-thang.component";
import { XuLyVanBanTheoDonViComponent } from "./xu-ly-van-ban-theo-don-vi/xu-ly-van-ban-theo-don-vi.component";
import { XuLyVanBanTheoPhongBanComponent } from "./xu-ly-van-ban-theo-phong-ban/xu-ly-van-ban-theo-phong-ban.component";
import { ThongTinChungTinhHinhXuLyComponent } from "./thong-tin-chung-tinh-hinh-xu-ly/thong-tin-chung-tinh-hinh-xu-ly.component";
import { HieuSuatXuLyvaDieuHanhComponent } from "./hieu-suat-xu-ly-va-dieu-hanh/hieu-suat-xu-ly-va-dieu-hanh.component";
import { ThongKeCacDonViTrucThuocSoYTeComponent } from "./thong-ke-cac-don-vi-truc-thuoc-so-y-te/thong-ke-cac-don-vi-truc-thuoc-so-y-te.component";
import { ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent } from "./thong-ke-cac-phong-ban-chuyen-mon-truc-thuoc-so-y-te/thong-ke-cac-phong-ban-chuyen-mon-truc-thuoc-so-y-te.componet";
import { QuanLyCongViecComponent } from "./quan-ly-cong-viec/quan-ly-cong-viec.component";

const routes: Routes = [
  { path: "thong-tin-chung", component: ThongTinChungComponent },
  { path: "xu-ly-van-ban-theo-thang", component: XuLyVanBanTheoThangComponent },
  {
    path: "xu-ly-van-ban-theo-don-vi",
    component: XuLyVanBanTheoDonViComponent,
  },
  {
    path: "xu-ly-van-ban-theo-phong-ban",
    component: XuLyVanBanTheoPhongBanComponent,
  },
  {
    path: "thong-tin-chung-tinh-hinh-xu-ly",
    component: ThongTinChungTinhHinhXuLyComponent,
  },
  {
    path: "hieu-suat-xu-ly-va-dieu-hanh",
    component: HieuSuatXuLyvaDieuHanhComponent,
  },
  {
    path: "thongke-dvtt",
    component: ThongKeCacDonViTrucThuocSoYTeComponent,
  },
  {
    path: "thongke-pbcm",
    component: ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent,
  },
  {
    path: "quan-ly-cong-viec",
    component: QuanLyCongViecComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VanBanDieuHanhRoutingModule {}
