import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { PieChartComponent } from "src/app/shared/components/chart-js/pie-chart/pie-chart.component";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";

@Component({
  selector: "app-thong-tin-chung-tinh-hinh-xu-ly",
  templateUrl: "./thong-tin-chung-tinh-hinh-xu-ly.component.html",
  styleUrls: ["./thong-tin-chung-tinh-hinh-xu-ly.component.scss"],
})
export class ThongTinChungTinhHinhXuLyComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};
  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  THE: any = {};
  THE1: any = {};
  THE2: any = {};
  listThang = [];
  listNam = [];
  isWithChart1: any;
  listCSKCB = [];
  public isFullScreen = false;
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl("", [Validators.required]),
  });
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
    //10
    "#b08646",
    "#b54635",
    "#34bdcd",
  ];

  // Pie chart
  @ViewChild("tyLeVbDenDungVaQuaHan", { static: true })
  tyLeVbDenDungVaQuaHan: PieChartComponent;
  @ViewChild("tyLeVbLienThong", { static: true })
  tyLeVbLienThong: PieChartComponent;

  // Bar chart
  @ViewChild("TkVanBanDenTheoThang", { static: true })
  TkVanBanDenTheoThang: BarChartComponent;
  @ViewChild("TkVanBanDenQuaHanTheoThang", { static: true })
  TkVanBanDenQuaHanTheoThang: BarChartComponent;
  @ViewChild("TkVanBanLienThongTheoThang", { static: true })
  TkVanBanLienThongTheoThang: BarChartComponent;
  @ViewChild("TkSoLuongTruyCapHeThongTheoThang", { static: true })
  TkSoLuongTruyCapHeThongTheoThang: BarChartComponent;

  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService,
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    this.lableMonth = true;
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }

    this.isFullScreen = false;
    this.detectMode();
    this.getData();
  }

  ngOnDestroy() {
    this.subscription.forEach((subscription) => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    // this.buildStyles();

    this.TkVanBanDenTheoThang.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanDenTheoThang.barChartOptions.legend.display =
      false;
    this.TkVanBanDenTheoThang.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkVanBanDenTheoThang.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanDenTheoThang.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

    this.TkVanBanDenQuaHanTheoThang.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanDenQuaHanTheoThang.barChartOptions.legend.display =
      false;
    this.TkVanBanDenQuaHanTheoThang.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkVanBanDenQuaHanTheoThang.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanDenQuaHanTheoThang.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

    this.TkVanBanLienThongTheoThang.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanLienThongTheoThang.barChartOptions.legend.display =
      false;
    this.TkVanBanLienThongTheoThang.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkVanBanLienThongTheoThang.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanLienThongTheoThang.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

    this.TkSoLuongTruyCapHeThongTheoThang.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkSoLuongTruyCapHeThongTheoThang.barChartOptions.legend.display =
      false;
    this.TkSoLuongTruyCapHeThongTheoThang.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkSoLuongTruyCapHeThongTheoThang.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkSoLuongTruyCapHeThongTheoThang.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;



  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 1 row spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }

  showFullScreen() {
    // Kiểm tra trạng thái fullscreen
    if (!this.isFullScreen) {
      // Nếu không ở chế độ fullscreen, yêu cầu chuyển sang fullscreen
      document.documentElement.requestFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = true;
    } else {
      // Nếu đang ở chế độ fullscreen, yêu cầu thoát fullscreen
      document.exitFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = false;
    }
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    let unit_type = "all";
    return { nam: year, thang: month.toString(), unit_type };
  }

  public getData(): void {
    this.getCacChiSoVbDen();
    this.getTyLeVbDen();
    this.getCacChiSoVbDi();
    this.getTyLeVbLienThong();
    this.getTKVbDenTheoThang();
    this.getTKVbDenQuaHanTheoThang();
    this.getTKVbLienThongTheoThang();
    this.getTkSoLuongTruyCapHeThongTheoThang();
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = "";
    let result = [];
    array.forEach((element) => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = "";
      }
    });
    if (temp !== "") {
      result.push(temp);
    }
    return result;
  }

  // -----------------------------------------------------------------------------------------------------------------------------

  getCacChiSoVbDen(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVBDen(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }
          this.THE = {
            TOTAL: this.getNumber(res.data[0].TOTAL),
            CXLTRONGHAN: this.getNumber(res.data[0].CXLTRONGHAN),
            CXLQUAHAN: this.getNumber(res.data[0].CXLQUAHAN),
            DXLTRONGHAN: this.getNumber(res.data[0].DXLTRONGHAN),
            DXLQUAHAN: this.getNumber(res.data[0].DXLQUAHAN),
            HTTRONGHAN: this.getNumber(res.data[0].HTTRONGHAN),
            HTQUAHAN: this.getNumber(res.data[0].HTQUAHAN),
            DUNGHAN: this.getNumber(res.data[0].DUNGHAN),
            QUAHAN: this.getNumber(res.data[0].QUAHAN),
            // TYLEDUNGHAN: this.getNumber(res.data[0].TYLEDUNGHAN),
            // TYLEQUAHAN: this.getNumber(res.data[0].TYLEQUAHAN),
          };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTyLeVbDen(): void {
    let obj = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVBDen(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return false;
          }

          let data = res.data;

          if (data.length == 0) {
            this.tyLeVbDenDungVaQuaHan.pieChartData = [];
            return false;
          }

          let tyLeVbDenDungHan = data[0].TYLEDUNGHAN;
          let tyLeVbDenQuaHan = data[0].TYLEQUAHAN;

          this.tyLeVbDenDungVaQuaHan.pieChartData = [
            tyLeVbDenDungHan,
            tyLeVbDenQuaHan,
          ];

          this.tyLeVbDenDungVaQuaHan.pieChartLabels = ["Đúng hạn", "Quá hạn"];

          this.tyLeVbDenDungVaQuaHan.pieChartColor = [
            { backgroundColor: COLOR_PALETTES.PALETTE_8 },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getCacChiSoVbDi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVBDi(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }
          this.THE1 = {
            VBDI: this.getNumber(res.data[0].VBDI),
            VBLIENTHONG: this.getNumber(res.data[0].VBLIENTHONG),
          };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTyLeVbLienThong(): void {
    let obj = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVBDi(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return false;
          }

          let data = res.data;

          if (data.length == 0) {
            this.tyLeVbLienThong.pieChartData = [];
            return false;
          }

          this.THE2 = {
            TYLEVBLIENTHONG: data[0].TYLEVBLIENTHONG
          }

          let tyLeVbLienThong = data[0].TYLEVBLIENTHONG;
          let tyLeVbNoiBo = data[0].TYLEVBNOIBO;

          this.tyLeVbLienThong.pieChartData = [tyLeVbLienThong, tyLeVbNoiBo];

          this.tyLeVbLienThong.pieChartLabels = ["Văn Bản Liên Thông", "Văn bản nội bộ"];

          this.tyLeVbLienThong.pieChartColor = [
            {
              backgroundColor: ["#007bff", "#ff7f00"],
              // borderColor: ["#46baec", "#fff"],
            },
          ];

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbDenTheoThang(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTKVBDenvaDenQuaHanTheoThang(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.TkVanBanDenTheoThang.barChartLabels = rs.data.map(
            (x) => x.THANG
          );

          // rs.data.sort((a, b) => b.TOTAL - a.TOTAL);

          let arrTotal = rs.data.map((x) => Math.round(x.TOTAL));

          this.TkVanBanDenTheoThang.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#F5AB00",
            },
          ];

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbDenQuaHanTheoThang(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTKVBDenvaDenQuaHanTheoThang(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.TkVanBanDenQuaHanTheoThang.barChartLabels = rs.data.map(
            (x) => x.THANG
          );

          // rs.data.sort((a, b) => b.QUAHAN - a.QUAHAN);

          let arrTotal = rs.data.map((x) => Math.round(x.QUAHAN));

          this.TkVanBanDenQuaHanTheoThang.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#FF3300",
            },
          ];

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbLienThongTheoThang(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService
        .GetCacChiSoVBDiLienThongTheoThang(obj)
        .subscribe(
          (rs: any) => {
            if (!rs.success) {
              return false;
            }

            this.TkVanBanLienThongTheoThang.barChartLabels = rs.data.map(
              (x) => x.THANG
            );

            // rs.data.sort((a, b) => b.VBLIENTHONG - a.VBLIENTHONG);

            let arrTotal = rs.data.map((x) => Math.round(x.VBLIENTHONG));

            this.TkVanBanLienThongTheoThang.barChartData = [
              {
                data: arrTotal,
                backgroundColor: "#33CCFF",
              },
            ];


          },
          (err) => {
            this.snackbar.showError(
              MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
              MESSAGE_TYPE.ERROR
            );
          }
        )
    );
  }

  getTkSoLuongTruyCapHeThongTheoThang(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTKSoLuongTruyCapTheoThang(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.TkSoLuongTruyCapHeThongTheoThang.barChartLabels = rs.data.map(
            (x) => x.THANG
          );

          // rs.data.sort((a, b) => b.TOTALACCESS - a.TOTALACCESS);

          let arrTotal = rs.data.map((x) => Math.round(x.TOTALACCESS));

          this.TkSoLuongTruyCapHeThongTheoThang.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#669900",
            },
          ];

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  onFilterChangeCSKB() {
    this.detectMode;
    this.getData();
  }

  getDynamicHeight(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "min-height: 100%; height: " + length * percent + "%";
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "min-height: 100%; min-width:100%; width: " + length * percent + "%";

  }

  getDynamicWidthAndHeight(obj: any, percent: number, percent1: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "min-height: 100%; min-width:100%; width: " + length * percent + "%; height: " + length * percent1 + "%";
  }
}
