import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThongTinChungTinhHinhXuLyComponent } from './thong-tin-chung-tinh-hinh-xu-ly.component';

describe('ThongTinChungTinhHinhXuLyComponent', () => {
  let component: ThongTinChungTinhHinhXuLyComponent;
  let fixture: ComponentFixture<ThongTinChungTinhHinhXuLyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThongTinChungTinhHinhXuLyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongTinChungTinhHinhXuLyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
