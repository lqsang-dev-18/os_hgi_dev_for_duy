import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { PieChartComponent } from "src/app/shared/components/chart-js/pie-chart/pie-chart.component";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";
import { ColorService } from "src/app/services/color.service";

@Component({
  selector: "app-quan-ly-cong-viec",
  templateUrl: "./quan-ly-cong-viec.component.html",
  styleUrls: ["./quan-ly-cong-viec.component.scss"],
})
export class QuanLyCongViecComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};
  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public isFullScreen = false;
  THE: any = {};
  listThang = [];
  listNam = [];
  isWithChart1: any;
  listCSKCB = [];
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl("", [Validators.required]),
    donVi: new FormControl("", [Validators.nullValidator]), // null or undefined
  });
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
    COLOR_PALETTES.PALETTE_1[3],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.GREY_TO_RED[7],
    COLOR_PALETTES.ORANGE_TO_PURPLE[7],
  ];

  // Pie chart
  @ViewChild("tyLeCongViecHTDungVaQuaHan", { static: true })
  tyLeCongViecHTDungVaQuaHan: PieChartComponent;

  // Bar chart
  @ViewChild("TkSoLuongCongViec", { static: true })
  TkSoLuongCongViec: BarChartComponent;
  @ViewChild("TkSoLuongCongViecHTTrongHan", { static: true })
  TkSoLuongCongViecHTTrongHan: BarChartComponent;
  @ViewChild("TkSoLuongCongViecHTQuaHan", { static: true })
  TkSoLuongCongViecHTQuaHan: BarChartComponent;

  // Labels for the pie chart
  PieChartLabels = ["Đã hoàn thành - đúng hạn"]; // Example labels

  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService,
    private color: ColorService
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    this.lableMonth = true;
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }
    this.detectMode();
    this.getData();
  }
  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    // this.buildStyles();
    this.isFullScreen = false;

    // Biểu đồ tỷ lệ công việc hoàn thành đúng và quá hạn 
    this.tyLeCongViecHTDungVaQuaHan.pieChartOptions.legend.position = "right";
    this.tyLeCongViecHTDungVaQuaHan.pieChartOptions.layout = { padding: { left: 100, top: 50, bottom: 15 } };

    // thống kê số lượng công việc
    this.TkSoLuongCongViec.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.TkSoLuongCongViec.barChartOptions.legend.display = false;
    this.TkSoLuongCongViec.barChartOptions.scales.yAxes[0].gridLines.display = true;
    this.TkSoLuongCongViec.barChartOptions.scales.yAxes[0].gridLines.color = "#fff";
    this.TkSoLuongCongViec.barChartOptions.scales.yAxes[0].gridLines.drawBorder = false;

    //thống kê số lượng công việc hoàn thành đúng hạn
    this.TkSoLuongCongViecHTTrongHan.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.TkSoLuongCongViecHTTrongHan.barChartOptions.legend.display = false;
    this.TkSoLuongCongViecHTTrongHan.barChartOptions.scales.yAxes[0].gridLines.display = true;
    this.TkSoLuongCongViecHTTrongHan.barChartOptions.scales.yAxes[0].gridLines.color = "#fff";
    this.TkSoLuongCongViecHTTrongHan.barChartOptions.scales.yAxes[0].gridLines.drawBorder = false;

    //thống kê số lượng công việc hoàn thành quá hạn
    this.TkSoLuongCongViecHTQuaHan.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.TkSoLuongCongViecHTQuaHan.barChartOptions.legend.display = false;
    this.TkSoLuongCongViecHTQuaHan.barChartOptions.scales.yAxes[0].gridLines.display = true;
    this.TkSoLuongCongViecHTQuaHan.barChartOptions.scales.yAxes[0].gridLines.color = "#fff";
    this.TkSoLuongCongViecHTQuaHan.barChartOptions.scales.yAxes[0].gridLines.drawBorder = false;
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 1 row spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }

  showFullScreen() {
    // Kiểm tra trạng thái fullscreen
    if (!this.isFullScreen) {
      // Nếu không ở chế độ fullscreen, yêu cầu chuyển sang fullscreen
      document.documentElement.requestFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = true;
    } else {
      // Nếu đang ở chế độ fullscreen, yêu cầu thoát fullscreen
      document.exitFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = false;
    }
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    return { nam: year, thang: month.toString() };
  }

  public getData(): void {
    this.getCacChiSoCongViec();
    this.getCacTkCongViecTheoDonVi();
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = "";
    let result = [];
    array.forEach((element) => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = "";
      }
    });
    if (temp !== "") {
      result.push(temp);
    }
    return result;
  }

  // -----------------------------------------------------------------------------------------------------------------------------

  getCacChiSoCongViec(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoCongViec(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }

          this.THE = {
            TOTAL: this.getNumber(res.data[0].TOTAL),
            CXLTRONGHAN: this.getNumber(res.data[0].CXLTRONGHAN),
            CXLQUAHAN: this.getNumber(res.data[0].CXLQUAHAN),
            HTTRONGHAN: this.getNumber(res.data[0].HTTRONGHAN),
            HTQUAHAN: this.getNumber(res.data[0].HTQUAHAN),
          };

          // Biểu đồ tỷ lệ công việc hoàn thành đúng và quá hạn 
          let data = res.data;

          let tyLeCongViecHTDungHan = data[0].TYLEHTTRONGHAN;
          let tyLeCongViecHTQuaHan = data[0].TYLEHTQUAHAN;

          this.tyLeCongViecHTDungVaQuaHan.pieChartData = [
            tyLeCongViecHTDungHan,
            tyLeCongViecHTQuaHan,
          ];

          this.tyLeCongViecHTDungVaQuaHan.pieChartLabels = ["Đúng hạn", "Quá hạn"];

          this.tyLeCongViecHTDungVaQuaHan.pieChartColor = [
            { backgroundColor: ["#02b8ab", "#EF5958"] },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getCacTkCongViecTheoDonVi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoCongViecHoanThanhTheoDonVi(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }

          // thống kê số lượng công việc
          this.TkSoLuongCongViec.barChartLabels = res.data.map(
            (x) => x.NAME
          );

          res.data.sort((a, b) => b.TOTAL - a.TOTAL);

          let arrTotal = res.data.map((x) => Math.round(x.TOTAL));

          this.TkSoLuongCongViec.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#F5AB00",
            },
          ];

          //thống kê số lượng công việc hoàn thành đúng hạn
          this.TkSoLuongCongViecHTTrongHan.barChartLabels = res.data.map(
            (x) => x.NAME
          );

          res.data.sort((a, b) => b.HTTRONGHAN - a.HTTRONGHAN);

          let arrHTTRONGHAN = res.data.map((x) => Math.round(x.HTTRONGHAN));

          this.TkSoLuongCongViecHTTrongHan.barChartData = [
            {
              data: arrHTTRONGHAN,
              backgroundColor: "#02b8ab",
            }
          ]

          //thống kê số lượng công việc hoàn thành quá hạn
          this.TkSoLuongCongViecHTQuaHan.barChartLabels = res.data.map(
            (x) => x.NAME
          );

          res.data.sort((a, b) => b.HTQUAHAN - a.HTQUAHAN);

          let arrHTQuaHan = res.data.map((x) => Math.round(x.HTQUAHAN));

          this.TkSoLuongCongViecHTQuaHan.barChartData = [
            {
              data: arrHTQuaHan,
              backgroundColor: "#EF5958"
            }
          ]


        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  };

  // -----------------------------------------------------------------------------------------------------------------------------

  onFilterChangeCSKB() {
    this.detectMode;
    this.getData();
  }

  //  getDynamicHeight(obj: any, percent: number) {
  //   let array: any[] = obj ? obj.data : [];
  //   let length = array ? array.length : 1;
  //   return "min-height: 100%; height: " + length * percent + "%";
  // }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "min-height: 100%; min-width:100%; width: " + length * percent + "%";
  }
}

