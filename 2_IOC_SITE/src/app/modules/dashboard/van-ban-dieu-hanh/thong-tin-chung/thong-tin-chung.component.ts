import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { COLOR_PALETTES, MESSAGE_COMMON, MESSAGE_TYPE } from 'src/app/constant/system-constant';
import { Subscription } from "rxjs";
import { SnackbarService } from "src/app/services/snackbar.service";
import { LineChartComponent } from "src/app/shared/components/chart-js/line-chart/line-chart.component";
import { BarChartComponent } from 'src/app/shared/components/chart-js/bar-chart/bar-chart.component';
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";

@Component({
  selector: 'app-thong-tin-chung',
  templateUrl: './thong-tin-chung.component.html',
  styleUrls: ['./thong-tin-chung.component.scss']
})
export class ThongTinChungComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};

  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public DON_VI_LIST: any = [];
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl("", [Validators.required]),
    madonvi: new FormControl([""]),
  });
  
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
  ];
  public sumraryInfo = { TONG_SO_DON_VI: 0, TONG_SO_DI: 0, TONG_SO_DEN: 0, TONG_SO_DI_DEN: 0 };

  @ViewChild("tongVBDiDenTheoDonViChart", { static: true })
  tongVBDiDenTheoDonViChart: LineChartComponent;
  @ViewChild("tkVBDiDenKySoChart", { static: true })
  tkVBDiDenKySoChart: LineChartComponent;
  @ViewChild("tkVBDenTreHanChart", { static: true })
  tkVBDenTreHanChart: BarChartComponent;
  @ViewChild("tkTinhHinhPheDuyetVBDenChart", { static: true })
  tkTinhHinhPheDuyetVBDenChart: BarChartComponent;

  isWithChartCT : any;
  lableDonVi = true;
  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    //this.formSearch.controls["month"].setValue([1,2,3,4,5,6,7,8,9,10,11,12]);
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }
    this.lableMonth = true;

    this.tongVBDiDenTheoDonViChart.lineChartOptions.legend.display = false;
    this.tkVBDiDenKySoChart.lineChartOptions.legend.display = false;
    this.tkVBDenTreHanChart.barChartOptions.legend.display = false;
    this.tkTinhHinhPheDuyetVBDenChart.barChartOptions.legend.display = false;

    this.detectMode();

    this.getData();
    this.getListDonVi();
  }

  ngOnDestroy() {
    this.subscription.forEach((subscription) => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.tongVBDiDenTheoDonViChart.lineChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tkVBDiDenKySoChart.lineChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tkVBDenTreHanChart.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tkTinhHinhPheDuyetVBDenChart.barChartOptions.maintainAspectRatio = !this.isFitPage;
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = (16 // padding top
        + 43.75 + 16 // form height and its margin bottom
        + 16); // 1 row spacing
      let rowHeight = 'calc((100% - ' + others + 'px) / 2)';
      this.rowStyles = { 'height': rowHeight, 'margin-bottom': '1rem' };
    }
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    let madonvi = this.formSearch.controls.madonvi.value;
    let result = { nam: year, thang: month.toString(), madonvi: madonvi == null ? "" : madonvi.toString() };

    return result;
  }

  public getData(): void {
    this.thongKe2ChiTieu();
    this.tkVBDiDenTheoDonVi();
    this.tkVBDiDenKySo();
    this.tkVBDenTreHan();
    this.tkTinhHinhPheDuyetVBDen();
  }

  getListDonVi() {
    //this.formSearch.controls.madonvi.reset();
    this.subscription.push(
      this.vanbandieuhanhService.getListDonVi().subscribe(
        (rs) => {
          this.DON_VI_LIST = rs.data;
          //this.formSearch.controls["madonvi"].setValue(rs.data.map(x => x.MA_DON_VI_CON));
          this.lableDonVi = true;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  public thongKe2ChiTieu(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.thongKe2ChiTieu(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;
      }
      this.sumraryInfo = rs.data[0];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  tkVBDiDenTheoDonVi() {
    let obj: any = this.getFilter();
    let listData = [];
    let arrVBDen = [];
    let arrVBDi = [];
    let arrDonVi = [];

    this.subscription.push(
      this.vanbandieuhanhService.GetTKVBDiDenTheoDonVi(obj).subscribe(
        (rs) => {
          listData = rs.data;
          if (listData && listData.length > 0) {
            listData.forEach((e) => {
              arrVBDen.push(e.TONG_SO_DEN == null ? 0 : e.TONG_SO_DEN);
              arrVBDi.push(e.TONG_SO_DI == null ? 0 : e.TONG_SO_DI);
              arrDonVi.push(this.convertToArray(' ', this.reduceLabel(e.TEN_DON_VI), 2));
            });
          }
          // nếu lơn hơn 10 thì show scroll
          if(arrDonVi.length <= 10){
            this.isWithChartCT = "10"
          }else if(arrDonVi.length <= 50){
            this.isWithChartCT = "50"
          } else{
            this.isWithChartCT = "100"
          }
          //Set label for chart
          this.tongVBDiDenTheoDonViChart.lineChartLabels = arrDonVi
          //Set data for chart
          this.tongVBDiDenTheoDonViChart.lineChartData = [
            {
              data: arrVBDen,
              label: "Tổng số VB đến",
              fill: false,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              backgroundColor: "#118AB2",
            },
            {
              data: arrVBDi,
              label: "Tổng số VB đi",
              fill: false,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              backgroundColor: "#FF9770"
            },
          ];

          this.tongVBDiDenTheoDonViChart.lineChartColors = [
            { borderColor: "#118AB2" },
            { borderColor: "#FF9770" },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  tkVBDiDenKySo() {
    let obj: any = this.getFilter();
    let listData = [];
    let arrVBDenKySo = [];
    let arrVBDiKySo = [];
    let arrDonVi = [];

    this.subscription.push(
      this.vanbandieuhanhService.GetTKVBDiDenKySo(obj).subscribe(
        (rs) => {
          listData = rs.data;
          if (listData && listData.length > 0) {
            listData.forEach((e) => {
              arrVBDenKySo.push(e.TONG_SO_DEN_KS == null ? 0 : e.TONG_SO_DEN_KS);
              arrVBDiKySo.push(e.TONG_SO_DI_KS == null ? 0 : e.TONG_SO_DI_KS);
              arrDonVi.push(this.convertToArray(' ', this.reduceLabel(e.TEN_DON_VI), 2));
            });
          }
          // nếu lơn hơn 10 thì show scroll
          if(arrDonVi.length <= 10){
            this.isWithChartCT = "10"
          }else if(arrDonVi.length <= 50){
            this.isWithChartCT = "50"
          } else{
            this.isWithChartCT = "100"
          }
          //Set label for chart
          this.tkVBDiDenKySoChart.lineChartLabels = arrDonVi
          //Set data for chart
          this.tkVBDiDenKySoChart.lineChartData = [
            {
              data: arrVBDenKySo,
              label: "Tổng số VB đến ký số",
              fill: false,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              backgroundColor: "#1982C4",
              
            },
            {
              data: arrVBDiKySo,
              label: "Tổng số VB đi ký số",
              fill: false,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              backgroundColor: "#6A4C93"
            },
          ];

          this.tkVBDiDenKySoChart.lineChartColors = [
            { borderColor: "#1982C4" },
            { borderColor: "#6A4C93" },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  tkVBDenTreHan() {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKVBDenTreHan(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data;
      this.tkVBDenTreHanChart.barChartLabels = [];
      this.tkVBDenTreHanChart.barChartData = [];
      if (!(result && result.length > 0)) {  
        return;
      }
      result = result.filter(x => x.TS_VBDE_DA_XU_LY_TRE_HAN > 0 || x.TS_VBDE_CHUA_XU_LY_TRE_HAN > 0 || x.TS_VBDE_CHUA_XU_LY_SAP_DEN_HAN > 0);
      this.tkVBDenTreHanChart.barChartLabels = result.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_DON_VI), 2));
      this.tkVBDenTreHanChart.barChartData.push({
        data: result.map(x => x.TS_VBDE_DA_XU_LY_TRE_HAN),
        label: "Trễ hạn (đã xử lý)",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[2],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.tkVBDenTreHanChart.barChartData.push({
        data: result.map(x => x.TS_VBDE_CHUA_XU_LY_TRE_HAN),
        label: "Trễ hạn (chưa xử lý)",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[3],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.tkVBDenTreHanChart.barChartData.push({
        data: result.map(x => x.TS_VBDE_CHUA_XU_LY_SAP_DEN_HAN),
        label: "Sắp đến hạn (3 ngày)",
        backgroundColor: COLOR_PALETTES.DUTCH_FIELD[4],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  tkTinhHinhPheDuyetVBDen() {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTinhHinhPheDuyetVBDen(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      let result = rs.data;
      this.tkTinhHinhPheDuyetVBDenChart.barChartLabels = [];
      this.tkTinhHinhPheDuyetVBDenChart.barChartData = [];
      if (!(result && result.length > 0)) {  
        return;
      }
      result = result.filter(x => x.VB_CHUA_DUYET > 0 || x.VB_DA_DUYET > 0);
      this.tkTinhHinhPheDuyetVBDenChart.barChartLabels = result.map(x => this.convertToArray(' ', this.reduceLabel(x.TEN_DON_VI_CON), 2));
      this.tkTinhHinhPheDuyetVBDenChart.barChartData.push({
        data: result.map(x => x.VB_DA_DUYET),
        label: "Văn bản đã duyệt",
        backgroundColor: COLOR_PALETTES.RETRO_METRO[1],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
      this.tkTinhHinhPheDuyetVBDenChart.barChartData.push({
        data: result.map(x => x.VB_CHUA_DUYET),
        label: "Văn bản chưa duyệt",
        backgroundColor: COLOR_PALETTES.RETRO_METRO[4],
        datalabels: {align: "end", anchor:"end", clamp: true}
      });
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }

  private reduceLabel(label: string) {
    return label.replace(/trung tâm y tế/gi, "TTYT")
                .replace(/bệnh viện/gi, "BV")
                .replace(/phòng khám/gi, "PK")
                .replace(/trạm y tế/gi, "TYT");
  }

}
