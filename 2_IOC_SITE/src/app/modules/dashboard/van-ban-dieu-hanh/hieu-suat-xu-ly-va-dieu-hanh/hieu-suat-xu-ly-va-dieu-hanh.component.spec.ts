import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { HieuSuatXuLyvaDieuHanhComponent } from "./hieu-suat-xu-ly-va-dieu-hanh.component";

describe("HieuSuatXuLyvaDieuHanhComponent", () => {
  let component: HieuSuatXuLyvaDieuHanhComponent;
  let fixture: ComponentFixture<HieuSuatXuLyvaDieuHanhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HieuSuatXuLyvaDieuHanhComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HieuSuatXuLyvaDieuHanhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
