import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";

@Component({
  selector: "app-hieu-suat-xu-ly-va-dieu-hanh",
  templateUrl: "./hieu-suat-xu-ly-va-dieu-hanh.component.html",
  styleUrls: ["./hieu-suat-xu-ly-va-dieu-hanh.component.scss"],
})
export class HieuSuatXuLyvaDieuHanhComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};
  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public isFullScreen = false;
  THE: any = {};
  THE1: any = {};
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl("", [Validators.required]),
    donVi: new FormControl("", [Validators.nullValidator]), // null or undefined
  });
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
    COLOR_PALETTES.PALETTE_1[3],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.GREY_TO_RED[7],
    COLOR_PALETTES.ORANGE_TO_PURPLE[7],
    COLOR_PALETTES.ORANGE_TO_PURPLE[0],
    COLOR_PALETTES.IOC[8],
    COLOR_PALETTES.PALETTE_5[0],
    COLOR_PALETTES.IOC[3],
  ];

  // Bar chart
  @ViewChild("Top5DonViCoVbDenNhieuNhat", { static: true })
  Top5DonViCoVbDenNhieuNhat: BarChartComponent;
  @ViewChild("Top5DonViCoVbdenDungHanNhieuNhat", { static: true })
  Top5DonViCoVbdenDungHanNhieuNhat: BarChartComponent;
  @ViewChild("Top5DonViCoVbdenQuaHanNhieuNhat", { static: true })
  Top5DonViCoVbdenQuaHanNhieuNhat: BarChartComponent;
  @ViewChild("Top5TruyCapNhieuNhat", { static: true })
  Top5TruyCapNhieuNhat: BarChartComponent;
  @ViewChild("Top5DonViCoVbDiNhieuNhat", { static: true })
  Top5DonViCoVbDiNhieuNhat: BarChartComponent;
  @ViewChild("Top5DonViCoVbLienThongNhieuNhat", { static: true })
  Top5DonViCoVbLienThongNhieuNhat: BarChartComponent;


  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService
  ) {}

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }

    this.lableMonth = true;
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }

    this.isFullScreen = false;
    this.detectMode();
    this.getData();
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.Top5DonViCoVbDenNhieuNhat.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.Top5DonViCoVbDenNhieuNhat.barChartOptions.legend.display =
      false;
    this.Top5DonViCoVbDenNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.Top5DonViCoVbDenNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.Top5DonViCoVbDenNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

      this.Top5DonViCoVbdenDungHanNhieuNhat.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.Top5DonViCoVbdenDungHanNhieuNhat.barChartOptions.legend.display =
      false;
    this.Top5DonViCoVbdenDungHanNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.Top5DonViCoVbdenDungHanNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.Top5DonViCoVbdenDungHanNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

      this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartOptions.legend.display =
      false;
    this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

       this.Top5TruyCapNhieuNhat.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.Top5TruyCapNhieuNhat.barChartOptions.legend.display =
      false;
    this.Top5TruyCapNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.Top5TruyCapNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.Top5TruyCapNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

      this.Top5DonViCoVbDiNhieuNhat.barChartType = "horizontalBar";
    this.Top5DonViCoVbDiNhieuNhat.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.Top5DonViCoVbDiNhieuNhat.barChartOptions.legend.display = false;
    this.Top5DonViCoVbDiNhieuNhat.barChartOptions.scales.xAxes[0].gridLines.display =
      true;
    this.Top5DonViCoVbDiNhieuNhat.barChartOptions.scales.xAxes[0].gridLines.color =
      "#fff";
    this.Top5DonViCoVbDiNhieuNhat.barChartOptions.scales.xAxes[0].gridLines.drawBorder =
      false;

       this.Top5DonViCoVbLienThongNhieuNhat.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.Top5DonViCoVbLienThongNhieuNhat.barChartOptions.legend.display =
      false;
    this.Top5DonViCoVbLienThongNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.Top5DonViCoVbLienThongNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.Top5DonViCoVbLienThongNhieuNhat.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 1 row spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }

  showFullScreen() {
    // Kiểm tra trạng thái fullscreen
    if (!this.isFullScreen) {
      // Nếu không ở chế độ fullscreen, yêu cầu chuyển sang fullscreen
      document.documentElement.requestFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = true;
    } else {
      // Nếu đang ở chế độ fullscreen, yêu cầu thoát fullscreen
      document.exitFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = false;
    }
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    return { nam: year, thang: month.toString() };
  }

  public getData(): void {
    this.GetTop5DonViCoVbDenNhieuNhat();
    this.GetTop5DonViCoNguoiDungThuongXuyenTruyCap();
    this.GetTrungBinhSoLuongVbDen();
    this.GetTop5DonViCoTyLeXlVbDungHan();
    this.GetTop5DonViCoTyLeXlVbQuaHan();
    this.GetTrungBinhSoLuongVbDi();
    this.GetTop5DonViCoVbDiNhieuNhat();
    this.GetTop5DonViCoVbLienThongNhieuNhat();
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = "";
    let result = [];
    array.forEach((element) => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = "";
      }
    });
    if (temp !== "") {
      result.push(temp);
    }
    return result;
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  // -----------------------------------------------------------------------------------------------------------------------------

  GetTop5DonViCoVbDenNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTop5DonViCoVbDenNhieuNhat(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.Top5DonViCoVbDenNhieuNhat.barChartLabels = rs.data.map((x) => x.NAME);

          // this.Top5DonViCoVbDenNhieuNhat.barChartLabels = rs.data.map((x) => {
          //   if (x.NAME.length > 6) {
          //     return x.NAME.substring(0, 6) + "...";
          //   } else {
          //     return x.NAME;
          //   }
          // });

          //  rs.data.sort((a, b) => b.TOTAL - a.TOTAL);

          let arrTotal = rs.data.map((x) => Math.round(x.TOTAL));

          this.Top5DonViCoVbDenNhieuNhat.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#F5AB00",
            },
          ];
          // this.Top5DonViCoVbDenNhieuNhat.barChartOptions = {
          //   responsive: true,
          //   maintainAspectRatio: false,
          //   legend: { display: false },
          //   layout: {},
          //   scales: {
          //     xAxes: [
          //       {
          //         gridLines: { display: false },
          //         ticks: { fontColor: "white" },
          //       },
          //     ],
          //     yAxes: [
          //       {
          //         gridLines: {
          //           display: true,
          //           color: "white",
          //           drawBorder: false,
          //         },
          //         ticks: {
          //           fontColor: "white",
          //           suggestedMin: 0,
          //         },
          //       },
          //     ],
          //   },
          //   plugins: {
          //     labels: false,
          //     datalabels: {
          //       anchor: "end",
          //       align: "top",
          //       color: "white",
          //     },
          //   },
          // };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  GetTop5DonViCoNguoiDungThuongXuyenTruyCap(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService
        .GetTop5DonViCoNguoiDungThuongXuyenTruyCap(obj)
        .subscribe(
          (rs: any) => {
            if (!rs.success) {
              return false;
            }

            this.Top5TruyCapNhieuNhat.barChartLabels = rs.data.map(
              (x) => {
               if (x.NAME.length > 6) {
                 return x.NAME.substring(0, 6) + "...";
               } else {
                 return x.NAME;
               }
             }
            );

            // rs.data.sort((a, b) => b.TOTALACCESS - a.TOTALACCESS);

            let arrTotal = rs.data.map((x) => Math.round(x.TOTALACCESS));

            this.Top5TruyCapNhieuNhat.barChartData = [
              {
                data: arrTotal,
                backgroundColor: "#669900",
              },
            ];
          },
          (err) => {
            this.snackbar.showError(
              MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
              MESSAGE_TYPE.ERROR
            );
          }
        )
    );
  }

  GetTrungBinhSoLuongVbDen(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTrungBinhSoLuongVbDen(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }
          this.THE = {
            AVG: this.getNumber(res.data[0].AVG),
          };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  GetTop5DonViCoTyLeXlVbDungHan(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTop5DonViCoTyLeXlVbDungHan(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.Top5DonViCoVbdenDungHanNhieuNhat.barChartLabels = rs.data.map(
            (x) => {
               if (x.NAME.length > 6) {
                 return x.NAME.substring(0, 6) + "...";
               } else {
                 return x.NAME;
               }
             }
          );

          // rs.data.sort((a, b) => b.TYLEDUNGHAN - a.TYLEDUNGHAN);

          let arrTotal = rs.data.map((x) => x.TYLEDUNGHAN);

          this.Top5DonViCoVbdenDungHanNhieuNhat.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#bce784",
            },
          ];
          // this.Top5DonViCoVbdenDungHanNhieuNhat.barChartOptions = {
          //   responsive: true,
          //   maintainAspectRatio: false,
          //   legend: { display: false },
          //   layout: {},
          //   scales: {
          //     xAxes: [
          //       {
          //         gridLines: { display: false },
          //         ticks: { fontColor: "white" },
          //       },
          //     ],
          //     yAxes: [
          //       {
          //         gridLines: {
          //           display: true,
          //           color: "white",
          //           drawBorder: false,
          //         },
          //         ticks: {
          //           fontColor: "white",
          //           suggestedMin: 0,
          //         },
          //       },
          //     ],
          //   },
          //   plugins: {
          //     labels: false,
          //     datalabels: {
          //       anchor: "end",
          //       align: "top",
          //       color: "white",
          //     },
          //   },
          // };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  GetTop5DonViCoTyLeXlVbQuaHan(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTop5DonViCoTyLeXlVbQuaHan(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartLabels = rs.data.map(
            (x) => {
               if (x.NAME.length > 6) {
                 return x.NAME.substring(0, 6) + "...";
               } else {
                 return x.NAME;
               }
             }
          );

          // rs.data.sort((a, b) => b.TYLEQUAHAN - a.TYLEQUAHAN);

          let arrTotal = rs.data.map((x) => x.TYLEQUAHAN);

          this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#F4511E",
            },
          ];
          // this.Top5DonViCoVbdenQuaHanNhieuNhat.barChartOptions = {
          //   responsive: true,
          //   maintainAspectRatio: false,
          //   legend: { display: false },
          //   layout: {},
          //   scales: {
          //     xAxes: [
          //       {
          //         gridLines: { display: false },
          //         ticks: { fontColor: "white" },
          //       },
          //     ],
          //     yAxes: [
          //       {
          //         gridLines: {
          //           display: true,
          //           color: "white",
          //           drawBorder: false,
          //         },
          //         ticks: {
          //           fontColor: "white",
          //           suggestedMin: 0,
          //         },
          //       },
          //     ],
          //   },
          //   plugins: {
          //     labels: false,
          //     datalabels: {
          //       anchor: "end",
          //       align: "top",
          //       color: "white",
          //     },
          //   },
          // };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  GetTrungBinhSoLuongVbDi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTrungBinhSoLuongVbDi(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }
          this.THE1 = {
            AVG: this.getNumber(res.data[0].AVG),
          };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  GetTop5DonViCoVbDiNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTop5DonViCoVbDiNhieuNhat(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.Top5DonViCoVbDiNhieuNhat.barChartLabels = rs.data.map((x) => {
            if (x.NAME.length > 6) {
              return x.NAME.substring(0, 6) + "...";
            } else {
              return x.NAME;
            }
          });

          // rs.data.sort((a, b) => b.VBDI - a.VBDI);

          let arrTotal = rs.data.map((x) => Math.round(x.VBDI));

          this.Top5DonViCoVbDiNhieuNhat.barChartData = [
            {
              data: arrTotal,
              backgroundColor: "#1982c4",
            },
          ];

          this.Top5DonViCoVbDiNhieuNhat.barChartType = "horizontalBar";


        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  GetTop5DonViCoVbLienThongNhieuNhat(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService
        .GetTop5DonViCoVbLienThongNhieuNhat(obj)
        .subscribe(
          (rs: any) => {
            if (!rs.success) {
              return false;
            }

           this.Top5DonViCoVbLienThongNhieuNhat.barChartLabels = rs.data.map(
             (x) => {
               if (x.NAME.length > 6) {
                 return x.NAME.substring(0, 6) + "...";
               } else {
                 return x.NAME;
               }
             }
           );

          //  rs.data.sort((a, b) => b.TYLEVBLIENTHONG - a.TYLEVBLIENTHONG);

            let arrTotal = rs.data.map((x) => Math.round(x.TYLEVBLIENTHONG));

            this.Top5DonViCoVbLienThongNhieuNhat.barChartData = [
              {
                data: arrTotal,
                backgroundColor: "#3F51B5",
              },
            ];
            
          },
          (err) => {
            this.snackbar.showError(
              MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
              MESSAGE_TYPE.ERROR
            );
          }
        )
    );
  }

  onFilterChangeCSKB() {
    this.detectMode;
    this.getData();
  }

  getDynamicWidthAndHeight(obj: any, percent: number, percent1: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "min-height: 130%; min-width:100%; width: " + length * percent + "%; height: " + length * percent1 + "%";
  }
}
