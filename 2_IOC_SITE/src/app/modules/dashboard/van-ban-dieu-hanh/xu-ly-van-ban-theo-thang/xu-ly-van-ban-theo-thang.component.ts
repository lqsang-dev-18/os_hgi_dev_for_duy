import { ColorService } from './../../../../services/color.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { COLOR_PALETTES, MESSAGE_COMMON, MESSAGE_TYPE } from 'src/app/constant/system-constant';
import { Subscription } from "rxjs";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from 'src/app/shared/components/chart-js/bar-chart/bar-chart.component';
import { LineChartComponent } from "src/app/shared/components/chart-js/line-chart/line-chart.component";
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";

@Component({
  selector: 'app-xu-ly-van-ban-theo-thang',
  templateUrl: './xu-ly-van-ban-theo-thang.component.html',
  styleUrls: ['./xu-ly-van-ban-theo-thang.component.scss']
})
export class XuLyVanBanTheoThangComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};

  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public DON_VI_LIST: any = [];
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl([""], [Validators.required]),
    madonvi: new FormControl([""]),
  });

  @ViewChild("tinhHinhKySoVBDen", { static: true }) tinhHinhKySoVBDen: BarChartComponent;
  @ViewChild("tinhHinhKySoVBDi", { static: true }) tinhHinhKySoVBDi: BarChartComponent;
  @ViewChild("tinhHinhDuyetVBDen", { static: true }) tinhHinhDuyetVBDen: BarChartComponent;
  @ViewChild("tinhHinhTiepNhanVBDen", { static: true }) tinhHinhTiepNhanVBDen: BarChartComponent;
  @ViewChild("tinhHinhXLVBDenBiTreHan", { static: true }) tinhHinhXLVBDenBiTreHan: LineChartComponent;
  @ViewChild("tinhHinhSDHeThong", { static: true }) tinhHinhSDHeThong: LineChartComponent;

  lableDonVi = true;
  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService,
    private color: ColorService
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    //this.formSearch.controls["month"].setValue([1,2,3,4,5,6,7,8,9,10,11,12]);
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }
    this.lableMonth = true;

    this.detectMode();

    this.tinhHinhKySoVBDen.barChartType = "horizontalBar";
    this.tinhHinhKySoVBDi.barChartType = "horizontalBar";
    this.tinhHinhDuyetVBDen.barChartType = "horizontalBar";
    this.tinhHinhTiepNhanVBDen.barChartType = "horizontalBar";
    this.tinhHinhXLVBDenBiTreHan.lineChartOptions.legend.display = false;
    this.tinhHinhSDHeThong.lineChartOptions.legend.display = false;

    this.getData();
    this.getListDonVi();
  }

  ngOnDestroy() {
    this.subscription.forEach(subscription => {
      if (subscription != undefined) subscription.unsubscribe();
    });
  }

  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.tinhHinhKySoVBDen.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tinhHinhKySoVBDen.barChartOptions.legend.position = "top";
    this.tinhHinhKySoVBDen.barChartOptions.legend.align = "start";
    this.tinhHinhKySoVBDen.barChartOptions.scales.xAxes[0].stacked = true;
    this.tinhHinhKySoVBDen.barChartOptions.scales.yAxes[0].stacked = true;
    this.tinhHinhKySoVBDi.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tinhHinhKySoVBDi.barChartOptions.legend.position = "top";
    this.tinhHinhKySoVBDi.barChartOptions.legend.align = "start";
    this.tinhHinhKySoVBDi.barChartOptions.scales.xAxes[0].stacked = true;
    this.tinhHinhKySoVBDi.barChartOptions.scales.yAxes[0].stacked = true;
    this.tinhHinhDuyetVBDen.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tinhHinhDuyetVBDen.barChartOptions.legend.position = "top";
    this.tinhHinhDuyetVBDen.barChartOptions.legend.align = "start";
    this.tinhHinhDuyetVBDen.barChartOptions.scales.xAxes[0].stacked = true;
    this.tinhHinhDuyetVBDen.barChartOptions.scales.yAxes[0].stacked = true;
    this.tinhHinhTiepNhanVBDen.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tinhHinhTiepNhanVBDen.barChartOptions.legend.position = "top";
    this.tinhHinhTiepNhanVBDen.barChartOptions.legend.align = "start";
    this.tinhHinhTiepNhanVBDen.barChartOptions.scales.xAxes[0].stacked = true;
    this.tinhHinhTiepNhanVBDen.barChartOptions.scales.yAxes[0].stacked = true;
    this.tinhHinhXLVBDenBiTreHan.lineChartOptions.maintainAspectRatio = !this.isFitPage;
    //this.tinhHinhXLVBDenBiTreHan.lineChartOptions.scales.xAxes[0].scaleLabel.labelString = "Tuần";
    this.tinhHinhSDHeThong.lineChartOptions.maintainAspectRatio = !this.isFitPage;
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = (16 // padding top
        + 43.75 + 16 // form height and its margin bottom
        + 16); // 1 row spacing
      let rowHeight = 'calc((100% - ' + others + 'px) / 2)';
      this.rowStyles = { 'height': rowHeight, 'margin-bottom': '1rem' };
    }
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value;
    let month = this.formSearch.controls.month.value;
    let madonvi = this.formSearch.controls.madonvi.value;
    let result = { nam: year, thang: month.toString(), madonvi: madonvi == null ? "" : madonvi.toString() };

    return result;
  }

  public getData(): void {
    this.tktinhHinhKySoVBDen();
    this.tktinhHinhKySoVBDi();
    this.tktinhHinhDuyetVBDen();
    this.tktinhHinhTiepNhanVBDen();
    this.tktinhHinhXLVBDenBiTreHan();
    this.tktinhHinhSDHeThong();
  }

  public getListDonVi(): void {
    //this.formSearch.controls.madonvi.reset();
    this.subscription.push(
      this.vanbandieuhanhService.getListDonVi().subscribe(
        (rs) => {
          this.DON_VI_LIST = rs.data;
          //this.formSearch.controls["madonvi"].setValue(rs.data.map(x => x.MA_DON_VI_CON));
          this.lableDonVi = true;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  // using PALETTE_3[2]
  public tktinhHinhKySoVBDen(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTinhHinhKySoVBDiDen(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.tinhHinhKySoVBDen.barChartLabels = rs.data.map(x => this.convertToArray(' ', x.TEN_KY, 4));
      let arrTongDenKS = rs.data.map(x => x.TONG_SO_DEN_KS);
      let arrTongDen0KS = rs.data.map(x => x.TONG_SO_DEN_0KS);
      this.tinhHinhKySoVBDen.barChartData = [
        { data: arrTongDenKS, label: 'Văn bản đến ký số', backgroundColor: COLOR_PALETTES.PALETTE_3[1] },
        { data: arrTongDen0KS, label: 'Văn bản đến không ký số', backgroundColor: COLOR_PALETTES.PALETTE_3[2] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using DUTCH_FIELD: 0, 1
  public tktinhHinhKySoVBDi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTinhHinhKySoVBDiDen(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.tinhHinhKySoVBDi.barChartLabels = rs.data.map(x => this.convertToArray(' ', x.TEN_KY, 4));
      let arrTongDiKS = rs.data.map(x => x.TONG_SO_DI_KS);
      let arrTongDi0KS = rs.data.map(x => x.TONG_SO_DI_0KS);
      this.tinhHinhKySoVBDi.barChartData = [
        { data: arrTongDiKS, label: 'Văn bản đi ký số', backgroundColor: COLOR_PALETTES.DUTCH_FIELD[0] },
        { data: arrTongDi0KS, label: 'Văn bản đi không ký số', backgroundColor: COLOR_PALETTES.DUTCH_FIELD[1] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using DUTCH_FIELD: 2, 3
  public tktinhHinhDuyetVBDen(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTinhHinhDuyetVBDen(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.tinhHinhDuyetVBDen.barChartLabels = rs.data.map(x => this.convertToArray(' ', x.TEN_KY, 4));
      let arrVBChuaDuyet = rs.data.map(x => x.VB_CHUA_DUYET);
      let arrVBDaDuyet = rs.data.map(x => x.VB_DA_DUYET);
      this.tinhHinhDuyetVBDen.barChartData = [
        { data: arrVBDaDuyet, label: 'Văn bản đã duyệt', backgroundColor: COLOR_PALETTES.DUTCH_FIELD[2] },
        { data: arrVBChuaDuyet, label: 'Văn bản chưa duyệt', backgroundColor: COLOR_PALETTES.DUTCH_FIELD[3] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using DUTCH_FIELD: 4, 5
  public tktinhHinhTiepNhanVBDen(): void {
    let obj: any = this.getFilter();
    this.subscription.push(this.vanbandieuhanhService.GetTKTinhHinhTiepNhanVBDen(obj).subscribe((rs: any) => {
      if (!rs.success) {
        return;;
      }
      this.tinhHinhTiepNhanVBDen.barChartLabels = rs.data.map(x => this.convertToArray(' ', x.TEN_KY, 4));
      let arrVBChuaTiepNhan = rs.data.map(x => x.TONG_CHUAXL);
      let arrVBDaTiepNhan = rs.data.map(x => x.TONG_SO_DEN);
      this.tinhHinhTiepNhanVBDen.barChartData = [
        { data: arrVBDaTiepNhan, label: 'Văn bản đã tiếp nhận', backgroundColor: COLOR_PALETTES.DUTCH_FIELD[4] },
        { data: arrVBChuaTiepNhan, label: 'Văn bản chưa tiếp nhận', backgroundColor: COLOR_PALETTES.DUTCH_FIELD[5] }
      ];
    }, err => {
      this.snackbar.showError(MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL, MESSAGE_TYPE.ERROR);
    }));
  }

  // using RIVER_NIGHTS: 1
  tktinhHinhXLVBDenBiTreHan() {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTKTinhHinhXLVBDenBiTreHan(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.tinhHinhXLVBDenBiTreHan.lineChartLabels = rs.data.map((x) => x.GIA_TRI_THANG);
          let arrTSVBDenTreHan = rs.data.map((x) => Math.round(x.TS_VBDE_TRE_HAN));

          this.tinhHinhXLVBDenBiTreHan.lineChartData = [
            {
              data: arrTSVBDenTreHan,
              label: "Tổng số VB đến trễ hạn",
              fill: true,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              borderColor: COLOR_PALETTES.RIVER_NIGHTS[1],
              backgroundColor: this.color.hexToRGB(COLOR_PALETTES.RIVER_NIGHTS[1], 0.4),
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  // using RIVER_NIGHTS: 1
  tktinhHinhSDHeThong() {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetTKTinhHinhSDHeThong(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          this.tinhHinhSDHeThong.lineChartLabels = rs.data.map((x) => x.THANG_BAOCAO);
          let arrSoNguoiTruyCap = rs.data.map((x) => Math.round(x.SO_NGUOI_TRUY_CAP));

          this.tinhHinhSDHeThong.lineChartData = [
            {
              data: arrSoNguoiTruyCap,
              label: "Số người truy cập",
              fill: true,
              pointRadius: 2,
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              borderColor: COLOR_PALETTES.RIVER_NIGHTS[2],
              backgroundColor: this.color.hexToRGB(COLOR_PALETTES.RIVER_NIGHTS[2], 0.4),
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = '';
    let result = [];
    array.forEach(element => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = '';
      }
    });
    if (temp !== '') {
      result.push(temp);
    }
    return result;
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }

}
