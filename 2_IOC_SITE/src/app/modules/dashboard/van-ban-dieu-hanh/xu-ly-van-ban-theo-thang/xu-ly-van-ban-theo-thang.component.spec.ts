import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XuLyVanBanTheoThangComponent } from './xu-ly-van-ban-theo-thang.component';

describe('XuLyVanBanTheoThangComponent', () => {
  let component: XuLyVanBanTheoThangComponent;
  let fixture: ComponentFixture<XuLyVanBanTheoThangComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XuLyVanBanTheoThangComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XuLyVanBanTheoThangComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
