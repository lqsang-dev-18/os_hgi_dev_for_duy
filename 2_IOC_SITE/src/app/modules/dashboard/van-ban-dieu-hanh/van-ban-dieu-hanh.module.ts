import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';

import { VanBanDieuHanhRoutingModule } from './van-ban-dieu-hanh-routing.module';
import { ThongTinChungComponent } from './thong-tin-chung/thong-tin-chung.component';
import { XuLyVanBanTheoThangComponent } from './xu-ly-van-ban-theo-thang/xu-ly-van-ban-theo-thang.component';
import { XuLyVanBanTheoDonViComponent } from './xu-ly-van-ban-theo-don-vi/xu-ly-van-ban-theo-don-vi.component';
import { XuLyVanBanTheoPhongBanComponent } from './xu-ly-van-ban-theo-phong-ban/xu-ly-van-ban-theo-phong-ban.component';
import { ThongTinChungTinhHinhXuLyComponent } from './thong-tin-chung-tinh-hinh-xu-ly/thong-tin-chung-tinh-hinh-xu-ly.component';
import { HieuSuatXuLyvaDieuHanhComponent } from './hieu-suat-xu-ly-va-dieu-hanh/hieu-suat-xu-ly-va-dieu-hanh.component';
import { ThongKeCacDonViTrucThuocSoYTeComponent } from './thong-ke-cac-don-vi-truc-thuoc-so-y-te/thong-ke-cac-don-vi-truc-thuoc-so-y-te.component';
import { ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent } from './thong-ke-cac-phong-ban-chuyen-mon-truc-thuoc-so-y-te/thong-ke-cac-phong-ban-chuyen-mon-truc-thuoc-so-y-te.componet';
import { QuanLyCongViecComponent } from './quan-ly-cong-viec/quan-ly-cong-viec.component';


@NgModule({
  declarations: [
    ThongTinChungComponent,
    XuLyVanBanTheoThangComponent,
    XuLyVanBanTheoDonViComponent,
    XuLyVanBanTheoPhongBanComponent,
    ThongTinChungTinhHinhXuLyComponent,
    HieuSuatXuLyvaDieuHanhComponent,
    ThongKeCacDonViTrucThuocSoYTeComponent,
    ThongKeCacPhongBanChuyenMonTrucThuocSoYTeComponent,
    QuanLyCongViecComponent
  ],
  imports: [CommonModule, VanBanDieuHanhRoutingModule, SharedModule],
})
export class VanBanDieuHanhModule {}
