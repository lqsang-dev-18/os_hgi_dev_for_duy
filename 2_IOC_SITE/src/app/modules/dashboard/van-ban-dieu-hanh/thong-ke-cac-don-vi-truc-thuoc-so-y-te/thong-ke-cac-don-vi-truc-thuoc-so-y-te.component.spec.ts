import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ThongKeCacDonViTrucThuocSoYTeComponent } from "./thong-ke-cac-don-vi-truc-thuoc-so-y-te.component";

describe("ThongKeCacDonViTrucThuocSoYTeComponent", () => {
  let component: ThongKeCacDonViTrucThuocSoYTeComponent;
  let fixture: ComponentFixture<ThongKeCacDonViTrucThuocSoYTeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThongKeCacDonViTrucThuocSoYTeComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThongKeCacDonViTrucThuocSoYTeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
