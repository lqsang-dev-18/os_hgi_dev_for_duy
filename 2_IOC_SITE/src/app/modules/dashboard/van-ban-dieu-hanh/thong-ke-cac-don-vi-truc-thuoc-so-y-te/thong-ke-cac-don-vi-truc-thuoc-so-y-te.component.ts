import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { VanBanDieuHanhService } from "src/app/services/van-ban-dieu-hanh.service";
import { LineChartComponent } from "src/app/shared/components/chart-js/line-chart/line-chart.component";
import { ColorService } from "src/app/services/color.service";

@Component({
  selector: "app-thong-ke-cac-don-vi-truc-thuoc-so-y-te",
  templateUrl: "./thong-ke-cac-don-vi-truc-thuoc-so-y-te.component.html",
  styleUrls: ["./thong-ke-cac-don-vi-truc-thuoc-so-y-te.component.scss"],
})
export class ThongKeCacDonViTrucThuocSoYTeComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};
  public YEARS: any[] = [];
  public MONTHS: any[] = [];
  public isFullScreen = false;
  THE: any = {};
  THE1: any = {};
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    month: new FormControl("", [Validators.required]),
  });
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
    COLOR_PALETTES.PALETTE_1[3],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.GREY_TO_RED[7],
    COLOR_PALETTES.ORANGE_TO_PURPLE[7],
    COLOR_PALETTES.ORANGE_TO_PURPLE[0],
    COLOR_PALETTES.IOC[8],
  ];

  // Bar chart
  @ViewChild("TkVanBanDenTheoDonVi", { static: true })
  TkVanBanDenTheoDonVi: BarChartComponent;
  @ViewChild("TkVanBanDenQuaHanTheoDonVi", { static: true })
  TkVanBanDenQuaHanTheoDonVi: BarChartComponent;
  @ViewChild("TkVanBanDiTheoDonVi", { static: true })
  TkVanBanDiTheoDonVi: BarChartComponent;
  @ViewChild("TkSoLuongTruyCapHeThongTheoDonVi", { static: true })
  TkSoLuongTruyCapHeThongTheoDonVi: BarChartComponent;
  @ViewChild("TkVanBanDiLienThongTheoDonVi", { static: true })
  TkVanBanDiLienThongTheoDonVi: BarChartComponent;

  //line chart
  @ViewChild("TkVanBanDenDungHanTheoDonVi", { static: true })
  TkVanBanDenDungHanTheoDonVi: LineChartComponent;

  lableMonth = true;

  constructor(
    private vanbandieuhanhService: VanBanDieuHanhService,
    private snackbar: SnackbarService,
    private color: ColorService
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    this.lableMonth = true;
    for (let i = 1; i <= 12; i++) {
      this.MONTHS.push({ id: i, text: "Tháng " + i });
    }

    this.isFullScreen = false;
    this.detectMode();
    this.getData();
  }
  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    // this.buildStyles();

    this.TkVanBanDenTheoDonVi.barChartType = "horizontalBar";
    this.TkVanBanDenTheoDonVi.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanDenTheoDonVi.barChartOptions.legend.display = false;
    this.TkVanBanDenTheoDonVi.barChartOptions.scales.xAxes[0].gridLines.display =
      true;
    this.TkVanBanDenTheoDonVi.barChartOptions.scales.xAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanDenTheoDonVi.barChartOptions.scales.xAxes[0].gridLines.drawBorder =
      false;

    this.TkVanBanDenDungHanTheoDonVi.lineChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanDenDungHanTheoDonVi.lineChartOptions.legend.display = false;
    this.TkVanBanDenDungHanTheoDonVi.lineChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkVanBanDenDungHanTheoDonVi.lineChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanDenDungHanTheoDonVi.lineChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

    this.TkVanBanDenQuaHanTheoDonVi.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanDenQuaHanTheoDonVi.barChartOptions.legend.display = false;
    this.TkVanBanDenQuaHanTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkVanBanDenQuaHanTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanDenQuaHanTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

    this.TkVanBanDiTheoDonVi.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanDiTheoDonVi.barChartOptions.legend.display = false;
    this.TkVanBanDiTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkVanBanDiTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanDiTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;

    this.TkVanBanDiLienThongTheoDonVi.barChartType = "horizontalBar";
    this.TkVanBanDiLienThongTheoDonVi.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkVanBanDiLienThongTheoDonVi.barChartOptions.legend.display = false;
    this.TkVanBanDiLienThongTheoDonVi.barChartOptions.scales.xAxes[0].gridLines.display =
      true;
    this.TkVanBanDiLienThongTheoDonVi.barChartOptions.scales.xAxes[0].gridLines.color =
      "#fff";
    this.TkVanBanDiLienThongTheoDonVi.barChartOptions.scales.xAxes[0].gridLines.drawBorder =
      false;

    this.TkSoLuongTruyCapHeThongTheoDonVi.barChartOptions.maintainAspectRatio =
      !this.isFitPage;
    this.TkSoLuongTruyCapHeThongTheoDonVi.barChartOptions.legend.display =
      false;
    this.TkSoLuongTruyCapHeThongTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.display =
      true;
    this.TkSoLuongTruyCapHeThongTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.color =
      "#fff";
    this.TkSoLuongTruyCapHeThongTheoDonVi.barChartOptions.scales.yAxes[0].gridLines.drawBorder =
      false;
  }
  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 1 row spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }

  showFullScreen() {
    // Kiểm tra trạng thái fullscreen
    if (!this.isFullScreen) {
      // Nếu không ở chế độ fullscreen, yêu cầu chuyển sang fullscreen
      document.documentElement.requestFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = true;
    } else {
      // Nếu đang ở chế độ fullscreen, yêu cầu thoát fullscreen
      document.exitFullscreen();
      // Cập nhật trạng thái fullscreen
      this.isFullScreen = false;
    }
  }

  public getFilter() {
    let year = this.formSearch.controls.year.value ?? "";
    let month = this.formSearch.controls.month.value ?? "-1";
    let unit_type = "dependent_unit"
    return { nam: year, thang: month.toString(), unit_type };
  }

  public getData(): void {
    this.getCacChiSoVbDen();
    this.getCacChiSoVbDi();
    this.getTKVbDenDungHanTheoDonVi();
    this.getTKVbDenQuaHanTheoDonVi();
    this.getTKVbDenTheoDonVi();
    this.getTKVbDiTheoDonVi();
    this.getTKVbDiLienThongTheoDonVi();
    this.getTKSLTruyCapTheoDonVi();
  }

  public getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  private convertToArray(character: string, value: string, step?: number) {
    let array = value.split(character);
    let count = 0;
    let temp = "";
    let result = [];
    array.forEach((element) => {
      temp += element + " ";
      count++;
      if (count == step ?? 1) {
        result.push(temp.trim());
        count = 0;
        temp = "";
      }
    });
    if (temp !== "") {
      result.push(temp);
    }
    return result;
  }

  // -----------------------------------------------------------------------------------------------------------------------------

  getCacChiSoVbDen(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVBDen(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }
          this.THE = {
            TOTAL: this.getNumber(res.data[0].TOTAL),
            DUNGHAN: this.getNumber(res.data[0].DUNGHAN),
            QUAHAN: this.getNumber(res.data[0].QUAHAN),
          };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getCacChiSoVbDi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVBDi(obj).subscribe(
        (res: any) => {
          if (!res.success) {
            return;
          }
          this.THE1 = {
            VBDI: this.getNumber(res.data[0].VBDI),
            VBLIENTHONG: this.getNumber(res.data[0].VBLIENTHONG),
          };
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbDenTheoDonVi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVbDenTheoDonVi(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          // this.TkVanBanDenTheoDonVi.barChartLabels = rs.data.map((x) => x.NAME);

          // this.TkVanBanDenTheoDonVi.barChartLabels = rs.data.map((x) => {
          //   if (x.NAME.length > 3) {
          //     return x.NAME.substring(0, 3) + "...";
          //   } else {
          //     return x.NAME;
          //   }
          // });

          // rs.data.sort((a, b) => b.TOTAL - a.TOTAL);

          // let arrTotal = rs.data.map((x) => Math.round(x.TOTAL));

          let data = rs.data;

          // Sắp xếp dữ liệu theo trường DUNGHAN
          data.sort((a, b) => b.TOTAL - a.TOTAL);

          let arrLabel = data.map((x) => x.NAME);
          let arrVbDen = data.map((x) => Math.round(x.TOTAL));

          // Tạo một đối tượng chứa nhãn và giá trị tương ứng
          let chartData = arrLabel.map((label, index) => ({
            label: label,
            value: arrVbDen[index]
          }));

          // Sắp xếp đối tượng theo giá trị giảm dần
          chartData.sort((a, b) => b.value - a.value);

          // Tách nhãn và giá trị sau khi đã sắp xếp
          let sortedLabels = chartData.map((item) => item.label);
          let sortedValues = chartData.map((item) => item.value);

          this.TkVanBanDenTheoDonVi.barChartLabels = sortedLabels.map((x) => x);

          this.TkVanBanDenTheoDonVi.barChartData = [
            {
              data: sortedValues,
              backgroundColor: this.CONST_COLOR[14],
            },
          ];

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbDenDungHanTheoDonVi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVbDenTheoDonVi(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          let data = rs.data;

          // Sắp xếp dữ liệu theo trường DUNGHAN
          data.sort((a, b) => b.DUNGHAN - a.DUNGHAN);

          let arrLabel = data.map((x) => x.NAME);
          let arrVbDenDungHan = data.map((x) => Math.round(x.DUNGHAN));

          // Tạo một đối tượng chứa nhãn và giá trị tương ứng
          let chartData = arrLabel.map((label, index) => ({
            label: label,
            value: arrVbDenDungHan[index]
          }));

          // Sắp xếp đối tượng theo giá trị giảm dần
          chartData.sort((a, b) => b.value - a.value);

          // Tách nhãn và giá trị sau khi đã sắp xếp
          let sortedLabels = chartData.map((item) => item.label);
          let sortedValues = chartData.map((item) => item.value);

          this.TkVanBanDenDungHanTheoDonVi.lineChartLabels = sortedLabels.map((x) => x.split(' '));

          this.TkVanBanDenDungHanTheoDonVi.lineChartData = [
            {
              data: sortedValues,
              label: "Tên đơn vị",
              fill: true,
              pointStyle: "rectRot",
              pointBackgroundColor: "yellow",
              lineTension: 0,
              datalabels: { align: "end", anchor: "center" },
              borderColor: COLOR_PALETTES.BLUES[7],
              backgroundColor: this.color.hexToRGB(
                COLOR_PALETTES.BLUES[7],
                0.4
              ),
            },
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbDenQuaHanTheoDonVi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetCacChiSoVbDenTheoDonVi(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          // this.TkVanBanDenQuaHanTheoDonVi.barChartLabels = rs.data.map((x) => {
          //   if (x.NAME.length > 3) {
          //     return x.NAME.substring(0, 3) + "...";
          //   } else {
          //     return x.NAME;
          //   }
          // });

          // rs.data.sort((a, b) => b.QUAHAN - a.QUAHAN);

          // let arrTotal = rs.data.map((x) => Math.round(x.QUAHAN));

          let data = rs.data;

          // Sắp xếp dữ liệu theo trường DUNGHAN
          data.sort((a, b) => b.QUAHAN - a.QUAHAN);

          let arrLabel = data.map((x) => x.NAME);
          let arrVbDenQuaHan = data.map((x) => Math.round(x.QUAHAN));

          // Tạo một đối tượng chứa nhãn và giá trị tương ứng
          let chartData = arrLabel.map((label, index) => ({
            label: label,
            value: arrVbDenQuaHan[index]
          }));

          // Sắp xếp đối tượng theo giá trị giảm dần
          chartData.sort((a, b) => b.value - a.value);

          // Tách nhãn và giá trị sau khi đã sắp xếp
          let sortedLabels = chartData.map((item) => item.label);
          let sortedValues = chartData.map((item) => item.value);

          this.TkVanBanDenQuaHanTheoDonVi.barChartLabels = sortedLabels.map((x) => x);

          this.TkVanBanDenQuaHanTheoDonVi.barChartData = [
            {
              data: sortedValues,
              backgroundColor: this.CONST_COLOR[12],
            },
          ];

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbDiTheoDonVi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetSoLuongVbDiTheoDonVi(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          // this.TkVanBanDiTheoDonVi.barChartLabels = rs.data.map((x) => {
          //   if (x.NAME.length > 3) {
          //     return x.NAME.substring(0, 3) + "...";
          //   } else {
          //     return x.NAME;
          //   }
          // });

          // rs.data.sort((a, b) => b.VBDI - a.VBDI);

          // let arrTotal = rs.data.map((x) => Math.round(x.VBDI));

          let data = rs.data;

          // Sắp xếp dữ liệu theo trường DUNGHAN
          data.sort((a, b) => b.VBDI - a.VBDI);

          let arrLabel = data.map((x) => x.NAME);
          let arrVbDi = data.map((x) => Math.round(x.VBDI));

          // Tạo một đối tượng chứa nhãn và giá trị tương ứng
          let chartData = arrLabel.map((label, index) => ({
            label: label,
            value: arrVbDi[index]
          }));

          // Sắp xếp đối tượng theo giá trị giảm dần
          chartData.sort((a, b) => b.value - a.value);

          // Tách nhãn và giá trị sau khi đã sắp xếp
          let sortedLabels = chartData.map((item) => item.label);
          let sortedValues = chartData.map((item) => item.value);

          this.TkVanBanDiTheoDonVi.barChartLabels = sortedLabels.map((x) => x);

          this.TkVanBanDiTheoDonVi.barChartData = [
            {
              data: sortedValues,
              backgroundColor: this.CONST_COLOR[14],
            },
          ];


        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKVbDiLienThongTheoDonVi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService.GetSoLuongVbDiTheoDonVi(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }

          // this.TkVanBanDiLienThongTheoDonVi.barChartLabels = rs.data.map(
          //   (x) => {
          //     if (x.NAME.length > 3) {
          //       return x.NAME.substring(0, 3) + "...";
          //     } else {
          //       return x.NAME;
          //     }
          //   }
          // );

          // rs.data.sort((a, b) => b.VBLIENTHONG - a.VBLIENTHONG);

          // let arrTotal = rs.data.map((x) => Math.round(x.VBLIENTHONG));

          let data = rs.data;

          // Sắp xếp dữ liệu theo trường DUNGHAN
          data.sort((a, b) => b.VBLIENTHONG - a.VBLIENTHONG);

          let arrLabel = data.map((x) => x.NAME);
          let arrVbLienThong = data.map((x) => Math.round(x.VBLIENTHONG));

          // Tạo một đối tượng chứa nhãn và giá trị tương ứng
          let chartData = arrLabel.map((label, index) => ({
            label: label,
            value: arrVbLienThong[index]
          }));

          // Sắp xếp đối tượng theo giá trị giảm dần
          chartData.sort((a, b) => b.value - a.value);

          // Tách nhãn và giá trị sau khi đã sắp xếp
          let sortedLabels = chartData.map((item) => item.label);
          let sortedValues = chartData.map((item) => item.value);

          this.TkVanBanDiLienThongTheoDonVi.barChartLabels = sortedLabels.map((x) => x);

          this.TkVanBanDiLienThongTheoDonVi.barChartData = [
            {
              data: sortedValues,
              backgroundColor: this.CONST_COLOR[14],
            },
          ];


        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getTKSLTruyCapTheoDonVi(): void {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.vanbandieuhanhService
        .GetSoLuongTruyCapHeThongTheoDonVi(obj)
        .subscribe(
          (rs: any) => {
            if (!rs.success) {
              return false;
            }
            // this.TkSoLuongTruyCapHeThongTheoDonVi.barChartLabels = rs.data.map(
            //   (x) => {
            //     if (x.NAME.length > 3) {
            //       return x.NAME.substring(0, 3) + "...";
            //     } else {
            //       return x.NAME;
            //     }
            //   }
            // );

            // this.TkSoLuongTruyCapHeThongTheoDonVi.barChartLabels = rs.data.map(
            //   (x) => x.NAME);

            // rs.data.sort((a, b) => b.TOTALACCESS - a.TOTALACCESS);

            // let arrTotal = rs.data.map((x) => Math.round(x.TOTALACCESS));

            let data = rs.data;

            // Sắp xếp dữ liệu theo trường DUNGHAN
            data.sort((a, b) => b.TOTALACCESS - a.TOTALACCESS);

            let arrLabel = data.map((x) => x.NAME);
            let arrTkTruyCap = data.map((x) => Math.round(x.TOTALACCESS));

            // Tạo một đối tượng chứa nhãn và giá trị tương ứng
            let chartData = arrLabel.map((label, index) => ({
              label: label,
              value: arrTkTruyCap[index]
            }));

            // Sắp xếp đối tượng theo giá trị giảm dần
            chartData.sort((a, b) => b.value - a.value);

            // Tách nhãn và giá trị sau khi đã sắp xếp
            let sortedLabels = chartData.map((item) => item.label);
            let sortedValues = chartData.map((item) => item.value);

            this.TkSoLuongTruyCapHeThongTheoDonVi.barChartLabels = sortedLabels.map((x) => x);


            this.TkSoLuongTruyCapHeThongTheoDonVi.barChartData = [
              {
                data: sortedValues,
                backgroundColor: "#669900",
              },
            ];
          },
          (err) => {
            this.snackbar.showError(
              MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
              MESSAGE_TYPE.ERROR
            );
          }
        )
    );
  }

  // -----------------------------------------------------------------------------------------------------------------------------

  onFilterChangeCSKB() {
    this.detectMode;
    this.getData();
  }

  // getDynamicHeight(obj: any, percent: number) {
  //   let array: any[] = obj ? obj.data : [];
  //   let length = array ? array.length : 1;
  //   return "min-height: 100%; height: " + length * percent + "%";
  // }

  getDynamicWidthAndHeight(obj: any, percent: number, percent1: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "min-height: 130%; min-width:100%; width: " + length * percent + "%; height: " + length * percent1 + "%";
  }
}
