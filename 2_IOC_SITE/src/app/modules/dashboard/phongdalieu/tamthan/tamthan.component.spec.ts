import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TamthanComponent } from './tamthan.component';

describe('TamthanComponent', () => {
  let component: TamthanComponent;
  let fixture: ComponentFixture<TamthanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TamthanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TamthanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
