import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhongdalieuComponent } from './phongdalieu.component';

describe('PhongdalieuComponent', () => {
  let component: PhongdalieuComponent;
  let fixture: ComponentFixture<PhongdalieuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhongdalieuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhongdalieuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
