import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThanhTraSYTRoutingModule } from './thanh-tra-syt-routing.module';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SharedModule } from 'src/app/shared/shared.module';

import { ThanhTraSYTComponent } from './thanh-tra-syt/thanh-tra-syt.component';
import { XuLyDonComponent } from './xu-ly-don/xu-ly-don.component';
import { ThanhTraNoiDungComponent } from './thanh-tra-noi-dung/thanh-tra-noi-dung.component';
import { ThanhTraLinhVucComponent } from './thanh-tra-linh-vuc/thanh-tra-linh-vuc.component';

@NgModule({
  declarations: [ThanhTraSYTComponent, XuLyDonComponent, ThanhTraNoiDungComponent, ThanhTraLinhVucComponent],
  imports: [
    CommonModule,
    ThanhTraSYTRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSlideToggleModule,
    SharedModule
  ]
})
export class ThanhTraSYTModule { }
