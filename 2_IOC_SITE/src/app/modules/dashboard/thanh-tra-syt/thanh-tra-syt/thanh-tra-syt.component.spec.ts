import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThanhTraSYTComponent } from './thanh-tra-syt.component';

describe('ThanhTraSYTComponent', () => {
  let component: ThanhTraSYTComponent;
  let fixture: ComponentFixture<ThanhTraSYTComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThanhTraSYTComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThanhTraSYTComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
