import { TaiChinhKeHoachService } from "../../../../services/tai-chinh-ke-hoach.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { CommonFunctionService } from "src/app/services/common-function.service";
import { ThanhTraSYTService } from "src/app/services/thanh-tra-syt.service";
import { DmChungService } from 'src/app/services/dm-chung.service';

@Component({
  selector: "app-thanh-tra-syt",
  templateUrl: "./thanh-tra-syt.component.html",
  styleUrls: ["./thanh-tra-syt.component.scss"],
})
export class ThanhTraSYTComponent implements OnInit {
  subscription: Subscription[] = [];

  isFitPage = false;
  formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    quy: new FormControl("Tất cả"),
  });

  YEARS: any[] = [];
  MONTHS: any[] = [];
  QUARTERS: any[] = [];
  rowStyles: any = {};
  THANHTRA: any = {};
  TONG_PHAM_VI: any = {};
  listQuy: any = {};
  isthongKeCKTTKT = false
  isthongKeTCCK = false
  isthongKeCNCK = false
  istienPSLuyKeCK = false
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
  ];

  @ViewChild("thongKeCKTTKT", { static: true }) thongKeCKTTKT: BarChartComponent;
  @ViewChild("thongKeTCCK", { static: true }) thongKeTCCK: BarChartComponent;

  @ViewChild("thongKeCNCK", { static: true }) thongKeCNCK: BarChartComponent;
  @ViewChild("tienPSLuyKeCK", { static: true }) tienPSLuyKeCK: BarChartComponent;

  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private dmchungService: DmChungService,
    private thanhTraSYT: ThanhTraSYTService,
    private commonService: CommonFunctionService
  ) {

    this.THANHTRA = {
      TONG_TTKT: 0,
      TONG_TT: 0,
      TONG_KT: 0,
      TONG_TTKT_TOCHUC: 0,
      TONG_TTKT_CANHAN: 0,
    },
    this.TONG_PHAM_VI = {
      TONG_VI_PHAM: 0,
      TC_VI_PHAM: 0,
      CN_VI_PHAM: 0,
      TONG_TIEN_XU_PHAT_TC: 0,
      TONG_TIEN_XU_PHAT_CN: 0,
    }
  }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }

    this.detectMode();
    this.getListQuy();
    this.getData();
  }

  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== "responsive";
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.thongKeCKTTKT.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.thongKeCKTTKT.barChartOptions.responsive = true;
    this.thongKeTCCK.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.thongKeTCCK.barChartOptions.responsive = true;
    this.thongKeCNCK.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tienPSLuyKeCK.barChartOptions.maintainAspectRatio = !this.isFitPage;
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 2 rows spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }

  getFilter() {
    let params = {};
    if (this.formSearch.controls.year.value) {
      params["nam"] = this.formSearch.controls.year.value;
    }
    params["quy"] = this.formSearch.get("quy").value == "Tất cả"?"0": this.formSearch.get("quy").value;
    return params;
  }

  public getData(): void {
    this.getTongThanhTra();
    this.getTongPhamVi()
    this.getThanhTraKiemTraCK()
    this.getThanhTraKiemTraTCCK()
    this.getThanhTraKiemTraCaNhanCK()
    this.getTienPSLuyKeCK()
  }
  getListQuy(){
    this.subscription.push(
      this.dmchungService.getListQuy().subscribe(
        (rs) => {
          this.listQuy = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getTongThanhTra(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.thanhTraSYT.getThanhTraYte(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return;
          }
          this.THANHTRA = {
            TONG_TTKT: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_TTKT),
            TONG_TT: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_TT),
            TONG_KT: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_KT),
            TONG_TTKT_TOCHUC: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_TTKT_TOCHUC),
            TONG_TTKT_CANHAN: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_TTKT_CANHAN),
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getTongPhamVi(){
    let obj: any = this.getFilter();
    this.subscription.push(
      this.thanhTraSYT.getTongPhamVi(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return;
          }
          this.TONG_PHAM_VI = {
            TONG_VI_PHAM: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_VI_PHAM),
            TC_VI_PHAM: new Intl.NumberFormat("vi-VN").format(rs.data[0].TC_VI_PHAM),
            CN_VI_PHAM: new Intl.NumberFormat("vi-VN").format(rs.data[0].CN_VI_PHAM),
            TONG_TIEN_XU_PHAT_TC: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_TIEN_XU_PHAT_TC),
            TONG_TIEN_XU_PHAT_CN: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_TIEN_XU_PHAT_CN),
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getThanhTraKiemTraCK(){
    let obj: any = this.getFilter();
    let lableHT:any
    let lableCK:any
    this.isthongKeCKTTKT = true;
    this.subscription.push(
      this.thanhTraSYT.getThanhTraKiemTraCK(obj).subscribe(
        (rs) => {
          if(rs.data.length == 0){
            this.isthongKeCKTTKT = false;
          }
          else{
            this.isthongKeCKTTKT = true;
            this.thongKeCKTTKT.barChartLabels = rs.data.map(x => this.commonService.convertStringToArray(" ", x.LABEL, 3));
            lableHT = rs.data[0].ITEM_HT
            lableCK = rs.data[0].ITEM_CK
          }
          let arrHienTai = rs.data.map(x=>x.VALUE_HIEN_TAI);

          let arrCungKy = rs.data.map(x=>x.VALUE_CUNG_KY);
          this.thongKeCKTTKT.barChartData = [
            {
              data: arrHienTai,
              label: lableHT,
              backgroundColor: "#741b47",
              datalabels: {align: "end", anchor:"end", clamp: true}
            },
            {

              data:  arrCungKy,
              label: lableCK,
              backgroundColor: "#c27ba0",
              datalabels: {align: "end", anchor:"end", clamp: true}
            }
          ]}
      ))
  }
  getThanhTraKiemTraTCCK(){
    let obj: any = this.getFilter();
    let lableHT:any
    let lableCK:any

    this.isthongKeTCCK = true;
    this.subscription.push(
      this.thanhTraSYT.getThanhTraKiemToChucCK(obj).subscribe(
        (rs) => {
          if(rs.data.length == 0){
            this.isthongKeTCCK = false;
          }
          else{
            this.isthongKeTCCK = true;
            this.thongKeTCCK.barChartLabels = rs.data.map(x => this.commonService.convertStringToArray(" ", x.LABEL, 3));

            lableHT = obj.nam
            lableCK = obj.nam -1
          }
          let arrHienTai = rs.data.map(x=>x.VALUE_HIEN_TAI);

          let arrCungKy = rs.data.map(x=>x.VALUE_CUNG_KY);
          this.thongKeTCCK.barChartData = [
            {
              data: arrHienTai,
              label: "Lũy kế năm " + lableHT,
              backgroundColor: "#351c75",
              datalabels: {align: "end", anchor:"end", clamp: true}
            },
            {
              data:  arrCungKy,
              label:  "Lũy kế năm " + lableCK,
              backgroundColor: "#8e7cc3",
              datalabels: {align: "end", anchor:"end", clamp: true}
            }
          ]}
      ))
  }
  getThanhTraKiemTraCaNhanCK(){
    let obj: any = this.getFilter();
    let lableHT:any
    let lableCK:any
    this.isthongKeCNCK = true;
    this.subscription.push(
      this.thanhTraSYT.getThanhTraKiemCaNhanCK(obj).subscribe(
        (rs) => {
          if(rs.data.length == 0){
            this.isthongKeCNCK = false;
            this.thongKeCNCK.barChartLabels=[]
          }
          else{
            this.isthongKeCNCK = true;
            this.thongKeCNCK.barChartLabels = rs.data.map(x => this.commonService.convertStringToArray(" ", x.LABEL, 3));

            lableHT = obj.nam
            lableCK = obj.nam -1
          }
          let arrHienTai = rs.data.map(x=>x.VALUE_HIEN_TAI);

          let arrCungKy = rs.data.map(x=>x.VALUE_CUNG_KY);
          this.thongKeCNCK.barChartData = [
            {
              data: arrHienTai,
              label: "Lũy kế năm " + lableHT,
              backgroundColor: "#5f6d34",
              datalabels: {align: "end", anchor:"end", clamp: true}
            },
            {
              data:  arrCungKy,
              label:  "Lũy kế năm " + lableCK,
              backgroundColor: "#7ab490",
              datalabels: {align: "end", anchor:"end", clamp: true}
            }
          ]}
      ))
  }
  getTienPSLuyKeCK(){
    let obj: any = this.getFilter();
    let lableHT:any
    let lableCK:any
    this.istienPSLuyKeCK = true;
    this.subscription.push(
      this.thanhTraSYT.getTienPhatSinhLKCK(obj).subscribe(
        (rs) => {
          if(rs.data.length == 0){
            this.istienPSLuyKeCK = false;
            this.tienPSLuyKeCK.barChartLabels=[]
          }
          else{
            this.istienPSLuyKeCK = true;
            this.tienPSLuyKeCK.barChartLabels = rs.data.map(x => this.commonService.convertStringToArray(" ", x.LABEL, 3));

            lableHT = obj.nam
            lableCK = obj.nam -1
          }
          let arrHienTai = rs.data.map(x=>x.VALUE_HIEN_TAI);

          let arrCungKy = rs.data.map(x=>x.VALUE_CUNG_KY);
          this.tienPSLuyKeCK.barChartData = [
            {
              data: arrHienTai,
              label: "Lũy kế năm " + lableHT,
              backgroundColor: "#b45f06",
              datalabels: {align: "end", anchor:"end", clamp: true}
            },
            {
              data:  arrCungKy,
              label:  "Lũy kế năm " + lableCK,
              backgroundColor: "#e69138",
              datalabels: {align: "end", anchor:"end", clamp: true}
            }
          ]}
      ))
  }
  getNumber(value: any) {
    return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  }

  getDynamicWidth(obj: any, percent: number) {
    let array: any[] = obj ? obj.data : [];
    let length = array ? array.length : 1;
    return "width: " + length * percent + "%";
  }
}
