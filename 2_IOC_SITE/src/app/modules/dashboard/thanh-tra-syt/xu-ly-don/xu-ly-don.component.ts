import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { Subscription } from "rxjs";
import { PieChartComponent } from "src/app/shared/components/chart-js/pie-chart/pie-chart.component";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { ThanhTraSYTService } from "src/app/services/thanh-tra-syt.service";

@Component({
  selector: 'app-xu-ly-don',
  templateUrl: './xu-ly-don.component.html',
  styleUrls: ['./xu-ly-don.component.scss']
})
export class XuLyDonComponent implements OnInit {
  private subscription: Subscription[] = [];
  public isFitPage = true;
  public rowStyles: any = {};
  public YEARS: any[] = [];
  public QUARTER: any[] = [];
  THE: any = {};
  listThang = [];
  isWithChart1: any;
  public formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    year: new FormControl("", [Validators.required]),
    quarter: new FormControl("", [Validators.required])
  });
  CONST_COLOR: any[] = [
    COLOR_PALETTES.PALETTE_2[3],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[6],
    COLOR_PALETTES.ORANGE_TO_PURPLE[1],
    COLOR_PALETTES.PALETTE_4[1],
    COLOR_PALETTES.PALETTE_3[2],
    COLOR_PALETTES.PALETTE_3[3],
    COLOR_PALETTES.RIVER_NIGHTS[2],
    COLOR_PALETTES.RIVER_NIGHTS[1],
    COLOR_PALETTES.RIVER_NIGHTS[0],
  ];
  @ViewChild("daiDien", { static: true }) daiDien: BarChartComponent;
  @ViewChild("noiDung", { static: true }) noiDung: PieChartComponent;
  @ViewChild("tinhTrang", { static: true }) tinhTrang: PieChartComponent;
  @ViewChild("thuocThamQuyen", { static: true }) thuocThamQuyen: BarChartComponent;
  @ViewChild("khongThuocThamQuyen", { static: true }) khongThuocThamQuyen: BarChartComponent;

  constructor(
    private ThanhTraSYTService: ThanhTraSYTService,
    private snackbar: SnackbarService,
  ) { }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    this.formSearch.controls["year"].setValue(currentYear);
    for (let i = 2020; i <= currentYear; i++) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    let currentQuarter = (Math.floor((currentDate.getMonth() + 3)/3))-1;
    this.formSearch.controls["quarter"].setValue(currentQuarter);
    this.QUARTER.push({id:"-1",text:"Tất cả"});
    for (let i = 1; i <= 4; i++) {
      this.QUARTER.push({ id: i, text: "Quý " + i });
    }
    this.detectMode();
    this.daiDien.barChartOptions.legend.display = false;
    this.thuocThamQuyen.barChartOptions.legend.display = false;
    this.khongThuocThamQuyen.barChartOptions.legend.display = false;
    this.getData();
  }
  public detectMode() {
    this.isFitPage = true;
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.buildStyles();

    this.daiDien.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.thuocThamQuyen.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.khongThuocThamQuyen.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.noiDung.pieChartOptions.maintainAspectRatio = !this.isFitPage;
    this.tinhTrang.pieChartOptions.maintainAspectRatio = !this.isFitPage;
    // this.tiemVATPhuNuCoThai.barChartOptions.maintainAspectRatio = !this.isFitPage;
  }
  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = (16 // padding top
        + 43.75 + 16 // form height and its margin bottom
        + 16); // 1 row spacing
      let rowHeight = 'calc((100% - ' + others + 'px) / 2)';
      this.rowStyles = { 'height': rowHeight, 'margin-bottom': '1rem' };
    }
  }
  showFullScreen() {
    document.documentElement.requestFullscreen();

  }
  public getFilter() {
    let year = this.formSearch.controls.year.value ?? "";
    let quarter = this.formSearch.controls.quarter.value ?? "-1";
    return { nam: year, quy: quarter};
  }
  public getData(): void {
     this.DaiDienTrenMotDon();
    this.KQXL_KhongThuocThamQuyen();
    this.KQXL_ThuocThamQuyen();
    this.PhanLoaiTheoNoiDung();
    this.PhanLoaiTinhTrang();
    this.get6TheTong();
 }
 private convertToArray(character: string, value: string, step?: number) {
  let array = value.split(character);
  let count = 0;
  let temp = '';
  let result = [];
  array.forEach(element => {
    temp += element + " ";
    count++;
    if (count == step ?? 1) {
      result.push(temp.trim());
      count = 0;
      temp = '';
    }
  });
  if (temp !== '') {
    result.push(temp);
  }
  return result;
}
 DaiDienTrenMotDon(): void {
  let obj: any = this.getFilter();
  this.subscription.push(
    this.ThanhTraSYTService.getDaiDienTrenMotDon(obj).subscribe(
      (rs: any) => {
        if (!rs.success) {
          return false;
        }
        this.daiDien.barChartLabels = [
          this.convertToArray(' ',"Kỳ trước - Đơn có nhiều người đứng tên",2),
          this.convertToArray(' ',"Kỳ trước - Đơn có một người đứng tên",2),
          this.convertToArray(' ',"Kỳ trước - Đơn khác",2),
          this.convertToArray(' ',"Kỳ này - Đơn có nhiều người đứng tên",2),
          this.convertToArray(' ',"Kỳ này - Đơn có một người đứng tên",2),
          this.convertToArray(' ',"Kỳ này - Đơn khác",2),
        ];
        //let label = rs.data.map((x) => x.LABEL);
        let arrN1 = rs.data.map((x) => Math.round(x.KTCS_DON_NHIEU_NGUOI_DUNG_TEN));
        let arrN2 = rs.data.map((x) => Math.round(x.KTCS_DON_MOT_NGUOI_DUNG_TEN));
        let arrN3 = rs.data.map((x) => Math.round(x.KTCS_DON_KHAC));
        let arrN4 = rs.data.map((x) => Math.round(x.TK_DON_NHIEU_NGUOI_DUNG_TEN));
        let arrN5 = rs.data.map((x) => Math.round(x.TK_DON_MOT_NGUOI_DUNG_TEN));
        let arrN6 = rs.data.map((x) => Math.round(x.TK_DON_KHAC));


        let arrData = [arrN1[0], arrN2[0], arrN3[0], arrN4[0], arrN5[0], arrN6[0]];
        this.daiDien.barChartData = [
          {
            data: arrData,
            backgroundColor: COLOR_PALETTES.DUTCH_FIELD[2],
            datalabels: { align: "top", anchor: "center", clamp: true },
          }
        ];
      },
      (err) => {
        this.snackbar.showError(
          MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
          MESSAGE_TYPE.ERROR
        );
      }
    )
  );
}

PhanLoaiTheoNoiDung(): void {
  let obj = this.getFilter();
  this.subscription.push(
    this.ThanhTraSYTService.getPhanLoaiTheoNoiDung(obj).subscribe(
      (rs: any) => {
        if (!rs.success) {
          return false;
        }
        let data = rs.data;
        if (data.length == 0) {
          this.noiDung.pieChartData = [];
          return false;
        }
        let KhieuNai = data[0].ND_KHIEU_NAI;
        let ToCao = data[0].ND_TO_CAO;
        let KienNghi = data[0].ND_KIEN_NGHI;

        this.noiDung.pieChartData = [
          KhieuNai,
          ToCao,
          KienNghi
        ];
        this.noiDung.pieChartLabels = [
          "Khiếu nại",
          "Tố cáo",
          "Kiến nghị",
        ];
        this.noiDung.pieChartColor = [
          { backgroundColor: COLOR_PALETTES.PALETTE_4 },
        ];
      },

      (err) => {
        this.snackbar.showError(
          MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
          MESSAGE_TYPE.ERROR
        );
      }
    )
  );
}

PhanLoaiTinhTrang(): void {
  let obj = this.getFilter();
  this.subscription.push(
    this.ThanhTraSYTService.getPhanLoaiTheoTinhTrang(obj).subscribe(
      (rs: any) => {
        if (!rs.success) {
          return false;
        }
        let data = rs.data;
        if (data.length == 0) {
          this.tinhTrang.pieChartData = [];
          return false;
        }
        let DaGiaiQuyet = data[0].TT_DA_GIAI_QUYET;
        let DangGiaiQuyet = data[0].TT_DANG_GIAI_QUYET;
        let ChuaGiaiQuyet = data[0].TT_CHUA_GIAI_QUYET;

        this.tinhTrang.pieChartData = [
          DaGiaiQuyet,
          DangGiaiQuyet,
          ChuaGiaiQuyet
        ];
        this.tinhTrang.pieChartLabels = [
          "Đã giải quyết",
          "Đang giải quyết",
          "Chưa giải quyết",
        ];
        this.tinhTrang.pieChartColor = [
          { backgroundColor: COLOR_PALETTES.RIVER_NIGHTS },
        ];
      },

      (err) => {
        this.snackbar.showError(
          MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
          MESSAGE_TYPE.ERROR
        );
      }
    )
  );
}

KQXL_ThuocThamQuyen(): void {
  let obj: any = this.getFilter();
  this.subscription.push(
    this.ThanhTraSYTService.getKQXLyThuocThamQuyen(obj).subscribe(
      (rs: any) => {
        if (!rs.success) {
          return false;
        }
        this.thuocThamQuyen.barChartLabels = [
          this.convertToArray(' ',"Khiếu nại",2),
          this.convertToArray(' ',"Tố cáo",2),
          this.convertToArray(' ',"Kiến nghị",2),
        ];
        //let label = rs.data.map((x) => x.LABEL);
        let arrN1 = rs.data.map((x) => Math.round(x.KQXL_TTQ_KHIEU_NAI));
        let arrN2 = rs.data.map((x) => Math.round(x.KQXL_TTQ_TO_CAO));
        let arrN3 = rs.data.map((x) => Math.round(x.KQXL_TTQ_KIEN_NGHI));

        let arrData = [arrN1[0], arrN2[0], arrN3[0]];
        this.thuocThamQuyen.barChartData = [
          {
            data: arrData,
            backgroundColor: COLOR_PALETTES.SPRING_PASTELS[3],
            datalabels: { align: "end", anchor: "center", clamp: true },
          },
        ];
      },
      (err) => {
        this.snackbar.showError(
          MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
          MESSAGE_TYPE.ERROR
        );
      }
    )
  );
}

KQXL_KhongThuocThamQuyen(): void {
  let obj: any = this.getFilter();
  this.subscription.push(
    this.ThanhTraSYTService.getKQXLyKhongThuocThamQuyen(obj).subscribe(
      (rs: any) => {
        if (!rs.success) {
          return false;
        }
        this.khongThuocThamQuyen.barChartLabels = [
          this.convertToArray(' ',"Hướng dẫn",2),
          this.convertToArray(' ',"Chuyển đơn",2),
          this.convertToArray(' ',"Đôn đốc giải quyết",2),
        ];
        //let label = rs.data.map((x) => x.LABEL);
        let arrN1 = rs.data.map((x) => Math.round(x.KQXL_KTTQ_HUONG_DAN));
        let arrN2 = rs.data.map((x) => Math.round(x.KQXL_KTTQ_CHUYEN_DON));
        let arrN3 = rs.data.map((x) => Math.round(x.KQXL_KTTQ_DON_DOC_GIAI_QUYET));

        let arrData = [arrN1[0], arrN2[0], arrN3[0]];
        this.khongThuocThamQuyen.barChartData = [
          {
            data: arrData,
            backgroundColor: COLOR_PALETTES.RETRO_METRO[1],
            datalabels: { align: "end", anchor: "center", clamp: true },
          },
        ];
      },
      (err) => {
        this.snackbar.showError(
          MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
          MESSAGE_TYPE.ERROR
        );
      }
    )
  );
}
get6TheTong(){
  let obj: any = this.getFilter();
  this.subscription.push(
    this.ThanhTraSYTService.get6TheTong(obj).subscribe(
      (rs: any) => {
        if (!rs.success) {
          return;
        }
        this.THE = {
          TONG_DON: new Intl.NumberFormat("vi-VN").format(rs.data[0].TONG_DON),
          DON_DA_XU_LY: new Intl.NumberFormat("vi-VN").format(rs.data[0].DON_DA_XU_LY),
          DON_DU_DK_XU_LY: new Intl.NumberFormat("vi-VN").format(rs.data[0].DON_DU_DK_XU_LY),
          VU_VIEC_DU_DK_XU_LY: new Intl.NumberFormat("vi-VN").format(rs.data[0].VU_VIEC_DU_DK_XU_LY),
          VBPD_NHAN_DUOC_DO_CHUYEN_DON: new Intl.NumberFormat("vi-VN").format(rs.data[0].VBPD_NHAN_DUOC_DO_CHUYEN_DON),
          DON_KHONG_DU_DK_XU_LY: new Intl.NumberFormat("vi-VN").format(rs.data[0].DON_KHONG_DU_DK_XU_LY),
        }
      },
      (err) => {
        this.snackbar.showError(
          MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
          MESSAGE_TYPE.ERROR
        );
      }
    )
  );
}


}
