import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XuLyDonComponent } from './xu-ly-don.component';

describe('XuLyDonComponent', () => {
  let component: XuLyDonComponent;
  let fixture: ComponentFixture<XuLyDonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XuLyDonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XuLyDonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
