import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
  ID_TINH,
  MA_TINH

} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { CommonFunctionService } from "src/app/services/common-function.service";
import { ThanhTraSYTService } from "src/app/services/thanh-tra-syt.service";
import { DmChungService } from 'src/app/services/dm-chung.service';

@Component({
  selector: "app-thanh-tra-noi-dung",
  templateUrl: "./thanh-tra-noi-dung.component.html",
  styleUrls: ["./thanh-tra-noi-dung.component.scss"],
})
export class ThanhTraNoiDungComponent implements OnInit {
  subscription: Subscription[] = [];
  isFitPage = false;
  isSetHeigt = true;
  formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    loai: new FormControl(1, [Validators.required]),
    year: new FormControl("", [Validators.required]),
    quy: new FormControl(),
    thang: new FormControl(),
    huyen: new FormControl(),
    xa: new FormControl(),
    tungay: new FormControl(new Date().toISOString()),
    denngay: new FormControl(new Date().toISOString()),
  });

  YEARS: any[] = [];
  rowStyles: any = {};
  THANHTRA: any = {};
  listQuy: any = [];
  listThang: any = [];
  listHuyen: any = [];
  listXa: any = [];
  isthongKeTSNBPHC = false;
  isthongKeSTVP = false;
  isthongKeTSNBKT = false;
  isthongKeSTVPDV = false;
  CONST_COLOR: any[] = [
    COLOR_PALETTES.RETRO_METRO[6],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[4],
    COLOR_PALETTES.PALETTE_4[3],
    COLOR_PALETTES.PALETTE_2[4],
    COLOR_PALETTES.IOC[5],
  ];


  @ViewChild("thongKeTSNBPHC", { static: true }) thongKeTSNBPHC: BarChartComponent;
  @ViewChild("thongKeSTVP", { static: true }) thongKeSTVP: BarChartComponent;
  @ViewChild("thongKeTSNBKT", { static: true }) thongKeTSNBKT: BarChartComponent;
  @ViewChild("thongKeSTVPDV", { static: true }) thongKeSTVPDV: BarChartComponent;
  @ViewChild('formSearchHeight') formSearchHeight: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private dmchungService: DmChungService,
    private thanhTraNoiDung: ThanhTraSYTService,
    private commonService: CommonFunctionService
  ) {


  }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    let currentMonth = currentDate.getMonth() + 1;
    this.formSearch.controls["year"].setValue(currentYear);
    this.formSearch.controls["thang"].setValue(currentMonth.toString());
    this.formSearch.controls["quy"].setValue((Math.ceil(currentMonth / 3)).toString());
    for (let i = currentYear; i >= 2020; i--) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    this.detectMode();
    this.getListQuy();
    this.getListThang();
    this.getListHuyen(true);

  }
  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== "responsive";
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);

    this.thongKeTSNBPHC.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.thongKeTSNBPHC.barChartOptions.responsive = true;
  }


  showFullScreen() {
    document.documentElement.requestFullscreen();
  }
  buildStyles() {
    this.rowStyles = {};
    if (this.isFitPage) {
      let others =
        16 + // padding top
        43.75 +
        16 + // form height and its margin bottom
        16; // 2 rows spacing
      let rowHeight = "calc((100% - " + others + "px) / 2)";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }
  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    //return [year, month, day].join('-');
    return [day, month, year].join('-');
  }
  getFilter() {
    let params = {};
    params["ma_tinh"] = MA_TINH;
    params["ma_huyen"] = this.formSearch.get("huyen").value;
    params["ma_xa"] = this.formSearch.get("xa").value;
    params["loai_cbx"] = this.formSearch.get("loai").value
    params["nam"] = this.formSearch.controls.year.value;
    params["quy"] = this.formSearch.get("quy").value == "Tất cả" ? "0" : this.formSearch.get("quy").value;
    params["thang"] = this.formSearch.get("thang").value == "Tất cả" ? "0" : this.formSearch.get("thang").value;
    params["tungay"] = this.formatDate(this.formSearch.get("tungay").value).toString();
    params["denngay"] = this.formatDate(this.formSearch.get("denngay").value).toString();
    return params;
  }

  public getData(): void {
    this.getTongSoNguoiBiPHC();
    this.getTongSoTienViPham();
    this.getTongSoNguoiBiKhoiTo();
    this.getTongSoTienViPhamDonVi();
    if (this.isSetHeigt)
      this.buildStyles();
  }

  getListQuy() {
    this.subscription.push(
      this.dmchungService.getListQuy().subscribe(
        (rs) => {
          this.listQuy = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListThang() {
    this.subscription.push(
      this.dmchungService.getListThang().subscribe(
        (rs) => {
          this.listThang = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListHuyen(init: boolean = false) {
    var obj = {
      idTinh: ID_TINH,
    };

    this.subscription.push(
      this.dmchungService.getListHuyen(obj).subscribe(
        (rs) => {
          this.listHuyen = rs.data;
          if (init) {
            this.formSearch.controls["huyen"].setValue(this.listHuyen[0].MA_HUYEN);
            this.getListXa(this.listHuyen[0].MA_HUYEN, init);
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListXa(maHuyen: number, init: boolean = false) {
    let index = this.listHuyen.findIndex(x => x.MA_HUYEN === maHuyen);
    var obj = { idHuyen: this.listHuyen[index].ID };

    this.subscription.push(
      this.dmchungService.getListXa(obj).subscribe(
        (rs) => {
          this.listXa = rs.data;
          this.formSearch.controls["xa"].setValue(this.listXa[0].MA_XA);
          if (init) {
            this.getData();
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  // getNumber(value: any) {
  //   return new Intl.NumberFormat("vi-VN").format(Math.round(value));
  // }

  // getDynamicWidth(obj: any, percent: number) {
  //   let array: any[] = obj ? obj.data : [];
  //   let length = array ? array.length : 1;
  //   return "width: " + length * percent + "%";
  // }

  getTongSoNguoiBiPHC() {
    let obj: any = this.getFilter();
    this.isthongKeTSNBPHC = true;
    this.subscription.push(
      this.thanhTraNoiDung.getNoiDungCaNhanViPhamCungKy(obj).subscribe(
        (rs) => {
          if (rs.data.length == 0) {
            this.isthongKeTSNBPHC = false;
          }
          else {
            let dataChart = rs.data[0];
            this.thongKeTSNBPHC.barChartLabels = [dataChart.CUNGKY, dataChart.KY];
            this.thongKeTSNBPHC.barChartData = [
              {
                data: [dataChart.CANHAN ?? 0, dataChart.CANHAN_CUNGKY ?? 0],
                backgroundColor: this.CONST_COLOR[1],
                label: "Tổng số",
                datalabels: { align: "end", anchor: "end", clamp: true },
                barThickness: 50, // Width của column (đơn vị pixel)
              }
            ]
          }

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      ))
  }
  getTongSoTienViPham() {
    let obj: any = this.getFilter();
    this.isthongKeSTVP = true;
    this.subscription.push(
      this.thanhTraNoiDung.getNoiDungTienViPhamCungKy(obj).subscribe(
        (rs) => {
          if (rs.data.length == 0) {
            this.isthongKeSTVP = false;
          }
          else {
            let dataChart = rs.data[0];
            this.thongKeSTVP.barChartLabels = [dataChart.CUNGKY, dataChart.KY];
            this.thongKeSTVP.barChartData = [
              {
                data: [dataChart.VIPHAM ?? 0, dataChart.VIPHAM_CUNGKY ?? 0],
                backgroundColor: this.CONST_COLOR[2],
                label: "Tổng số",
                datalabels: { align: "end", anchor: "end", clamp: true },
                barThickness: 50, // Width của column (đơn vị pixel)
              }
            ]
          }

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      ))
  }

  getTongSoNguoiBiKhoiTo() {
    let obj: any = this.getFilter();
    this.isthongKeTSNBKT = true;
    this.subscription.push(
      this.thanhTraNoiDung.getNoiDungDoiTuongKhoiToCungKy(obj).subscribe(
        (rs) => {
          if (rs.data.length == 0) {
            this.isthongKeTSNBKT = false;
          }
          else {
            let dataChart = rs.data[0];
            this.thongKeTSNBKT.barChartLabels = [dataChart.CUNGKY, dataChart.KY];
            this.thongKeTSNBKT.barChartData = [
              {
                data: [dataChart.KHOITO ?? 0, dataChart.KHOITO_CUNGKY ?? 0],
                backgroundColor: this.CONST_COLOR[4],
                label: "Tổng số",
                datalabels: { align: "end", anchor: "end", clamp: true },
                barThickness: 50, // Width của column (đơn vị pixel)
              }
            ]
          }

        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      ))
  }
  getTongSoTienViPhamDonVi() {
    let obj: any = this.getFilter();
    this.isthongKeSTVPDV = true;
    this.subscription.push(
      this.thanhTraNoiDung.getNoiDungTienViPhamDonVi(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return false;
          }
          this.thongKeSTVPDV.barChartLabels = rs.data.map((x) => x.DONVI);
          this.thongKeSTVPDV.barChartData = [
            {
              data: rs.data.map((x) => Math.round(x.VIPHAM)),
              label: "Tổng số",
              datalabels: { align: "end", anchor: "end", clamp: true },
              barThickness: 50, // Width của column (đơn vị pixel)
            }
          ];
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      ))
  }

}

