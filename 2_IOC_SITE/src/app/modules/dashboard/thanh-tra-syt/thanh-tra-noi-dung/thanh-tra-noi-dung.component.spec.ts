import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThanhTraNoiDungComponent } from './thanh-tra-noi-dung.component';

describe('ThanhTraNoiDungComponent', () => {
  let component: ThanhTraNoiDungComponent;
  let fixture: ComponentFixture<ThanhTraNoiDungComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThanhTraNoiDungComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThanhTraNoiDungComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
