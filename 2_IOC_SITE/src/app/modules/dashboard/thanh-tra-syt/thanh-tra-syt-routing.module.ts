import { ThanhTraLinhVucComponent } from './thanh-tra-linh-vuc/thanh-tra-linh-vuc.component';
import { ThanhTraSYTComponent } from './thanh-tra-syt/thanh-tra-syt.component';
import { XuLyDonComponent } from './xu-ly-don/xu-ly-don.component';
import { ThanhTraNoiDungComponent } from './thanh-tra-noi-dung/thanh-tra-noi-dung.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: ThanhTraSYTComponent },
  { path: "thanh-tra-syt", component: ThanhTraSYTComponent },
  { path: "xu-ly-don", component: XuLyDonComponent },
  { path: "thanh-tra-syt/noidung", component: ThanhTraNoiDungComponent },
  { path: "thanh-tra-syt/linh-vuc", component: ThanhTraLinhVucComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ThanhTraSYTRoutingModule {}
