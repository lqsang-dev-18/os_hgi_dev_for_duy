import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThanhTraLinhVucComponent } from './thanh-tra-linh-vuc.component';

describe('ThanhTraSYTComponent', () => {
  let component: ThanhTraLinhVucComponent;
  let fixture: ComponentFixture<ThanhTraLinhVucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThanhTraLinhVucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThanhTraLinhVucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
