import { Component, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { Subscription } from "rxjs";
import {
  COLOR_PALETTES,
  MESSAGE_COMMON,
  MESSAGE_TYPE,
  ID_TINH,
  MA_TINH
} from "src/app/constant/system-constant";
import { SnackbarService } from "src/app/services/snackbar.service";
import { BarChartComponent } from "src/app/shared/components/chart-js/bar-chart/bar-chart.component";
import { CommonFunctionService } from "src/app/services/common-function.service";
import { ThanhTraSYTService } from "src/app/services/thanh-tra-syt.service";
import { DmChungService } from 'src/app/services/dm-chung.service';

@Component({
  selector: "app-thanh-tra-linh-vuc",
  templateUrl: "./thanh-tra-linh-vuc.component.html",
  styleUrls: ["./thanh-tra-linh-vuc.component.scss"],
})
export class ThanhTraLinhVucComponent implements OnInit {
  subscription: Subscription[] = [];
  isFitPage = false;
  isSetHeigt = true;
  formSearch = new FormGroup({
    isFitPage: new FormControl(this.isFitPage),
    loai: new FormControl(0, [Validators.required]),
    year: new FormControl("", [Validators.required]),
    quy: new FormControl(),
    thang: new FormControl(),
    huyen: new FormControl(),
    xa: new FormControl(),
    tungay: new FormControl(new Date().toISOString()),
    denngay: new FormControl(new Date().toISOString()),
  });
  showSearch : any[] = [false,false,false,false];
  YEARS: any[] = [];
  rowStyles: any = {};
  THANHTRA: any = {};
  listQuy: any = [];
  listThang: any = [];
  listHuyen: any = [];
  listXa: any = [];
  isthongKeLVVPCK = false
  CONST_COLOR: any[] = [
    COLOR_PALETTES.RETRO_METRO[6],
    COLOR_PALETTES.PALETTE_1[0],
    COLOR_PALETTES.RIVER_NIGHTS[4],
    COLOR_PALETTES.PALETTE_4[3],
    COLOR_PALETTES.PALETTE_2[4],
    COLOR_PALETTES.IOC[5],
  ];

  @ViewChild("thongKeLVVPCK", { static: true }) thongKeLVVPCK: BarChartComponent;
  @ViewChild('formSearchHeight') formSearchHeight: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private snackbar: SnackbarService,
    private dmchungService: DmChungService,
    private thanhTraLinhVuc: ThanhTraSYTService,
    private commonService: CommonFunctionService
  ) {

    this.THANHTRA = {
      TONG_TTHC: 0,
      TONG_TTCN: 0,
      TONG_KTHC: 0,
      TONG_KTCN: 0,
    }

  }

  ngOnInit(): void {
    let currentDate = new Date();
    let currentYear = currentDate.getFullYear();
    let currentMonth = currentDate.getMonth() + 1;
    this.formSearch.controls["year"].setValue(currentYear);
    this.formSearch.controls["thang"].setValue(currentMonth.toString());
    this.formSearch.controls["quy"].setValue((Math.ceil(currentMonth / 3)).toString());
    for (let i = currentYear; i >= 2020; i--) {
      this.YEARS.push({ id: i, text: "Năm " + i });
    }
    this.detectMode();
    this.getListQuy();
    this.getListThang();
    this.getListHuyen(true);
    this.reloadShowSearch();
  }
  public reloadShowSearch(){
    // 0:Năm/1:Quý/2:Tháng/3:TuNgayDenNgay:
    var valueLoai = this.formSearch.get("loai").value;
    switch(valueLoai){
      case 0: // theo năm
        this.showSearch = [true,false,false,false];
        break;
      case 1: // theo tháng
        this.showSearch = [true,false,true,false];
        break;
      case 2: // theo quý
        this.showSearch = [true,true,false,false];
        break;
      case 3: // theo ngày
        this.showSearch = [false,false,false,true];
        break;
      default:
        this.showSearch = [false,false,false,false];
        break;
    }
  }
  public detectMode() {
    let mode = this.activatedRoute.snapshot.params.mode;
    this.isFitPage = mode === null || mode !== "responsive";
    this.formSearch.controls.isFitPage.setValue(this.isFitPage);
    this.thongKeLVVPCK.barChartOptions.maintainAspectRatio = !this.isFitPage;
    this.thongKeLVVPCK.barChartOptions.responsive = true;
  }

  showFullScreen() {
    document.documentElement.requestFullscreen();
  }

  buildStyles() {
    this.isSetHeigt = false;
    this.rowStyles = {};
    if (this.isFitPage) {
      let others = this.formSearchHeight.nativeElement.offsetHeight + 32;
      let rowHeight = "calc((100% - " + others + "px))";
      this.rowStyles = { height: rowHeight, "margin-bottom": "1rem" };
    }
  }

  formatDate(date) {
    var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2)
      month = '0' + month;
    if (day.length < 2)
      day = '0' + day;

    return [day, month, year].join('-');
  }

  getFilter() {
    let params = {};
    params["loai_cbx"] = this.formSearch.get("loai").value
    params["nam"] = this.showSearch[0] ? this.formSearch.controls.year.value : "0";
    params["quy"] = this.showSearch[1] ? (this.formSearch.get("quy").value == "Tất cả" ? "0" : this.formSearch.get("quy").value) : "0";
    params["thang"] = this.showSearch[2] ? (this.formSearch.get("thang").value == "Tất cả" ? "0" : this.formSearch.get("thang").value) : "0";
    params["tungay"] = this.showSearch[3] ? this.formatDate(this.formSearch.get("tungay").value).toString() : "01-01-1970";
    params["denngay"] = this.showSearch[3] ? this.formatDate(this.formSearch.get("denngay").value).toString() : "01-01-1970";
    params["ma_huyen"] = this.formSearch.get("huyen").value;
    params["ma_xa"] = this.formSearch.get("xa").value;
    params["ma_tinh"] = MA_TINH;
    return params;
  }

  public getData(): void {
    this.reloadShowSearch();
    this.get4TheTongLinhVuc();
    this.getLinhVucViPhamCK();
    if(this.isSetHeigt)
      this.buildStyles();
    
  }
  getListQuy() {
    this.subscription.push(
      this.dmchungService.getListQuy().subscribe(
        (rs) => {
          rs.data.shift();
          this.listQuy = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }
  getListThang() {
    this.subscription.push(
      this.dmchungService.getListThang().subscribe(
        (rs) => {
          rs.data.shift();
          this.listThang = rs.data;
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getListHuyen(init: boolean = false) {
    var obj = {
      idTinh: ID_TINH,
    };

    this.subscription.push(
      this.dmchungService.getListHuyen(obj).subscribe(
        (rs) => {
          this.listHuyen = rs.data;
          if (init) {
            this.formSearch.controls["huyen"].setValue(this.listHuyen[0].MA_HUYEN);
            this.getListXa(this.listHuyen[0].MA_HUYEN, init);
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getListXa(maHuyen: number, init: boolean = false) {
    let index = this.listHuyen.findIndex(x => x.MA_HUYEN === maHuyen);
    var obj = { idHuyen: this.listHuyen[index].ID };

    this.subscription.push(
      this.dmchungService.getListXa(obj).subscribe(
        (rs) => {
          this.listXa = rs.data;
          this.formSearch.controls["xa"].setValue(this.listXa[0].MA_XA);
          if (init) {
            this.getData();
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  get4TheTongLinhVuc() {
    let obj: any = this.getFilter();
    this.subscription.push(
      this.thanhTraLinhVuc.get4TheTongLinhVuc(obj).subscribe(
        (rs: any) => {
          if (!rs.success) {
            return;
          }
          if (rs.data.length < 1 || rs.data == undefined) {
            this.THANHTRA = {
              TONG_TTHC: 0,
              TONG_TTCN: 0,
              TONG_KTHC: 0,
              TONG_KTCN: 0,
            }
          } else {
            this.THANHTRA = {
              TONG_TTHC: new Intl.NumberFormat("vi-VN").format(rs.data[0].THANHTRA_HANHCHINH ?? 0),
              TONG_TTCN: new Intl.NumberFormat("vi-VN").format(rs.data[0].THANHTRA_CHUYENNGANH ?? 0),
              TONG_KTHC: new Intl.NumberFormat("vi-VN").format(rs.data[0].KIEMTRA_HANHCHINH ?? 0),
              TONG_KTCN: new Intl.NumberFormat("vi-VN").format(rs.data[0].KIEMTRA_CHUYENNGANH ?? 0),
            }
          }
        },
        (err) => {
          this.snackbar.showError(
            MESSAGE_COMMON.GET_DATA_NOT_SUCCESSFUL,
            MESSAGE_TYPE.ERROR
          );
        }
      )
    );
  }

  getLinhVucViPhamCK() {
    let obj: any = this.getFilter();
    this.isthongKeLVVPCK = true;
    this.subscription.push(
      this.thanhTraLinhVuc.getLinhVucViPhamCungKy(obj).subscribe(
        (rs) => {
          if (rs.data.length == 0) {
            this.isthongKeLVVPCK = false;
          }
          else {
            let dataChart = rs.data[0];
            this.thongKeLVVPCK.barChartLabels = [dataChart.CUNGKY, dataChart.KY];
            this.thongKeLVVPCK.barChartData = [
              {
                data: [dataChart.Y_CUNGKY ?? 0, dataChart.Y ?? 0],
                label: "Cơ sở y",
                backgroundColor: this.CONST_COLOR[4],
                datalabels: { align: "end", anchor: "end", clamp: true },
              },
              {
                data: [dataChart.DUOC_CUNGKY ?? 0, dataChart.DUOC ?? 0],
                label: "Cơ sở dược",
                backgroundColor: this.CONST_COLOR[5],
                datalabels: { align: "end", anchor: "end", clamp: true }
              }
            ]
          }

        }
      ))
  }

}
