import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QD4210Component } from './qd4210.component';

describe('QD4210Component', () => {
  let component: QD4210Component;
  let fixture: ComponentFixture<QD4210Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QD4210Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QD4210Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
