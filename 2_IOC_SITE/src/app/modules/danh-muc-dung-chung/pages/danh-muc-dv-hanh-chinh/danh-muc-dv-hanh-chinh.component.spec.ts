import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhMucDvHanhChinhComponent } from './danh-muc-dv-hanh-chinh.component';

describe('DanhMucDvHanhChinhComponent', () => {
  let component: DanhMucDvHanhChinhComponent;
  let fixture: ComponentFixture<DanhMucDvHanhChinhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanhMucDvHanhChinhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhMucDvHanhChinhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
