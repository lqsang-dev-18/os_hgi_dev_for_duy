import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhMucDanTocComponent } from './danh-muc-dan-toc.component';

describe('DanhMucDanTocComponent', () => {
  let component: DanhMucDanTocComponent;
  let fixture: ComponentFixture<DanhMucDanTocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanhMucDanTocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhMucDanTocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
