import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhMucDonViComponent } from './danh-muc-don-vi.component';

describe('DanhMucDonViComponent', () => {
  let component: DanhMucDonViComponent;
  let fixture: ComponentFixture<DanhMucDonViComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanhMucDonViComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhMucDonViComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
