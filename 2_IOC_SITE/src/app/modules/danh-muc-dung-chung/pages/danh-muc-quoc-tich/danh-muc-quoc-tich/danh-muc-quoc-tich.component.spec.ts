import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhMucQuocTichComponent } from './danh-muc-quoc-tich.component';

describe('DanhMucQuocTichComponent', () => {
  let component: DanhMucQuocTichComponent;
  let fixture: ComponentFixture<DanhMucQuocTichComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanhMucQuocTichComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhMucQuocTichComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
