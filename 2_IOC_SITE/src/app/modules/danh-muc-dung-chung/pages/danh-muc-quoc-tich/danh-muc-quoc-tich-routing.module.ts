import { DanhMucQuocTichComponent } from './danh-muc-quoc-tich/danh-muc-quoc-tich.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: '',
    component: DanhMucQuocTichComponent,
    children: [
      {
        path: '',
        component: DanhMucQuocTichComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})

export class DanhMucQuocTichRoutingModule {}
