import { DanhMucDungChungModule } from './../../danh-muc-dung-chung.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DanhMucQuocTichRoutingModule } from './danh-muc-quoc-tich-routing.module';
import { DanhMucQuocTichComponent } from './danh-muc-quoc-tich/danh-muc-quoc-tich.component';


@NgModule({
  declarations: [DanhMucQuocTichComponent],
  imports: [
    CommonModule,
    DanhMucQuocTichRoutingModule,
    DanhMucDungChungModule
  ],
  exports: [
    DanhMucQuocTichComponent
  ]
})
export class DanhMucQuocTichModule { }
