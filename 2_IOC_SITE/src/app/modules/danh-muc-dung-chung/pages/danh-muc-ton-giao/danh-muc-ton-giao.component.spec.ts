import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DanhMucTonGiaoComponent } from './danh-muc-ton-giao.component';

describe('DanhMucTonGiaoComponent', () => {
  let component: DanhMucTonGiaoComponent;
  let fixture: ComponentFixture<DanhMucTonGiaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DanhMucTonGiaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DanhMucTonGiaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
