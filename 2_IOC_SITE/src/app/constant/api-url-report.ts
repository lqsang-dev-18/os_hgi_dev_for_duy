export const rootUrl = "https://dev-api-report.yteangiang.vn/CSDLYTE_REPORT/api/";
export const apiUrl = {
    PHAN_QUYEN: {
        DANH_SACH_USER: rootUrl + "getDanhSachUser",
        PHAN_QUYEN_REPORT: rootUrl + "getDanhSachUser",
        XOA_QUYEN_REPORT: rootUrl + "getDanhSachUser",
        DANH_SACH_REPORT: rootUrl + "reports",
    },
    DM_REPORT: {
        UPSERT: rootUrl + "insertupdatereport",
    },
    DM_FIELD_REPORT: {
        UPSERT: rootUrl + "insertupdatefieldreport",
    }
}