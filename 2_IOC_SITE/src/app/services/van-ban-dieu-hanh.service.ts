import { apiUrl } from "./../constant/api-url";
import { BaseService } from "./base.service";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class VanBanDieuHanhService extends BaseService {
  //Danh mục đơn vị
  getListDonVi(): Observable<any> {
    return super.get(apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG.LAY_DS_DON_VI);
  }

  //Thống kê 2 chỉ tiêu - thông tin chung
  thongKe2ChiTieu(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG.THONG_KE_2_CHI_TIEU,
      requestData
    );
  }

  //Thống kê văn bản đi đến theo đơn vị
  GetTKVBDiDenTheoDonVi(requestParam): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG.TK_VB_DI_DEN_THEO_DON_VI,
      requestParam
    );
  }

  //Thống kê văn bản đi đến ký số
  GetTKVBDiDenKySo(requestParam): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG.TK_VB_DI_DEN_KY_SO,
      requestParam
    );
  }

  //Thống kê VB đến trễ hạn
  GetTKVBDenTreHan(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG.TK_VB_DEN_TRE_HAN,
      requestData
    );
  }

  //Thống kê tình hình phê duyệt văn bản đến
  GetTKTinhHinhPheDuyetVBDen(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG.TK_TINH_HINH_PHE_DUYET_VB_DEN,
      requestData
    );
  }

  //Thống kê tình hình ký số văn bản đến
  GetTKTinhHinhKySoVBDiDen(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_THANG
        .TK_TINH_HINH_KY_SO_VB_DI_DEN,
      requestData
    );
  }

  //Thống kê tình hình duyệt văn bản đến
  GetTKTinhHinhDuyetVBDen(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_THANG
        .TK_TINH_HINH_DUYET_VB_DEN,
      requestData
    );
  }

  //Thống kê tình hình duyệt văn bản đến
  GetTKTinhHinhTiepNhanVBDen(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_THANG
        .TK_TINH_HINH_TIEP_NHAN_VB_DEN,
      requestData
    );
  }

  //Thống kê tình hình xử lý văn bản đến bị trễ hạn
  GetTKTinhHinhXLVBDenBiTreHan(requestParam): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_THANG
        .TK_HINH_HINH_XL_VB_DEN_BI_TRE_HAN,
      requestParam
    );
  }

  //Thống kê tình hình sử dụng hệ thống
  GetTKTinhHinhSDHeThong(requestParam): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_THANG
        .TK_HINH_HINH_SD_HE_THONG,
      requestParam
    );
  }

  //Thống kê top 5 đơn vị có văn bản đi đến nhiều nhất
  GetTKTop5DonViCoVBDiDenNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_DON_VI
        .TK_TOP_5_DON_VI_CO_VB_DI_DEN_NHIEU_NHAT,
      requestData
    );
  }

  //Thống kê top 5 đơn vị có văn bản đến trễ hạn nhiều nhất
  GetTKTop5DonViCoVBDenTreHanNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_DON_VI
        .TK_TOP_5_DON_VI_CO_VB_DEN_TRE_HAN_NHIEU_NHAT,
      requestData
    );
  }

  //Thống kê top 5 đơn vị có số người truy cập nhiều nhất
  GetTKTop5DonViCoSoNguoiTruyCapNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_DON_VI
        .TK_TOP_5_DON_VI_CO_SO_NGUOI_TRUY_CAP_NHIEU_NHAT,
      requestData
    );
  }

  //Thống kê top 5 đơn vị có nhiều văn bản đến chưa tiếp nhận
  GetTKTop5DonViCoNhieuVBDenChuaTiepNhan(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_DON_VI
        .TK_TOP_5_DON_VI_CO_NHIEU_VB_DEN_CHUA_TIEP_NHAN,
      requestData
    );
  }

  //Danh mục phòng ban
  getListPhongBan(): Observable<any> {
    return super.get(apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG.LAY_DS_PHONG_BAN);
  }

  //Thống kê văn bản đi đến theo phòng ban
  GetTKVBDiDenTheoPhongBan(requestParam): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_PHONG_BAN
        .TK_VB_DI_DEN_THEO_PHONG_BAN,
      requestParam
    );
  }

  //Thống kê top 10 cá nhân nhận văn bản đến nhiều nhất
  GetTKTop10CaNhanNhanVBDenNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_PHONG_BAN
        .TK_TOP_10_CA_NHAN_NHAN_VB_DEN_NHIEU_NHAT,
      requestData
    );
  }

  //Thống kê top 10 cá nhân tham mưu văn bản đi nhiều nhất
  GetTKTop10CaNhanThamMuuVBDiNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_PHONG_BAN
        .TK_TOP_10_CA_NHAN_THAM_MUU_VB_DI_NHIEU_NHAT,
      requestData
    );
  }

  //Thống kê VB đến trễ hạn theo phòng ban
  GetTKVBDenTreHanTheoPhongBan(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.XU_LY_VAN_BAN_THEO_PHONG_BAN
        .TK_VB_DEN_TRE_HAN_THEO_PHONG_BAN,
      requestData
    );
  }

  //Các chỉ số văn bản đến
  GetCacChiSoVBDen(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG_TINH_HINH_XU_LY
        .CAC_CHI_SO_VAN_BAN_DEN,
      requestData
    );
  }

  //Các chỉ số văn bản đến
  GetCacChiSoVBDi(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG_TINH_HINH_XU_LY
        .CAC_CHI_SO_VAN_BAN_DI,
      requestData
    );
  }

  //Thống kê văn bản đến và đến quá hạn theo tháng
  GetTKVBDenvaDenQuaHanTheoThang(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG_TINH_HINH_XU_LY
        .TK_VB_DEN_VA_DEN_QUA_HAN_THEO_THANG,
      requestData
    );
  }

  //Thống kê văn bản liên thông theo tháng
  GetCacChiSoVBDiLienThongTheoThang(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG_TINH_HINH_XU_LY
        .CAC_CHI_SO_VAN_BAN_DI_LIEN_THONG_THEO_THANG,
      requestData
    );
  }

  //Thống kê lượng truy cập theo tháng
  GetTKSoLuongTruyCapTheoThang(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.THONG_TIN_CHUNG_TINH_HINH_XU_LY
        .TK_SO_LUONG_TRUY_CAP_THEO_THANG,
      requestData
    );
  }

  //top 5 don vi co van ban den nhieu nhat
  GetTop5DonViCoVbDenNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TOP5_DON_VI_CO_VB_DEN_NHIEU_NHAT,
      requestData
    );
  }

  // trung binh so luong van ban den
  GetTrungBinhSoLuongVbDen(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TRUNG_BINH_SO_LUONG_VB_DEN,
      requestData
    );
  }

  // top 5 van ban den dung han
  GetTop5DonViCoTyLeXlVbDungHan(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TOP5_DON_VI_CO_VB_DEN_DUNG_HAN_NHIEU_NHAT,
      requestData
    );
  }

  // top 5 van ban den qua han
  GetTop5DonViCoTyLeXlVbQuaHan(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TOP5_DON_VI_CO_VB_DEN_QUA_HAN_NHIEU_NHAT,
      requestData
    );
  }

  // top 5 nguoi dung truy cap thuong xuyen
  GetTop5DonViCoNguoiDungThuongXuyenTruyCap(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TOP5_TRUY_CAP_HE_THONG,
      requestData
    );
  }

  // trung binh so luong van ban di
  GetTrungBinhSoLuongVbDi(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TRUNG_BINH_SO_LUONG_VB_DI,
      requestData
    );
  }

  // top 5 don vi co van ban di nhieu nhat
  GetTop5DonViCoVbDiNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TOP5_DON_VI_CO_VB_DI_NHIEU_NHAT,
      requestData
    );
  }

  // top 5 don vi co van ban lien thong nhieu nhat
  GetTop5DonViCoVbLienThongNhieuNhat(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.HIEU_SUAT_XU_LY_VA_DIEU_HANH
        .TOP5_DON_VI_CO_VB_LIEN_THONG_NHIEU_NHAT,
      requestData
    );
  }

  // lấy số lượng các chỉ số văn bản đến theo đơn vị
  GetCacChiSoVbDenTheoDonVi(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.TK_DON_VI_TRUC_THUOC_SO_Y_TE
        .TK_VB_DEN_THEO_DON_VI,
      requestData
    );
  }

  // lấy số lượng văn bản đi theo đơn vị
  GetSoLuongVbDiTheoDonVi(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.TK_DON_VI_TRUC_THUOC_SO_Y_TE
        .TK_VB_DI_THEO_DON_VI,
      requestData
    );
  }

  //lấy số lượt truy cập hệ thống theo đơn vị
  GetSoLuongTruyCapHeThongTheoDonVi(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.TK_DON_VI_TRUC_THUOC_SO_Y_TE
        .TK_SL_TRUY_CAP_THEO_DON_VI,
      requestData
    );
  }

  //lấy các chỉ số công việc
  GetCacChiSoCongViec(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.QUAN_LY_CONG_VIEC.CONG_VIEC,
      requestData
    );
  }

  //lấy các chỉ số công việc hoàn thành theo đơn vị
  GetCacChiSoCongViecHoanThanhTheoDonVi(requestData): Observable<any> {
    return super.get(
      apiUrl.VAN_BAN_DIEU_HANH.QUAN_LY_CONG_VIEC.CONG_VIEC_DON_VI,
      requestData
    );
  }
}
