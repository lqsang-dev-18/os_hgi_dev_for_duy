import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { apiUrl } from '../constant/api-url';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ChiCucAtvstpService extends BaseService {

  getTKDVAnUong(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TK_DV_AN_UONG, requestData);
  }

  getTLDVAnUong(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TL_DV_AN_UONG, requestData);
  }

  getTKCSKD(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TK_CO_SO_KINH_DOANH, requestData);
  }

  getTLCSKD(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TL_CO_SO_KINH_DOANH, requestData);
  }

  getTKTuCongBo(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TK_TU_CONG_BO_SP, requestData);
  }

  //thanh tra
  getThanhTraTheoLoaiHinh(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TT_THEO_LOAI_HINH, requestData);
  }

  getThanhTraTheoDot(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TT_THEO_DOT, requestData);
  }

  getThanhTraTong(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TT_TONG, requestData);
  }
  getTheTongATVSLD(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.TONG_ATVSLD, requestData);
  }
  getNhomLoaiSPCapGCN(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.NHOM_LOAI_SP_CAP_GCN, requestData);
  }
  getDVAUCapGCN(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.DVAU_CAP_GCN, requestData);
  }
  getNhomLoaiSPHetHan(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.NHOM_LOAI_SP_HET_HAN, requestData);
  }
  getDVAUHetHan(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.DVAU_HET_HAN, requestData);
  }
  getNhomLoaiSPTuCongBo(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.NHOM_LOAI_SP_TU_CONG_BO, requestData);
  }
  getLoaiCoSo(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_ATVSTP.LOAI_CO_SO_CAP_GCN, requestData);
  }
}
