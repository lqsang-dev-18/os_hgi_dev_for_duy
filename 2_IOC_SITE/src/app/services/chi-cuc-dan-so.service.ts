import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { apiUrl } from "../constant/api-url";
import { BaseService } from "./base.service";

@Injectable({
  providedIn: "root",
})
export class ChiCucDanSoService extends BaseService {

  getTKDanSoTheoHuyen(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_DAN_SO.TK_DAN_SO_THEO_HUYEN, requestData);
  }

  getTLDanSoGioiTinh(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_DAN_SO.TL_GIOI_TINH, requestData);
  }
  
  getTLDanSoTTNT(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_DAN_SO.TL_THANHTHI_NONGTHON, requestData);
  }
  
  getTKGTNhomTuoi(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_DAN_SO.TK_GT_NHOM_TUOI, requestData);
  }
  
  getTKSDBPTranhThai(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_DAN_SO.TK_SD_TRANH_THAI, requestData);
  }
  
  getTKSinhConThu3(requestData): Observable<any> {
    return super.get(apiUrl.CHI_CUC_DAN_SO.TK_SINH_CON_THU_3, requestData);
  }
  
}
