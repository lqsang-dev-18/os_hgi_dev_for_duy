import { apiUrl } from "../constant/api-url";
import { BaseService } from './base.service';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ThanhTraSYTService extends BaseService {

  //get 4 the tong
  getThanhTraYte(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.THANH_TRA_SYT, requestParam);
  }
  getTongPhamVi(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.TONG_VI_PHAM, requestParam);
  }
  getThanhTraKiemTraCK(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.THANH_TRA_KIEM_TRA_CK, requestParam);
  }
  getThanhTraKiemToChucCK(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.THANH_TRA_KIEM_TRA_TC_CK, requestParam);
  }
  getThanhTraKiemCaNhanCK(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.THANH_TRA_KIEM_TRA_CN_CK, requestParam);
  }
  getTienPhatSinhLKCK(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.TIEN_PHAT_SINH_LUYKE_CK, requestParam);
  }
  getDaiDienTrenMotDon(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.DAI_DIEN_TREN_MOT_DON, requestParam);
  }
  getPhanLoaiTheoTinhTrang(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.PHAN_LOAI_THEO_TINH_TRANG, requestParam);
  }
  getPhanLoaiTheoNoiDung(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.PHAN_LOAI_THEO_NOI_DUNG, requestParam);
  }
  getKQXLyThuocThamQuyen(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.KET_QUA_XU_LY_THUOC_THAM_QUYEN, requestParam);
  }
  getKQXLyKhongThuocThamQuyen(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.KET_QUA_XU_LY_KHONG_THUOC_THAM_QUYEN, requestParam);
  }
  get6TheTong(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.XU_LY_DON_6_THE_TONG, requestParam);
  }
  getNoiDungCaNhanViPhamCungKy(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.NOIDUNG_CANHAN_VIPHAM_CUNGKY, requestParam);
  }
  getNoiDungTienViPhamCungKy(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.NOIDUNG_TIEN_VIPHAM_CUNGKY, requestParam);
  }
  getNoiDungDoiTuongKhoiToCungKy(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.NOIDUNG_DOITUONG_KHOITO_CUNGKY, requestParam);
  }
  getNoiDungTienViPhamDonVi(requestParam): Observable<any> {
    return super.get(apiUrl.THANH_TRA_YTE.NOIDUNG_TIEN_VIPHAM_DONVI, requestParam);
  }
  get4TheTongLinhVuc(requestParam):Observable<any>{
    return super.get(apiUrl.THANH_TRA_YTE.THANH_TRA_LINH_VUC_4_THE_TONG,requestParam);
  }
  getLinhVucViPhamCungKy(requestParam):Observable<any>{
    return super.get(apiUrl.THANH_TRA_YTE.LINH_VUC_VI_PHAM_CUNG_KY,requestParam);
  }
}
