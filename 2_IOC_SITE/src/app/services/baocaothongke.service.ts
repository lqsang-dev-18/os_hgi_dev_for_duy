import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { apiUrl, rootUrl } from "../constant/api-url";
import { POST_BODY_TYPE } from "../constant/system-constant";
import { BaseService } from "./base.service";

@Injectable({
  providedIn: "root",
})
export class BCTKService extends BaseService {
    layDSXa(requestData): Observable<any> {
        return super.get(apiUrl.BCTK.LAY_DS_XA,requestData)
    }
    layDSHuyen(requestData): Observable<any> {
        return super.get(apiUrl.BCTK.LAY_DS_HUYEN,requestData)
    }
    layDSCSKCB(requestData): Observable<any> {
        return super.get(apiUrl.BCTK.LAY_DS_CSKCB, requestData);
    }
    layDSMauBC(requestData): Observable<any> {
        return super.get(apiUrl.BCTK.LAY_DS_MAU_BC, requestData);
    }
    loadBaoCao(linkAPI,requestData): Observable<any> {
        return super.get(rootUrl + linkAPI, requestData);
    }
    // baoCaoMau02(requestData): Observable<any> {
    //     return super.get(apiUrl.BCTK.BC_HD_KCB_MAU_02, requestData);
    // }
    // baoCaoMau03(requestData): Observable<any> {
    //     return super.get(apiUrl.BCTK.BC_HD_DIEU_TRI_MAU_03, requestData);
    // }
    // baoCaoMau06(requestData): Observable<any> {
    //     return super.get(apiUrl.BCTK.BC_HD_CLS_MAU_06, requestData);
    // }
    // baoCaoMau14(requestData): Observable<any> {
    //     return super.get(apiUrl.BCTK.BC_BENH_TAT_TU_VONG_MAU_14, requestData);
    // }
    
}
