import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PieHighchartComponent } from './pie-highchart.component';

describe('PieHighchartComponent', () => {
  let component: PieHighchartComponent;
  let fixture: ComponentFixture<PieHighchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PieHighchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PieHighchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
