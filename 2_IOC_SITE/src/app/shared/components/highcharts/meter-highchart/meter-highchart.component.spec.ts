import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeterHighchartComponent } from './meter-highchart.component';

describe('MeterHighchartComponent', () => {
  let component: MeterHighchartComponent;
  let fixture: ComponentFixture<MeterHighchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeterHighchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeterHighchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
