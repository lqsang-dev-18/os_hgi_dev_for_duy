import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxTreeCheckboxesComponent } from './ngx-treecheckboxes.component';

describe('NgxTreeCheckboxesComponent', () => {
  let component: NgxTreeCheckboxesComponent;
  let fixture: ComponentFixture<NgxTreeCheckboxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgxTreeCheckboxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxTreeCheckboxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
