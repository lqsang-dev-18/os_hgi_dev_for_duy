﻿namespace AGG.IOC.YTE.Common.Response
{
    public class ResponsePageList : ResponseList
    {
        public int total_page { get; set; }
        public int total_row { get; set; }
    }
}
