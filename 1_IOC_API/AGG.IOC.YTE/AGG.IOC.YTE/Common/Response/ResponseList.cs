﻿
namespace AGG.IOC.YTE.Common.Response
{
    public class ResponseList
    {
        public bool success { get; set; }
        public string? message { get; set; }
        public IEnumerable<dynamic>? data { get; set; }
    }
}