﻿
namespace AGG.IOC.YTE.Common.Response
{
    public class ResponseExecute
    {
        public int id { get; set; }
        public bool success { get; set; }
        public string? message { get; set; }
    }
}