﻿
namespace AGG.IOC.YTE.Common.Response
{
    public class ResponseSingle
    {
        public bool success { get; set; }
        public string? message { get; set; }
        public dynamic? data { get; set; }
    }
}