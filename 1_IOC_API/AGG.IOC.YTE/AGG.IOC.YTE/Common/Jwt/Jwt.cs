﻿using AGG.IOC.YTE.Models.Authentication;
using Microsoft.Net.Http.Headers;

namespace AGG.IOC.YTE.Common.Jwt
{
    public class Jwt
    {
        public static UserToken DecodeToken(HttpRequest request)
        {
            string accessToken = request.Headers[HeaderNames.Authorization];
            var handler = new System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(accessToken.Replace("Bearer ", ""));
            return new UserToken(
                token.Claims.First(claim => claim.Type == "MA_USER").Value ?? "",
                token.Claims.First(claim => claim.Type == "ID_LOG_IN").Value ?? "",
                token.Claims.First(claim => claim.Type == "LEVEL_USER").Value ?? "",
                token.Claims.First(claim => claim.Type == "MA_DON_VI").Value ?? "");
        }

        public static string GetAccessToken(HttpRequest request)
        {
            var accessToken = request.Headers[HeaderNames.Authorization];
            return accessToken;
        }
    }
}
