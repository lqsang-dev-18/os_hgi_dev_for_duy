﻿namespace AGG.IOC.YTE.Common.FileHelper
{
    public class JsonHelper
    {
        public static List<object> JsonToOjectArray(string dir)
        {
            string json = File.ReadAllText(dir);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<List<object>>(json) ?? new List<object>();
        }
    }
}
