﻿using Newtonsoft.Json;
using System.Xml;

namespace AGG.IOC.YTE.Common.FileHelper
{
    public class XmlHelper
    {
        public static string XmlToJson(string dir)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(dir);
            return JsonConvert.SerializeXmlNode(doc);
        }

        // Convert File Xml to Object
        // filepath: đường dẫn chứa file Xml
        public static T DeserializeToObject<T>(string filepath) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StreamReader sr = new StreamReader(filepath))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        // Convert String to Object
        // input: nội dung trong file Xml
        public static T DeserializeByStringToObject<T>(string input) where T : class
        {
            System.Xml.Serialization.XmlSerializer ser = new System.Xml.Serialization.XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }
    }
}