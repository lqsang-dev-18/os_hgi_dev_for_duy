﻿using ExcelDataReader;
using System.Data;

namespace AGG.IOC.YTE.Common.FileHelper
{
    internal class ExcelToDataTable
    {
        public static DataTable? Convert(string path)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            DataSet result = new DataSet();
            try
            {
                FileStream fs = File.Open(path, FileMode.Open, FileAccess.Read);

                // Check extention is XLS, XLSX
                IExcelDataReader reader;
                if (Path.GetExtension(path).ToLower() == ".xls")
                {
                    reader = ExcelReaderFactory.CreateBinaryReader(fs);
                    result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });
                    reader.Close();
                }
                else if (Path.GetExtension(path).ToLower() == ".xlsx")
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
                    result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    });
                    reader.Close();
                }
            }
            catch (Exception)
            {
                return null;
            }
            return result.Tables[0];
        }

        public static DataTable? ConvertXLSX(string Path)
        {
            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            DataSet result = new DataSet();
            try
            {
                FileStream fs = File.Open(Path, FileMode.Open, FileAccess.Read);
                IExcelDataReader reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
                result = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
                reader.Close();
            }
            catch (Exception)
            {
                return null;
            }
            return result.Tables[0];
        }
    }
}