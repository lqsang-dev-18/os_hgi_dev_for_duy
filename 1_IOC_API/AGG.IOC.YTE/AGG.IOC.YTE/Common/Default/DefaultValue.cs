﻿using System;
namespace AGG.IOC.YTE.Common.Default
{
    public class DefaultValue
    {
        public string? Values { get; set; }

        public DefaultValue(string key)
        {
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
            IConfiguration configuration = builder.Build();
            Values = configuration.GetValue<string>("Default:" + key);
        }
    }
}

