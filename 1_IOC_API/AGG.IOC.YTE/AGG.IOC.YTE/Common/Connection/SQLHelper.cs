﻿using AGG.IOC.YTE.Common.Response;
using Dapper;
using Dapper.Oracle;
using Newtonsoft.Json.Linq;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Diagnostics;
using System.Reflection;

namespace AGG.IOC.YTE.Common.Connection
{
    public class SQLHelper : IDisposable
    {
        private string? ConnectionString { get; set; }

        public SQLHelper(string? type = null)
        {
            if(type == null)
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
                IConfiguration configuration = builder.Build();
                ConnectionString = configuration.GetValue<string>("ConnectionStrings:CSDL.YTE");
            }
            else
            {
                var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: false);
                IConfiguration configuration = builder.Build();
                ConnectionString = configuration.GetValue<string>("ConnectionStrings:CSDL.YTE."+type); 
            }
        }

        public ResponseList GetList(string store, OracleDynamicParameters? param = null)
        {
            using (var db = new OracleConnection(ConnectionString))
            {
                var Response = new ResponseList();                
                try
                {
                    // Trường hợp Controller không truyền Parameters, cần khởi tạo giá trị này
                    if (param == null)
                    {
                        param = new OracleDynamicParameters();
                    }

                    param.Add("P_RS", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
                    Response.data = db.Query<dynamic>(store, param, commandType: CommandType.StoredProcedure);

                    if (Response.data.Count() == 0)
                    {
                        Response.message = "Không tìm thấy dữ liệu";
                    }
                    else
                    {
                        Response.message = "Lấy dữ liệu thành công";
                    }
                    Response.success = true;
                }
                catch (Exception ex)
                {
                    Response.data = new List<dynamic>();
                    Response.message = ex.Message;
                    Response.success = false;
                }
                finally
                {
                    db.Close();
                }
                return Response;
            }
        }

        public ResponseList GetList(string store, object obj)
        {
            using (var db = new OracleConnection(ConnectionString))
            {
                var Response = new ResponseList();
                var param = new OracleDynamicParameters();
                try
                {
                    param.Add("P_RS", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        string key = "P_" + prop.Name.ToUpper();
                        if (key.IndexOf("_TEMP") == -1)
                        {
                            var value = prop.GetValue(obj);
                            if (key.IndexOf("_OUT") != -1)
                            {
                                param.Add(key, value, direction: ParameterDirection.Output);
                            }
                            else if (key.IndexOf("_INOUT") != -1)
                            {
                                param.Add(key, value, direction: ParameterDirection.InputOutput);
                            }
                            else
                            {
                                param.Add(key, value);
                            }
                        }
                    }

                    var data = db.Query<dynamic>(store, param, commandType: CommandType.StoredProcedure);
                    Response.data = data;
                    if (data.Count() == 0)
                    {
                        Response.message = "Không tìm thấy dữ liệu";
                    }
                    else
                    {
                        Response.message = "Lấy dữ liệu thành công";
                    }
                    Response.success = true;
                }
                catch (Exception ex)
                {
                    Response.data = new List<dynamic>();
                    Response.message = ex.Message;
                    Response.success = false;
                }
                finally
                {
                    db.Close();
                }

                return Response;
            }
        }

        public ResponsePageList GetPageList(string store, object obj)
        {
            using (var db = new OracleConnection(ConnectionString))
            {
                var Response = new ResponsePageList();
                var param = new OracleDynamicParameters();
                try
                {
                    param.Add("P_RS", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        string key = "P_" + prop.Name.ToUpper();
                        var value = prop.GetValue(obj);
                        if (key.IndexOf("_OUT") != -1)
                        {
                            param.Add(key, value, direction: ParameterDirection.Output);
                        }
                        else if (key.IndexOf("_INOUT") != -1)
                        {
                            param.Add(key, value, direction: ParameterDirection.InputOutput);
                        }
                        else
                        {
                            param.Add(key, value);
                        }
                    }

                    Response.data = db.Query<dynamic>(store, param, commandType: CommandType.StoredProcedure);
                    Response.success = true;

                    int total_row;
                    if (Response.data.Count() > 0)
                    {
                        Response.message = "Lấy dữ liệu thành công";
                        total_row = (int)(Response.data.First().TOTAL_ROW);
                    }
                    else
                    {
                        Response.message = "Không tìm thấy dữ liệu";
                        total_row = 0;
                    }
                                        
                    int size = param.Get<int>("P_SIZE");
                    int total_page = total_row / size;
                    total_page = (total_row % size == 0) ? total_page : (total_page + 1);
                    Response.total_page = total_page;
                    Response.total_row = total_row;
                }
                catch (Exception ex)
                {
                    Response.data = new List<dynamic>();
                    Response.message = ex.Message;
                    Response.success = false;
                }
                finally
                {
                    db.Close();
                }

                return Response;
            }
        }

        public ResponseSingle GetSingle(string store, OracleDynamicParameters? param = null)
        {
            using (var db = new OracleConnection(ConnectionString))
            {
                var Response = new ResponseSingle();
                try
                {
                    // Trường hợp Controller không truyền Parameters, cần khởi tạo giá trị này
                    if (param == null)
                    {
                        param = new OracleDynamicParameters();
                    }
                    param.Add("P_RS", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);

                    var data = db.Query<dynamic>(store, param, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Response.data = data;
                    Response.message = "Lấy dữ liệu thành công";
                    Response.success = true;
                }
                catch (Exception ex)
                {
                    Response.data = null;
                    Response.message = ex.Message;
                    Response.success = false;
                }
                finally
                {
                    db.Close();
                }

                return Response;
            }
        }

        public ResponseSingle GetSingle(string store, object obj)
        {
            using (var db = new OracleConnection(ConnectionString))
            {
                var param = new OracleDynamicParameters();
                var Response = new ResponseSingle();
                try
                {
                    param.Add("P_RS", dbType: OracleMappingType.RefCursor, direction: ParameterDirection.Output);
                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        string key = "P_" + prop.Name.ToUpper();
                        if (key.IndexOf("_TEMP") == -1)
                        {
                            var value = prop.GetValue(obj);
                            if (key.IndexOf("_OUT") != -1)
                            {
                                param.Add(key, value, direction: ParameterDirection.Output);
                            }
                            else if (key.IndexOf("_INOUT") != -1)
                            {
                                param.Add(key, value, direction: ParameterDirection.InputOutput);
                            }
                            else
                            {
                                param.Add(key, value);
                            }
                        }
                    }

                    var data = db.Query<dynamic>(store, param, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    Response.data = data;
                    Response.message = "Lấy dữ liệu thành công";
                    Response.success = true;
                }
                catch (Exception ex)
                {
                    Response.data = null;
                    Response.message = ex.Message;
                    Response.success = false;
                }
                finally
                {
                    db.Close();
                }

                return Response;
            }
        }

        public ResponseExecute Execute(string store, OracleDynamicParameters? param = null)
        {
            using (var db = new OracleConnection(ConnectionString))
            {
                var Response = new ResponseExecute();
                try
                {
                    db.Execute(store, param, commandType: CommandType.StoredProcedure);
                    Response.id = 1;
                    Response.success = true;
                    Response.message = "Thực thi thành công";
                }
                catch (Exception ex)
                {
                    Response.id = -1;
                    Response.success = false;
                    Response.message = ex.Message;
                }
                finally
                {
                    db.Close();
                }

                return Response;
            }
        }

        public ResponseExecute Execute(string store, object obj)
        {
            using (var db = new OracleConnection(ConnectionString))
            {
                var Response = new ResponseExecute();
                var param = new OracleDynamicParameters();
                try
                {
                    string key_out = "";
                    int count_out = 0;
                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        string key = "P_" + prop.Name.ToUpper();
                        var value = prop.GetValue(obj);
                        if (key.IndexOf("_INOUT") != -1)
                        {
                            param.Add(key, value, direction: ParameterDirection.InputOutput);
                            key_out = key;
                            count_out++;
                        }
                        else if (key.IndexOf("_OUT") != -1)
                        {
                            param.Add(key, value, direction: ParameterDirection.Output);
                            key_out = key;
                            count_out++;
                        }
                        else
                        {
                            param.Add(key, value);
                        }
                    }

                    db.Execute(store, param, commandType: CommandType.StoredProcedure);

                    int id = 0;
                    if (count_out == 1)
                    {
                        try
                        {
                            id = param.Get<int>(key_out);
                        }
                        catch
                        {
                            id = 0;
                        }
                    }

                    Response.id = id;
                    Response.success = true;
                    Response.message = "Thực thi thành công";
                }
                catch (Exception ex)
                {
                    Response.id = -1;
                    Response.success = false;
                    Response.message = ex.Message;
                }
                finally
                {
                    db.Close();
                }

                return Response;
            }
        }
       
        public void Dispose()
        {
        }
    }
}