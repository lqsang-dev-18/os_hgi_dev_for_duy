﻿using AGG.IOC.YTE.Common.Jwt;
using AGG.IOC.YTE.Models.Authentication;
using AGG.IOC.YTE.Repository.PhanQuyen;
using AGG.IOC.YTE.Repository.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net.NetworkInformation;
using System.Security.Claims;
using System.Text;

using System.Text.Json;
using AGG.IOC.YTE.Common.Default;

namespace AGG.IOC.YTE.Controllers.Authentication
{
    //[Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public IConfiguration _configuration;

        public AuthController(IConfiguration config)
        {
            _configuration = config;
        }

        [HttpPost("api/get-token")]
        public IActionResult Login ([FromForm] string username, [FromForm] string password)
        {

            var response = AuthRepository.XAC_THUC_USER(username, password, HttpContext.Connection.RemoteIpAddress?.ToString() ?? "", System.Net.Dns.GetHostName().ToString(), HttpContext.Connection.LocalIpAddress?.ToString() ?? "");
            if (response.data != null)
            {
                string status = response.data.STATUS.ToString();

                if (status == "1")
                {
                    var accessToken = GenerateJSONWebToken(response.data.MA_USER.ToString(), response.data.ID_LOG_IN.ToString(), response.data.LEVEL_USER.ToString(), response.data.MA_DON_VI);
                    SetJWTCookie(accessToken);
                    return Ok(new Token(true, "Cấp Token thành công.", "Bearer Token", "180", accessToken));
                }
                else if (status == "0")
                {
                    return BadRequest(new Token(false, "Tài khoản đã bị khoá !", "", "", ""));
                }
            }
            return BadRequest(new Token(false, "Tài khoản hoặt mật khẩu chưa đúng !", "", "", ""));
        }

        private string GenerateJSONWebToken(string ma_user, string id_log_in, string level_user, string ma_don_vi)
        {
            var claims = new[] {
                        new Claim("MA_USER", ma_user),
                        new Claim("ID_LOG_IN", id_log_in),
                        new Claim("LEVEL_USER", level_user),
                        new Claim("MA_DON_VI", ma_don_vi)
                    };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Issuer"],
                audience: _configuration["Jwt:Audience"],
                claims: claims,
                expires: DateTime.Now.AddHours(3),
                signingCredentials: credentials
                );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private void SetJWTCookie(string token)
        {
            string duration = new DefaultValue("Token_Duration").Values ?? "0";
            var cookieOptions = new CookieOptions
            {
                HttpOnly = true,
                Expires = DateTime.UtcNow.AddHours(Int32.Parse(duration)),
            };
            Response.Cookies.Append("jwtCookie", token, cookieOptions);
        }

        [Route("api/user/lay-list-menu-ioc")]
        [HttpGet]
        public IActionResult GET_LIST_MENU_BY_USER()
        {
            // Decode Jwt
            UserToken us = Jwt.DecodeToken(Request);

            //Thực hiện yêu cầu
            var response = RolesRepository.PROC_LAY_MENU_USER(Convert.ToInt32(us.MA_USER ?? "-1"));

            if (response.success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }

        [Route("api/user/lay-combobox-don-vi")]
        [HttpGet]
        public IActionResult GET_LIST_DM_DON_VI()
        {
            // Decode Jwt
            UserToken us = Jwt.DecodeToken(Request);

            //Thực hiện yêu cầu
            var response = RolesRepository.GET_LIST_DM_DONVI(us.MA_DON_VI ?? "");

            if (response.success)
            {
                return Ok(response);
            }
            return BadRequest(response);
        }

        [Route("api/user/lay-thong-tin-chi-tiet-nguoi-dung")]
        [HttpGet]
        public IActionResult GET_USERS_THONGTIN(int ma_user)
        {
            // Decode Jwt
            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            //Thực hiện yêu cầu
            var response = RolesRepository.GET_USERS_THONGTIN(ma_user, us.MA_DON_VI ?? "", us.LEVEL_USER);

            if (response.data != null)
            {
                return Ok(response);
            }
            response.success = true;
            response.message = "Không có dữ liệu !";
            return BadRequest(response);

        }
    }
}