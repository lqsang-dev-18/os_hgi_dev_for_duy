﻿using AGG.IOC.YTE.Repository.BaoCaoThongKe;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.BaoCaoThongKe
{
    [Authorize]
    [ApiController]
    public class NghiepVuYController : ControllerBase
    {
        public IConfiguration _configuration;
        public NghiepVuYController(IConfiguration config)
        {
            _configuration = config;
        }

        [HttpGet("api/get-ds-huyen")]
        public IActionResult GET_DS_HUYEN(string? gia_tri)
        {
            //api phục vụ tính năng tra cứu dữ liệu 831, không xác thực user
            var ma_tinh = _configuration["Default:Ma_Tinh"];
            var response = NghiepVuYRepository.PROC_LAY_DS_HUYEN(gia_tri, Convert.ToInt32(ma_tinh));
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("api/get-ds-xa")]
        public IActionResult GET_DS_XA(string? gia_tri, int ma_huyen)
        {
            //api phục vụ tính năng tra cứu dữ liệu 831, không xác thực user
            var ma_tinh = _configuration["Default:Ma_Tinh"];
            var response = NghiepVuYRepository.PROC_LAY_DS_XA(gia_tri, ma_huyen, Convert.ToInt32(ma_tinh));
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("api/get-ds-cskcb")]
        public IActionResult GET_DS_CSKCB(string? gia_tri)
        {
            //api phục vụ tính năng tra cứu dữ liệu, không xác thực user
            var response = NghiepVuYRepository.PROC_LAY_DS_CSKCB(gia_tri);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("api/get-ds-mau-bao-cao")]
        public IActionResult GET_DS_MAU_BAO_CAO(int idLinhVuc)
        {
            //api phục vụ tính năng tra cứu dữ liệu, không xác thực user
            var response = NghiepVuYRepository.PROC_LAY_DS_MAU_BAO_CAO(idLinhVuc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
