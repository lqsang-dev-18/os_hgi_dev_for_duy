﻿using AGG.IOC.YTE.Common.Response;
using AGG.IOC.YTE.Repository.Reports.NghiepVuY;
using AGG.IOC.YTE.Repository.Reports.TamThanKinh_Phong_DaLieu;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Reports.TamThanKinh_Phong_DaLieu
{
    [Route("api/reports/tam-than-kinh-phong-da-lieu")]
    [ApiController]
    [Authorize]
    public class TamThanKinh_Phong_DaLieuController : ControllerBase
    {
        private TamThanKinh_Phong_DaLieuRepository baoCaoRepository;

        public TamThanKinh_Phong_DaLieuController()
        {
            baoCaoRepository = new TamThanKinh_Phong_DaLieuRepository();
        }

        [HttpGet("view-tam-than-kinh-phong-dalieu")]
        public IActionResult VIEW_TAMTHANKINH_PHONG_DALIEU(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DM_TAM_THAN_PHONG_DALIEU";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page,size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("view-phat-hien-benh-moi")]
        public IActionResult VIEW_PHATHIEN_BENHMOI(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DS_PHAT_HIEN_BENH_MOI";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page, size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("view-benh-nhan-quan-ly")]
        public IActionResult VIEW_BENHNHAN_QUANLY(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DS_BENH_NHAN_QUAN_LY";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page, size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("view-benh-nhan-DHTL")]
        public IActionResult VIEW_BENHNHAN_DHTL(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DS_BENH_NHAN_DHTL";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page, size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("view-benh-nhan-hoan-thanh-DHTL")]
        public IActionResult VIEW_BENHNHAN_HOANTHANH_DHTL(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DS_BENH_NHAN_HOAN_THANH_DHTL";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page, size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("view-benh-nhan-giam-sat")]
        public IActionResult VIEW_BENHNHAN_GIAMSAT(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DS_BENH_NHAN_GIAM_SAT";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page, size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("view-nguoi-tiep-xuc-BN-phong")]
        public IActionResult VIEW_NGUOITIEPXUC_BENHNHAN_PHONG(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DS_NGUOI_TIEP_XUC_BN_PHONG";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page, size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("view-benh-nhan-STI")]
        public IActionResult VIEW_BENHNHAN_STI(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size)
        {
            //Tên PROC
            string nameProc = "PROC_GET_PAGE_LIST_DS_BENH_NHAN_STI";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = TamThanKinh_Phong_DaLieuRepository.GET_LIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = TamThanKinh_Phong_DaLieuRepository.GET_PAGELIST_BAOCAO_ALL(keyword, tungay, denngay, thang, quy, nam, page, size, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }
    }
}
