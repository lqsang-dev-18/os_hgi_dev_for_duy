﻿using AGG.IOC.YTE.Common.Response;
using AGG.IOC.YTE.Repository.DanhMucChung;
using AGG.IOC.YTE.Repository.Reports.NghiepVuY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Reports.NghiepVuY
{
    [Route("api/reports/nghiep-vu-y/syt")]
    [ApiController]
    [Authorize]
    public class BaoCaoController : Controller
    {
        private BaoCaoRepository baoCaoRepository;

        public BaoCaoController()
        {
            baoCaoRepository = new BaoCaoRepository();
        }

        [HttpGet("hoat-dong-kcb-b02")]
        public IActionResult THONGKE_HOATDONG_KCB_B02(string tungay, string denngay, string? ma_huyen, string? ma_xa, string? macsyt, int? page, int? size, int type, int? nam, int? thang)
        {
            //Tên PROC
            string nameProc = "PROC_RP_HOATDONG_KCB_B02";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = BaoCaoRepository.GET_LIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, nam, thang, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = BaoCaoRepository.GET_PAGELIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, page, size, nam, thang, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("hoat-dong-dieu-tri-b03")]
        public IActionResult THONGKE_HOATDONG_DIEUTRI_B03(string tungay, string denngay, string? ma_huyen, string? ma_xa, string? macsyt, int? page, int? size, int type, int? nam, int? thang)
        {
            //Tên PROC
            string nameProc = "PROC_RP_HOATDONG_DIEUTRI_B03";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = BaoCaoRepository.GET_LIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, nam, thang, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = BaoCaoRepository.GET_PAGELIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, page, size, nam, thang, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("hoat-dong-cls-b06")]
        public IActionResult THONGKE_HOATDONG_CLS_B06(string tungay, string denngay, string? ma_huyen, string? ma_xa, string? macsyt, int? page, int? size, int type, int? nam, int? thang)
        {
            //Tên PROC
            string nameProc = "PROC_RP_HOATDONG_CLS_B06";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = BaoCaoRepository.GET_LIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, nam, thang, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = BaoCaoRepository.GET_PAGELIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, page, size, nam, thang, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }

        [HttpGet("benh-tat-tu-vong-b14")]
        public IActionResult THONGKE_BENHTAT_TUVONG_B14(string tungay, string denngay, string? ma_huyen, string? ma_xa, string? macsyt, int? page, int? size, int type, int? nam, int? thang)
        {
            //Tên PROC
            string nameProc = "PROC_RP_BENHTAT_TUVONG_B14";
            var response_list = new ResponseList();
            var response_pageList = new ResponsePageList();
            if (page is null)
            {
                response_list = BaoCaoRepository.GET_LIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, nam, thang, nameProc);
                if (response_list.success)
                {
                    return Ok(response_list);
                }
            }
            else
            {
                response_pageList = BaoCaoRepository.GET_PAGELIST_BAOCAO_ALL(tungay, denngay, ma_huyen, ma_xa, macsyt, page, size, nam, thang, nameProc);
                if (response_pageList.success)
                {
                    return Ok(response_pageList);
                }
            }
            return BadRequest(page is null ? response_list : response_pageList);
        }
    }
}
