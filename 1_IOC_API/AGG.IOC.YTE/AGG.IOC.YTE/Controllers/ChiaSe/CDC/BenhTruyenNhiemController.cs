﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.ChiaSe.CDC
{
    [Authorize]
    [Route("api/chia-se/cdc/benh-truyen-nhiem")]
    [ApiController]
    public class BenhTruyenNhiemController : ControllerBase
    {
        private BenhTruyenNhiemRepository benhTruyenNhiemRepository;

        public BenhTruyenNhiemController()
        {
            benhTruyenNhiemRepository = new BenhTruyenNhiemRepository();
        } 
        [HttpGet("tk-chi-tieu-cong-tac-tiem-chung-mr")]
        public IActionResult GetListTKChiTieuCongTacTCMR(int nam, int thang, string? maHuyen)
        {
            string nameProc = "PROC_TK_CHITIEU_THUCHIEN_TIEMCHUNG_MR";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, maHuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-cong-tac-phong-chong-dai-thao-duong")]
        public IActionResult GetListTKCongTacPhongChongDTD(int nam, int thang, string? maHuyen)
        {
            string nameProc = "PROC_TK_CT_PC_DAI_THAO_DUONG";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, maHuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-cong-tac-phong-chong-roi-loan-do-thieu-i-ot")]
        public IActionResult GetListTKCongTacPCRoiLoanDoThieuIOT(int nam, int thang, string? maHuyen)
        {
            string nameProc = "PROC_TK_CT_PC_THIEU_IOT";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, maHuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-phong-chong-hen-phe-quan")]
        public IActionResult GetListTKPhongChongHenPheQuan(int nam, int thang, string? maHuyen)
        {
            string nameProc = "PROC_TK_CT_PC_HEN_PHE_QUAN";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, maHuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
