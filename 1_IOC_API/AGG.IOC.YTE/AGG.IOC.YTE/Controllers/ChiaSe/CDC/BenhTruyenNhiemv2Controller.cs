﻿using AGG.IOC.YTE.Repository.ChiaSe;
using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.ChiaSe.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/benh-truyen-nhiem-v2")]
    public class BenhTruyenNhiemv2Controller : Controller
    {
        private BenhTruyenNhiemv2Repository benhTruyenNhiemRepository;

        public BenhTruyenNhiemv2Controller()
        {
            benhTruyenNhiemRepository = new BenhTruyenNhiemv2Repository();
        }

        [HttpGet("sl-tieu-chay")]
        public IActionResult GetListSLSotRet(int? loai_cbx, int nam, int? thang, int? quy, int huyen)
        {
            string nameProc = "PKG_BENH_TRUYEN_NHIEM.PROC_SL_BENH_TIEU_CHAY";
            var response = benhTruyenNhiemRepository.GetThongKeCDCBenhTieuChay(loai_cbx, nam, thang, quy, huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-sl-mac-viem-gan-c-do-virus")]
        public IActionResult GetListViemGanC(int loai_cbx, int quy, int nam, int thang, int huyen)
        {
            string nameProc = "PKG_BENH_TRUYEN_NHIEM.PROC_SL_VIEM_GAN_C_DO_VIRUS";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(loai_cbx, quy, nam, thang, huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-cong-tac-phong-chong-suy-dinh-duong-vitamin-a")]
        public IActionResult GetListTKCongTacPCSuyDinhDuongVitaminA(int nam, int thang, string? huyen)
        {
            string nameProc = "PROC_TK_CT_PC_SUY_DINH_DUONG_VITAMIN_A";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-cong-tac-phong-chong-tang-huyet-ap")]
        public IActionResult GetListTKCongTacPCTangHuyetAp(int nam, int thang, string? huyen)
        {
            string nameProc = "PROC_TK_CT_PC_TANG_HUYET_AP";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-sl-sot-phat-ban-nghi-soi")]
        public IActionResult GetListSLSotPhatBanNghiSoi(int loai_cbx, int quy, int nam, int thang, int huyen)
        {
            string nameProc = "PKG_BENH_TRUYEN_NHIEM.PROC_SL_SOT_PHATBAN_NGHISOI";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(loai_cbx, quy, nam, thang, huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-sl-mac-covid")]
        public IActionResult GetListTKSoCaMacCovid(int loai_cbx, int quy, int nam, int thang, int huyen)
        {
            string nameProc = "PKG_BENH_TRUYEN_NHIEM.PROC_SL_BENH_COVID";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(loai_cbx, quy, nam, thang, huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-phong_chong-tieu-chay")]
        public IActionResult GetListTKPhongChongTieuChay(int loai_cbx, int quy, int nam, int thang, int huyen)
        {
            string nameProc = "PKG_THONG_KE_CHI_TIEU.PROC_TK_CHI_TIEU_PC_TIEU_CHAY";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(loai_cbx, quy, nam, thang, huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
