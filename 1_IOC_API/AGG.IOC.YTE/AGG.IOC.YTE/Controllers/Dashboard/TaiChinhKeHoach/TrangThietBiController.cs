﻿using AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach;
using AGG.IOC.YTE.Repository.Dashboard.ToChucCanBo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.TaiChinhKeHoach
{
    [ApiController]
    [Authorize]
    [Route("api/tai-chinh-ke-hoach/trang-thiet-bi")]
    public class TrangThietBiController : Controller
    {
        private TrangThietBiRepository trangThietBiRepository;
        public TrangThietBiController()
        {
            trangThietBiRepository = new TrangThietBiRepository();
        }
        [HttpGet("7-the-tong")]
        public IActionResult GetList7TheTong(string? madonvi, string? nam)
        {
            string nameProc = "PROC_TTB_7_THE_TONG";
            var response = trangThietBiRepository.GetListTrangThietBi(madonvi, nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nam-su-dung")]
        public IActionResult GetListNamSuDung(string? madonvi, string? nam)
        {
            string nameProc = "PROC_TTB_DONVI_NAM_SU_DUNG";
            var response = trangThietBiRepository.GetListTrangThietBi(madonvi, nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nguon-goc")]
        public IActionResult GetListNguonGoc(string? madonvi, string? nam)
        {
            string nameProc = "PROC_TTB_DONVI_NGUON_GOC";
            var response = trangThietBiRepository.GetListTrangThietBi(madonvi, nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("don-vi-so-luong")]
        public IActionResult GetListDonViSoLuong(string? madonvi, string? nam)
        {
            string nameProc = "PROC_TTB_DONVI_SOLUONG";
            var response = trangThietBiRepository.GetListTrangThietBi(madonvi, nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("trang-thai")]
        public IActionResult GetListTrangThai(string? madonvi, string? nam)
        {
            string nameProc = "PROC_TTB_DONVI_TOT_HU_KHAC";
            var response = trangThietBiRepository.GetListTrangThietBi(madonvi, nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("list-trang-thiet-bi-don-vi")]
        public IActionResult GetPageListTTBByDonVi(string? nam, string? maDonVi, int? trangThai, int? nguonGoc, int? page, int? size)
        {
            string nameProc = "PROC_TTB_LIST_TTB";
            var response = trangThietBiRepository.GetPageListTrangThietBi(nam, maDonVi, trangThai, nguonGoc, page, size, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
