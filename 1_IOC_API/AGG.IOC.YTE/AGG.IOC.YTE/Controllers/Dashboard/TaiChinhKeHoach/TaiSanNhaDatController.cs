﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach;
using Microsoft.AspNetCore.Mvc;



namespace AGG.IOC.YTE.Controllers.Dashboard.TaiChinhKeHoach
{
    [Route("api/tai-chinh-ke-hoach/tai-san-nha-nhat")]
    [ApiController]
    public class TaiSanNhaDatController : ControllerBase
    {
     
        private TaiSanNhaDatRepository taiSanNhaDatRepository;
        public TaiSanNhaDatController()
        {
            taiSanNhaDatRepository = new TaiSanNhaDatRepository();
        }
        [HttpGet("liet-ke-tai-san-theo-dien-tich-dat")]
        public IActionResult TkTheoDienTichDatAPI(int? id_donvi_quanly, string? nam_duavao_sudung)
        {
            string nameProc = "PROC_TAISAN_NHADAT_TK_THEO_DIENTICH_DAT";
            var response = taiSanNhaDatRepository.TkTheoDienTichDat(id_donvi_quanly,nam_duavao_sudung,nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("liet-ke-id-ten-don-vi")]
        public IActionResult GetIdTenDonVi()
        {
            string nameProc = "PROC_TAISAN_NHADAT_LAY_DONVI_THANHVIEN";
            var response = taiSanNhaDatRepository.GetDsIdDonVi(nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("liet-ke-tai-san-dien-tich-dat")]
        public IActionResult TkTheo2DienTichNhaAPI(int? id_donvi_quanly,string? nam_duavao_sudung)
        {
            string nameProc = "PROC_TAISAN_NHADAT_TK_THEO_2LOAI_DIENTICH_NHA";
            var response = taiSanNhaDatRepository.TkTheo2DienTichNha(id_donvi_quanly, nam_duavao_sudung, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("liet-ke-tai-san-theo-hien-trang-nha")]
        public IActionResult TkTheoHienTrangNhaAPI(int? id_donvi_quanly, string? nam_duavao_sudung)
        {
            string nameProc = " PROC_TAISAN_NHADAT_TK_THEO_HIENTRANG_SUDUNG_NHA";
            var response = taiSanNhaDatRepository.TkTheoHienTrangNha(id_donvi_quanly,nam_duavao_sudung, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("liet-ke-tai-san-theo-nguyen-gia-nha-va-dat")]
        public IActionResult TkTheoNguyenGiaNhaVaDatAPI(int? id_donvi_quanly, string? nam_duavao_sudung)
        {
            string nameProc = "PROC_TAISAN_NHADAT_TK_THEO_NGUYENGIA_NHA_DAT";
            var response = taiSanNhaDatRepository.TkTheoNguyenGiaNhaVaDat(id_donvi_quanly, nam_duavao_sudung, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("liet-ke-tai-san-theo-hien-trang-cong-trinh")]
        public IActionResult TkeTheoHienTrangCongTrinhAPI(int? id_donvi_quanly, string? nam_duavao_sudung)
        {
            string nameProc = "PROC_TAISAN_NHADAT_TK_THEO_HIENTRANG_CONGTRINH";
            var response = taiSanNhaDatRepository.TkeTheoHienTrangCongTrinh(id_donvi_quanly, nam_duavao_sudung, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("liet-ke-tai-san-theo-dien-tich-dat-va-dien-tich-nha")]
        public IActionResult TkeTheoDienTichDatVa2DienTichNhaAPI(int? id_donvi_quanly, string? nam_duavao_sudung)
        {
            string nameProc = "PROC_TAISAN_NHADAT_TK_THEO_DIENTICH_DAT_VA_2LOAI_DIENTICH_NHA";
            var response = taiSanNhaDatRepository.TkeTheoDienTichDatVa2DienTichNha(id_donvi_quanly, nam_duavao_sudung, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

    }
}
