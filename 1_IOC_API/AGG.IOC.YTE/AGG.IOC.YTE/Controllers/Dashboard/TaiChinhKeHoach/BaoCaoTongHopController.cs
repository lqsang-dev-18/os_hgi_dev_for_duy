﻿using AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
namespace AGG.IOC.YTE.Controllers.Dashboard.TaiChinhKeHoach
{
    [ApiController]
    [Authorize]
    [Route("api/tai-chinh-ke-hoach/bao-cao-tong-hop")]
    public class BaoCaoTongHopController : ControllerBase
    {
        private BaoCaoTongHopRepository baoCaoTongHopRepository;
        public BaoCaoTongHopController()
        {
            baoCaoTongHopRepository = new BaoCaoTongHopRepository();
        }
        [HttpGet("tai-san")]
        public IActionResult GetTaiSan(string? nam, string? quy)
        {
            string nameProc = "PROC_KHTC_BAOCAO_TONGHOP_TAISAN";
            var response = baoCaoTongHopRepository.BaoCaoTongHop(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nguon-von")]
        public IActionResult GetNguonVon(string? nam, string? quy)
        {
            string nameProc = "PROC_KHTC_BAOCAO_TONGHOP_NGUONVON";
            var response = baoCaoTongHopRepository.BaoCaoTongHop(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("4-the-tong")]
        public IActionResult Get4TheTong(string? nam, string? quy)
        {
            string nameProc = "PROC_KHTC_BAOCAO_TONGHOP_4_THE_TONG";
            var response = baoCaoTongHopRepository.BaoCaoTongHop(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }

}
