﻿using AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.TaiChinhKeHoach
{
    [ApiController]
    [Authorize]
    [Route("api/tai-chinh-ke-hoach/cong-tac-y-te")]
    public class CongTacYTeController : Controller
    {
        private CongTacYTeRepository congTacYTeRepository;
        public CongTacYTeController()
        {
            congTacYTeRepository = new CongTacYTeRepository();
        }
        [HttpGet("luot-kham-benh")]
        public IActionResult GetListLuotKhamBenh(int? nam, int? thang)
        {
            string nameProc = "PROC_CTYT_LUOT_KHAM_BENH";
            var response = congTacYTeRepository.GetListCongTacYTe(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("dieu-tri-noi-tru")]
        public IActionResult GetListDieuTriNoiTru(int? nam, int? thang)
        {
            string nameProc = "PROC_CTYT_DIEUTRI_NOITRU";
            var response = congTacYTeRepository.GetListCongTacYTe(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tai-nan-giao-thong")]
        public IActionResult GetListTaiNanGiaoThong(int? nam, int? thang)
        {
            string nameProc = "PROC_CTYT_TNGT";
            var response = congTacYTeRepository.GetListCongTacYTe(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tinh-hinh-dich-benh")]
        public IActionResult GetListTinhHinhDichBenh(int? nam, int? thang)
        {
            string nameProc = "PROC_CTYT_TINH_HINH_DICH_BENH";
            var response = congTacYTeRepository.GetListCongTacYTe(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tinh-hinh-nhiem-hiv-aids-tv")]
        public IActionResult GetListTinhHinhNhiemBenh(int? nam, int? thang)
        {
            string nameProc = "PROC_CTYT_NHIEM_HIV_AIDS_TV";
            var response = congTacYTeRepository.GetListCongTacYTe(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-tiem-chung-mr")]
        public IActionResult GetListTK_TiemCungMoRong(int nam, int? thang)
        {
            string nameProc = "PROC_TK_TIEM_CHUNG_MR_HGI";
            var response = congTacYTeRepository.GetListCongTacYTe(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
