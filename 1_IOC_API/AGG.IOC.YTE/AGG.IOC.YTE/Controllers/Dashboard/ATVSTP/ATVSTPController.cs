﻿using AGG.IOC.YTE.Repository.ATVSTP;
using AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.ATVSTP
{
    [ApiController]
    [Authorize]
    [Route("api/an-toan-ve-sinh-thuc-pham")]
    public class ATVSTPController : Controller
    {
        private ATVSTPReposotory aTVSTPReposotory;
        public ATVSTPController()
        {
            aTVSTPReposotory = new ATVSTPReposotory();
        }
        //[HttpGet("tk-co-so-kinh-doanh")]
        //public IActionResult GetListTKCoSoKinhDoanh(string? nam, string? qui)
        //{
        //    string nameProc = "PROC_ATVSTP_TK_COSO_KINHDOANH";
        //    var response = aTVSTPReposotory.GetListATVSTP(nam, qui, nameProc);
        //    return response.success ? Ok(response) : BadRequest(response);
        //}
        //[HttpGet("tk-dich-vu-an-uong")]
        //public IActionResult GetListTKDVAnUong(string? nam, string? qui)
        //{
        //    string nameProc = "PROC_ATVSTP_TK_DV_ANUONG";
        //    var response = aTVSTPReposotory.GetListATVSTP(nam, qui, nameProc);
        //    return response.success ? Ok(response) : BadRequest(response);
        //}
        //[HttpGet("tk-tu-cong-bo-sp")]
        //public IActionResult GetListTKTuCongBoSP(string? nam, string? qui)
        //{
        //    string nameProc = "PROC_ATVSTP_TUCONGBO_SANPHAM";
        //    var response = aTVSTPReposotory.GetListATVSTP(nam, qui, nameProc);
        //    return response.success ? Ok(response) : BadRequest(response);
        //}
        [HttpGet("dvau-het-han")]
        public IActionResult DVAUHetHan(string? nam, string? quy)
        {
            string nameProc = "PROC_ATVSTP_CAPGIAY_HET_HAN_NHOM_DVAU";
            var response = aTVSTPReposotory.GetListATVSTP(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nhom-loai-sp-het-han")]
        public IActionResult NhomLoaiSPHetHan(string? nam, string? quy)
        {
            string nameProc = "PROC_ATVSTP_CAPGIAY_HET_HAN_NHOM_LOAI_SP";
            var response = aTVSTPReposotory.GetListATVSTP(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nhom-loai-sp-cap-gcn")]
        public IActionResult NhomLoaiSPCapGCN(string? nam, string? quy)
        {
            string nameProc = "PROC_ATVSTP_CAPGIAY_THEO_NHOM_LOAI_SP";
            var response = aTVSTPReposotory.GetListATVSTP(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("dvau-cap-gcn")]
        public IActionResult DVAUCapGCN(string? nam, string? quy)
        {
            string nameProc = "PROC_ATVSTP_CAPGIAY_THEO_NHOM_DVAU";
            var response = aTVSTPReposotory.GetListATVSTP(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("loai-co-so")]
        public IActionResult LoaiCoSo(string? nam, string? quy)
        {
            string nameProc = "PROC_ATVSTP_CAPGIAY_THEO_PHAN_LOAI_CO_SO";
            var response = aTVSTPReposotory.GetListATVSTP(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tu-cong-bo")]
        public IActionResult TuCongBo(string? nam, string? quy)
        {
            string nameProc = "PROC_ATVSTP_CAPGIAY_TU_CONG_BO_THEO_NHOM_LOAI_SP";
            var response = aTVSTPReposotory.GetListATVSTP(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }




        //[HttpGet("tl-dich-vu-an-uong")]
        //public IActionResult GetListTLDVAnUong(string? nam, string? qui)
        //{
        //    string nameProc = "PROC_ATVSTP_TYLE_DV_ANUONG";
        //    var response = aTVSTPReposotory.GetListATVSTP(nam, qui, nameProc);
        //    return response.success ? Ok(response) : BadRequest(response);
        //}

        //thanh tra attp
        [HttpGet("thanh-tra-theo-loai-hinh")]
        public IActionResult ThanhTraTheoLoaiHinh(string? nam, string? doan_thanh_tra)
        {
            string nameProc = "PROC_ATVSTP_TT_THEO_LOAIHINH";
            var response = aTVSTPReposotory.GetListATVSTP2(nam, doan_thanh_tra, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thanh-tra-theo-dot")]
        public IActionResult ThanhTraTheoDot(string? nam, string? doan_thanh_tra)
        {
            string nameProc = "PROC_ATVSTP_TT_THEO_DOT";
            var response = aTVSTPReposotory.GetListATVSTP2(nam, doan_thanh_tra, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thanh-tra-tong")]
        public IActionResult ThanhTraTong(string? nam)
        {
            string nameProc = "PROC_ATVSTP_TT_TONGSO";
            var response = aTVSTPReposotory.GetListATVSTP2(nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        //Create by hen
        [HttpGet("atvstp-the-tong")]
        public IActionResult GetTheTongATVSTP(string? nam, string? qui)
        {
            string nameProc = "PROC_ATVSTP_THONG_KE_4_THE_TONG";
            var response = aTVSTPReposotory.GetListATVSTP(nam, qui, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        
    }
    
}
