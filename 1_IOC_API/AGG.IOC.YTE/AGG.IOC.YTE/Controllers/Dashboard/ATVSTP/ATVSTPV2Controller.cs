﻿using AGG.IOC.YTE.Repository.ATVSTP;
using AGG.IOC.YTE.Repository.Dashboard.NghiepVuDuoc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.ATVSTP
{
    [Route("api/an-toan-ve-sinh-thuc-pham")]
    //[Authorize]
    [ApiController]
    public class ATVSTPV2Controller : ControllerBase
    {
        private ATVSTPV2Reposotory aTVSTPReposotory;
        public ATVSTPV2Controller()
        {
            aTVSTPReposotory = new ATVSTPV2Reposotory();
        }

        [HttpGet("co-so-tong")]
        public IActionResult GetListTKSoLuongCSKDDuocCapATTP(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_TK_SL_CSKD_DUOC_CAP_ATTP_HGI";
            var response = aTVSTPReposotory.GetListATVSTP(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("co-so-don-vi")]
        public IActionResult GetListThongKeSoLuongCoSo(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_TK_SL_CO_SO_HGI";
            var response = aTVSTPReposotory.GetListATVSTP(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("co-so-cap-gcn-don-vi")]
        public IActionResult GetListThongKeSoLuongCoSoDuocCapGiayChungNhan(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_TK_SL_CO_SO_DUOC_CAP_CHUNG_NHAN_HGI";
            var response = aTVSTPReposotory.GetListATVSTP(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("co-so-tu-cong-bo-don-vi")]
        public IActionResult GetListThongKeSoLuongSanPhamTuCongBo(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_TK_SL_SP_TU_CONG_BO_HGI";
            var response = aTVSTPReposotory.GetListATVSTP(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("co-so-tu-cong-bo-bi-huy-don-vi")]
        public IActionResult GetListThongKeSLSPTuCongBoBiHuy(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_TK_SL_SP_TU_CONG_BO_BI_HUY_HGI";
            var response = aTVSTPReposotory.GetListATVSTP(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
