﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using AGG.IOC.YTE.Repository.Dashboard.GiamDinhYKhoa;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.GiamDinhYKhoa
{
    [ApiController]
    [Authorize]
    [Route("api/giam-dinh-y-khoa")]
    public class GiamDinhYKhoaController : Controller
    {
        private GiamDinhYKhoaRepository giamDinhYKhoaRepository;
        public GiamDinhYKhoaController()
        {
            giamDinhYKhoaRepository = new GiamDinhYKhoaRepository();
        }
        [HttpGet("so-ca-kham-giam-dinh")]
        public IActionResult GetListSoCaKhamGiamDinh(string? nam, string? qui, string? thang)
        {
            string nameProc = "PROC_YKHOA_CAKHAM_GIAMDINH";
            var response = giamDinhYKhoaRepository.GetListGiamDinhYKhoa(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("so-ca-kham-giam-dinh-khuyet-tat")]
        public IActionResult GetListSoCaKhamGiamDinhKhuyetTat(string? nam, string? qui, string? thang)
        {
            string nameProc = "PROC_YKHOA_CAKHAM_GIAMDINH_KHUYETTAT";
            var response = giamDinhYKhoaRepository.GetListGiamDinhYKhoa(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
    
}
