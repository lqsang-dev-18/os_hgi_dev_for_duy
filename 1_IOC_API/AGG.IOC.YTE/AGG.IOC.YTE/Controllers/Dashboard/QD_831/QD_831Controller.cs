﻿using AGG.IOC.YTE.Repository.Dashboard.QD_831;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.QD_831
{
    [Route("api/qd-831")]
    [ApiController]
    [Authorize]
    public class QD_831Controller : Controller
    {
        private QD_831Repository QD_831Repository;

        public QD_831Controller()
        {
            QD_831Repository = new QD_831Repository();
        }

        [HttpGet("hssk-sl")]
        public IActionResult GetTKDanSo_SLHSSKTaoLapVaSLHSSKCapNhatDuLieu(int nam, int? quy, int? thang, int tinh, int? huyen, int? xa)
        {
            var response = QD_831Repository.GetTKDanSo_SLHSSKTaoLapVaSLHSSKCapNhatDuLieu(nam, quy, thang, tinh, huyen, xa);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("hssk-tile")]
        public IActionResult GetTyLeHSSKCapNhatSoVoiTyLeHSSKTaoLap(int nam, int? quy, int? thang, int tinh, int? huyen, int? xa)
        {
            var response = QD_831Repository.GetTyLeHSSKCapNhatSoVoiTyLeHSSKTaoLap(nam, quy, thang, tinh,huyen, xa);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
