﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AGG.IOC.YTE.Repository.PhanQuyen;
using AGG.IOC.YTE.Repository.Dashboard;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using AGG.IOC.YTE.Repository.Dashboard.CSDL831;
using System.ComponentModel;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AGG.IOC.YTE.Controllers.Dashboard
{
    //[Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ThongTinHanhChinhController : Controller
    {
        [Route("api/dashboard-831/lay-thong-ke-gioi-tinh")]
        [HttpGet]
        public IActionResult THONGKE_GIOITINH_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_GIOITINH_831(ma_huyen, ma_xa);

            if (response.success)
            {
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-tong-hssk")]
        [HttpGet]
        public IActionResult THONGKE_TONG_HSSK_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

             var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
              return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_TONG_HSSK_831(ma_huyen, ma_xa);

            if (response.success)
            {
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-tong-ho-khau")]
        [HttpGet]
        public IActionResult THONGKE_TONG_HOKHAU_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

             var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
              return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_TONG_HOKHAU_831(ma_huyen, ma_xa);

            if (response.success)
            {
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-tien-su-benh-tat")]
        [HttpGet]
        public IActionResult THONGKE_TIEUSU_BENHTAT_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

             var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
              return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_TIEUSU_BENHTAT_831(ma_huyen, ma_xa);

            if (response.success)
            {
                var result = new List<ConvertToChart>();
                foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
                {
                    foreach(var item in row.ToList())
                    {
                        var obj = new ConvertToChart(item.Key.ToString(), Int32.Parse(item.Value.ToString() ?? "0"));
                        result.Add(obj);
                    }
                }
                response.data = result;
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-tien-su-benh-tat-gia-dinh")]
        [HttpGet]
        public IActionResult THONGKE_TIENSU_BENHTAT_GIADINH_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

             var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
              return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_TIENSU_BENHTAT_GIADINH_831(ma_huyen, ma_xa);

            if (response.success)
            {
                var result = new List<ConvertToChart>();
                foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
                {
                    foreach (var item in row.ToList())
                    {
                        var obj = new ConvertToChart(item.Key.ToString(), Int32.Parse(item.Value.ToString() ?? "0"));
                        result.Add(obj);
                    }
                }
                response.data = result;
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-khuyet-tat")]
        [HttpGet]
        public IActionResult THONGKE_KHUYETTAT_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
              return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_KHUYETTAT_831(ma_huyen, ma_xa);

            if (response.success)
            {
                var result = new List<ConvertToChart>();
                foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
                {
                    foreach (var item in row.ToList())
                    {
                        var obj = new ConvertToChart(item.Key.ToString(), Int32.Parse(item.Value.ToString() ?? "0"));
                        result.Add(obj);
                    }
                }
                response.data = result;
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-hssk-theo-do-tuoi")]
        [HttpGet]
        public IActionResult THONGKE_TONG_HSSK_THEO_DOTUOI_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_TONG_HSSK_THEO_DOTUOI_831(ma_huyen, ma_xa);

            if (response.success)
            {
                
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-skss-so-lan-co-thai")]
        [HttpGet]
        public IActionResult THONGKE_SUCKHOESINHSAN_SOLANCOTHAI_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_SUCKHOESINHSAN_SOLANCOTHAI_831(ma_huyen, ma_xa);

            if (response.success)
            {

                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-skss-ty-le-say-thai")]
        [HttpGet]
        public IActionResult THONGKE_SUCKHOESINHSAN_TYLE_SAYTHAI_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_SUCKHOESINHSAN_TYLE_SAYTHAI_831(ma_huyen, ma_xa);

            if (response.success)
            {
                var result = new List<ConvertToChart>();
                foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
                {
                    foreach (var item in row.ToList())
                    {
                        var obj = new ConvertToChart(item.Key.ToString(),(item.Value));
                        result.Add(obj);
                    }
                }
                response.data = result;
                return Ok(response);
            }
            return BadRequest(response);

        }

        [Route("api/dashboard-831/lay-thong-ke-skss-ty-le-pha-thai")]
        [HttpGet]
        public IActionResult THONGKE_SUCKHOESINHSAN_TYLE_PHATHAI_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_SUCKHOESINHSAN_TYLE_PHATHAI_831(ma_huyen, ma_xa);

            if (response.success)
            {
                var result = new List<ConvertToChart>();
                foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
                {
                    foreach (var item in row.ToList())
                    {
                        var obj = new ConvertToChart(item.Key.ToString(), (item.Value));
                        result.Add(obj);
                    }
                }
                response.data = result;
                return Ok(response);
            }
            return BadRequest(response);

        }
        [Route("api/dashboard-831/lay-thong-ke-skss-sl-de-thuong-de-mo")]
        [HttpGet]
        public IActionResult THONGKE_SUCKHOESINHSAN_SL_DETHUONG_DEMO_831(string? ma_huyen, string? ma_xa)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = ThongTinHanhChinhRepository.THONGKE_SUCKHOESINHSAN_SL_DETHUONG_DEMO_831(ma_huyen, ma_xa);

            if (response.success)
            {
                var result = new List<ConvertToChart>();
                foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
                {
                    foreach (var item in row.ToList())
                    {
                        var obj = new ConvertToChart(item.Key.ToString(), (item.Value));
                        result.Add(obj);
                    }
                }
                response.data = result;
                return Ok(response);
            }
            return BadRequest(response);

        }

    }
}

