﻿using AGG.IOC.YTE.Repository.DanhMucChung;
using AGG.IOC.YTE.Repository.Dashboard.NghiepVuY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.NghiepVuY
{
    [Route("api/thong-tin-chung")]
    [ApiController]
    [Authorize]
    public class ThongTinChungController : Controller
    {
        private ThongTinChung m_thongTinChung;
        public ThongTinChungController()
        {
            m_thongTinChung = new ThongTinChung();
        }

        [HttpGet("sl-tong-hoso-kcb")]
        public IActionResult GetTKHoSoKCB(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //--------------------------------------------------|
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetTKHoSoKCB(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tong-sl-cap-cuu")]
        public IActionResult GetTKSLCapCuu(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetTKSLCapCuu(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-tong-sl-tu-vong")]
        public IActionResult GetTKSLTuVong(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetTKSLTuVong(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-sl-ra-vao-vien")]
        public IActionResult GetTKSLRaVaoVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetTKSLRaVaoVien(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-sl-cap-cuu-tu-vong")]
        public IActionResult GetTKSLCAPCUUTUVONG(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetTKSLCAPCUUTUVONG(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-luot-kham-benh")]
        public IActionResult GetTKKCB(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetTKKCB(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-luot-kham-benh-ngoai-tru-noi-tru")]
        public IActionResult GetTKNgoaiTruNoiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {

            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetTKNgoaiTruNoiTru(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("sl-noi-tru-ngoai-tru")]
        public IActionResult GetSLNgoaiTruNoiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {

            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetSLNgoaiTruNoiTru(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-luot-vao-vien")]
        public IActionResult GetSLVaoVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {

            var response = m_thongTinChung.GetSLVaoVien(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-luot-ra-vien")]
        public IActionResult GetSLRaVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetSLRaVien(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("sl-ca-nang-xin-ve")]
        public IActionResult GetSLCaNangXinVe(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var response = m_thongTinChung.GetSLCaNangXinVe(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("sl-chuyen-vien")]
        public IActionResult GetSLChuyenVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetSLChuyenVien(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("sl-noi-tru-cuoi-ky")]
        public IActionResult GetSLNoiTruCuoiKy(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetSLNoiTruCuoiKy(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        //Số lượng nội trú cách tính mới
        [HttpGet("sl-noi-tru")]
        public IActionResult GetSLNoiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetSLNoiTru(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        //Số lượng ngoai trú cách tính mới
        [HttpGet("sl-ngoai-tru")]
        public IActionResult GetSLNgoaiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            //Thực hiện yêu cầu
            var response = m_thongTinChung.GetSLNgoaiTru(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
        //Số lượng ngoai trú cách tính mới
        
        [HttpGet("sl-cuoi-ky-noi-tru")]
        public IActionResult GetSLCuoiKyNew(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {

            var response = m_thongTinChung.GetSLCuoiKyNew(loai_cbx, tungay, denngay, nam, thang, quy, huyen, loaihinh, cosokb);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
