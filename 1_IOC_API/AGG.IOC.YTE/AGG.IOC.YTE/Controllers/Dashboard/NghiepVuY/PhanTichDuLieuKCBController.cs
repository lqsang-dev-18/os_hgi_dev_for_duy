﻿using AGG.IOC.YTE.Repository.Dashboard.NghiepVuY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.NghiepVuY
{
    [ApiController]
    [Authorize]
    [Route("api/phan-tich-du-lieu-kcb")]
    public class PhanTichDuLieuKCBController : ControllerBase
    {
       /* [HttpGet("ty-le-kcb-co-bhyt")]
        public IActionResult TK_TyleKCB_CoBHYT(string? ma_huyen, string? nam, string? quy, string? thang, int? loai_thoigian, int? loai_hinh, int? loai_sosanh)
        {
            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = PhanTichDuLieuKCBRepository.PROC_TK_SO_HO_SO_BHYT(us.MA_DON_VI, ma_huyen, nam, quy, thang, loai_thoigian, loai_hinh, loai_sosanh);
            return response.success ? Ok(response) : BadRequest(response);
        }*/

        private PhanTichDuLieuKCBRepository phanTichDuKCBRepository;
        public PhanTichDuLieuKCBController()
        {
            phanTichDuKCBRepository = new PhanTichDuLieuKCBRepository();
        }

        [HttpGet("ty-le-kcb-co-bhyt")]
        public IActionResult GetListTyLeKCBBHYT(int? loaiHinh, int? loaiSS, string? nam, string? qui, string? thang, string? maHuyen, string? tungay, string? denngay, int? loai,  string? maCSKCB)
        {
            //string nameProc = "PROC_KCB_TYLE_KCB_COBHYT";
            string nameProc = "PROC_KCB_TYLE_KCB_COBHYT_HGI";
            var response = phanTichDuKCBRepository.GetListPhanTichDLKCB(loaiHinh, loaiSS, nam, qui, thang, maHuyen, maCSKCB, tungay, denngay, loai, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-kcb-loai-kham")]
        public IActionResult GetListTKeKCBLoaiKham(int? loaiHinh, int? loaiSS, string? nam, string? qui, string? thang, string? maHuyen, string? maCSKCB, string? tungay, string? denngay, int? loai)
        {
            string nameProc = "PROC_KCB_THEO_LOAI_KHAM_HGI";
            var response = phanTichDuKCBRepository.GetListPhanTichDLKCB(loaiHinh, loaiSS, nam, qui, thang, maHuyen, maCSKCB, tungay, denngay,loai, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-chi-phi-kcb")]
        public IActionResult GetListTKeChiPhiKCB(int? loaiHinh, int? loaiSS, string? nam, string? qui, string? thang, string? maHuyen, string? maCSKCB, string? tungay, string? denngay, int? loai)
        {
            string nameProc = "PROC_KCB_CHIPHI_THANHTOAN_HGI";
            var response = phanTichDuKCBRepository.GetListPhanTichDLKCB(loaiHinh, loaiSS, nam, qui, thang, maHuyen, maCSKCB, tungay, denngay, loai, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-cap-cuu-tu-vong")]
        public IActionResult GetListTKeCapCuuTuVong(int? loaiHinh, int? loaiSS, string? nam, string? qui, string? thang, string? maHuyen, string? maCSKCB, string? tungay, string? denngay, int? loai)
        {
            string nameProc = "PROC_KCB_CAPCUU_TUVONG_HGI";
            var response = phanTichDuKCBRepository.GetListPhanTichDLKCB(loaiHinh, loaiSS, nam, qui, thang, maHuyen, maCSKCB, tungay, denngay, loai, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
