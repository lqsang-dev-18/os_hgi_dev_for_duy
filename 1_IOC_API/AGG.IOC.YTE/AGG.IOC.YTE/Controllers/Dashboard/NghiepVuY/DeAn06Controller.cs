﻿using AGG.IOC.YTE.Models.Authentication;
using AGG.IOC.YTE.Repository.DanhMucChung;
using AGG.IOC.YTE.Repository.Dashboard.DuLieuKCB;
using AGG.IOC.YTE.Repository.Dashboard.NghiepVuY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.NghiepVuY
{
    [ApiController]
    [Authorize]
    [Route("api/nghiep-vu-y/de-an-06")]
    public class DeAn06Controller : Controller
    {
        private DeAn06Repository deAn06Repository;

        public DeAn06Controller()
        {
            deAn06Repository = new DeAn06Repository();
        }

        [HttpGet("4-chi-tieu")]
        public IActionResult GetList4ChiTieu(string? nam, string? qui, string? thang, string? tuyen)
        {
            string nameProc = "PROC_DA06_THONGKE_4_CHITIEU";
            var response = deAn06Repository.GetListDeAn06(nam, qui, thang, tuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thuc-hien-nhiem-vu")]
        public IActionResult GetListThucHienNhiemVu(string? nam, string? qui, string? thang, string? tuyen)
        {
            string nameProc = "PROC_DA06_THUCHIEN_NHIEMVU";
            var response = deAn06Repository.GetListDeAn06(nam, qui, thang, tuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("kcb-bang-cccd")]
        public IActionResult GetListKCBBangCCCD(string? nam, string? qui, string? thang, string? tuyen)
        {
            string nameProc = "PROC_DA06_KCB_CCCD_BHYT";
            var response = deAn06Repository.GetListDeAn06(nam, qui, thang, tuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("luu-tru-truc-tuyen")]
        public IActionResult GetListLuuTruTrucTuyen(string? nam, string? qui, string? thang, string? tuyen)
        {
            string nameProc = "PROC_DA06_THONGBAO_LUUTRU_TRUCTUYEN";
            var response = deAn06Repository.GetListDeAn06(nam, qui, thang, tuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ltdl-giay-ksk-lai-xe")]
        public IActionResult GetListLTDLGiayKSKLaiXe(string? nam, string? qui, string? thang, string? tuyen)
        {
            string nameProc = "PROC_DA06_LTDL_KSK_LAIXE";
            var response = deAn06Repository.GetListDeAn06(nam, qui, thang, tuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("cai-dat-vneid-muc-2")]
        public IActionResult GetListCaiDatVNEIDMuc2(string? nam, string? qui, string? thang, string? tuyen)
        {
            string nameProc = "PROC_DA06_NVYT_VNEID_MUC2";
            var response = deAn06Repository.GetListDeAn06(nam, qui, thang, tuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
