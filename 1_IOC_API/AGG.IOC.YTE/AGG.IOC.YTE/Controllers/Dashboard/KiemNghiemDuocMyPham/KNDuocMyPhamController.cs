﻿//using AGG.IOC.YTE.Repository.Dashboard.CDC;
using AGG.IOC.YTE.Repository.Dashboard.KiemNghiemDuocMyPham;
using AGG.IOC.YTE.Repository.Dashboard.GiamDinhYKhoa;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace AGG.IOC.YTE.Controllers.Dashboard.KiemNghiemDuocMyPham
{
    [ApiController]
    [Authorize]
    [Route("api/kn-duoc-my-pham")]
    public class KNDuocMyPhamController : Controller
    {
        private KNDuocMyPhamRepository KNDuocMyPhamRepository;
        public KNDuocMyPhamController()
        {
            KNDuocMyPhamRepository = new KNDuocMyPhamRepository();
        }
        [HttpGet("mau-lay")]
        public IActionResult GetSoMauLay(string? nam, string? quy, string? thang, string? ma_huyen)
        {
            string nameProc = "PROC_KNDUOCMYPHAM_MAU_LAY_THEO_LOAI";
            var response = KNDuocMyPhamRepository.KiemNghiemDuocMyPham(nam, quy, thang, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("mau-gui")]
        public IActionResult GetSoMauGui(string? nam, string? quy, string? thang, string? ma_huyen)
        {
            string nameProc = "PROC_KNDUOCMYPHAM_MAU_GUI_THEO_LOAI";
            var response = KNDuocMyPhamRepository.KiemNghiemDuocMyPham(nam, quy, thang, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("mau-lay-khong-dat")]
        public IActionResult GetSoMauLayKhongDat(string? nam, string? quy, string? thang, string? ma_huyen)
        {
            string nameProc = "PROC_KNDUOCMYPHAM_MAU_LAY_THEO_LOAI_KHONG_DAT";
            var response = KNDuocMyPhamRepository.KiemNghiemDuocMyPham(nam, quy, thang, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("mau-gui-khong-dat")]
        public IActionResult GetSoMauGuiKhongDat(string? nam, string? quy, string? thang, string? ma_huyen)
        {
            string nameProc = "PROC_KNDUOCMYPHAM_MAU_GUI_THEO_LOAI_KHONG_DAT";
            var response = KNDuocMyPhamRepository.KiemNghiemDuocMyPham(nam, quy, thang, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("mau-lay-gui-theo-loai-co-so")]
        public IActionResult GetSoMauLayGuiTheoLoaiCoSo(string? nam, string? quy, string? thang, string? ma_huyen)
        {
            string nameProc = "PROC_KNDUOCMYPHAM_MAU_GUI_LAY_THEO_CO_SO";
            var response = KNDuocMyPhamRepository.KiemNghiemDuocMyPham(nam, quy, thang, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("4-the-tong")]
        public IActionResult Get4TheTong(string? nam, string? quy, string? thang, string? ma_huyen)
        {
            string nameProc = "PROC_KNDUOCMYPHAM_4_THE_TONG";
            var response = KNDuocMyPhamRepository.KiemNghiemDuocMyPham(nam, quy, thang, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
