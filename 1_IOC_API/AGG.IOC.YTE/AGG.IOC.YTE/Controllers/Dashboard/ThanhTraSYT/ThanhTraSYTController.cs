﻿using AGG.IOC.YTE.Repository.Dashboard.ThanhTraSYT;
using AGG.IOC.YTE.Repository.PhanQuyen;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AGG.IOC.YTE.Controllers.Dashboard.ThanhTraSYT
{
    [Route("api/thanh-tra-syt")]
    [ApiController]
    //[Authorize]
    public class ThanhTraSYTController : ControllerBase
    {
        private ThanhTraSYTRepository _ThanhTraSYTRepository;

        public ThanhTraSYTController()
        {
            _ThanhTraSYTRepository = new ThanhTraSYTRepository();
        }

        [HttpGet("5-the-tong-ttkt")]
        public IActionResult GetList5TheTongTTKT(string nam, string quy)
        {
            string nameProc = "PROC_TK_5_THE_TONG_TTKT";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("5-the-tong-vi-pham")]
        public IActionResult GetList5TheTongViPham(string nam, string quy)
        {
            string nameProc = "PROC_TK_5_THE_TONG_VI_PHAM";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("so-sanh-ttkt-luy-ke_cung_ky")]
        public IActionResult GetListSoSanhTTTKLuyKeCungKy(string nam, string quy)
        {
            string nameProc = "PROC_SS_TTKT_LUY_KE_CUNG_KY";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("so-sanh-ttkt-to-chuc-luy-ke_cung_ky")]
        public IActionResult GetListSoSanhTTTKToChucLuyKeCungKy(string nam, string quy)
        {
            string nameProc = "PROC_SS_TTKT_TOCHUC_LUY_KE_CUNG_KY";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("so-sanh-ttkt-ca-nhan-luy-ke_cung_ky")]
        public IActionResult GetListSoSanhTTTKCaNhanLuyKeCungKy(string nam, string quy)
        {
            string nameProc = "PROC_SS_TTKT_CANHAN_LUY_KE_CUNG_KY";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("so-sanh-tien-phat-luy-ke_cung_ky")]
        public IActionResult GetListSoSanhTienPhatLuyKeCungKy(string nam, string quy)
        {
            string nameProc = "PROC_SS_TIEN_PHAT_LUY_KE_CUNG_KY";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-dai-dien-tren-don")]
        public IActionResult GetTkDaiDienTrenMotDon(string nam, string quy)
        {
            string nameProc = "PROC_DAI_DIEN_TREN_MOT_DON";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("phan-loai-theo-noi-dung")]
        public IActionResult GetPhanLoaiTheoNoiDung(string nam, string quy)
        {
            string nameProc = "PROC_PHAN_LOAI_THEO_NOI_DUNG";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("phan-loai-theo-tinh-trang")]
        public IActionResult GetPhanLoaiTheoTinhTrang(string nam, string quy)
        {
            string nameProc = "PROC_PHAN_LOAI_THEO_TINH_TRANG";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("ket-qua-xu-ly-thuoc-tham-quyen")]
        public IActionResult GetKetQuaXuLyThuocThamQuyen(string nam, string quy)
        {
            string nameProc = "PROC_KETQUAXULY_THUOCTHAMQUYEN";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("ket-qua-xu-ly-khong-thuoc-tham-quyen")]
        public IActionResult GetKetQuaXuLyKhongThuocThamQuyen(string nam, string quy)
        {
            string nameProc = "PROC_KETQUAXULY_KHONGTHUOCTHAMQUYEN";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("6-the-tong")]
        public IActionResult Get6TheTong(string nam, string quy)
        {
            string nameProc = "PROC_TONG_6_THE";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(nam, quy, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}