using AGG.IOC.YTE.Repository.Dashboard.CSDL831;
using AGG.IOC.YTE.Repository.Dashboard;
using AGG.IOC.YTE.Repository.Dashboard.DanSo;
using AGG.IOC.YTE.Repository.Dashboard.ToChucCanBo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.DanSo
{
    [ApiController]
    [Authorize]
    [Route("api/danso")]
    public class DanSoController : ControllerBase
    {
        private DanSoRepository DanSoRepository;

        public DanSoController()
        {
            DanSoRepository = new DanSoRepository();
        }
        [HttpGet("thong-ke-dan-so-cac-huyen")]
        public IActionResult GetListThongKeDanSo(int? nam,string? ma_huyen)
        {
            string nameProc = "PROC_DAN_SO_THONG_KE_DAN_SO";
            var response = DanSoRepository.GetListThongKeDanSo(nam,ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-dan-so-theo-do-tuoi")]
        public IActionResult DanSoTheoDoTuoi(int? nam, string? ma_huyen)
        {
            string nameProc = "PROC_DAN_SO_THONG_KE_THEO_DO_TUOI";
            var response = DanSoRepository.DanSoTheoDoTuoi(nam, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-dan-so-theo-gioi-tinh")]
        public IActionResult DanSoTheoGioiTinh(int? nam, string? ma_huyen)
        {
            string nameProc = "PROC_DAN_SO_TY_LE_NAM_NU";
            var response = DanSoRepository.DanSoTheoGioiTinh(nam, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-dan-so-theo-thanh-thi-nong-thon")]
        public IActionResult DanSoTheoThanhThiNongThon(int? nam, string? ma_huyen)
        {
            string nameProc = "PROC_DAN_SO_TY_LE_THANH_THI_VA_NONG_THON";
            var response = DanSoRepository.DanSoTheoThanhThiNongThon(nam, ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-dan-so-bien-phap-tranh-thai")]
        public IActionResult DanSoBienPhapTranhThai(int? nam, string? ma_huyen)
        {
            string nameProc = "PROC_DAN_SO_BIEN_PHAP_TRANH_THAI";
            //var response = DanSoRepository.DanSoBienPhapTranhThai(nam, ma_huyen, nameProc);
            var response = DanSoRepository.DanSoBienPhapTranhThai(nam, ma_huyen, nameProc);

            if (response.success)
            {
                try {
                    var result = new List<ConvertToChart>();
                    foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
                    {
                        foreach (var item in row.ToList())
                        {
                            var obj = new ConvertToChart(item.Key.ToString(), Int32.Parse(item.Value.ToString() ?? "0"));
                            result.Add(obj);
                        }
                    }
                    response.data = result;
                    return Ok(response);

                } catch (Exception) {

                    return Ok(response);
                }
            }
            return BadRequest(response);
            //return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-ty-le-sinh-con-thu-3-tro-len")]
        public IActionResult TyLeSinhConThu3TroLen(int? nam, string? ma_huyen)
        {
            string nameProc = "PROC_DAN_SO_TY_LE_SINH_CON_THU_3_TRO_LEN";
            var response = DanSoRepository.TyLeSinhConThu3TroLen(nam,ma_huyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        //PHÂN TÍCH SỐ LIỆU

        [HttpGet("thong-ke-dan-so-so-sanh-lien-ke")]
        public IActionResult GetListThongKeDanSoSSLienKe(int? nam)
        {
            string nameProc = "PROC_DANSO_TONG_DANSO";
            var response = DanSoRepository.GetListThongKeDanSoSSLienKe(nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-dan-so-theo-do-tuoi-so-sanh-lien-ke")]
        public IActionResult DanSoTheoDoTuoiSSLienKe(int? nam)
        {
            string nameProc = "PROC_DAN_SO_THONG_KE_THEO_DO_TUOI_SS_LIEN_KE";
            var response = DanSoRepository.DanSoTheoDoTuoiSSLienKe(nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-dan-so-theo-gioi-tinh-so-sanh-lien-ke")]
        public IActionResult DanSoTheoGioiTinhSSLienKe(int? nam)
        {
            string nameProc = "PROC_DANSO_TYLE_GIOITINH";
            var response = DanSoRepository.DanSoTheoGioiTinhSSLienKe(nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-dan-so-theo-thanh-thi-nong-thon-so-sanh-lien-ke")]
        public IActionResult DanSoTheoThanhThiNongThonSSLienKe(int? nam)
        {
            string nameProc = "PROC_DANSO_TYLE_KHUVUC";
            var response = DanSoRepository.DanSoTheoThanhThiNongThonSSLienKe(nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        //[HttpGet("thong-ke-dan-so-bien-phap-tranh-thai")]
        //public IActionResult DanSoBienPhapTranhThai(int? nam, string? ma_huyen)
        //{
        //    string nameProc = "PROC_DAN_SO_BIEN_PHAP_TRANH_THAI";
        //    //var response = DanSoRepository.DanSoBienPhapTranhThai(nam, ma_huyen, nameProc);
        //    var response = DanSoRepository.DanSoBienPhapTranhThai(nam, ma_huyen, nameProc);

        //    if (response.success)
        //    {
        //        var result = new List<ConvertToChart>();
        //        foreach (var row in response.data.Cast<IDictionary<String, Object>>().ToList())
        //        {
        //            foreach (var item in row.ToList())
        //            {
        //                var obj = new ConvertToChart(item.Key.ToString(), Int32.Parse(item.Value.ToString() ?? "0"));
        //                result.Add(obj);
        //            }
        //        }
        //        response.data = result;
        //        return Ok(response);
        //    }
        //    return BadRequest(response);
        //    //return response.success ? Ok(response) : BadRequest(response);
        //}
        [HttpGet("thong-ke-ty-le-sinh-con-thu-3-tro-len-so-sanh-lien-ke")]
        public IActionResult TyLeSinhConThu3TroLenSSLienKe(int? nam)
        {
            string nameProc = "PROC_DANSO_TYLE_SINHCON_THU_3";
            var response = DanSoRepository.TyLeSinhConThu3TroLenSSLienKe(nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }


    }
}
