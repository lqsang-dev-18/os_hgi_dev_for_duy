﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/hiv")]
    public class HIVController : Controller
    {
        private HIVRepository hIVRepository;
        public HIVController()
        {
            hIVRepository = new HIVRepository();
        }
        [HttpGet("tong-quan-dich")]
        public IActionResult GetListTongQuanDich(string? nam, string? khoan)
        {
            string nameProc = "PROC_HIV_TONGQUAN_DICH";
            var response = hIVRepository.GetListCDCHIV(nam, khoan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("theo-do-tuoi")]
        public IActionResult GetListTheoDoTuoi(string? nam, string? khoan)
        {
            string nameProc = "PROC_HIV_NGUOI_NHIEM_THEO_DO_TUOI";
            var response = hIVRepository.GetListCDCHIV(nam, khoan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("theo-gioi-tinh")]
        public IActionResult GetListTheoGioiTinh(string? nam, string? khoan)
        {
            string nameProc = "PROC_HIV_NGUOI_NHIEM_THEO_GIOI_TINH";
            var response = hIVRepository.GetListCDCHIV(nam, khoan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("theo-nguy-co")]
        public IActionResult GetListTheoNguyCo(string? nam, string? khoan)
        {
            string nameProc = "PROC_HIV_NGUOI_NHIEM_THEO_NGUY_CO";
            var response = hIVRepository.GetListCDCHIV(nam, khoan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("so-mau-hiv")]
        public IActionResult GetListSoMauHIV(string? nam, string? khoan)
        {
            string nameProc = "PROC_HIV_SO_MAU_HIV";
            var response = hIVRepository.GetListCDCHIV(nam, khoan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ty-le-nguy-co")]
        public IActionResult GetListTyLeNguyCo(string? nam, string? khoan)
        {
            string nameProc = "PROC_HIV_TY_LE_THEO_NGUY_CO";
            var response = hIVRepository.GetListCDCHIV(nam, khoan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nguoi-nhiem-theo-doi-tuong")]
        public IActionResult GetListNguoiNhiemTheoDoiTuong(string? nam, string? khoan)
        {
            string nameProc = "PROC_HIV_NGUOI_NHIEM_THEO_DOI_TUONG";
            var response = hIVRepository.GetListCDCHIV(nam, khoan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }

}
