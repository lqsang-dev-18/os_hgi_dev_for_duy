﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Mvc;
namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [Route("api/cdc")]
    [ApiController]
    public class SucKhoeSinhSanController : Controller
    {
        private SucKhoeSinhSanRepository SucKhoeSinhSanRepository;
        public SucKhoeSinhSanController()
        {
            SucKhoeSinhSanRepository = new SucKhoeSinhSanRepository();
        }
        [HttpGet("suc-khoe-sinh-san")]
        public IActionResult GetSKSS(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_SUC_KHOE_SINH_SAN";
            var response = SucKhoeSinhSanRepository.getSKSS(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("vitamin-a/tre-em")]
        public IActionResult VitaminATreEm(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_VITAMIN_A_TRE_EM";
            var response = SucKhoeSinhSanRepository.getSKSS(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("vitamin-a/ba-me")]
        public IActionResult VitaminABaMe(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_VITAMIN_A_BA_ME";
            var response = SucKhoeSinhSanRepository.getSKSS(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
