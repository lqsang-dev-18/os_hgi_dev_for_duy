﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/hoat-dong-dinh-duong")]
    public class HoatDongDinhDuongController : Controller
    {
        private HoatDongDinhDuongRepository HoatDongDinhDuongRepository;
        public HoatDongDinhDuongController()
        {
            HoatDongDinhDuongRepository = new HoatDongDinhDuongRepository();
        }
        [HttpGet("ty-le-suy-dinh-duong")]
        public IActionResult TyLeSuyDinhDuong(string? nam)
        {
            string nameProc = "PROC_CDC_TY_LE_SUY_DINH_DUONG";
            var response = HoatDongDinhDuongRepository.TyLeSuyDinhDuong(nam, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("hoat-dong-dinh-duong")]
        public IActionResult HoatDongDinhDuong(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_HOAT_DONG_DINH_DUONG";
            var response = HoatDongDinhDuongRepository.HoatDongDinhDuong(nam,thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
