﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/benh-khong-lay")]
    public class BenhKhongLayController : Controller
    {
        private BenhKhongLayRepository benhKhongLayRepository;

        public BenhKhongLayController()
        {
            benhKhongLayRepository = new BenhKhongLayRepository();
        }
        [HttpGet("dai-thao-duong-huyen")]
        public IActionResult GetListDaiThaoDuongHuyen(string? nam, string? qui, string? thang)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_DAITHAODUONG_HUYEN";
            var response = benhKhongLayRepository.GetListCDCBenhKhongLay(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("dai-thao-duong-tinh")]
        public IActionResult GetListDaiThaoDuongTinh(string? nam, string? qui, string? thang)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_DAITHAODUONG_TINH";
            var response = benhKhongLayRepository.GetListCDCBenhKhongLay(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tang-huyet-ap-huyen")]
        public IActionResult GetListTangHuyetApHuyen(string? nam, string? qui, string? thang)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_TANGHUYETAP_HUYEN";
            var response = benhKhongLayRepository.GetListCDCBenhKhongLay(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tang-huyet-ap-tinh")]
        public IActionResult GetListTangHuyetApTinh(string? nam, string? qui, string? thang)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_TANGHUYETAP_TINH";
            var response = benhKhongLayRepository.GetListCDCBenhKhongLay(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("4-the-tong")]
        public IActionResult GetList4TheTong(string? nam, string? qui, string? thang)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_4_THE_TONG";
            var response = benhKhongLayRepository.GetListCDCBenhKhongLay(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ty-le-tu-vong")]
        public IActionResult GetListTyLeTuVong(string? nam)
        {
            string qui = null;
            string thang = null;
            string nameProc = "PROC_BENH_KHONG_LAY_TYLE_TUVONG_TINH";
            var response = benhKhongLayRepository.GetListCDCBenhKhongLay(nam, qui, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
