﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/kiem-dich-y-te")]
    public class KiemDichYTeController : Controller
    {
        private KiemDichYTeRepository kiemDichYTeRepository;

        public KiemDichYTeController()
        {
            kiemDichYTeRepository = new KiemDichYTeRepository();
        }
        [HttpGet("nguoi-nhap-xuat-canh")]
        public IActionResult GetListNguoiNhapXuatCanh(string? nam, string? tuan, string? thang)
        {
            string nameProc = "PROC_KIEM_DICH_Y_TE_NGUOI_V2";
            var response = kiemDichYTeRepository.GetListCDCKiemDichYTe(nam, tuan, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("phuong-tien")]
        public IActionResult PhuongTienNhapCanh(string? nam, string? tuan, string? thang)
        {
            string nameProc = "PROC_CDC_KIEM_DICH_Y_TE_PHUONG_TIEN_V2";
            var response = kiemDichYTeRepository.GetListCDCKiemDichYTe(nam, tuan, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        //[HttpGet("phuong-tien-xuat-canh")]
        //public IActionResult PhuongTienXuatCanh(string? nam, string? tuan, string? thang)
        //{
        //    string nameProc = "PROC_CDC_KIEM_DICH_Y_TE_PHUONG_TIEN_XUAT_CANH";
        //    var response = kiemDichYTeRepository.GetListCDCKiemDichYTe(nam, tuan,thang, nameProc);
        //    return response.success ? Ok(response) : BadRequest(response);
        //}
        //[HttpGet("phuong-tien-qua-canh")]
        //public IActionResult PhuongTienQuaCanh(string? nam, string? tuan, string? thang)
        //{
        //    string nameProc = "PROC_CDC_KIEM_DICH_Y_TE_PHUONG_TIEN_QUA_CANH";
        //    var response = kiemDichYTeRepository.GetListCDCKiemDichYTe(nam, tuan,thang, nameProc);
        //    return response.success ? Ok(response) : BadRequest(response);
        //}
        [HttpGet("4-the-tong")]
        public IActionResult _4TheTong(string? nam, string? tuan, string? thang)
        {
            string nameProc = "PROC_CDC_KIEM_DICH_Y_TE_4_THE_TONG_V2";
            var response = kiemDichYTeRepository.GetListCDCKiemDichYTe(nam, tuan, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
