﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/benh-truyen-nhiem")]
    public class BenhTruyenNhiemController : Controller
    {
        private BenhTruyenNhiemRepository benhTruyenNhiemRepository;

        public BenhTruyenNhiemController()
        {
            benhTruyenNhiemRepository = new BenhTruyenNhiemRepository();
        }
        [HttpGet("mac-soc-chet-sxh")]
        public IActionResult GetListMacSocChetSXH(string? nam, string? tuan)
        {
            string nameProc = "PROC_TRUYEN_NHIEM_MAC_SOC_CHET_SXH";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, tuan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("mac-chet-tcm")]
        public IActionResult GetListMacChetTCM(string? nam, string? tuan)
        {
            string nameProc = "PROC_TRUYEN_NHIEM_MAC_CHET_TCM";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, tuan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("du-bao-sxh")]
        public IActionResult GetListDuBaoSXH(string? nam, string? tuan)
        {
            string nameProc = "PROC_TRUYEN_NHIEM_DUBAODICH_SXH";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, tuan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("du-bao-tcm")]
        public IActionResult GetListDuBaoTCM(string? nam, string? tuan)
        {
            string nameProc = "PROC_TRUYEN_NHIEM_DUBAODICH_TCM";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, tuan, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("viem-gan-b")]
        public IActionResult GetListViemGanB(int nam, int thang, string? maHuyen)
        {
            string nameProc = "PROC_TK_SL_VIEM_GAN_B";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, maHuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("sl-sot-ret")]
        public IActionResult GetListSLSotRet(int nam, int thang, string? maHuyen)
        {
            string nameProc = "PROC_TK_SL_SOT_RET";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, maHuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("viem-gan-a")]
        public IActionResult GetListViemGanA(int nam, int thang, string? maHuyen)
        {
            string nameProc = "PROC_TK_SL_VIEM_GAN_A";
            var response = benhTruyenNhiemRepository.GetListCDCBenhTruyenNhiem(nam, thang, maHuyen, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        
    }
}
