﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{

    [ApiController]
    [Authorize]
    [Route("api/cdc/benh-khong-lay-hgi")]
    public class BenhKhongLayHGIController : Controller
    {
        private BenhKhongLayHGIRepository benhKhongLayHGIRepository;

        public BenhKhongLayHGIController()
        {
            benhKhongLayHGIRepository = new BenhKhongLayHGIRepository();
        }
        [HttpGet("dai-thao-duong-huyen")]
        public IActionResult GetListDaiThaoDuongHuyen(string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_DAITHAODUONG_HUYEN_HGI";
            var response = benhKhongLayHGIRepository.GetListCDCBenhKhongLay(nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("dai-thao-duong-tinh")]
        public IActionResult GetListDaiThaoDuongTinh(string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_DAITHAODUONG_TINH_HGI";
            var response = benhKhongLayHGIRepository.GetListCDCBenhKhongLay(nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tang-huyet-ap-huyen")]
        public IActionResult GetListTangHuyetApHuyen(string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_TANGHUYETAP_HUYEN_HGI";
            var response = benhKhongLayHGIRepository.GetListCDCBenhKhongLay(nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tang-huyet-ap-tinh")]
        public IActionResult GetListTangHuyetApTinh(string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_TANGHUYETAP_TINH_HGI";
            var response = benhKhongLayHGIRepository.GetListCDCBenhKhongLay(nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("4-the-tong")]
        public IActionResult GetList4TheTong(string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_4_THE_TONG_HGI";
            var response = benhKhongLayHGIRepository.GetListCDCBenhKhongLay(nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ty-le-tu-vong")]
        public IActionResult GetListTyLeTuVong(string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_BENH_KHONG_LAY_TYLE_TUVONG_TINH_HGI";
            var response = benhKhongLayHGIRepository.GetListCDCBenhKhongLay(nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
