﻿using AGG.IOC.YTE.Repository.Dashboard.PhapY;
using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/xet-nghiem-pkdk")]
    public class XetNghiem_PKDK_Controller : ControllerBase
    {
        private XetNghiem_PKDaKhoa_Repository XetNghiem_PKDaKhoa_Repository;
        public XetNghiem_PKDK_Controller()
        {
            XetNghiem_PKDaKhoa_Repository = new XetNghiem_PKDaKhoa_Repository();
        }
        [HttpGet("xet-nghiem")]
        public IActionResult XetNghiem(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_XET_NGHIEM";
            var response = XetNghiem_PKDaKhoa_Repository.XetNghiem_PKDaKhoa(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("suc-khoe-sinh-san")]
        public IActionResult PKDK_SUC_KHOE_SINH_SAN(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_KHOA_PK_DA_KHOA_SKSS";
            var response = XetNghiem_PKDaKhoa_Repository.XetNghiem_PKDaKhoa(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("chan-doan-hinh-anh")]
        public IActionResult PKDK_CHAN_DOAN_HINH_ANH(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_KHOA_PK_DA_KHOA_CDHA";
            var response = XetNghiem_PKDaKhoa_Repository.XetNghiem_PKDaKhoa(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("kham-suc-khoe")]
        public IActionResult PKDK_KHAM_SUC_KHOE(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_KHOA_PK_DA_KHOA_KSK";
            var response = XetNghiem_PKDaKhoa_Repository.XetNghiem_PKDaKhoa(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tiem-chung")]
        public IActionResult PKDK_TIEM_CHUNG(string? nam, string? thang)
        {
            string nameProc = "PROC_CDC_KHOA_PK_DA_KHOA_TIEM_CHUNG";
            var response = XetNghiem_PKDaKhoa_Repository.XetNghiem_PKDaKhoa(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }


    }
}
