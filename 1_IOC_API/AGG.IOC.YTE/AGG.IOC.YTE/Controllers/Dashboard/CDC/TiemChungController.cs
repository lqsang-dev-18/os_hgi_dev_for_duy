﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using AGG.IOC.YTE.Repository.Dashboard.NghiepVuY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/tiem-chung")]
    public class TiemChungController : Controller
    {
        private TiemChungRepository tiemChungRepository;

        public TiemChungController()
        {
            tiemChungRepository = new TiemChungRepository();
        }

        [HttpGet("day-du-te-1-tuoi")]
        public IActionResult GetListDayDuTE1Tuoi(string? nam, string? thang)
        {
            string nameProc = "PROC_TC_TYLE_TE_1TUOI";
            var response = tiemChungRepository.GetListCDCTiemChung(nam,  thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("viem-gan-b-tre-so-sinh")]
        public IActionResult GetListViemGanBTESoSinh(string? nam, string? thang)
        {
            string nameProc = "PROC_TC_TYLE_VIEMGAN_B";
            var response = tiemChungRepository.GetListCDCTiemChung(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("mo-rong-te-18-thang")]
        public IActionResult GetListMoRongTE18Thang(string? nam, string? thang)
        {
            string nameProc = "PROC_TC_TYLE_TE_18THANG";
            var response = tiemChungRepository.GetListCDCTiemChung(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("dpt-te-18-thang")]
        public IActionResult GetListDPTTE18Thang(string? nam, string? thang)
        {
            string nameProc = "PROC_TC_TYLE_DPT_TE_18THANG";
            var response = tiemChungRepository.GetListCDCTiemChung(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("mui-2-3-vnnb")]
        public IActionResult GetListMui23VNNB(string? nam, string? thang)
        {
            string nameProc = "PROC_TC_TYLE_MUI_2_3_VNNB";
            var response = tiemChungRepository.GetListCDCTiemChung(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("vat-phu-nu-co-thai")]
        public IActionResult GetListVATPhuNuCoThai(string? nam, string? thang)
        {
            string nameProc = "PROC_TC_TYLE_VAT_PHUNU_COTHAI";
            var response = tiemChungRepository.GetListCDCTiemChung(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
