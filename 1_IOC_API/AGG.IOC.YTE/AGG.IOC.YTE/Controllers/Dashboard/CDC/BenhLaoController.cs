﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CDC
{
    [ApiController]
    [Authorize]
    [Route("api/cdc/benh-lao")]
    public class BenhLaoController : ControllerBase
    {
        private BenhLaoRepository benhLaoRepository;

        public BenhLaoController()
        {
            benhLaoRepository = new BenhLaoRepository();
        }
        [HttpGet("phat-hien-lao")]
        public IActionResult GetListPhatHienLao(string? nam, string? thang)
        {
            string nameProc = "PROC_LAO_KHAM_PHATHIEN";
            var response = benhLaoRepository.GetListCDCBenhLao(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thu-nhan-dieu-tri-lao-cac-the")]
        public IActionResult GetListDieuTriCacThe(string? nam, string? thang)
        {
            string nameProc = "PROC_LAO_DIEUTRI_LAO_CACTHE";
            var response = benhLaoRepository.GetListCDCBenhLao(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thu-nhan-dieu-tri-lao-khang-thuoc")]
        public IActionResult GetListDieuTriKhangThuoc(string? nam, string? thang)
        {
            string nameProc = "PROC_LAO_DIEUTRI_LAO_KHANGTHUOC";
            var response = benhLaoRepository.GetListCDCBenhLao(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("6-the-tong")]
        public IActionResult GetList6TheTong(string? nam, string? thang)
        {
            string nameProc = "PROC_LAO_6_THE_TONG";
            var response = benhLaoRepository.GetListCDCBenhLao(nam, thang, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

    }
}
