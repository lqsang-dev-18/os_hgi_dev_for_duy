﻿using AGG.IOC.YTE.Repository.Dashboard.ToChucCanBo;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Runtime.CompilerServices;

namespace AGG.IOC.YTE.Controllers.Dashboard.ToChucCanBo
{
    [ApiController]
    [Authorize]
    [Route("api/to-chuc-can-bo")]
    public class ToChucCanBoController : ControllerBase
    {
        private ToChucCanBoRepository toChucCanBoRepository;
        public ToChucCanBoController()
        {
            toChucCanBoRepository = new ToChucCanBoRepository();
        }
        [HttpGet("ty-le-vien-chuc-hop-dong")]
        public IActionResult GetListTyLeVienChucHopDong(string? nam,string maDonVi)
        {
            string nameProc = "PROC_TCCB_VIENCHUC_HOPDONG_V1";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("quan-ly-nha-nuoc")]
        public IActionResult GetListQuanLyNhaNuoc(string? nam, string maDonVi)
        {
            string nameProc = "PROC_TCCB_QUANLY_NHANUOC_V1";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("trinh-do-chuyen-mon")]
        public IActionResult GetListTrinhDoChuyenMon(string? nam, string maDonVi)
        {
            string nameProc = "PROC_TCCB_TRINH_DO_CHUYEN_MON_V1";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ty-le-chinh-tri")]
        public IActionResult GetListTyLeChinhTri(string? nam, string maDonVi)
        {
            string nameProc = "PROC_TCCB_TYLE_CHINHTRI_V1";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ty-le-vien-chuc-gioi-tinh")]
        public IActionResult GetListVienChucGioiTinh(string? nam, string maDonVi)
        {
            string nameProc = "PROC_TCCB_VIENCHUC_GIOITINH";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("4-the-tong")]
        public IActionResult GetList4TheTong(string? nam, string maDonVi)
        {
            string nameProc = "PROC_TCCB_4_THE_TONG_V1";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk_tong_theo_bs")]
        public IActionResult GetTongTheoBs(string? nam, string maDonVi)
        {
            string nameProc = "PROC_TCCB_LOAI_CHUYEN_MON";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ty-le-can-bo-theo-gioi-tinh")]
        public IActionResult GetListTyLeCanBoTheoGioiTinh(string nam, string? maDonVi)
        {
            string nameProc = "PROC_TY_CANBO_THEO_GIOITINH";
            var response = toChucCanBoRepository.GetListToChucCanBo(nam, maDonVi, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
