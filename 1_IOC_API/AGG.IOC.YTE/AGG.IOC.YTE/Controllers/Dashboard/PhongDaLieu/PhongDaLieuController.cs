﻿using AGG.IOC.YTE.Repository.Dashboard.PhongDaLieu;
using AGG.IOC.YTE.Repository.Dashboard.VanBanDieuHanh;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.PhongDaLieu
{
    public class PhongDaLieuController : Controller
    {
        private PhongDaLieuRepository phongDaLieuRepository;
        public PhongDaLieuController()
        {
            phongDaLieuRepository = new PhongDaLieuRepository();
        }
        [HttpGet("api/tam-than-kinh-phong-da-lieu/benhnhan-phong-dalieu")]
        public IActionResult GetChiTieuPhongDaLieu(int? nam, string? thang, string? tungay, string? denngay)
        {
            var response = phongDaLieuRepository.GetChiTieu(nam, thang, tungay, denngay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("api/tam-than-kinh-phong-da-lieu/benhnhan-phong-dalieu-donvi")]
        public IActionResult GetChiTieuPhongDaLieuDonVi(int? nam, string? thang, string? tungay, string? denngay)
        {
            var response = phongDaLieuRepository.GetPhongDaLieuDonVi(nam, thang, tungay, denngay);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
