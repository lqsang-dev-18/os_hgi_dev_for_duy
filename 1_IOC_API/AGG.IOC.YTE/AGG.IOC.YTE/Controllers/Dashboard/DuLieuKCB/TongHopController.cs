using AGG.IOC.YTE.Repository.Dashboard.DuLieuKCB;
using AGG.IOC.YTE.Repository.PhanQuyen;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.DuLieuKCB
{
    [ApiController]
    [Authorize]
    [Route("api/kham-chua-benh/tong-hop")]
    public class TongHopController : ControllerBase
    {
        private TongHopRepository tongHopRepository;

        public TongHopController()
        {
            tongHopRepository = new TongHopRepository();
        }

        [HttpGet("tk-tong-so-ho-so")]
        public IActionResult TK_TongSoHoSo(int? nam, int? thang, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_TongSoHoSo(nam, thang, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-ho-so-vao-vien")]
        public IActionResult TK_HoSoVaoVien(string? nam, string? thang, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_HoSoVaoVien(nam, thang, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-cap-cuu")]
        public IActionResult TK_CapCuu(string? nam, string? thang, string? maCSKCB, int? page, int? size)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_CapCuu(nam, thang, maCSKCB, page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tu-vong")]
        public IActionResult TK_TuVong(string? nam, string? thang, string? maCSKCB, int? page, int? size)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_TuVong(nam, thang, maCSKCB, page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-kham-theo-ngay")]
        public IActionResult TK_KhamTheoNgay(string? nam, string? thang, string? maCSKCB, int maLoaiKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_KhamTheoNgay(nam, thang, maCSKCB, maLoaiKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-ra-vao-ngay")]
        public IActionResult TK_RaVaoNgay(string? nam, string? thang, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_RaVaoNgay(nam, thang, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-noi-tru-ngoai-tru-ngay")]
        public IActionResult TK_NoiTruNgoaiTruNgay(string? nam, string? thang, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_NoiTruNgoaiTruNgay(nam, thang, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-kham-bhyt-ngay")]
        public IActionResult TK_KhamBHYTNgay(string? nam, string? thang, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_KhamBHYTNgay(nam, thang, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-cap-cuu-tu-vong-ngay")]
        public IActionResult TK_CapCuuTuVongNgay(string? nam, string? thang, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_CapCuuTuVongNgay(nam, thang, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tai-nan-thuong-tich-ngay")]
        public IActionResult TK_TaiNanThuongTichNgay(string? nam, string? thang, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_TaiNanThuongTichNgay(nam, thang, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        /*====================== NguyenDTT-AGG: Implement APIs for IOC dashboard ======================*/
        [HttpGet("thong-ke-tong-quan-1")]
        public IActionResult TKeTongQuan1(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeTongQuan1(maDonVi ?? "", maHuyen ?? "", loaiHinh,  tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-su-dung-dich-vu-ky-thuat")]
        public IActionResult TKeSuDungDVKT(string? maDonVi, string? maHuyen, string tuNgay, string denNgay, int rowNum)
        {
            // Check role first
            var role = Role_API.CHECK_PHAN_QUYEN_API(Request);
            if (!role.ACCESS) {
                return BadRequest(role.MESSAGE);
            }
            // Get data
            var response = tongHopRepository.TKeSuDungDVKT(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay, rowNum);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-so-ho-so-kham-chua-benh-bhyt")]
        public IActionResult TKeSoHoSoBHYT(string? maDonVi, string? maHuyen, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeSoHoSoBHYT(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-gioi-tinh-kham-chua-benh")]
        public IActionResult TKeGioiTinhKhamChuaBenh(string? maDonVi, string? maHuyen, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeGioiTinhKhamChuaBenh(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-so-ngay-dieu-tri")]
        public IActionResult TKeSoNgayDieuTri(string? maDonVi, string? maHuyen, string tuNgay, string denNgay)
        {
            // Check role first
            var role = Role_API.CHECK_PHAN_QUYEN_API(Request);
            if (!role.ACCESS) {
                return BadRequest(role.MESSAGE);
            }
            // Get data
            var response = tongHopRepository.TKeSoNgayDieuTri(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-cac-benh-pho-bien")]
        public IActionResult TKeTopCacBenhPhoBien(string? maDonVi, string? maHuyen, string tuNgay, string denNgay, int rowNum)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeTopCacBenhPhoBien(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay, rowNum);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-cac-benh-pho-bien-theo-khu-vuc")]
        public IActionResult TKeTopCacBenhPhoBienTheoKhuVuc(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay, int rowNum)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeTopCacBenhPhoBienTheoKhuVuc(maDonVi ?? "", maHuyen ?? "", loaiHinh, tuNgay, denNgay, rowNum);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-tai-nan-thuong-tich")]
        public IActionResult TKeTaiNanThuongTich(string? maDonVi, string? maHuyen, string tuNgay, string denNgay, int rowNum)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeTaiNanThuongTich(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay, rowNum);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-tai-nan-thuong-tich-theo-khu-vuc")]
        public IActionResult TKeTaiNanThuongTichTheoKhuVuc(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay, int rowNum)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeTaiNanThuongTichTheoKhuVuc(maDonVi ?? "", maHuyen ?? "", loaiHinh, tuNgay, denNgay, rowNum);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-chi-phi-kham-chua-benh")]
        public IActionResult TKeChiPhiKhamChuaBenh(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeChiPhiKhamChuaBenh(maDonVi ?? "", maHuyen ?? "", loaiHinh, tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-chi-phi-kham-chua-benh-theo-khu-vuc")]
        public IActionResult TKeChiPhiKhamChuaBenhTheoKhuVuc(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay, int? rowNum)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeChiPhiKhamChuaBenhTheoKhuVuc(maDonVi ?? "", maHuyen ?? "", loaiHinh, tuNgay, denNgay, rowNum);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-so-ca-cap-cuu")]
        public IActionResult TKeSoCaCapCuu(string? maDonVi, string? maHuyen, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeSoCaCapCuu(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-so-ca-tu-vong")]
        public IActionResult TKeSoCaTuVong(string? maDonVi, string? maHuyen, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeSoCaTuVong(maDonVi ?? "", maHuyen ?? "", tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-nhom-tuoi-kham-chua-benh")]
        public IActionResult TKeNhomTuoiKhamChuaBenh(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeNhomTuoiKhamChuaBenh(maDonVi ?? "", maHuyen ?? "", loaiHinh, tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-kham-chua-benh-bao-hiem")]
        public IActionResult TKeKhamChuaBenhBaoHiem(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            // Temporary remove checking role, comeback later
            // Get data
            var response = tongHopRepository.TKeKhamChuaBenhBaoHiem(maDonVi ?? "", maHuyen ?? "", loaiHinh, tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }
        /*====================== END: Implement APIs for IOC dashboard ======================*/


        /*====================== TUYENNT.AGG: Implement APIs for IOC dashboard ======================*/
        [HttpGet("thong-ke-top-10-benh-nguy-co-cao")]
        public IActionResult TKTop10BenhNguyCoCao(string? maDonVi, string? maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            // Get data
            var response = tongHopRepository.TKTop10BenhNguyCoCao(maDonVi ?? "", maHuyen ?? "", loaiHinh, tuNgay, denNgay);
            return response.success ? Ok(response) : BadRequest(response);
        }
        /*====================== END: Implement APIs for IOC dashboard ======================*/


        [HttpGet("tk-kham-bhyt")]
        public IActionResult TK_KhamBHYT(int? nam, int? thang, string? maCSKCB, int? maLoaiKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null && thang != null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_KhamBHYT(nam, thang, maCSKCB, maLoaiKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-theo-do-tuoi")]
        public IActionResult TK_TheoDoTuoi(int? nam, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_TheoDoTuoi(nam, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }


        [HttpGet("tk-theo-loai-benh")]
        public IActionResult TK_TheoLoaiBenh(int? nam, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_TheoLoaiBenh(nam, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }


        [HttpGet("tk-chi-phi-thanh-toan")]
        public IActionResult TK_ChiPhiBenhNhanThanhToan(int? nam, string? maCSKCB)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            if (nam == null)
            {
                return BadRequest("This param [nam] is not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_ChiPhiBenhNhanThanhToan(nam, maCSKCB);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thong-ke-kcb-khong-bhyt")]
        public IActionResult TK_KCB_KhongBHYT(string tuNgay, string denNgay, string? maHuyen, string? maDonVi, int? loaiHinh)
        {
            if (tuNgay == null)
            {
                return BadRequest("This param [tuNgay] must be not null!");
            } else if (denNgay == null)
            {
                return BadRequest("This param [denNgay] must be not null!");
            }

            //Thực hiện yêu cầu
            var response = tongHopRepository.TK_KCB_KhongBHYT(tuNgay, denNgay, maHuyen, maDonVi, loaiHinh);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}