﻿using AGG.IOC.YTE.Repository.Dashboard.PhapY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.PhapY
{
    [ApiController]
    //[Authorize]
    [Route("api/phap-y")]
    public class PhapYController : ControllerBase
    {
        private PhapYRepository phapYRepository;
        public PhapYController()
        {
            phapYRepository = new PhapYRepository();
        }
        [HttpGet("nhan-luc")]
        public IActionResult GetListNhanLuc(string? nam)
        {
            string nameProc = "PROC_PHAPY_NHAN_LUC";
            var response = phapYRepository.GetListPhapY(nam, null, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("so-ca-giam-dinh")]
        public IActionResult GetListSoCaGiamDinh(string? nam)
        {
            string nameProc = "PROC_PHAPY_LOAI_GIAM_DINH";
            var response = phapYRepository.GetListPhapY(nam, null, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
