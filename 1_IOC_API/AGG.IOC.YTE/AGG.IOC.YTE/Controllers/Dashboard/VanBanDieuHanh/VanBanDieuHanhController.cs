﻿using AGG.IOC.YTE.Repository.DanhMucChung;
using AGG.IOC.YTE.Repository.Dashboard.VanBanDieuHanh;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.VanBanDieuHanh
{
    [Route("api/van-ban-dieu-hanh")]
    [ApiController]
    [Authorize]
    public class VanBanDieuHanhController : Controller
    {
        private VanBanDieuHanhRepository vanBanDieuHanhRepository;

        public VanBanDieuHanhController()
        {
            vanBanDieuHanhRepository = new VanBanDieuHanhRepository();
        }

        [HttpGet("don-vi")]
        public IActionResult GetListDonVi(int? page, int? size)
        {
            var response = vanBanDieuHanhRepository.GetListDonVi(page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-ke-2-chi-tieu")]
        public IActionResult GetList2ChiTieu(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetList2ChiTieu(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-vb-di-den-theo-don-vi")]
        public IActionResult GetTKVBDIDENTHEODONVI(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKVBDIDENTHEODONVI(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-vb-di-den-ky-so")]
        public IActionResult GetTKVBDIDENKYSO(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKVBDIDENKYSO(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-vb-den-tre-han")]
        public IActionResult GetTKVBDENTREHAN(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKVBDENTREHAN(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tinh-hinh-phe-duyet-vb-den")]
        public IActionResult GetTKTINHHINHPHEDUYETVBDEN(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTINHHINHPHEDUYETVBDEN(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tinh-hinh-ky-so-vb-di-den")]
        public IActionResult GetTKTINHHINHKYSOVBDIDEN(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTINHHINHKYSOVBDIDEN(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tinh-hinh-duyet-vb-den")]
        public IActionResult GetTKTINHHINHDUYETVBDEN(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTINHHINHDUYETVBDEN(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tinh-hinh-tiep-nhan-vb-den")]
        public IActionResult GetTKTINHHINHTIEPNHANVBDEN(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTINHHINHTIEPNHANVBDEN(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tinh-hinh-xl-vb-den-bi-tre-han")]
        public IActionResult GetTKTINHHINHXULYVBDENBITRENHAN(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTINHHINHXULYVBDENBITRENHAN(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-tinh-hinh-sd-he-thong")]
        public IActionResult GetTKTINHHINHSUDUNGHETHONG(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTINHHINHSUDUNGHETHONG(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-top-5-don-vi-co-vb-di-den-nhieu-nhat")]
        public IActionResult GetTKTOP5DONVICOVBDIDENNHIEUNHAT(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTOP5DONVICOVBDIDENNHIEUNHAT(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-top-5-don-vi-co-vb-den-tre-han-nhieu-nhat")]
        public IActionResult GetTKTOP5DONVICOVBDENTREHANNHIEUNHAT(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTOP5DONVICOVBDENTREHANNHIEUNHAT(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-top-5-don-vi-co-so-nguoi-truy-cap-nhieu-nhat")]
        public IActionResult GetTKTOP5DONVICOSONGUOITRUYCAPNHIEUNHAT(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTOP5DONVICOSONGUOITRUYCAPNHIEUNHAT(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-top-5-don-vi-co-nhieu-vb-den-chua-tiep-nhan")]
        public IActionResult GetTKTOP5DONVICONHIEUVBDENCHUATIEPNHAN(string? nam, string? thang, string? madonvi)
        {
            var response = vanBanDieuHanhRepository.GetTKTOP5DONVICONHIEUVBDENCHUATIEPNHAN(nam, thang, madonvi);
            return response.success ? Ok(response) : BadRequest(response);
        }

        //*** dasdboard dữ liệu theo phòng ban ***//

        [HttpGet("phong-ban")]
        public IActionResult GetListPhongBan(int? page, int? size)
        {
            var response = vanBanDieuHanhRepository.GetListPhongBan(page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-vb-di-den-theo-phong-ban")]
        public IActionResult GetTKVBDIDENTHEOPHONGBAN(string? nam, string? thang, string? maphongban)
        {
            var response = vanBanDieuHanhRepository.GetTKVBDIDENTHEOPHONGBAN(nam, thang, maphongban);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-top-10-ca-nhan-nhan-vb-den-nhieu-nhat")]
        public IActionResult GetTKTOP10CANHANNHANVBDENNHIEUNHAT(string? nam, string? thang, string? maphongban)
        {
            var response = vanBanDieuHanhRepository.GetTKTOP10CANHANNHANVBDENNHIEUNHAT(nam, thang, maphongban);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-top-10-ca-nhan-tham-muu-vb-di-nhieu-nhat")]
        public IActionResult GetTKTOP10CANHANTHAMMUUVBDINHIEUNHAT(string? nam, string? thang, string? maphongban)
        {
            var response = vanBanDieuHanhRepository.GetTKTOP10CANHANTHAMMUUVBDINHIEUNHAT(nam, thang, maphongban);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tk-vb-den-tre-han-theo-phong-ban")]
        public IActionResult GetTKVBDENTREHANTHEOPHONGBAN(string? nam, string? thang, string? maphongban)
        {
            var response = vanBanDieuHanhRepository.GetTKVBDENTREHANTHEOPHONGBAN(nam, thang, maphongban);
            return response.success ? Ok(response) : BadRequest(response);
        }

        //*** dasdboard thông tin chung tình hình xử lý ***//

        [HttpGet("vbden")]
        public IActionResult GetCacChiSoVBDen(string? nam, string? thang, string unit_type)
        {
            var response = vanBanDieuHanhRepository.GetCacChiSoVBDen(nam, thang, unit_type);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbdi")]
        public IActionResult GetCacChiSoVBDi(string? nam, string? thang, string unit_type)
        {
            var response = vanBanDieuHanhRepository.GetCacChiSoVBDi(nam, thang, unit_type);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbden-theothang")]
        public IActionResult GetTKVBDenvaDenQuaHanTheoThang(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTKVBDenvaDenQuaHanTheoThang(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thong-tin-chung-tinh-hinh-xu-ly-vbdi-theothang")]
        public IActionResult GetCacChiSoVBDiLienThongTheoThang(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetCacChiSoVBDiLienThongTheoThang(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thongketruycap")]
        public IActionResult GetTKSoLuongTruyCapTheoThang(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTKSoLuongTruyCapTheoThang(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        //*** dasdboard Hieu Suat Xu ly Va Dieu Hanh ***//

        [HttpGet("vbden-top5")]
        public IActionResult GetTop5DonViCoVbDenNhieuNhat(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTop5DonViCoVbDenNhieuNhat(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbden-trungbinh")]
        public IActionResult GetTrungBinhSoLuongVbDen(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTrungBinhSoLuongVbDen(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbden-top5-dunghan")]
        public IActionResult GetTop5DonViCoTyLeXlVbDungHan(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTop5DonViCoTyLeXlVbDungHan(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbden-top5-quahan")]
        public IActionResult GetTop5DonViCoTyLeXlVbQuaHan(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTop5DonViCoTyLeXlVbQuaHan(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thongketruycap-top5")]
        public IActionResult GetTop5DonViCoNguoiDungThuongXuyenTruyCap(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTop5DonViCoNguoiDungThuongXuyenTruyCap(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbdi-trungbinh")]
        public IActionResult GetTrungBinhSoLuongVbDi(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTrungBinhSoLuongVbDi(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbdi-top5")]
        public IActionResult GetTop5DonViCoVbDiNhieuNhat(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTop5DonViCoVbDiNhieuNhat(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbdi-lienthong-top5")]
        public IActionResult GetTop5DonViCoVbLienThongNhieuNhat(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetTop5DonViCoVbLienThongNhieuNhat(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }


        //*** dasdboard Thống kê đơn vị và phòng ban chuyên môn  trực thuộc sở y tế ***//

        [HttpGet("vbden-donvi")]
        public IActionResult GetCacChiSoVbDenTheoDonVi(string? nam, string? thang, string unit_type)
        {
            var response = vanBanDieuHanhRepository.GetCacChiSoVbDenTheoDonVi(nam, thang, unit_type);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("vbdi-donvi")]
        public IActionResult GetSoLuongVbDiTheoDonVi(string? nam, string? thang, string unit_type)
        {
            var response = vanBanDieuHanhRepository.GetSoLuongVbDiTheoDonVi(nam, thang, unit_type);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("thongketruycap-donvi")]
        public IActionResult GetSoLuongTruyCapHeThongTheoDonVi(string? nam, string? thang, string unit_type)
        {
            var response = vanBanDieuHanhRepository.GetSoLuongTruyCapHeThongTheoDonVi(nam, thang, unit_type);
            return response.success ? Ok(response) : BadRequest(response);
        }

        //*** dasdboard quản lý công việc ***//

        [HttpGet("congviec")]
        public IActionResult GetCacChiSoCongViec(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetCacChiSoCongViec(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("congviec-donvi")]
        public IActionResult GetCacChiSoCongViecHoanThanhTheoDonVi(string? nam, string? thang)
        {
            var response = vanBanDieuHanhRepository.GetCacChiSoCongViecHoanThanhTheoDonVi(nam, thang);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
