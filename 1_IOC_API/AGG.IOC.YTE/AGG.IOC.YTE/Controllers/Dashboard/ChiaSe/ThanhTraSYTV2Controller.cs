﻿using AGG.IOC.YTE.Repository.Dashboard.CDC;
using AGG.IOC.YTE.Repository.Dashboard.ThanhTraSYT;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.ChiaSe
{
    [Route("api/thanh-tra-syt")]
    [ApiController]
    //[Authorize]
    public class ThanhTraSYTV2Controller : Controller
    {
        private ThanhTraSYTRepository _ThanhTraSYTRepository;

        public ThanhTraSYTV2Controller()
        {
            _ThanhTraSYTRepository = new ThanhTraSYTRepository();
        }

        [HttpGet("4-thetong-linhvuc")]
        public IActionResult GetChiSoThanhTraLinhVuc(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_GET_TTR_LINHVUC_CS";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("linhvuc-vipham-cungky")]
        public IActionResult GetTKCoSoViPham(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_GET_TTR_LINHVUC_TK";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("noidung-canhan-vipham-cungky")]
        public IActionResult GetTKTongSoNguoiBiPhatHanhChinhSoSanhCungKy(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_GET_TTR_NOIDUNG_CN_HC_CK";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("noidung-tien-vipham-cungky")]
        public IActionResult GetTKTongSoTienViPhamSoSanhCungKy(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_GET_TTR_NOIDUNG_VP_CK";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("noidung-doituong-khoito-cungky")]
        public IActionResult GetTKTongSoNguoiBiKhoiToSoSanhCungKy(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_GET_TTR_NOIDUNG_KT_CK";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("noidung-tien-vipham-donvi")]
        public IActionResult GetTKTongSoTienViPhamTheoDiaPhuong(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_GET_TTR_NOIDUNG_VP_DP";
            var response = _ThanhTraSYTRepository.GetData_DashboardThanhTra(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
