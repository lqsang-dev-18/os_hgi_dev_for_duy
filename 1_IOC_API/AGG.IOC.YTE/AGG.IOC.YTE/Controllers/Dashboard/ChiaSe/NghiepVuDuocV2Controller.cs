﻿using AGG.IOC.YTE.Repository.ChiaSe;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.ChiaSe
{
    [Route("api/[controller]")]
    [ApiController]
    public class NghiepVuDuocV2Controller : ControllerBase
    {
        private NghiepVuDuocV2Repository nghiepVuDuocRepository;
        public NghiepVuDuocV2Controller()
        {
            nghiepVuDuocRepository = new NghiepVuDuocV2Repository();
        }

        [HttpGet("nvd-cap-gcn-tong")]
        public IActionResult GetListTongTheGCN(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_NVD_TONG_HGI";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("nvd-so-cs-theo-huyen-hgi")]
        public IActionResult GetListCoSoTheoHuyen(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_NVD_TK_SO_CS_THEO_HUYEN_HGI";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("nvd-ty-le-hinh-thuc-to-chuc-hgi")]
        public IActionResult TyLeHinhThucToChuc(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_NVD_TILE_HINHTHUC_HGI";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("nvd-gcn-gan-nhat-hgi")]
        public IActionResult GetListDSGCNGanNhat(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay)
        {
            string nameProc = "PROC_NVD_DANHSACH_CAP_GCN_HGI";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(ma_tinh, ma_huyen, ma_xa, loai_cbx, nam, quy, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
