﻿using AGG.IOC.YTE.Repository.Dashboard.NghiepVuDuoc;
using AGG.IOC.YTE.Repository.Dashboard.PhapY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.CongBoMyPham
{
    [ApiController]
    [Authorize]
    [Route("api/nghiep-vu-duoc/cong-bo-my-pham")]
    public class CongBoMyPhamController : ControllerBase
    {
        private CongBoMyPhamRepository congBoMyPhamRepository;
        public CongBoMyPhamController()
        {
            congBoMyPhamRepository = new CongBoMyPhamRepository();
        }
        [HttpGet("6-the-tong")]
        public IActionResult GetList6TheTong(int? loai_cbx, string? nam, string? thang, string? quy, string? tungay, string? denngay)
        {
            string nameProc = "PROC_CONG_BO_MY_PHAM_TONG_SO_NHAN_HANG";
            var response = congBoMyPhamRepository.GetListCongBoMyPham(loai_cbx, nam, thang, quy, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-cty-san-xuat-theo-huyen")]
        public IActionResult getCtySanXuatTheoHuyen(int? loai_cbx, string? nam, string? thang, string? quy, string? tungay, string? denngay)
        {
            string nameProc = "PROC_CONG_BO_MY_PHAM_CTY_THEO_HUYEN";
            var response = congBoMyPhamRepository.GetListCongBoMyPham(loai_cbx, nam, thang, quy, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-san-phan-theo-cty-san-xuat")]
        public IActionResult getSanPhamTheoCtySanXuat(int? loai_cbx, string? nam, string? thang, string? quy, string? tungay, string? denngay)
        {
            string nameProc = "PROC_CONG_BO_MY_PHAM_SAN_PHAM_THEO_CTY";
            var response = congBoMyPhamRepository.GetListCongBoMyPham(loai_cbx, nam, thang, quy, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("tk-nhan-hieu-theo-cty")]
        public IActionResult getNhanHieuTheoCty(int? loai_cbx, string? nam, string? thang, string? quy, string? tungay, string? denngay)
        {
            string nameProc = "PROC_CONG_BO_MY_PHAM_NHAN_HANG_THEO_CTY";
            var response = congBoMyPhamRepository.GetListCongBoMyPham(loai_cbx, nam, thang, quy, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("6-the-tong-chi-tiet")]
        public IActionResult GetList6TheTongChiTiet(int? loai_cbx, string? nam, string? thang, string? quy, string? tungay, string? denngay, string? loai, int? page, int? size)
        {
            string nameProc = "PROC_CONG_BO_MY_PHAM_TONG_THE_CHI_TIET";
            var response = congBoMyPhamRepository.GetPageListCongBoMyPham(loai_cbx, nam, thang, quy, tungay, denngay, loai, page, size, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
