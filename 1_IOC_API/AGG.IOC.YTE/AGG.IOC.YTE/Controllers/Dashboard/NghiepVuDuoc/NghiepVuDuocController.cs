﻿using AGG.IOC.YTE.Repository.Dashboard.NghiepVuDuoc;
using AGG.IOC.YTE.Repository.Dashboard.PhapY;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.NghiepVuDuoc
{
    [ApiController]
    //[Authorize]
    [Route("api/nghiep-vu-duoc")]
    public class NghiepVuDuocController : ControllerBase
    {
        private NghiepVuDuocRepository nghiepVuDuocRepository;
        public NghiepVuDuocController()
        {
            nghiepVuDuocRepository = new NghiepVuDuocRepository();
        }
        [HttpGet("3-tong-the")]
        public IActionResult GetListTongThe(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_NVD_3_THE_TONG";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("van-bang-cchn")]
        public IActionResult GetListVanBangCCHN(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_NVD_LOAI_VAN_BANG";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ds-cchn-gan-nhat")]
        public IActionResult GetListDSCCHNGanNhat(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_NVD_DS_CCHN_GAN_NHAT";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("ds-cchn-theo-loai")]
        public IActionResult GetListDSCCHNTheoLoai(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay, string? loai, int? page, int? size)
        {
            string nameProc = "PROC_NVD_DS_CCHN_THEO_LOAI";
            var response = nghiepVuDuocRepository.GetPageListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, loai, page, size, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }

        // tình hình cấp gcn
        [HttpGet("nvd-cap-gcn-tong")]
        public IActionResult GetListTongTheGCN(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_NVD_TONG";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nvd-so-cs-theo-huyen")]
        public IActionResult GetListCoSoTheoHuyen(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_NVD_TK_SO_CS_THEO_HUYEN";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nvd-ty-le-hinh-thuc-to-chuc")]
        public IActionResult TyLeHinhThucToChuc(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_NVD_TILE_HINHTHUC";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nvd-gcn-gan-nhat")]
        public IActionResult GetListDSGCNGanNhat(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay)
        {
            string nameProc = "PROC_NVD_DANHSACH_CAP_GCN";
            var response = nghiepVuDuocRepository.GetListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nvd-gcn-theo-loai")]
        public IActionResult GetListDSGCNTheoLoai(int? loai_cbx, string? nam, string? thang, string? tungay, string? denngay, string? loai, int? page, int? size)
        {
            string nameProc = "PROC_NVD_DANHSACH_GCN_THEO_LOAI";
            var response = nghiepVuDuocRepository.GetPageListNghiepVuDuoc(loai_cbx, nam, thang, tungay, denngay, loai, page, size, nameProc);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}
