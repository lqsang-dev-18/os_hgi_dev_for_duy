﻿using AGG.IOC.YTE.Repository.Dashboard.TrungTamKiemNghiem;
using Microsoft.AspNetCore.Mvc;

namespace AGG.IOC.YTE.Controllers.Dashboard.TrungTamKiemNghiem
{
    public class KyThuatKiemNghiemController : Controller
    {
        private KyThuatKiemNghiemRepository kyThuatKiemNghiemRepository;
        public KyThuatKiemNghiemController()
        {
            kyThuatKiemNghiemRepository = new KyThuatKiemNghiemRepository();
        }
        [HttpGet("api/ky-thuat-kiem-nghiem/so-lieu-chi-tieu-phuong-phap-kiem-nghiem")]
        public IActionResult GetChiTieuPPKiemNghiem(int? nam, string? thang, string? tungay, string? denngay)
        {
            var response = kyThuatKiemNghiemRepository.GetChiTieu(nam, thang, tungay, denngay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("api/ky-thuat-kiem-nghiem/so-lieu-chi-tieu-dinh-tinh")]
        public IActionResult GetChiTieuDinhTinh(int? nam, string? thang, string? tungay, string? denngay)
        {
            var response = kyThuatKiemNghiemRepository.GetChiTieuDinhTinh(nam, thang, tungay, denngay);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("api/ky-thuat-kiem-nghiem/so-lieu-chi-tieu-dinh-luong")]
        public IActionResult GetChiTieuDinhLuong(int? nam, string? thang, string? tungay, string? denngay)
        {
            var response = kyThuatKiemNghiemRepository.GetChiTieuDinhLuong(nam, thang, tungay, denngay);
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}

