﻿
using AGG.IOC.YTE.Repository.DanhMucChung;
using AGG.IOC.YTE.Repository.PhanQuyen;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AGG.IOC.YTE.Controllers.DanhMucChung
{
    [Route("api/danh-muc-dung-chung")]
    [ApiController]
    [Authorize]
    public class DanhMucChungController : Controller
    {
        private DanhMucChungRepository danhMucChungRepository;

        public DanhMucChungController()
        {
            danhMucChungRepository = new DanhMucChungRepository();
        }

        [HttpGet("quoc-tich")]
        public IActionResult GetListQuocTich(string? keyword, int? page, int? size)
        {
            var response = danhMucChungRepository.GetListQuocTich(keyword, page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("dan-toc")]
        public IActionResult GetListDanToc(string? keyword, int? page, int? size)
        {
            var response = danhMucChungRepository.GetListDanToc(keyword, page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("ton-giao")]
        public IActionResult GetListTonGiao(string? keyword, int? page, int? size)
        {
            var response = danhMucChungRepository.GetListTonGiao(keyword, page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("don-vi")]
        public IActionResult GetListDonVi(string? keyword, int? page, int? size, int? donViCha)
        {
            var response = danhMucChungRepository.GetListDonVi(keyword, page, size, donViCha);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("co-so-kham-chua-benh")]
        public IActionResult GetListCoSoKhamChuaBenh(string? keyword, decimal? maHuyen, int? loaiHinh)
        {
            var response = danhMucChungRepository.GetListCoSoKhamChuaBenh(keyword, maHuyen, loaiHinh);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("tinh")]
        public IActionResult GetListTinh(string? keyword, int? page, int? size)
        {
            var response = danhMucChungRepository.GetListTinh(keyword, page, size);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("huyen")]
        public IActionResult GetListHuyen(string? keyword, int? page, int? size, int? idTinh)
        {
            var response = danhMucChungRepository.GetListHuyen(keyword, page, size, idTinh);
            return response.success ? Ok(response) : BadRequest(response);
        }

        [HttpGet("xa")]
        public IActionResult GetListXa(string? keyword, int? page, int? size, int? idTinh, int? idHuyen)
        {
            var response = danhMucChungRepository.GetListXa(keyword, page, size, idTinh, idHuyen);
            return response.success ? Ok(response) : BadRequest(response);
        }


        [HttpGet("dm-ky")]
        public IActionResult GetListHuyen(string? loaiKy, int? year, int? quy, int? month, int? startYear)
        {

            //Kiểm tra quyền API -------------------------------|

            var us = Role_API.CHECK_PHAN_QUYEN_API(Request);

            if (!us.ACCESS)
            {
                return BadRequest(us.MESSAGE);
            }
            //--------------------------------------------------|

            //Thực hiện yêu cầu
            var response = danhMucChungRepository.GetDMKy(loaiKy, year, quy, month, startYear);
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("nam")]
        public IActionResult GetListNam()
        {
            //Thực hiện yêu cầu
            var response = danhMucChungRepository.GetListNam();
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("thang")]
        public IActionResult GetListThang()
        {
            //Thực hiện yêu cầu
            var response = danhMucChungRepository.GetListThang();
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("quy")]
        public IActionResult GetListQuy()
        {
            //Thực hiện yêu cầu
            var response = danhMucChungRepository.GetListQuy();
            return response.success ? Ok(response) : BadRequest(response);
        }
        [HttpGet("danh-muc-donvi")]
        public IActionResult getDanhMucDonVi()
        {
            //Thực hiện yêu cầu
            var response = danhMucChungRepository.getDanhMucDonVi();
            return response.success ? Ok(response) : BadRequest(response);
        }
    }
}

