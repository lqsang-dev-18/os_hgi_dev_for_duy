﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;
using System.Runtime.CompilerServices;
using static Microsoft.Extensions.Logging.EventSource.LoggingEventSource;

namespace AGG.IOC.YTE.Repository.DanhMucChung
{
    public class DanhMucChungRepository
    {
        public static ResponsePageList GET_PAGELIST_DM_DONVI(string keywork, int page, int size)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    PAGE = page,
                    SIZE = size
                };
                return csdl.GetPageList("HISAGG_DEV.GET_PAGELIST_DM_DONVI", obj);
            }
        }

        //Lấy danh mục quốc tịch
        public ResponsePageList GetListQuocTich(string? keyword, int? page, int? size)
        {
            var paramters = new
            {
                KEYWORD = keyword ?? "",
                PAGE = page ?? 1,
                SIZE = size ?? 10
            };
            var context = new SQLHelper();
            return context.GetPageList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_PAGE_LIST_QUOC_TICH", paramters);

        }

        //Lấy danh mục dân tộc
        public ResponsePageList GetListDanToc(string? keyword, int? page, int? size)
        {
            var paramters = new
            {
                KEYWORD = keyword ?? "",
                PAGE = page ?? 1,
                SIZE = size ?? 10
            };
            var context = new SQLHelper();
            return context.GetPageList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_PAGE_LIST_DAN_TOC", paramters);

        }

        //Lấy danh mục tôn giáo
        public ResponseList GetListTonGiao(string? keyword, int? page, int? size)
        {
            var paramters = new
            {
                KEYWORD = keyword ?? ""
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_LIST_TON_GIAO", paramters);

        }

        //Lấy danh mục đơn vị
        public ResponsePageList GetListDonVi(string? keyword, int? page, int? size, int? donViCha)
        {
            var paramters = new
            {
                KEYWORD = keyword ?? "",
                PAGE = page ?? 1,
                SIZE = size ?? 10,
                DON_VI_CHA = donViCha ?? null
            };
            var context = new SQLHelper();
            return context.GetPageList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_PAGE_LIST_DON_VI", paramters);

        }

        //Lấy danh mục đơn vị
        public ResponseList GetListCoSoKhamChuaBenh(string? keyword, decimal? maHuyen, int? loaiHinh) {
            var paramters = new {
                KEYWORD = keyword ?? "",
                MA_HUYEN = maHuyen,
                LOAI_HINH = loaiHinh
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_DANH_SACH_CO_SO_KCB", paramters);

        }

        //Lấy danh mục tỉnh, thành
        public ResponsePageList GetListTinh(string? keyword, int? page, int? size)
        {
            var paramters = new
            {
                KEYWORD = keyword ?? "",
                PAGE = page ?? 1,
                SIZE = size ?? 10
            };
            var context = new SQLHelper();
            return context.GetPageList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_PAGE_LIST_TINH", paramters);

        }

        //Lấy danh mục quận huyện
        public ResponsePageList GetListHuyen(string? keyword, int? page, int? size, int? idTinh)
        {
            var paramters = new
            {
                KEYWORD = keyword ?? "",
                PAGE = page ?? 1,
                SIZE = size ?? 10,
                ID_TINH = idTinh ?? null
            };
            var context = new SQLHelper();
            return context.GetPageList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_PAGE_LIST_HUYEN", paramters);

        }

        //Lấy danh mục xã phường
        public ResponsePageList GetListXa(string? keyword, int? page, int? size, int? idTinh, int? idHuyen)
        {
            var paramters = new
            {
                KEYWORD = keyword ?? "",
                PAGE = page ?? 1,
                SIZE = size ?? 10,
                ID_TINH = idTinh ?? null,
                ID_HUYEN = idHuyen ?? null
            };
            var context = new SQLHelper();
            return context.GetPageList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_PAGE_LIST_XA", paramters);

        }

        //Lấy danh sách kỳ
        public ResponseList GetDMKy(string? loaiKy, int? year, int? quy, int? month, int? startYear)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    LOAI_KY = loaiKy,
                    YEAR = year,
                    QUY = quy,
                    MONTH = month,
                    START_YEAR = startYear,
                };
                return csdl.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GET_DM_KY", obj);
            }

        }

        //Lấy danh sách Năm
        public ResponseList GetListNam()
        {
            var paramters = new
            {
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GIATRI_NAM", paramters);

        }
        //Lấy danh sách Năm
        public ResponseList GetListThang()
        {
            var paramters = new
            { };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GIATRI_THANG", paramters);

        }
        //Lấy danh sách Năm
        public ResponseList GetListQuy()
        {
            var paramters = new
            { };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_GIATRI_QUY", paramters);

        }
        public ResponseList getDanhMucDonVi()
        {
            var paramters = new
            { };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_LAY_DM_DONVI", paramters);

        }
    }
}

