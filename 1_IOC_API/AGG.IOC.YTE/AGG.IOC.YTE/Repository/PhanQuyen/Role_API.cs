﻿using System;
using System.Net.NetworkInformation;
using System.Security.Policy;
using AGG.IOC.YTE.Common.Response;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http;
using AGG.IOC.YTE.Common.Jwt;
using AGG.IOC.YTE.Models.Authentication;

namespace AGG.IOC.YTE.Repository.PhanQuyen
{
    public class Role_API
    {
        public bool ACCESS{ get; set; }
        public string MESSAGE { get; set; }
        public string ID_LOG_IN { get; set; }
        public string MA_USER { get; set; }
        public string MA_API { get; set; }
        public string USERNAME { get; set; }
        public string LEVEL_USER { get; set; }
        public string MA_DON_VI { get; set; }

        public Role_API (bool access, string message, string id_log_in, string ma_user, string ma_api, string username, string level_user, string ma_don_vi)
        {
            this.ACCESS = access;
            this.MESSAGE = message;
            this.ID_LOG_IN = id_log_in;
            this.MA_USER = ma_user;
            this.MA_API = ma_api;
            this.USERNAME = username;
            this.LEVEL_USER = level_user;
            this.MA_DON_VI = ma_don_vi;
        }

        public static Role_API CHECK_PHAN_QUYEN_API(HttpRequest request)
        {
            string url_api = request.GetEncodedPathAndQuery();

            // Decode Jwt
            UserToken user = Jwt.DecodeToken(request);

            // Kiểm tra quyền user
            if (url_api.Contains("?"))
            {
                url_api = url_api.Substring(0, url_api.IndexOf("?"));
            }
            else
            {
                url_api = url_api.Substring(0, url_api.Length);
            }

            
            if(user.LEVEL_USER == "-171195")
            {
                return new Role_API(true, "Super admin access !", user.ID_LOG_IN?.ToString() ?? "", user.MA_USER?.ToString() ?? "", "-1", "Super admin", user.LEVEL_USER, user.MA_DON_VI ?? "");
            }
            else
            {
                var response = RolesRepository.CHECK_PHAN_QUYEN_API(url_api, user.MA_USER ?? "");

                if (response.data != null)
                {
                    if (Int32.Parse(user.LEVEL_USER ?? "999999999999999999999999") >= 0 && Int32.Parse(user.LEVEL_USER ?? "999999999999999999999999") <= Int32.Parse(response.data.LEVEL_USER))
                    {
                        return new Role_API(true, "Đã xác thực!", user.ID_LOG_IN?.ToString() ?? "", user.MA_USER?.ToString() ?? "", response.data.MA_API.ToString(), response.data.USERNAME, user.LEVEL_USER, user.MA_DON_VI);
                    }
                    else
                    {
                        return new Role_API(false, "Cấp người dùng chưa đủ quyền!", "", "", "", "", "", "");
                    }
                }
                else
                {
                    response = RolesRepository.CHECK_DM_API(url_api);

                    if (response.data != null)
                    {
                        return new Role_API(false, "Không có quyền truy cập API !", "", "", "", "", "", "");
                    }
                    else
                    {
                        return new Role_API(false, "Chức năng chưa có trong danh mục !", "", "", "", "", "", "");
                    }
                }
            }
            

        }

    }
}

