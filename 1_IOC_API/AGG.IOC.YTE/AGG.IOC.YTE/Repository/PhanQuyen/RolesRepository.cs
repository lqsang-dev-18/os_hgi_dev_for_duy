﻿using System;
using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;
using AGG.IOC.YTE.Common.MD5;
using System.Data;
namespace AGG.IOC.YTE.Repository.PhanQuyen
{
    public class RolesRepository
    {
        public static ResponseSingle CHECK_PHAN_QUYEN_API(string duong_dan, string ma_user)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    DUONG_DAN = duong_dan,
                    MA_USER = ma_user
                };
                return csdl.GetSingle("HISAGG_DEV.PKG_PHANQUYEN_USERS.PROC_GET_ROLE_USER", obj);
            }
        }

        public static ResponseSingle CHECK_DM_API(string duong_dan)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    DUONG_DAN = duong_dan,
                };
                return csdl.GetSingle("HISAGG_DEV.PKG_PHANQUYEN_USERS.PROC_GET_DM_API", obj);
            }
        }

        public static ResponseList PROC_IOC_LAY_DS_MENU()
        {
            using (var csdl = new SQLHelper())
            {
                return csdl.GetList("HISAGG_DEV.PROC_IOC_LAY_DS_MENU");
            }
        }

        public static ResponseList GET_LIST_DM_DONVI(string ma_don_vi)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_DON_VI = ma_don_vi
                };
                return csdl.GetList("HISAGG_DEV.PKG_PHANQUYEN_USERS.PROC_GET_LIST_DM_DONVI", obj);
            }
        }

        public static ResponseSingle GET_USERS_THONGTIN(int ma_user, string ma_don_vi, string level_user)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_USER = ma_user,
                    MA_DON_VI = ma_don_vi,
                    LEVEL_USER = level_user
                };
                return csdl.GetSingle("HISAGG_DEV.PKG_PHANQUYEN_USERS.PROC_GET_USER_THONGTIN", obj);
            }
        }

        public static ResponseList PROC_LAY_MENU_USER(int? ma_user)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_USER = ma_user ?? -1
                };
                return csdl.GetList("HISAGG_DEV.PKG_PHANQUYEN_CAUHINHHETHONG.PROC_LAY_MENU_THEO_USER_IOC", obj);
            }
        }
    }
}

