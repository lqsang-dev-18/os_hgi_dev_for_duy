﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;
namespace AGG.IOC.YTE.Repository.Dashboard.KiemNghiemDuocMyPham
{
    public class KNDuocMyPhamRepository
    {
        public ResponseList KiemNghiemDuocMyPham(string? nam, string? quy, string? thang,string? ma_huyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                QUY = quy,
                THANG = thang,
                MA_HUYEN = ma_huyen
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
