﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.PhapY
{
    public class PhapYRepository
    {
        public ResponseList GetListPhapY(string? nam, string? thang, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        
    }
}
