﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.PhongDaLieu
{
    public class PhongDaLieuRepository
    {
        public ResponseList GetChiTieu(int? nam, string? thang, string? tungay, string? denngay)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PROC_GET_PHONG_DA_LIEU", paramters);

        }

        public ResponseList GetPhongDaLieuDonVi(int? nam, string? thang, string? tungay, string? denngay)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PROC_GET_PHONG_DA_LIEU_DONVI", paramters);

        }
    }
}
