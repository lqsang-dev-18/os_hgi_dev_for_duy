﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.DanSo
{
    public class DanSoRepository
    {
        public ResponseList GetListThongKeDanSo(int? nam,string? ma_huyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MA_HUYEN = ma_huyen ?? "",
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList DanSoTheoDoTuoi(int? nam,string? ma_huyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MA_HUYEN = ma_huyen ?? "",
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList DanSoTheoGioiTinh(int? nam, string? ma_huyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MA_HUYEN = ma_huyen ?? "",
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList DanSoTheoThanhThiNongThon(int? nam, string? ma_huyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MA_HUYEN = ma_huyen ?? "",
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList DanSoBienPhapTranhThai(int? nam, string? ma_huyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MA_HUYEN = ma_huyen ?? "",
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList TyLeSinhConThu3TroLen(int? nam,string? ma_huyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MA_HUYEN = ma_huyen ?? "",
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList GetListThongKeDanSoSSLienKe(int? nam, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList DanSoTheoDoTuoiSSLienKe(int? nam, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList DanSoTheoGioiTinhSSLienKe(int? nam, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
               NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList DanSoTheoThanhThiNongThonSSLienKe(int? nam, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList TyLeSinhConThu3TroLenSSLienKe(int? nam, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
