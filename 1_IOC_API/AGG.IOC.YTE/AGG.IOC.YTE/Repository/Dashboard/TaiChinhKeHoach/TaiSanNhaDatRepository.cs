﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach
{
    public class TaiSanNhaDatRepository
    {
        public ResponseList TkTheoDienTichDat(int? id_donvi_quanly, string? nam_duavao_sudung, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                ID_DONVI = id_donvi_quanly,
                NAM_DUA_VAO_SU_DUNG = nam_duavao_sudung

            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList TkTheo2DienTichNha(int? id_donvi_quanly,string? nam_duavao_sudung,string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                ID_DONVI=id_donvi_quanly,
                NAM_DUA_VAO_SU_DUNG=nam_duavao_sudung
            };
            var context = new SQLHelper();
            return context.GetList(sql,paramters);
        }
        public ResponseList GetDsIdDonVi( string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
               
            };
            var context = new SQLHelper();
            return context.GetList(sql);
        }
        public ResponseList TkTheoHienTrangNha(int? id_donvi_quanly, string? nam_duavao_sudung, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                ID_DONVI = id_donvi_quanly,
                NAM_DUA_VAO_SU_DUNG = nam_duavao_sudung
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList TkTheoNguyenGiaNhaVaDat(int? id_donvi_quanly, string? nam_duavao_sudung, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                ID_DONVI = id_donvi_quanly,
                NAM_DUA_VAO_SU_DUNG = nam_duavao_sudung

            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList TkeTheoHienTrangCongTrinh(int? id_donvi_quanly, string? nam_duavao_sudung, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                ID_DONVI = id_donvi_quanly,
                NAM_DUA_VAO_SU_DUNG = nam_duavao_sudung
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList TkeTheoDienTichDatVa2DienTichNha(int? id_donvi_quanly, string? nam_duavao_sudung, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                ID_DONVI = id_donvi_quanly,
                NAM_DUA_VAO_SU_DUNG = nam_duavao_sudung
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
