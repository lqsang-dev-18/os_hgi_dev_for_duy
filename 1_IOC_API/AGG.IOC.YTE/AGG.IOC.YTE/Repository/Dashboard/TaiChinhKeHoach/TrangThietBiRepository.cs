﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach
{
    public class TrangThietBiRepository
    {
        public ResponseList GetListTrangThietBi(string? madonvi, string? nam, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MADONVI = madonvi,
                NAM = nam
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }

        public ResponsePageList GetPageListTrangThietBi(string? nam, string? maDonVi, int? trangThai, int? nguonGoc, int? page, int? size, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {

                NAM = nam,
                MA_DV = maDonVi,
                TRANG_THAI = trangThai,
                NGUON_GOC = nguonGoc,
                PAGE = page,
                SIZE = size,
            };
            var context = new SQLHelper();
            return context.GetPageList(sql, paramters);
        }

    }
}
