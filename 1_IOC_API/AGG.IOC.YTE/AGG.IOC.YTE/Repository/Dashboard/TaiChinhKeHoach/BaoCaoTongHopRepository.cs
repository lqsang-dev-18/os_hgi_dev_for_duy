﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.TaiChinhKeHoach
{
    public class BaoCaoTongHopRepository
    {
        public ResponseList BaoCaoTongHop(string? nam, string? quy, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                QUY = quy
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
