﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.TrungTamKiemNghiem
{
    public class KyThuatKiemNghiemRepository
    {
        public ResponseList GetChiTieu(int? nam, string? thang, string? tungay, string? denngay)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PROC_GET_CHITIEU_PHUONGPHAP_KIEM_NGHIEM", paramters);

        }

        public ResponseList GetChiTieuDinhTinh(int? nam, string? thang, string? tungay, string? denngay)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PROC_GET_CHI_TIEU_DINH_TINH", paramters);

        }
        public ResponseList GetChiTieuDinhLuong(int? nam, string? thang, string? tungay, string? denngay)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PROC_GET_CHI_TIEU_DINH_LUONG", paramters);

        }
    }
}

