﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;
using AGG.IOC.YTE.Repository.DanhMucChung;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Data.Common;
using System.Drawing;
using System.Reflection.Metadata;

namespace AGG.IOC.YTE.Repository.Dashboard.VanBanDieuHanh
{
    public class VanBanDieuHanhRepository
    {
        //Lấy danh mục đơn vị
        public ResponsePageList GetListDonVi(int? page, int? size)
        {
            var paramters = new
            {
                PAGE = page ?? 1,
                SIZE = size ?? 100
            };
            var context = new SQLHelper();
            return context.GetPageList("CSDLYTE_IOFFICE.PROC_GET_PAGE_LIST_DON_VI", paramters);

        }

        //Lấy dữ liệu 2 chỉ tiêu dashboard thông tin chung
        public ResponseList GetList2ChiTieu(string? nam, string? thang, string? madonvi)
        {
            string sql = "CSDLYTE_IOFFICE.PROC_TK_2_CHI_TIEU";
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }

        //Thống kê văn bản đi đến theo đơn vị
        public ResponseList GetTKVBDIDENTHEODONVI(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_VB_DI_DEN_THEO_DON_VI", paramters);

        }

        //Thống kê văn bản đi đến ký số
        public ResponseList GetTKVBDIDENKYSO(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_VB_DI_DEN_KY_SO", paramters);

        }

        //Thống kê văn bản đến trễ hạn
        public ResponseList GetTKVBDENTREHAN(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_VB_DEN_TRE_HAN", paramters);

        }

        //Thống kê tình hình phê duyệt văn bản đến
        public ResponseList GetTKTINHHINHPHEDUYETVBDEN(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TINH_HINH_PHE_DUYET_VB_DEN", paramters);

        }

        //Thống kê tình hình ký số văn bản đến
        public ResponseList GetTKTINHHINHKYSOVBDIDEN(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TINH_HINH_KY_SO_VB_DI_DEN", paramters);

        }

        //Thống kê tình hình duyệt văn bản đến
        public ResponseList GetTKTINHHINHDUYETVBDEN(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TINH_HINH_PHE_DUYET_VB_DEN_THEO_THANG", paramters);

        }

        //Thống kê tình hình tiếp nhận văn bản đến
        public ResponseList GetTKTINHHINHTIEPNHANVBDEN(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TINH_HINH_TIEP_NHAN_VB_DEN", paramters);

        }

        //Thống kê tình hình xử lý văn bản đến bị trễ hạn
        public ResponseList GetTKTINHHINHXULYVBDENBITRENHAN(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TINH_HINH_XL_VB_DEN_BI_TRE_HAN", paramters);

        }

        //Thống kê tình hình sử dụng hệ thống
        public ResponseList GetTKTINHHINHSUDUNGHETHONG(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TINH_HINH_SD_HE_THONG", paramters);

        }

        //Thống kê top 5 đơn vị có văn bản đi đến nhiều nhất
        public ResponseList GetTKTOP5DONVICOVBDIDENNHIEUNHAT(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TOP_5_DV_CO_VB_DI_DEN_NHIEU_NHAT", paramters);

        }

        //Thống kê top 5 đơn vị có văn bản đến trễ hạn nhiều nhất
        public ResponseList GetTKTOP5DONVICOVBDENTREHANNHIEUNHAT(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TOP_5_DV_CO_VB_DEN_TRE_HAN_NHIEU_NHAT", paramters);

        }

        //Thống kê top 5 đơn vị có số người truy cập nhiều nhất
        public ResponseList GetTKTOP5DONVICOSONGUOITRUYCAPNHIEUNHAT(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TOP_5_DV_CO_SO_NGUOI_TRUY_CAP_NHIEU_NHAT", paramters);

        }

        //Thống kê top 5 đơn vị có nhiều văn bản đến chưa tiếp nhận
        public ResponseList GetTKTOP5DONVICONHIEUVBDENCHUATIEPNHAN(string? nam, string? thang, string? madonvi)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MADONVI = madonvi
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TOP_5_DV_CO_NHIEU_VB_DEN_CHUA_TIEP_NHAN", paramters);

        }

        //***   dashboard dữ liệu theo phòng ban   ***//

        //Lấy danh mục phòng ban
        public ResponsePageList GetListPhongBan(int? page, int? size)
        {
            var paramters = new
            {
                PAGE = page ?? 1,
                SIZE = size ?? 100
            };
            var context = new SQLHelper();
            return context.GetPageList("CSDLYTE_IOFFICE.PROC_GET_PAGE_LIST_PHONG_BAN", paramters);

        }

        //Thống kê văn bản đi đến theo phòng ban
        public ResponseList GetTKVBDIDENTHEOPHONGBAN(string? nam, string? thang, string? maphongban)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MAPHONGBAN = maphongban
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_VB_DI_DEN_THEO_PHONG_BAN", paramters);

        }

        //Thống kê top 10 cá nhân nhận văn bản đến nhiều nhất
        public ResponseList GetTKTOP10CANHANNHANVBDENNHIEUNHAT(string? nam, string? thang, string? maphongban)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MAPHONGBAN = maphongban
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TOP_10_CN_NHAN_VB_DEN_NHIEU_NHAT", paramters);

        }

        //Thống kê top 10 cá nhân tham mưu văn bản đi nhiều nhất
        public ResponseList GetTKTOP10CANHANTHAMMUUVBDINHIEUNHAT(string? nam, string? thang, string? maphongban)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MAPHONGBAN = maphongban
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_TOP_10_CN_THAM_MUU_VB_DI_NHIEU_NHAT", paramters);

        }

        //Thống kê văn bản đến trễ hạn theo phòng ban
        public ResponseList GetTKVBDENTREHANTHEOPHONGBAN(string? nam, string? thang, string? maphongban)
        {
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MAPHONGBAN = maphongban
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PROC_TK_VB_DEN_TRE_HAN_THEO_PHONG_BAN", paramters);

        }

        //***   dashboard dữ liệu thông tin chung tình hình xử lý   ***//

        //Các chỉ số văn bản đến
        public ResponseList GetCacChiSoVBDen(string? nam, string? thang, string unit_type)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang,
                UNIT_TYPE = unit_type
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_INCOMINGDOC", parameter);
        }

        //Các chỉ số văn bản đi
        public ResponseList GetCacChiSoVBDi(string? nam, string? thang, string unit_type)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang,
                UNIT_TYPE = unit_type
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_OUTDOC",parameter);
        }

        //Thống kê văn bản đến, đến quá hạn theo tháng
        public ResponseList GetTKVBDenvaDenQuaHanTheoThang(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_INCOMINGDOC_MONTH",parameter);
        }

        //Các chỉ số văn bản đi liên thông theo tháng
        public ResponseList GetCacChiSoVBDiLienThongTheoThang(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_OUTDOC_MONTH",parameter);
        }

        //Thống kê số lượng truy cập theo tháng
        public ResponseList GetTKSoLuongTruyCapTheoThang(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_LOGINREPORT", parameter);
        }

        //***   dashboard dữ liệu hiệu suất xử lý và điều hành   ***//

        //Lấy nhóm 5 đơn vị có văn bản đến nhiều nhất
        public ResponseList GetTop5DonViCoVbDenNhieuNhat(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_INCOMINGDOC_TOP5", parameter);
        }

        //lấy trung bình số lượng văn bản đến 
        public ResponseList GetTrungBinhSoLuongVbDen(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_INCOMINGDOC_AVG", parameter);
        }

        //Lấy nhóm 5 đơn vị có tỷ lệ xử lý văn bản đến đúng hạn cao nhất
        public ResponseList GetTop5DonViCoTyLeXlVbDungHan(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_INCOMINGDOC_TOP5_ONTIME", parameter);
        }

        //Lấy nhóm 5 đơn vị có tỷ lệ xử lý văn bản đến quá hạn cao nhất
        public ResponseList GetTop5DonViCoTyLeXlVbQuaHan(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_INCOMINGDOC_TOP5_LATE", parameter);
        }

        //Lấy nhóm 5 đơn vị có người dùng thường xuyên truy cập hệ thống
        public ResponseList GetTop5DonViCoNguoiDungThuongXuyenTruyCap(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_LOGINREPORT_TOP5", parameter);
        }

        //lấy số lượng văn bản đi trung bình
        public ResponseList GetTrungBinhSoLuongVbDi(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_OUTDOC_AVG", parameter);
        }

        //lấy nhóm 5 đơn vị có văn bản đi nhiều nhất
        public ResponseList GetTop5DonViCoVbDiNhieuNhat(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_OUTDOC_TOP5", parameter);
        }

        //lấy nhóm 5 đơn vị có văn bản liên thông nhiều nhất
        public ResponseList GetTop5DonViCoVbLienThongNhieuNhat(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_OUTDOC_TOP5_CONNECTED", parameter);
        }

        //***   dashboard thong ke don vi truc thuoc so y te  ***//


        // lấy số lượng các chỉ số văn bản đến theo đơn vị
        public ResponseList GetCacChiSoVbDenTheoDonVi(string? nam, string? thang,string unit_type)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang,
                UNIT_TYPE = unit_type
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_INCOMINGDOC_UNIT", parameter);
        }

        // lấy số lượng văn bản đi theo đơn vị
        public ResponseList GetSoLuongVbDiTheoDonVi(string? nam, string? thang, string unit_type)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang,
                UNIT_TYPE = unit_type
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_OUTDOC_UNIT", parameter);    
        }

        //lấy số lượt truy cập hệ thống theo đơn vị
        public ResponseList GetSoLuongTruyCapHeThongTheoDonVi(string? nam, string? thang, string unit_type)
        {
            var parameter = new
            { 
                NAM = nam,
                THANG = thang,
                UNIT_TYPE = unit_type
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_LOGINREPORT_UNIT", parameter);
        }

        //***   dashboard thong ke don vi truc thuoc so y te  ***//

        public ResponseList GetCacChiSoCongViec(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_TASKMANAGER", parameter);
        }

        public ResponseList GetCacChiSoCongViecHoanThanhTheoDonVi(string? nam, string? thang)
        {
            var parameter = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_IOFFICE.PKG_HGI_DASHBOARD.PROC_GET_TASKMANAGER_UNIT", parameter);
        }
    }
}
