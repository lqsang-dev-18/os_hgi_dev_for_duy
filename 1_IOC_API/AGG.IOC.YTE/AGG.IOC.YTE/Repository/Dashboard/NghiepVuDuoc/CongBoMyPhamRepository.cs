﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.NghiepVuDuoc
{
    public class CongBoMyPhamRepository
    {
        public ResponseList GetListCongBoMyPham(int? loai_cbx, string? nam, string? thang, string? quy, string? tungay, string? denngay, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                NAM = nam,
                QUY = quy,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }

        public ResponsePageList GetPageListCongBoMyPham(int? loai_cbx, string? nam, string? thang, string? quy, string? tungay, string? denngay, string? loai, int? page, int? size, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                NAM = nam,
                QUY = quy,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay,
                LOAI = loai,
                PAGE = page,
                SIZE = size,
            };
            var context = new SQLHelper();
            return context.GetPageList(sql, paramters);
        }
    }
}
