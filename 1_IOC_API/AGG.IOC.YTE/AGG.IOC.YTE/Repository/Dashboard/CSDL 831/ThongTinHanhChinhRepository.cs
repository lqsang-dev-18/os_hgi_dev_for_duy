﻿using System;
using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Default;
using AGG.IOC.YTE.Common.Response;
using Microsoft.AspNetCore.DataProtection.KeyManagement;

namespace AGG.IOC.YTE.Repository.Dashboard
{
	public class ThongTinHanhChinhRepository
	{
        public static ResponseList THONGKE_GIOITINH_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_GIOITINH", obj);
            }
        }

        public static ResponseList THONGKE_TONG_HSSK_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_TONG_HSSK", obj);
            }
        }
        public static ResponseList THONGKE_TONG_HOKHAU_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_TONG_HOKHAU", obj);
            }
        }
        public static ResponseList THONGKE_TIEUSU_BENHTAT_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_TIEUSU_BENHTAT", obj);
            }
        }
        public static ResponseList THONGKE_TIENSU_BENHTAT_GIADINH_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_TIENSU_BENHTAT_GIADINH", obj);
            }
        }
        public static ResponseList THONGKE_KHUYETTAT_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_KHUYETTAT", obj);
            }
        }
        public static ResponseList THONGKE_TONG_HSSK_THEO_DOTUOI_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_HSSK_THEOSOTUOI", obj);
            }
        }
        public static ResponseList THONGKE_SUCKHOESINHSAN_SOLANCOTHAI_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_SKSS_SLCOTHAI", obj);
            }
        }
        public static ResponseList THONGKE_SUCKHOESINHSAN_TYLE_SAYTHAI_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_SKSS_TYLE_SAYTHAI", obj);
            }
        }
        public static ResponseList THONGKE_SUCKHOESINHSAN_TYLE_PHATHAI_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_SKSS_TYLE_PHATHAI", obj);
            }
        }
        public static ResponseList THONGKE_SUCKHOESINHSAN_SL_DETHUONG_DEMO_831(string ma_huyen, string ma_xa)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_TINH = new DefaultValue("MA_TINH").Values,
                    MA_HUYEN = ma_huyen ?? "",
                    MA_XA = ma_xa ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_831_THONGKE_SKSS_SL_DETHUONG_DEMO", obj);
            }
        }
    }
}

