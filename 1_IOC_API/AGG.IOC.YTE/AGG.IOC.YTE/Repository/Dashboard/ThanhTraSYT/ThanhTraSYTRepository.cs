﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.ThanhTraSYT
{
    public class ThanhTraSYTRepository
    {
        public ResponseList GetData_DashboardThanhTra(string nam,string quy, string nameProc)
        {
            string sql = "HISAGG_DEV.PKG_SYT_PHONG_THANH_TRA." + nameProc;
            var paramters = new
            {
                NAM = Int32.Parse(nam),
                QUY = Int32.Parse(quy)
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }

        public ResponseList GetData_DashboardThanhTra(int ma_tinh, int ma_huyen, int ma_xa, int loai_cbx, int nam, int quy, int thang, string tungay, string denngay, string nameProc)
        {
            string sql = "CSDLYTE_SYT." + nameProc;
            var paramters = new
            {
                MA_TINH = ma_tinh,
                MA_HUYEN = ma_huyen,
                MA_XA = ma_xa,
                LOAI_CBX = loai_cbx,
                NAM = nam,
                QUY = quy,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
