﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.CDC
{
    public class BenhKhongLayHGIRepository
    {
        public ResponseList GetListCDCBenhKhongLay(string? nam, string? thang, string? tungay, string? denngay, string? nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                TUNGAY = tungay,
                DENNGAY = denngay
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
