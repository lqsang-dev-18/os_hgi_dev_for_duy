﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.CDC
{
    public class XetNghiem_PKDaKhoa_Repository
    {
        public ResponseList XetNghiem_PKDaKhoa(string? nam, string? thang, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        //public ResponseList PKDK_SUC_KHOE_SINH_SAN(string? nam, string? thang, string nameProc)
        //{
        //    string sql = "HISAGG_DEV." + nameProc;
        //    var paramters = new
        //    {
        //        NAM = nam,
        //        THANG = thang
        //    };
        //    var context = new SQLHelper();
        //    return context.GetList(sql, paramters);
        //}
        //public ResponseList PKDK_CDHA(string? nam, string? thang, string nameProc)
        //{
        //    string sql = "HISAGG_DEV." + nameProc;
        //    var paramters = new
        //    {
        //        NAM = nam,
        //        THANG = thang
        //    };
        //    var context = new SQLHelper();
        //    return context.GetList(sql, paramters);
        //}
        //public ResponseList PKDK_KSK(string? nam, string? thang, string nameProc)
        //{
        //    string sql = "HISAGG_DEV." + nameProc;
        //    var paramters = new
        //    {
        //        NAM = nam,
        //        THANG = thang
        //    };
        //    var context = new SQLHelper();
        //    return context.GetList(sql, paramters);
        //}
        //public ResponseList PKDK_TIEM_CHUNG(string? nam, string? thang, string nameProc)
        //{
        //    string sql = "HISAGG_DEV." + nameProc;
        //    var paramters = new
        //    {
        //        NAM = nam,
        //        THANG = thang
        //    };
        //    var context = new SQLHelper();
        //    return context.GetList(sql, paramters);
        //}

    }
}
