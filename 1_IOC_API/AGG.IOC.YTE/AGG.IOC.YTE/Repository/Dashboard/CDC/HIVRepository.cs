﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.CDC
{
    public class HIVRepository
    {
        public ResponseList GetListCDCHIV(string? nam, string? khoan, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                KHOAN = khoan
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
