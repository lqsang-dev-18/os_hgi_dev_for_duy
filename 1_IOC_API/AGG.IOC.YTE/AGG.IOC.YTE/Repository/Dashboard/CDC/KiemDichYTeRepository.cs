﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.CDC
{
    public class KiemDichYTeRepository
    {
        public ResponseList GetListCDCKiemDichYTe(string? nam, string? tuan, string? thang, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                TUAN = tuan,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
