﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.CDC
{
    public class BenhTruyenNhiemRepository
    {
        public ResponseList GetListCDCBenhTruyenNhiem(string? nam, string? tuan, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                TUAN = tuan
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList GetListCDCBenhTruyenNhiem(int nam, int thang, string? maHuyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                MA_HUYEN = maHuyen
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
