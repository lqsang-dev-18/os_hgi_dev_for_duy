﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.CDC
{
    public class BenhKhongLayRepository
    {
        public ResponseList GetListCDCBenhKhongLay(string? nam, string? qui, string? thang, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                QUI = qui,
                THANG = thang
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
