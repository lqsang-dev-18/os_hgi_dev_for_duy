﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.NghiepVuY
{
    public class DeAn06Repository
    {
        //Lấy dữ liệu theo tiêu chí Đề Án 06
        public ResponseList GetListDeAn06(string? nam, string? qui, string? thang, string? tuyen, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                QUI = qui,
                THANG = thang,
                TUYEN = tuyen
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
