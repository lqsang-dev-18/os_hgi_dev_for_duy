﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.NghiepVuY
{
    public class ThongTinChung
    {
        //Thống kê hồ sơ khám bênh
        public ResponseList GetTKHoSoKCB(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {

            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_TK_SL_KCB", paramters);

        }
        //Thống kê số lượng cấp cứu
        public ResponseList GetTKSLCapCuu(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_TK_SL_CAPCUU", paramters);

        }
        //Thống kê số lượng tử vong
        public ResponseList GetTKSLTuVong(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_TK_SL_TUVONG", paramters);

        }
        //Thống kê lượt ra vào viện
        public ResponseList GetTKSLRaVaoVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_TKLUOT_RA_VAO", paramters);

        }
        //Thống kê lượt cấp cứu và tử vong
        public ResponseList GetTKSLCAPCUUTUVONG(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_TK_CAPCUU_TUVONG", paramters);

        }
        //Thống kê lượt khám bệnh theo TT Y TẾ
        public ResponseList GetTKKCB(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_TK_KCB", paramters);

        }
        //Thống kê lượt khám ngoại trú
        public ResponseList GetSLNgoaiTruNoiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_NOI_TRU_NGOAI_TRU", paramters);

        }

        //Thống kê lượt khám ngoại trú
        public ResponseList GetTKNgoaiTruNoiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_TK_NOI_TRU_NGOAI_TRU", paramters);

        }

        //Thống kê lượt vào viện
        public ResponseList GetSLVaoVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_LUOT_VAO_VIEN", paramters);

        }
        //Thống kê lượt ra viện
        public ResponseList GetSLRaVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_LUOT_RA_VIEN", paramters);

        }
        //Thống kê số lượng ca nặng xin về
        public ResponseList GetSLCaNangXinVe(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_CA_NANG_XIN_VE", paramters);

        }
        //Thống kê số lượng chuyển viện
        public ResponseList GetSLChuyenVien(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_CHUYEN_VIEN", paramters);

        }
        //Thống kê lượt nội trú cuối kỳ
        public ResponseList GetSLNoiTruCuoiKy (int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_NOI_TRU_CUOI_KY", paramters);

        }
        //Thống kê lượt nội trú cách tính mới
        public ResponseList GetSLNoiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_NOI_TRU", paramters);
        }
        public ResponseList GetSLNgoaiTru(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_NGOAI_TRU", paramters);
        }
        
        public ResponseList GetSLCuoiKyNew(int? loai_cbx, string? tungay, string? denngay, int? nam, int? thang, int? quy, int? huyen, string loaihinh, string cosokb)
        {
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                TUNGAY = tungay,
                DENNGAY = denngay,
                NAM = nam,
                THANG = thang,
                QUY = quy,
                HUYEN = huyen,
                LOAIHINH = loaihinh,
                COSOKB = cosokb
            };
            var context = new SQLHelper();
            return context.GetList("HISAGG_DEV.PKG_IOC_THONGTIN_CHUNG_1.PROC_SL_NOI_TRU_CUOI_KY", paramters);

        }
    }
}
