﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.NghiepVuY
{
    public class PhanTichDuLieuKCBRepository
    {
        /* public static ResponseList PROC_TK_SO_HO_SO_BHYT(string madv, string? ma_huyen, string? nam, string? quy, string? thang, int? loai_thoigian, int? loai_hinh, int? loai_sosanh)
         {
             var paramters = new
             {
                 MA_DON_VI = madv,
                 MA_HUYEN = ma_huyen,
                 NAM = nam,
                 THANG = thang,
                 QUY = quy,
                 LOAI_TG = loai_thoigian,
                 LOAI_HINH = loai_hinh ?? -1,
                 LOAI_SS = loai_sosanh,
             };
             var context = new SQLHelper();
             return context.GetList("PKG_IOC_PTDL_KCB.PROC_TK_SO_HO_SO_BHYT", paramters);
         }*/
        public ResponseList GetListPhanTichDLKCB(int? loaiHinh, int? loaiSS, string? nam, string? qui, string? thang, string? maHuyen, string? maCSKCB, string? tungay, string? dengay, int? loai, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                MA_HUYEN = maHuyen,
                LOAI_HINH = loaiHinh,
                LOAI_SS = loaiSS,
                NAM = nam,
                QUI = qui,
                THANG = thang,
                MA_CSKCB = maCSKCB,
                TUNGAY= tungay,
                DENNGAY= dengay,
                LOAI= loai,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
