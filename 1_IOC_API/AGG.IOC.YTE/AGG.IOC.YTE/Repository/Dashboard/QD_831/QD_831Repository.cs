﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.QD_831
{
    public class QD_831Repository 
    {

        // lấy chỉ số “Thống kê dân số, số lượng hồ sơ sức khỏe được tạo lập và số lượng hồ sơ sức khỏe được cập nhật dữ liệu”
        public ResponseList GetTKDanSo_SLHSSKTaoLapVaSLHSSKCapNhatDuLieu(int nam, int? quy, int? thang, int tinh, int? huyen, int? xa )
        {
            var paramters = new
            {
               NAM = nam,
               QUY = quy,
               THANG = thang,
               TINH = tinh,
               HUYEN = huyen,
               XA = xa
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_831_MOBILE.PKG_DB831_TAOLAP.PROC_GET_SL", paramters);

        }

        // lấy chỉ số “Tỉ lệ lập hồ sơ sức khỏe được cập nhật so với tỉ lệ hồ sơ sức khỏe được tạo lập”
        public ResponseList GetTyLeHSSKCapNhatSoVoiTyLeHSSKTaoLap(int nam, int? quy, int? thang, int tinh, int? huyen, int? xa)
        {
            var parameters = new
            {
                NAM = nam,
                QUY = quy,
                THANG = thang,
                TINH = tinh,
                HUYEN = huyen,
                XA = xa
            };
            var context = new SQLHelper();
            return context.GetList("CSDLYTE_831_MOBILE.PKG_DB831_TAOLAP.PROC_GET_TL", parameters);
        }
    }
}
