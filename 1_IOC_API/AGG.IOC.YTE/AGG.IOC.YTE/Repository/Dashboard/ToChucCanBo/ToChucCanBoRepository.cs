﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Dashboard.ToChucCanBo
{
    public class ToChucCanBoRepository
    {
        public ResponseList GetListToChucCanBo(string? nam, string maDonVi, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            if (maDonVi == "-1") { maDonVi = null; }
            var paramters = new
            {
                NAM = nam,
                COSOKCB = maDonVi,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
