using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;
using System.Globalization;

namespace AGG.IOC.YTE.Repository.Dashboard.DuLieuKCB
{
    public class TongHopRepository
    {
        //Lấy tổng số hồ sơ KCB
        public ResponseSingle TK_TongSoHoSo(int? nam, int? thang, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam,
                    THANG = thang,
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetSingle("HISAGG_DEV.PROC_KCB_TK_TONGSO_HOSO", obj);
            }

        }
        //Lấy số hồ sơ vào viện
        public ResponseSingle TK_HoSoVaoVien(string nam, string thang, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetSingle("HISAGG_DEV.PROC_KCB_TK_HOSO_VAOVIEN", obj);
            }

        }

        //Lấy thống kê cấp cứu
        public ResponsePageList TK_CapCuu(string nam, string thang, string maCSKCB, int? page, int? size)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? "",
                    PAGE = page ?? 1,
                    SIZE = size ?? 10,
                };
                return csdl.GetPageList("HISAGG_DEV.PROC_KCB_TK_CAPCUU", obj);
            }
        }

        //Lấy thống kê tử vong
        public ResponsePageList TK_TuVong(string nam, string thang, string maCSKCB, int? page, int? size)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? "",
                    PAGE = page ?? 1,
                    SIZE = size ?? 10,
                };
                return csdl.GetPageList("HISAGG_DEV.PROC_KCB_TK_TUVONG", obj);
            }
        }

        //Lấy thống kê hồ sơ khám theo ngày trong tháng
        public ResponseList TK_KhamTheoNgay(string nam, string thang, string maCSKCB, int maLoaiKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? "",
                    MA_LOAI_KCB = maLoaiKCB
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_LUOTKHAM_THEONGAY", obj);
            }
        }

        //Lấy thống kê lượt ra vào theo ngày
        public ResponseList TK_RaVaoNgay(string nam, string thang, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_RA_VAO_NGAY", obj);
            }
        }

        //Lấy thống kê điều trị nội trú, ngoại trú theo ngày
        public ResponseList TK_NoiTruNgoaiTruNgay(string nam, string thang, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_NOITRU_NGOAITRU_NGAY", obj);
            }
        }

        //Lấy thống kê theo dõi khám bệnh sử dụng BHYT
        public ResponseList TK_KhamBHYTNgay(string nam, string thang, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_KHAM_BHYT_NGAY", obj);
            }
        }

		//Lấy thống kê theo dõi khám bệnh sử dụng BHYT
        public ResponseList TK_CapCuuTuVongNgay(string nam, string thang, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_CAPCUU_TUVONG_NGAY", obj);
            }
        }

        //Lấy thống kê tai nạn thương tích theo ngày
        public ResponseList TK_TaiNanThuongTichNgay(string nam, string thang, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam ?? "",
                    THANG = thang ?? "",
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_TNTT_NGAY", obj);
            }
        }
        /*====================== NguyenDTT-AGG: Implement APIs for IOC dashboard ======================*/
        /// <summary>
        /// Thống kê tổng quan 1
        /// </summary>
        /// <param name="maDonVi"></param>
        /// <param name="maHuyen"></param>
        /// <param name="tuNgay"></param>
        /// <param name="denNgay"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeTongQuan1(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_TONG_QUAN_1", obj);
        }

        /// <summary>
        /// Thống kê sử dụng dịch vụ kỹ thuật các tháng trong năm
        /// </summary>
        /// <param name="year"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeSuDungDVKT(string maDonVi, string maHuyen, string tuNgay, string denNgay, int topNum)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay, ROW_NUM = topNum };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_SU_DUNG_DVKT", obj);
        }

        /// <summary>
        /// Thống kê số lượng hồ sơ khám chữa bệnh có bảo hiểm y tế
        /// </summary>
        /// <param name="year"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeSoHoSoBHYT(string maDonVi, string maHuyen, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_SO_HO_SO_BHYT", obj);
        }

        /// <summary>
        /// Thống kê tỷ lệ giới tính khám chữa bệnh
        /// </summary>
        /// <param name="maDonVi"></param>
        /// <param name="maHuyen"></param>
        /// <param name="tuNgay"></param>
        /// <param name="denNgay"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeGioiTinhKhamChuaBenh(string maDonVi, string maHuyen, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_GIOI_TINH_KCB", obj);
        }

        /// <summary>
        /// Thống kê số ngày điều trị theo các tháng trong năm
        /// </summary>
        /// <param name="year"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeSoNgayDieuTri(string maDonVi, string maHuyen, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_SO_NGAY_DIEU_TRI", obj);
        }

        /// <summary>
        /// Thống kê các bệnh phổ biến
        /// </summary>
        /// <param name="year"></param>
        /// <param name="topNum"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeTopCacBenhPhoBien(string maDonVi, string maHuyen, string tuNgay, string denNgay, int topNum)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay, ROW_NUM = topNum };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_CAC_BENH_PHO_BIEN", obj);
        }

        /// <summary>
        /// Thống kê các bệnh phổ biến theo khu vực
        /// </summary>
        /// <param name="maCoSo"></param>
        /// <param name="year"></param>
        /// <param name="topNum"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeTopCacBenhPhoBienTheoKhuVuc(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay, int rowNum)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay, ROW_NUM = rowNum };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_CAC_BENH_PHO_BIEN_THEO_KHU_VUC", obj);
        }

        /// <summary>
        /// Thống kê tai nạn thương tích
        /// </summary>
        /// <param name="year"></param>
        /// <param name="topNum"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeTaiNanThuongTich(string maDonVi, string maHuyen, string tuNgay, string denNgay, int topNum)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay, ROW_NUM = topNum };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_TAI_NAN_THUONG_TICH", obj);
        }

        /// <summary>
        /// Thống kê tai nạn thương tích theo khu vực
        /// </summary>
        /// <param name="maCoSo"></param>
        /// <param name="year"></param>
        /// <param name="topNum"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeTaiNanThuongTichTheoKhuVuc(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay, int rowNum)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay, ROW_NUM = rowNum };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_TAI_NAN_THUONG_TICH_THEO_KHU_VUC", obj);
        }

        /// <summary>
        /// Thống kê chi phí khám chữa bệnh
        /// </summary>
        /// <param name="year"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeChiPhiKhamChuaBenh(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_CHI_PHI_KHAM_CHUA_BENH", obj);
        }

        /// <summary>
        /// Thống kê chi phí khám chữa bệnh theo khu vực
        /// </summary>
        /// <param name="maCoSo"></param>
        /// <param name="year"></param>
        /// <param name="topNum"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeChiPhiKhamChuaBenhTheoKhuVuc(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay, int? rowNum)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay, ROW_NUM = rowNum };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_CHI_PHI_KHAM_CHUA_BENH_THEO_KHU_VUC", obj);
        }

        /// <summary>
        /// Thống kê số ca cấp cứu
        /// </summary>
        /// <param name="year"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeSoCaCapCuu(string maDonVi, string maHuyen, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_SO_CA_CAP_CUU", obj);
        }

        /// <summary>
        /// Thống kê số ca tử vong
        /// </summary>
        /// <param name="year"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeSoCaTuVong(string maDonVi, string maHuyen, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_SO_CA_TU_VONG", obj);
        }

        /// <summary>
        /// Thống kê số ca tử vong
        /// </summary>
        /// <param name="maDonVi"></param>
        /// <param name="maHuyen"></param>
        /// <param name="tuNgay"></param>
        /// <param name="denNgay"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeNhomTuoiKhamChuaBenh(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_NHOM_TUOI_KCB", obj);
        }

        /// <summary>
        /// Thống kê khám chữa bệnh bảo hiểm
        /// </summary>
        /// <param name="maDonVi"></param>
        /// <param name="maHuyen"></param>
        /// <param name="tuNgay"></param>
        /// <param name="denNgay"></param>
        /// <returns>list of data</returns>
        public ResponseList TKeKhamChuaBenhBaoHiem(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_DON_VI = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("CSDLYTE_4210.PKG_4210_IOC.PROC_TK_KHAM_CHUA_BENH_BAO_HIEM", obj);
        }
        /*====================== END: Implement APIs for IOC dashboard ======================*/


        /*====================== TUYENNT.AGG: Implement APIs for IOC dashboard ======================*/
        /// <summary>
        /// Thống kê khám chữa bệnh bảo hiểm
        /// </summary>
        /// <param name="maDonVi"></param>
        /// <param name="maHuyen"></param>
        /// <param name="tuNgay"></param>
        /// <param name="denNgay"></param>
        /// <returns>list of data</returns>
        public ResponseList TKTop10BenhNguyCoCao(string maDonVi, string maHuyen, int? loaiHinh, string tuNgay, string denNgay)
        {
            var csdl = new SQLHelper();
            var obj = new { MA_CSYT = maDonVi, MA_HUYEN = maHuyen, LOAI_HINH = loaiHinh, TU_NGAY = tuNgay, DEN_NGAY = denNgay };
            return csdl.GetList("HISAGG_DEV.PROC_TK_BENHTAT_TUVONG_TOP10", obj);
        }
        /*====================== END: Implement APIs for IOC dashboard ======================*/

        //Lấy số lượt khám sử dụng BHYT
        public ResponseSingle TK_KhamBHYT(int? nam, int? thang, string maCSKCB, int? maLoaiKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam,
                    THANG = thang,
                    MA_CSKCB = maCSKCB ?? "",
                    MA_LOAI_KCB = maLoaiKCB,
                };
                return csdl.GetSingle("HISAGG_DEV.PROC_KCB_TK_SL_KHAM_BHYT", obj);
            }

        }

        //Lấy thống kê theo độ tuổi
        public ResponseList TK_TheoDoTuoi(int? nam, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam,
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_TUOI", obj);
            }

        }

        //Lấy thống kê theo loại bệnh phổ biến
        public ResponseList TK_TheoLoaiBenh(int? nam, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam,
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_BENH_PHO_BIEN", obj);
            }

        }

        //Lấy thống kê bệnh nhân thanh toán
        public ResponseList TK_ChiPhiBenhNhanThanhToan(int? nam, string maCSKCB)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    NAM = nam,
                    MA_CSKCB = maCSKCB ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PROC_KCB_TK_CHI_PHI_THANH_TOAN", obj);
            }

        }
        //Lấy thống kê tỷ lệ KCB không sử dụng BHYT
        public ResponseList TK_KCB_KhongBHYT(string tuNgay, string denNgay, string? maHuyen, string? maDonVi, int? loaiHinh)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    TUNGAY = DateTime.ParseExact(tuNgay,"dd/mm/yyyy",CultureInfo.InvariantCulture),
                    DENNGAY = DateTime.ParseExact(denNgay, "dd/mm/yyyy", CultureInfo.InvariantCulture),
                    MA_HUYEN = maHuyen,
                    MA_DON_VI = maDonVi,
                    LOAI_HINH = loaiHinh
                };
                return csdl.GetList("HISAGG_DEV.PROC_TK_TY_LE_KCB_KHONG_BHYT", obj);
            }

        }
    }
}
