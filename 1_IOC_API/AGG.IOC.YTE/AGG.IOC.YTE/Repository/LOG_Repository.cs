﻿using System;
using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;
using System.Text.Json;

namespace AGG.IOC.YTE.Repository
{
    public class LOG_Repository
    {
        public static ResponseExecute PROC_LOG_CHUC_NANG(string id_log_in, string ma_api, string ip_client, dynamic param_obj, string ghi_chu)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    ID_ACTION_OUT = 0,
                    ID_LOG_IN = Int32.Parse(id_log_in),
                    Ma_API = Int32.Parse(ma_api),
                    IP_CLIENT = ip_client,
                    KEY_PARAMTER_OBJ =  JsonSerializer.Serialize<dynamic>(param_obj),
                    GHI_CHU = ghi_chu
                };
                return csdl.Execute("HISAGG_DEV.PROC_LOG_CHUC_NANG", obj);
            }
        }
    }
}

