﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.ChiaSe
{
    public class BenhTruyenNhiemv2Repository
    {
        public ResponseList GetThongKeCDCBenhTieuChay(int? loai_cbx, int nam, int? thang, int? quy, int huyen, string nameProc)
        {
            string sql = "CSDLYTE_SYT." + nameProc;
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                NAM = nam,
                QUY = quy,
                THANG = thang,
                HUYEN = huyen
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }

        public ResponseList GetListCDCBenhTruyenNhiem(int nam, int thang, string? maHuyen, string nameProc)
        {
            string sql = "CSDLYTE_SYT." + nameProc;
            var paramters = new
            {
                NAM = nam,
                THANG = thang,
                HUYEN = maHuyen
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }

        public ResponseList GetListCDCBenhTruyenNhiem(int loai_cbx, int quy, int nam, int thang, string? huyen, string nameProc)
        {
            string sql = "CSDLYTE_SYT." + nameProc;
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                NAM = nam,
                QUY = quy,
                THANG = thang,
                HUYEN = huyen
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }

        public ResponseList GetListCDCBenhTruyenNhiem(int loai_cbx, int quy, int nam, int thang, int huyen, string nameProc)
        {
            string sql = "CSDLYTE_SYT." + nameProc;
            var paramters = new
            {
                LOAI_CBX = loai_cbx,
                NAM = nam,
                QUY = quy,
                THANG = thang,
                HUYEN = huyen
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
