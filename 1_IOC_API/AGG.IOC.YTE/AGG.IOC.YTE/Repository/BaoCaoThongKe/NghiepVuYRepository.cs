﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.BaoCaoThongKe
{
    public class NghiepVuYRepository
    {
        public static ResponseList PROC_LAY_DS_XA(string? gia_tri, int ma_huyen, int ma_tinh)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    MA_HUYEN = ma_huyen,
                    gia_tri = gia_tri ?? "",
                    ma_tinh = ma_tinh
                };
                return csdl.GetList("HISAGG_DEV.PKG_HSSK_831.PROC_LAY_DS_XA", obj);
            }
        }
        public static ResponseList PROC_LAY_DS_HUYEN(string? gia_tri, int ma_tinh)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    ma_tinh = ma_tinh,
                    gia_tri = gia_tri ?? "",
                };
                return csdl.GetList("HISAGG_DEV.PKG_HSSK_831.PROC_LAY_DS_HUYEN", obj);
            }
        }
        public static ResponseList PROC_LAY_DS_CSKCB(string? gia_tri)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    gia_tri = gia_tri ?? ""
                };
                return csdl.GetList("HISAGG_DEV.PKG_PHANQUYEN_CAUHINHHETHONG.PROC_LAY_DS_CSKCB", obj);
            }
        }
        public static ResponseList PROC_LAY_DS_MAU_BAO_CAO(int ID)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    ID_LINH_VUC = ID
                };
                return csdl.GetList("HISAGG_DEV.PKG_DM_CHUNG.PROC_LAY_DS_MAU_BAO_CAO", obj);
            }
        }
    }
}
