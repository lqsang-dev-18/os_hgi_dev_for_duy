﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Reports.TamThanKinh_Phong_DaLieu
{
    public class TamThanKinh_Phong_DaLieuRepository
    {
        public static ResponsePageList GET_PAGELIST_BAOCAO_ALL(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, int? page, int? size, string nameProc)
        {
            using (var csdl = new SQLHelper())
            {
                string sql = "HISAGG_DEV." + nameProc;
                var obj = new
                {
                    TUNGAY = tungay,
                    DENNGAY = denngay,
                    QUY = quy,
                    THANG = thang,
                    NAM = nam,
                    KEYWORD = keyword,
                    PAGE = page,
                    SIZE = size
                };
                return csdl.GetPageList(sql, obj);
            }
        }
        public static ResponseList GET_LIST_BAOCAO_ALL(string? keyword, string? tungay, string? denngay, string? thang, string? quy, string? nam, string nameProc)
        {
            using (var csdl = new SQLHelper())
            {
                string sql = "HISAGG_DEV." + nameProc;
                var obj = new
                {
                    TUNGAY = tungay,
                    DENNGAY = denngay,
                    QUY = quy,
                    THANG = thang,
                    NAM = nam,
                    KEYWORD = keyword,
                };
                return csdl.GetList(sql, obj);
            }
        }
    }
}
