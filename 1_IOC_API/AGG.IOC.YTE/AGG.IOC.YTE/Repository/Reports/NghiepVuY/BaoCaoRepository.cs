﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.Reports.NghiepVuY
{
    public class BaoCaoRepository
    {
        public static ResponsePageList GET_PAGELIST_BAOCAO_ALL(string tungay, string denngay, string ma_huyen, string ma_xa, string macsyt, int? page, int? size, int? nam, int? thang, string nameProc)
        {
            using (var csdl = new SQLHelper())
            {
                string sql = "HISAGG_DEV." + nameProc;
                var obj = new
                {
                    TUNGAY = tungay,
                    DENNGAY = denngay,
                    MA_HUYEN = ma_huyen,
                    MA_XA = ma_xa,
                    MA_CSYT = macsyt,
                    THANG = thang,
                    NAM = nam,
                    PAGE = page,
                    SIZE = size
                };
                return csdl.GetPageList(sql, obj);
            }
        }
        public static ResponseList GET_LIST_BAOCAO_ALL(string tungay, string denngay, string? ma_huyen, string? ma_xa, string? macsyt, int? nam, int? thang, string nameProc)
        {
            using (var csdl = new SQLHelper())
            {
                string sql = "HISAGG_DEV." + nameProc;
                var obj = new
                {
                    TUNGAY = tungay,
                    DENNGAY = denngay,
                    MA_HUYEN = ma_huyen,
                    THANG = thang,
                    NAM = nam,
                    MA_XA = ma_xa,
                    MA_CSYT = macsyt
                };
                return csdl.GetList(sql, obj);
            }
        }
    }
}
