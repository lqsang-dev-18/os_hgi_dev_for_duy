﻿using System;
using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;
using AGG.IOC.YTE.Common.MD5;

namespace AGG.IOC.YTE.Repository.Authentication
{
	public class AuthRepository
	{
        public static ResponseSingle XAC_THUC_USER(string username, string password, string ip_client, string host_name, string ip_server)
        {
            using (var csdl = new SQLHelper())
            {
                var obj = new
                {
                    USERNAME = username.Replace(" ", ""),
                    PASSWORD = MD5.CreateMD5(password.Replace(" ", "")),
                    IP_CLIENT = ip_client,
                    SERVER_NAME = host_name,
                    IP_SERVER = ip_server,
                    SYSTEM = 1
                };
                return csdl.GetSingle("HISAGG_DEV.PKG_PHANQUYEN_USERS.PROC_XAC_THUC_USER", obj);
            }
        }

    }
}

