﻿using AGG.IOC.YTE.Common.Connection;
using AGG.IOC.YTE.Common.Response;

namespace AGG.IOC.YTE.Repository.ATVSTP
{
    public class ATVSTPReposotory
    {
        public ResponseList GetListATVSTP(string? nam, string? quy, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                QUY = quy
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList GetListATVSTP2(string? nam, string? doan_thanh_tra, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
                DOAN_THANH_TRA = doan_thanh_tra
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
        public ResponseList GetListATVSTP2(string? nam, string nameProc)
        {
            string sql = "HISAGG_DEV." + nameProc;
            var paramters = new
            {
                NAM = nam,
            };
            var context = new SQLHelper();
            return context.GetList(sql, paramters);
        }
    }
}
