﻿namespace AGG.IOC.YTE.Models.Authentication
{
    public class UserToken
    {
        public string? MA_USER { get; set; }
        public string? ID_LOG_IN { get; set; }
        public string? LEVEL_USER { get; set; }
        public string? MA_DON_VI { get; set; }

        public UserToken(string ma_user, string id_log_in, string level_user, string ma_don_vi)
        {
            this.MA_USER = ma_user;
            this.ID_LOG_IN = id_log_in;
            this.LEVEL_USER = level_user;
            this.MA_DON_VI = ma_don_vi;
        }
    }

    public class Token
    {
        public bool? success { get; set; }
        public string? message { get; set; }
        public string? type { get; set; }
        public string? time { get; set; }
        public dynamic? token { get; set; }

        public Token(bool success, string message, string type, string time, dynamic token)
        {
            this.success = success;
            this.message = message;
            this.type = type;
            this.time = time;
            this.token = token;
        }
    }
}