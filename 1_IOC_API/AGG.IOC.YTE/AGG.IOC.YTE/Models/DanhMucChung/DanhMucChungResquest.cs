﻿namespace AGG.IOC.YTE.Models.DanhMucChung
{
    public class DanhMucChungResquest
    {
        public int loai_cbx { get; set; }
        public string nam { get; set; }
        public string thang { get; set; }
        public string quy { get; set; }
        public string tungay { get; set; }
        public string denngay { get; set; }
        public string huyen { get; set; }
        public string loaihinh { get; set; }
        public List<string> cosokb { get; set; }
    }
}
