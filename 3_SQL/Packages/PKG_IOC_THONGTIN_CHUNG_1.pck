﻿CREATE OR REPLACE PACKAGE PKG_IOC_THONGTIN_CHUNG_1 AS

  PROCEDURE PROC_TK_SL_KCB(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                           P_TUNGAY   NVARCHAR2,
                           P_DENNGAY  NVARCHAR2,
                           P_NAM      NUMBER,
                           P_THANG    NUMBER,
                           P_QUY      NUMBER,
                           P_HUYEN    NUMBER,
                           P_LOAIHINH VARCHAR2,
                           P_COSOKB   NVARCHAR2,
                           P_RS       OUT SYS_REFCURSOR);

  PROCEDURE PROC_TK_SL_CAPCUU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                              P_TUNGAY   NVARCHAR2,
                              P_DENNGAY  NVARCHAR2,
                              P_NAM      NUMBER,
                              P_THANG    NUMBER,
                              P_QUY      NUMBER,
                              P_HUYEN    NUMBER,
                              P_LOAIHINH VARCHAR2,
                              P_COSOKB   NVARCHAR2,
                              P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_TK_SL_TUVONG(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                              P_TUNGAY   NVARCHAR2,
                              P_DENNGAY  NVARCHAR2,
                              P_NAM      NUMBER,
                              P_THANG    NUMBER,
                              P_QUY      NUMBER,
                              P_HUYEN    NUMBER,
                              P_LOAIHINH VARCHAR2,
                              P_COSOKB   NVARCHAR2,
                              P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_TKLUOT_RA_VAO(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                               P_TUNGAY   NVARCHAR2,
                               P_DENNGAY  NVARCHAR2,
                               P_NAM      NUMBER,
                               P_THANG    NUMBER,
                               P_QUY      NUMBER,
                               P_HUYEN    NUMBER,
                               P_LOAIHINH VARCHAR2,
                               P_COSOKB   NVARCHAR2,
                               P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_TK_CAPCUU_TUVONG(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                  P_TUNGAY   NVARCHAR2,
                                  P_DENNGAY  NVARCHAR2,
                                  P_NAM      NUMBER,
                                  P_THANG    NUMBER,
                                  P_QUY      NUMBER,
                                  P_HUYEN    NUMBER,
                                  P_LOAIHINH VARCHAR2,
                                  P_COSOKB   NVARCHAR2,
                                  P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_TK_KCB(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                        P_TUNGAY   NVARCHAR2,
                        P_DENNGAY  NVARCHAR2,
                        P_NAM      NUMBER,
                        P_THANG    NUMBER,
                        P_QUY      NUMBER,
                        P_HUYEN    NUMBER,
                        P_LOAIHINH VARCHAR2,
                        P_COSOKB   NVARCHAR2,
                        P_RS       OUT SYS_REFCURSOR);
  --- MỚI NHẤT CÓ LẤY THEO CSKB VÀ LOAIHINH
  PROCEDURE PROC_SL_KCB(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                        P_TUNGAY   NVARCHAR2,
                        P_DENNGAY  NVARCHAR2,
                        P_NAM      NUMBER,
                        P_THANG    NUMBER,
                        P_QUY      NUMBER,
                        P_HUYEN    NUMBER,
                        P_LOAIHINH VARCHAR2,
                        P_COSOKB   NVARCHAR2,
                        P_RS       OUT SYS_REFCURSOR);

  PROCEDURE PROC_SL_LUOT_RA_VIEN(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                 P_TUNGAY   NVARCHAR2,
                                 P_DENNGAY  NVARCHAR2,
                                 P_NAM      NUMBER,
                                 P_THANG    NUMBER,
                                 P_QUY      NUMBER,
                                 P_HUYEN    NUMBER,
                                 P_LOAIHINH VARCHAR2,
                                 P_COSOKB   NVARCHAR2,
                                 P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_SL_LUOT_VAO_VIEN(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                  P_TUNGAY   NVARCHAR2,
                                  P_DENNGAY  NVARCHAR2,
                                  P_NAM      NUMBER,
                                  P_THANG    NUMBER,
                                  P_QUY      NUMBER,
                                  P_HUYEN    NUMBER,
                                  P_LOAIHINH VARCHAR2,
                                  P_COSOKB   NVARCHAR2,
                                  P_RS       OUT SYS_REFCURSOR);

  PROCEDURE PROC_TK_NOI_TRU_NGOAI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                      P_TUNGAY   NVARCHAR2,
                                      P_DENNGAY  NVARCHAR2,
                                      P_NAM      NUMBER,
                                      P_THANG    NUMBER,
                                      P_QUY      NUMBER,
                                      P_HUYEN    NUMBER,
                                      P_LOAIHINH VARCHAR2,
                                      P_COSOKB   NVARCHAR2,
                                      P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_SL_CA_NANG_XIN_VE(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                   P_TUNGAY   NVARCHAR2,
                                   P_DENNGAY  NVARCHAR2,
                                   P_NAM      NUMBER,
                                   P_THANG    NUMBER,
                                   P_QUY      NUMBER,
                                   P_HUYEN    NUMBER,
                                   P_LOAIHINH VARCHAR2,
                                   P_COSOKB   NVARCHAR2,
                                   P_RS       OUT SYS_REFCURSOR);

  PROCEDURE PROC_SL_CHUYEN_VIEN(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                P_TUNGAY   NVARCHAR2,
                                P_DENNGAY  NVARCHAR2,
                                P_NAM      NUMBER,
                                P_THANG    NUMBER,
                                P_QUY      NUMBER,
                                P_HUYEN    NUMBER,
                                P_LOAIHINH VARCHAR2,
                                P_COSOKB   NVARCHAR2,
                                P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_SL_NOI_TRU_NGOAI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                      P_TUNGAY   NVARCHAR2,
                                      P_DENNGAY  NVARCHAR2,
                                      P_NAM      NUMBER,
                                      P_THANG    NUMBER,
                                      P_QUY      NUMBER,
                                      P_HUYEN    NUMBER,
                                      P_LOAIHINH VARCHAR2,
                                      P_COSOKB   NVARCHAR2,
                                      P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_SL_NOI_TRU_CUOI_KY(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                    P_TUNGAY   NVARCHAR2,
                                    P_DENNGAY  NVARCHAR2,
                                    P_NAM      NUMBER,
                                    P_THANG    NUMBER,
                                    P_QUY      NUMBER,
                                    P_HUYEN    NUMBER,
                                    P_LOAIHINH VARCHAR2,
                                    P_COSOKB   NVARCHAR2,
                                    P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_SL_NOI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                            P_TUNGAY   NVARCHAR2,
                            P_DENNGAY  NVARCHAR2,
                            P_NAM      NUMBER,
                            P_THANG    NUMBER,
                            P_QUY      NUMBER,
                            P_HUYEN    NUMBER,
                            P_LOAIHINH VARCHAR2,
                            P_COSOKB   NVARCHAR2,
                            P_RS       OUT SYS_REFCURSOR);
  PROCEDURE PROC_SL_NGOAI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                              P_TUNGAY   NVARCHAR2,
                              P_DENNGAY  NVARCHAR2,
                              P_NAM      NUMBER,
                              P_THANG    NUMBER,
                              P_QUY      NUMBER,
                              P_HUYEN    NUMBER,
                              P_LOAIHINH VARCHAR2,
                              P_COSOKB   NVARCHAR2,
                              P_RS       OUT SYS_REFCURSOR);

END PKG_IOC_THONGTIN_CHUNG_1;
/
CREATE OR REPLACE PACKAGE BODY PKG_IOC_THONGTIN_CHUNG_1 AS
  PROCEDURE PROC_TK_SL_KCB(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                           P_TUNGAY   NVARCHAR2,
                           P_DENNGAY  NVARCHAR2,
                           P_NAM      NUMBER,
                           P_THANG    NUMBER,
                           P_QUY      NUMBER,
                           P_HUYEN    NUMBER,
                           P_LOAIHINH VARCHAR2,
                           P_COSOKB   NVARCHAR2,
                           P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := 'AND TO_CHAR(NGAY_RA_DATE, ''YYYY'') = ' || P_NAM || ' ';
      ELSE
        V_IF_CBX := ' 
                         AND TO_CHAR(NGAY_RA_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                         AND TO_CHAR(NGAY_RA_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
      ELSE
        V_IF_CBX := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM ||
                    ') AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_THANG || ') ';
      END IF;
    
    ELSE
      V_IF_CBX := 'AND (TRUNC(KCB.NGAY_RA_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD'')) ';
    END IF;
    V_SQL := '
                        SELECT SUM(SL) as TONG_SL_KHAMBENH,
                            SUM(CASE WHEN CAP = 1 THEN SL ELSE 0 END) AS SL_KHAMBENH_TRUNGUONG,
                            SUM(CASE WHEN CAP = 2 THEN SL ELSE 0 END) AS SL_KHAMBENH_CAPTINH, -- 109.775
                            SUM(CASE WHEN CAP = 3 THEN SL ELSE 0 END) AS SL_KHAMBENH_CAPHUYEN, -- 654.214
                            SUM(CASE WHEN CAP = 4 THEN SL ELSE 0 END) AS SL_KHAMBENH_CAPXA -- 590.759
                            
                        FROM(
                            SELECT COUNT(1) as SL 
                            ,DV.CAP
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 1 = 1
                            ' || V_IF_CBX || '
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            AND KCB.MA_LOAI_KCB in (1)
                            GROUP BY DV.CAP
                        )

        ';
    --        DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  
  END;

  PROCEDURE PROC_TK_SL_CAPCUU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                              P_TUNGAY   NVARCHAR2,
                              P_DENNGAY  NVARCHAR2,
                              P_NAM      NUMBER,
                              P_THANG    NUMBER,
                              P_QUY      NUMBER,
                              P_HUYEN    NUMBER,
                              P_LOAIHINH VARCHAR2,
                              P_COSOKB   NVARCHAR2,
                              P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM || ') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := ' TO_CHAR(KCB.NGAY_VAO_DATE, ''YYYY'') = ' || P_NAM || ' ';
      ELSE
        V_IF_CBX := ' 
                     TO_CHAR(KCB.NGAY_VAO_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                     AND TO_CHAR(KCB.NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '
            ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM || ') ';
      ELSE
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM ||
                    ') AND (EXTRACT(MONTH FROM NGAY_VAO_DATE) = ' ||
                    P_THANG || ') ';
      END IF;
    
    ELSE
      V_IF_CBX := '(TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD'')) ';
    END IF;
  
    V_SQL := '
                SELECT SUM(SL) AS SL_TU_CAPCU FROM (
                    SELECT KCB.MA_CSKCB, COUNT(1) AS SL
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                    WHERE 
                    ' || V_IF_CBX || '
                    ' || V_HUYEN || '
                    ' || V_LOAIHINH || '
                    ' || V_COSOKB || '
                    AND KCB.MA_LYDO_VVIEN = 2
                    GROUP BY KCB.MA_CSKCB
                )
            ';
    --        DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;
  PROCEDURE PROC_TK_SL_TUVONG(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                              P_TUNGAY   NVARCHAR2,
                              P_DENNGAY  NVARCHAR2,
                              P_NAM      NUMBER,
                              P_THANG    NUMBER,
                              P_QUY      NUMBER,
                              P_HUYEN    NUMBER,
                              P_LOAIHINH VARCHAR2,
                              P_COSOKB   NVARCHAR2,
                              P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := ' TO_CHAR(KCB.NGAY_RA_DATE, ''YYYY'') = ' || P_NAM || ' ';
      ELSE
        V_IF_CBX := ' 
                         TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                         AND TO_CHAR(KCB.NGAY_RA_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                    ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
      ELSE
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM ||
                    ') AND (EXTRACT(MONTH FROM KCB.NGAY_RA_DATE) = ' ||
                    P_THANG || ') ';
      END IF;
    
    ELSE
      V_IF_CBX := '(TRUNC(NGAY_RA_DATE) BETWEEN TO_DATE(''' || P_TUNGAY ||
                  ''', ''YYYY-MM-DD'') AND TO_DATE(''' || P_DENNGAY ||
                  ''', ''YYYY-MM-DD'')) ';
    END IF;
  
    V_SQL := '
                SELECT SUM(SL) AS SL_TU_VONG FROM (
                    SELECT KCB.MA_CSKCB, COUNT(1) AS SL
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                    WHERE 
                    ' || V_IF_CBX || '
                    ' || V_HUYEN || '
                    ' || V_LOAIHINH || '
                    ' || V_COSOKB || '
                    AND KCB.KET_QUA_DTRI = 5
                    GROUP BY KCB.MA_CSKCB
                )
            ';
    --        DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;
  PROCEDURE PROC_TKLUOT_RA_VAO(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                               P_TUNGAY   NVARCHAR2,
                               P_DENNGAY  NVARCHAR2,
                               P_NAM      NUMBER,
                               P_THANG    NUMBER,
                               P_QUY      NUMBER,
                               P_HUYEN    NUMBER,
                               P_LOAIHINH VARCHAR2,
                               P_COSOKB   NVARCHAR2,
                               P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_TUNGAY   VARCHAR2(200);
    V_DENNGAY  VARCHAR2(200);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
      V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSIF (P_QUY = 1) THEN
        V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ')
                                AND TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = ' ||
                     P_QUY || '';
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-01-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/01/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-01-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/03/2023
      ELSIF (P_QUY = 2) THEN
        V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ')
                                AND TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = ' ||
                     P_QUY || '';
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-04-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/04/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-04-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/06/2023
      ELSIF (P_QUY = 3) THEN
        V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ')
                                AND TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = ' ||
                     P_QUY || '';
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-07-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/07/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-07-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/09/2023
      ELSE
        V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ')
                                AND TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = ' ||
                     P_QUY || '';
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-10-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/10/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-10-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/12/2023
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSE
        V_IF_CBX  := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM ||
                     ') AND (EXTRACT(MONTH FROM KCB.NGAY_RA_DATE) = ' ||
                     P_THANG || ') ';
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY''), ''MM'') ';
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY'')) ';
      END IF;
    
    ELSE
      V_IF_CBX  := 'AND (TRUNC(KCB.NGAY_RA_DATE) BETWEEN TO_DATE(''' ||
                   P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                   P_DENNGAY || ''', ''YYYY-MM-DD'')) ';
      V_TUNGAY  := 'TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'') ';
    END IF;
  
    V_SQL := '
                SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
                 FROM(
                    SELECT  TEN_DON_VI, SUM(SL) SO_LUONG_RA, 0 SO_LUONG_VAO
                    FROM(      
                        SELECT DV.TEN_HIEN_THI TEN_DON_VI, KCB.MA_CSKCB, COUNT(1) AS SL
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 1 = 1 
                        ' || V_IF_CBX || '
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        AND KCB.MA_LOAI_KCB IN (3) 
                        GROUP BY KCB.MA_CSKCB, DV.TEN_HIEN_THI
                    )  GROUP BY TEN_DON_VI
                    UNION 
                    SELECT  TEN_DON_VI, 0 SO_LUONG_RA, SUM(SOLUONG_CK) as SO_LUONG_VAO
                        FROM(
                                SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SOLUONG_CK
                                FROM CSDLYTE_4210.CHECK_IN_OUT KCB  
                                JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
                                WHERE 1 = 1
                                ' || V_HUYEN || '
                                ' || V_LOAIHINH || '
                                ' || V_COSOKB || '
                                AND trunc(KCB.CHECK_IN_DATE) <= ' ||
             V_DENNGAY || '
                                AND trunc(KCB.CHECK_IN_DATE) >= ' ||
             V_TUNGAY || '
                                AND nvl(KCB.CHECK_OUT, 0) = 0
                                AND KCB.MA_LOAI_KCB IN (2) 
                                GROUP BY  DV.TEN_HIEN_THI
                            UNION ALL
                                SELECT DV.TEN_DON_VI, COUNT(1) AS SOLUONG_CK 
                                FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
                                JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                                WHERE 1 = 1
                                ' || V_HUYEN || '
                                ' || V_LOAIHINH || '
                                ' || V_COSOKB || '
                                 AND KCB.MA_LOAI_KCB in (3)
                                 AND trunc(KCB.NGAY_VAO_DATE) >= ' ||
             V_TUNGAY || '
                                 AND trunc(KCB.NGAY_VAO_DATE) <= ' ||
             V_DENNGAY || ' 
                                GROUP BY TEN_DON_VI
                        ) GROUP BY TEN_DON_VI
                    ) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;
  --  PROCEDURE PROC_TKLUOT_RA_VAO(
  --        P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
  --        P_TUNGAY NVARCHAR2,
  --        P_DENNGAY NVARCHAR2,
  --        P_NAM NUMBER,
  --        P_THANG NUMBER,
  --        P_QUY NUMBER,
  --        P_HUYEN NUMBER,
  --        P_LOAIHINH VARCHAR2,
  --        P_COSOKB NVARCHAR2,
  --        P_RS OUT SYS_REFCURSOR
  --  ) IS 
  --        V_SQL VARCHAR2(20000);
  --        V_IF_CBX VARCHAR2(10000);
  --        V_HUYEN VARCHAR2(2000);
  --        V_LOAIHINH VARCHAR2(200);
  --        V_COSOKB VARCHAR2(2000);
  --    BEGIN
  --        IF(P_HUYEN = 0) THEN
  --            V_HUYEN := '';
  --        ELSE
  --             V_HUYEN := 'AND DV.MA_HUYEN = '||P_HUYEN||'';
  --        END IF;
  --        IF(P_LOAIHINH = '2') THEN
  --            V_LOAIHINH := '';
  --        ELSE
  --             V_LOAIHINH := 'AND DV.TU_NHAN = '''||P_LOAIHINH||'''';
  --        END IF;
  --        IF(P_COSOKB = '0') THEN
  --            V_COSOKB := '';
  --        ELSE
  --             V_COSOKB := 'AND DV.MA_DON_VI IN( '||P_COSOKB||')';
  --        END IF;
  --        IF(P_LOAI_CBX = 0) THEN
  --           V_IF_CBX := '
  --                    SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
  --                        FROM((
  --                          SELECT TEN_DON_VI, 0 SO_LUONG_RA, SUM(SOLUONG_CK) SO_LUONG_VAO FROM (
  --                            SELECT TEN_DON_VI, SUM(SOLUONG_CK) SOLUONG_CK
  --                            FROM(
  --                                    SELECT DV.TEN_DON_VI, COUNT(MA_LK) SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT KCB  --920
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.CHECK_IN_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.CHECK_IN = 1
  --                                    AND KCB.MA_LOAI_KCB = (2)
  --                                    GROUP BY DV.TEN_DON_VI
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SOLUONG_CK --14691
  --                                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                     AND KCB.MA_LOAI_KCB = (3)
  --                                     GROUP BY DV.TEN_DON_VI
  --                               ) GROUP BY TEN_DON_VI 
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, - COUNT(CK.MA_LK) AS SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT CK
  --                                    JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB ON CK.MA_LK = KCB.MA_LK
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE
  --                                   (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.MA_LOAI_KCB = (3)
  --                                    GROUP BY DV.TEN_DON_VI
  --                            ) GROUP BY TEN_DON_VI
  --                       
  --                        ) UNION (
  --                            SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_RA, 0 SO_LUONG_VAO
  --                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
  --                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                            WHERE 
  --                            (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = '||P_NAM||')
  --                            AND KCB.MA_LOAI_KCB IN (3) 
  --                            '||V_HUYEN||'
  --                            '||V_LOAIHINH||'
  --                            '||V_COSOKB||'
  --                            GROUP BY DV.TEN_DON_VI
  --                        ) 
  --                        ) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
  --           
  ----                SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
  ----                FROM((
  ----                    SELECT  DV.TEN_DON_VI, 0 SO_LUONG_RA,  COUNT(KCB.CHECK_IN) SO_LUONG_VAO  
  ----                    FROM CSDLYTE_4210.CHECK_IN_OUT KCB 
  ----                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  ----                    WHERE 
  ----                    (EXTRACT(YEAR FROM KCB.CHECK_IN_DATE) = '||P_NAM||')
  ----                    AND KCB.CHECK_IN = 1 AND KCB.MA_LOAI_KCB IN (2,3)
  ----                    '||V_HUYEN||'
  ----                    '||V_LOAIHINH||'
  ----                    '||V_COSOKB||'
  ----                    GROUP BY DV.TEN_DON_VI
  ----                ) UNION
  ----                (
  ----                    SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_RA, 0 SO_LUONG_VAO
  ----                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
  ----                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  ----                    WHERE 
  ----                    (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = '||P_NAM||')
  ----                    AND KCB.MA_LOAI_KCB IN (3)  
  ----                    '||V_HUYEN||'
  ----                    '||V_LOAIHINH||'
  ----                    '||V_COSOKB||'
  ----                    GROUP BY DV.TEN_DON_VI
  ----                ) 
  ----            ) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
  --                
  --           '; 
  --        ELSIF(P_LOAI_CBX = 1) THEN
  --            IF(P_QUY = 0) THEN
  --                V_IF_CBX := ' 
  --                        SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
  --                        FROM((
  --                          SELECT TEN_DON_VI, 0 SO_LUONG_RA, SUM(SOLUONG_CK) SO_LUONG_VAO FROM (
  --                            SELECT TEN_DON_VI, SUM(SOLUONG_CK) SOLUONG_CK
  --                            FROM(
  --                                    SELECT DV.TEN_DON_VI, COUNT(MA_LK) SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT KCB  --920
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.CHECK_IN_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.CHECK_IN = 1
  --                                    AND KCB.MA_LK LIKE ''%noitru%'' OR KCB.MA_LK LIKE ''%NT%''
  --                                    GROUP BY DV.TEN_DON_VI
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SOLUONG_CK --14691
  --                                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                     AND KCB.MA_LOAI_KCB = (3)
  --                                     GROUP BY DV.TEN_DON_VI
  --                               ) GROUP BY TEN_DON_VI 
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, - COUNT(CK.MA_LK) AS SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT CK
  --                                    JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB ON CK.MA_LK = KCB.MA_LK
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE
  --                                   (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.MA_LOAI_KCB = (3)
  --                                    GROUP BY DV.TEN_DON_VI
  --                            ) GROUP BY TEN_DON_VI
  --                       
  --                        ) UNION (
  --                            SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_RA, 0 SO_LUONG_VAO
  --                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
  --                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                            WHERE 
  --                            (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = '||P_NAM||')
  --                            AND KCB.MA_LOAI_KCB IN (3) 
  --                            '||V_HUYEN||'
  --                            '||V_LOAIHINH||'
  --                            '||V_COSOKB||'
  --                            GROUP BY DV.TEN_DON_VI
  --                        ) ) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
  --                ';
  --            ELSE
  --                V_IF_CBX := '
  --                    SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
  --                        FROM((
  --                          SELECT TEN_DON_VI, 0 SO_LUONG_RA, SUM(SOLUONG_CK) SO_LUONG_VAO FROM (
  --                            SELECT TEN_DON_VI, SUM(SOLUONG_CK) SOLUONG_CK
  --                            FROM(
  --                                    SELECT DV.TEN_DON_VI, COUNT(MA_LK) SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT KCB  --920
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.CHECK_IN_DATE) = '||P_NAM||')
  --                                    AND TO_CHAR(KCB.CHECK_IN_DATE, ''Q'') = '||P_QUY||' 
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.CHECK_IN = 1
  --                                    AND KCB.MA_LK LIKE ''%noitru%'' OR KCB.MA_LK LIKE ''%NT%''
  --                                    GROUP BY DV.TEN_DON_VI
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SOLUONG_CK --14691
  --                                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    AND TO_CHAR(KCB.NGAY_VAO_DATE, ''Q'') = '||P_QUY||' 
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                     AND KCB.MA_LOAI_KCB = (3)
  --                                     GROUP BY DV.TEN_DON_VI
  --                               ) GROUP BY TEN_DON_VI 
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, - COUNT(CK.MA_LK) AS SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT CK
  --                                    JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB ON CK.MA_LK = KCB.MA_LK
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE
  --                                   (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    AND TO_CHAR(KCB.NGAY_VAO_DATE, ''Q'') = '||P_QUY||' 
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.MA_LOAI_KCB = (3)
  --                                    GROUP BY DV.TEN_DON_VI
  --                            ) GROUP BY TEN_DON_VI
  --                       
  --                        ) UNION (
  --                            SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_RA, 0 SO_LUONG_VAO
  --                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
  --                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                            WHERE 
  --                            (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = '||P_NAM||')
  --                            AND TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = '||P_QUY||' 
  --                            AND KCB.MA_LOAI_KCB IN (3) 
  --                            '||V_HUYEN||'
  --                            '||V_LOAIHINH||'
  --                            '||V_COSOKB||'
  --                            GROUP BY DV.TEN_DON_VI
  --                        )) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
  --                '; 
  --            END IF;
  --        ELSIF(P_LOAI_CBX = 2) THEN
  --            IF(P_THANG = 0) THEN
  --                V_IF_CBX := '  
  --                    SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
  --                        FROM((
  --                          SELECT TEN_DON_VI, 0 SO_LUONG_RA, SUM(SOLUONG_CK) SO_LUONG_VAO FROM (
  --                            SELECT TEN_DON_VI, SUM(SOLUONG_CK) SOLUONG_CK
  --                            FROM(
  --                                    SELECT DV.TEN_DON_VI, COUNT(MA_LK) SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT KCB  --920
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.CHECK_IN_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.CHECK_IN = 1
  --                                    AND KCB.MA_LK LIKE ''%noitru%'' OR KCB.MA_LK LIKE ''%NT%''
  --                                    GROUP BY DV.TEN_DON_VI
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SOLUONG_CK --14691
  --                                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                     AND KCB.MA_LOAI_KCB = (3)
  --                                     GROUP BY DV.TEN_DON_VI
  --                               ) GROUP BY TEN_DON_VI 
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, - COUNT(CK.MA_LK) AS SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT CK
  --                                    JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB ON CK.MA_LK = KCB.MA_LK
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE
  --                                   (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.MA_LOAI_KCB = (3)
  --                                    GROUP BY DV.TEN_DON_VI
  --                            ) GROUP BY TEN_DON_VI
  --                       
  --                        ) UNION (
  --                            SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_RA, 0 SO_LUONG_VAO
  --                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
  --                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                            WHERE 
  --                            (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = '||P_NAM||')
  --                            AND KCB.MA_LOAI_KCB IN (3) 
  --                            '||V_HUYEN||'
  --                            '||V_LOAIHINH||'
  --                            '||V_COSOKB||'
  --                            GROUP BY DV.TEN_DON_VI
  --                        ) ) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
  --                    
  --                '; 
  --            ELSE
  --                V_IF_CBX := '
  --                    SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
  --                        FROM((
  --                          SELECT TEN_DON_VI, 0 SO_LUONG_RA, SUM(SOLUONG_CK) SO_LUONG_VAO FROM (
  --                            SELECT TEN_DON_VI, SUM(SOLUONG_CK) SOLUONG_CK
  --                            FROM(
  --                                    SELECT DV.TEN_DON_VI, COUNT(MA_LK) SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT KCB  --920
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.CHECK_IN_DATE) = '||P_NAM||')
  --                                    AND (EXTRACT(MONTH FROM KCB.CHECK_IN_DATE) = '||P_THANG||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.CHECK_IN = 1
  --                                    AND KCB.MA_LK LIKE ''%noitru%'' OR KCB.MA_LK LIKE ''%NT%''
  --                                    GROUP BY DV.TEN_DON_VI
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SOLUONG_CK --14691
  --                                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE 
  --                                    (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                    AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = '||P_THANG||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                     AND KCB.MA_LOAI_KCB = (3)
  --                                     GROUP BY DV.TEN_DON_VI
  --                               ) GROUP BY TEN_DON_VI 
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, - COUNT(CK.MA_LK) AS SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT CK
  --                                    JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB ON CK.MA_LK = KCB.MA_LK
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE
  --                                   (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = '||P_NAM||')
  --                                   AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = '||P_THANG||')
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.MA_LOAI_KCB = (3)
  --                                    GROUP BY DV.TEN_DON_VI
  --                            ) GROUP BY TEN_DON_VI
  --                       
  --                        ) UNION (
  --                            SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_RA, 0 SO_LUONG_VAO
  --                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
  --                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                            WHERE 
  --                            (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = '||P_NAM||')
  --                            AND (EXTRACT(MONTH FROM KCB.NGAY_RA_DATE) = '||P_THANG||')
  --                            AND KCB.MA_LOAI_KCB IN (3) 
  --                            '||V_HUYEN||'
  --                            '||V_LOAIHINH||'
  --                            '||V_COSOKB||'
  --                            GROUP BY DV.TEN_DON_VI
  --                        ) ) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
  --                 '; 
  --            END IF;
  --        ELSE
  --            V_IF_CBX := '
  --                        SELECT TEN_DON_VI, SUM(SO_LUONG_RA) SO_LUONG_RA, SUM(SO_LUONG_VAO) SO_LUONG_VAO
  --                        FROM((
  --                          SELECT TEN_DON_VI, 0 SO_LUONG_RA, SUM(SOLUONG_CK) SO_LUONG_VAO FROM (
  --                            SELECT TEN_DON_VI, SUM(SOLUONG_CK) SOLUONG_CK
  --                            FROM(
  --                                    SELECT DV.TEN_DON_VI, COUNT(MA_LK) SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT KCB  --920
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
  --                                    WHERE 
  --                                    (TRUNC(KCB.CHECK_IN_DATE) BETWEEN TO_DATE('''||P_TUNGAY||''', ''YYYY-MM-DD'') AND TO_DATE('''||P_DENNGAY||''', ''YYYY-MM-DD''))
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.CHECK_IN = 1
  --                                    AND KCB.MA_LK LIKE ''%noitru%'' OR KCB.MA_LK LIKE ''%NT%''
  --                                    GROUP BY DV.TEN_DON_VI
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SOLUONG_CK --14691
  --                                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE 
  --                                    (TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE('''||P_TUNGAY||''', ''YYYY-MM-DD'') AND TO_DATE('''||P_DENNGAY||''', ''YYYY-MM-DD''))
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                     AND KCB.MA_LOAI_KCB = (3)
  --                                     GROUP BY DV.TEN_DON_VI
  --                               ) GROUP BY TEN_DON_VI 
  --                                UNION ALL
  --                                    SELECT DV.TEN_DON_VI, - COUNT(CK.MA_LK) AS SOLUONG_CK
  --                                    FROM CSDLYTE_4210.CHECK_IN_OUT CK
  --                                    JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB ON CK.MA_LK = KCB.MA_LK
  --                                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                                    WHERE
  --                                    (TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE('''||P_TUNGAY||''', ''YYYY-MM-DD'') AND TO_DATE('''||P_DENNGAY||''', ''YYYY-MM-DD''))
  --                                    '||V_HUYEN||'
  --                                    '||V_LOAIHINH||'
  --                                    '||V_COSOKB||'
  --                                    AND KCB.MA_LOAI_KCB = (3)
  --                                    GROUP BY DV.TEN_DON_VI
  --                            ) GROUP BY TEN_DON_VI
  --                       
  --                        ) UNION (
  --                            SELECT DV.TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_RA, 0 SO_LUONG_VAO
  --                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
  --                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
  --                            WHERE 
  --                             (TRUNC(KCB.NGAY_RA_DATE) BETWEEN TO_DATE('''||P_TUNGAY||''', ''YYYY-MM-DD'') AND TO_DATE('''||P_DENNGAY||''', ''YYYY-MM-DD''))
  --                            AND KCB.MA_LOAI_KCB IN (3) 
  --                            '||V_HUYEN||'
  --                            '||V_LOAIHINH||'
  --                            '||V_COSOKB||'
  --                            GROUP BY DV.TEN_DON_VI
  --                        ) ) GROUP BY TEN_DON_VI ORDER BY SO_LUONG_RA DESC
  --            ';
  --        END IF;
  --
  --        V_SQL :='
  --               '||V_IF_CBX||'
  --            ';
  ----        DBMS_OUTPUT.PUT_LINE(V_SQL);
  --        OPEN P_RS FOR V_SQL;
  --    END;
  PROCEDURE PROC_TK_CAPCUU_TUVONG(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                  P_TUNGAY   NVARCHAR2,
                                  P_DENNGAY  NVARCHAR2,
                                  P_NAM      NUMBER,
                                  P_THANG    NUMBER,
                                  P_QUY      NUMBER,
                                  P_HUYEN    NUMBER,
                                  P_LOAIHINH VARCHAR2,
                                  P_COSOKB   NVARCHAR2,
                                  P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(20000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := '
                 SELECT A.TEN_DON_VI, A.SO_LUONG_CAPCUU, B.SO_LUONG_TUVONG FROM(        
                    SELECT  DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_CAPCUU
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                    WHERE 
                    (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                  P_NAM || ') 
                    AND KCB.MA_LYDO_VVIEN = 2
                    ' || V_HUYEN || '
                    ' || V_LOAIHINH || '
                    ' || V_COSOKB || '
                    GROUP BY DV.TEN_HIEN_THI
                ) A LEFT JOIN (
                    SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_TUVONG
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                    WHERE 
                    (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' ||
                  P_NAM || ')    
                    ' || V_HUYEN || '
                    ' || V_LOAIHINH || '
                    ' || V_COSOKB || '
                    AND KET_QUA_DTRI = 5
                    GROUP BY DV.TEN_HIEN_THI
                ) B ON A.TEN_DON_VI = B.TEN_DON_VI 
                ORDER BY A.SO_LUONG_CAPCUU DESC 
           ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := ' 
                        SELECT A.TEN_DON_VI, A.SO_LUONG_CAPCUU, B.SO_LUONG_TUVONG FROM(        
                            SELECT  DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_CAPCUU
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            TO_CHAR(KCB.NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                            AND KCB.MA_LYDO_VVIEN = 2
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) A LEFT JOIN (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_TUVONG
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            TO_CHAR(KCB.NGAY_RA_DATE, ''YYYY'') = ' ||
                    P_NAM || '  
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            AND KET_QUA_DTRI = 5
                            GROUP BY DV.TEN_HIEN_THI 
                        ) B ON A.TEN_DON_VI = B.TEN_DON_VI 
                        ORDER BY A.SO_LUONG_CAPCUU DESC 
                    ';
      ELSE
        V_IF_CBX := ' 
                        SELECT A.TEN_DON_VI, A.SO_LUONG_CAPCUU, B.SO_LUONG_TUVONG FROM(        
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_CAPCUU
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            TO_CHAR(KCB.NGAY_VAO_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                            AND TO_CHAR(KCB.NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                            AND KCB.MA_LYDO_VVIEN = 2
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) A LEFT JOIN (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_TUVONG
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                            AND TO_CHAR(KCB.NGAY_RA_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            AND KET_QUA_DTRI = 5
                            GROUP BY DV.TEN_HIEN_THI 
                        ) B ON A.TEN_DON_VI = B.TEN_DON_VI 
                        ORDER BY A.SO_LUONG_CAPCUU DESC 
                    ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := '  
                    SELECT A.TEN_DON_VI, A.SO_LUONG_CAPCUU, B.SO_LUONG_TUVONG FROM(        
                        SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_CAPCUU
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_NAM || ') 
                        AND KCB.MA_LYDO_VVIEN = 2
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB ||
                    '                      
                        GROUP BY DV.TEN_HIEN_THI 
                    ) A FULL JOIN (
                        SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_TUVONG
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' ||
                    P_NAM || ')   
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        AND KET_QUA_DTRI = 5
                        GROUP BY DV.TEN_HIEN_THI 
                    ) B ON A.TEN_DON_VI = B.TEN_DON_VI 
                   ORDER BY A.SO_LUONG_CAPCUU DESC 
                ';
      ELSE
        V_IF_CBX := '
                        SELECT A.TEN_DON_VI, A.SO_LUONG_CAPCUU, B.SO_LUONG_TUVONG FROM(        
                        SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_CAPCUU
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_NAM || ') 
                        AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_THANG || ')   
                        AND KCB.MA_LYDO_VVIEN = 2
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        GROUP BY DV.TEN_HIEN_THI 
                    ) A FULL JOIN (
                        SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_TUVONG
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' ||
                    P_NAM ||
                    ')   
                        AND (EXTRACT(MONTH FROM KCB.NGAY_RA_DATE) = ' ||
                    P_THANG || ')   
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        AND KET_QUA_DTRI = 5
                        GROUP BY DV.TEN_HIEN_THI 
                    ) B ON A.TEN_DON_VI = B.TEN_DON_VI 
                    ORDER BY A.SO_LUONG_CAPCUU DESC 
                 ';
      END IF;
    ELSE
      V_IF_CBX := '
                         SELECT A.TEN_DON_VI, A.SO_LUONG_CAPCUU, B.SO_LUONG_TUVONG FROM(        
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_CAPCUU
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            (TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD''))
                            AND KCB.MA_LYDO_VVIEN = 2
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY  DV.TEN_HIEN_THI 
                        ) A LEFT JOIN (
                            SELECT DV.TEN_HIEN_THI  TEN_DON_VI, COUNT(KCB.MA_LK) AS SO_LUONG_TUVONG
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            (TRUNC(KCB.NGAY_RA_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD''))
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            AND KET_QUA_DTRI = 5
                            GROUP BY DV.TEN_HIEN_THI 
                        ) B ON A.TEN_DON_VI = B.TEN_DON_VI 
                       ORDER BY A.SO_LUONG_CAPCUU DESC 
            ';
    END IF;
  
    V_SQL := '
               ' || V_IF_CBX || '
            ';
    --        DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;

  PROCEDURE PROC_TK_KCB(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                        P_TUNGAY   NVARCHAR2,
                        P_DENNGAY  NVARCHAR2,
                        P_NAM      NUMBER,
                        P_THANG    NUMBER,
                        P_QUY      NUMBER,
                        P_HUYEN    NUMBER,
                        P_LOAIHINH VARCHAR2,
                        P_COSOKB   NVARCHAR2,
                        P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
    V_TU_NGAY  DATE;
    V_DEN_NGAY DATE;
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
  
    -- dangth: Xử lý chuỗi thời gian
    IF P_LOAI_CBX = 0 THEN
      -- xem dữ liệu theo năm
      V_TU_NGAY  := TO_DATE('01/01/' || P_NAM || ' 00:00:00',
                            'DD/MM/YYYY HH24:MI:SS');
      V_DEN_NGAY := TO_DATE('31/12/' || P_NAM || ' 00:00:00',
                            'DD/MM/YYYY HH24:MI:SS');
    ELSIF P_LOAI_CBX = 1 THEN
      -- xem dữ liệu theo quý
      IF P_QUY = 0 THEN
        V_TU_NGAY  := TO_DATE('01/01/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
        V_DEN_NGAY := TO_DATE('31/12/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
      ELSIF P_QUY = 1 THEN
        V_TU_NGAY  := TO_DATE('01/01/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
        V_DEN_NGAY := TO_DATE('31/03/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
      ELSIF P_QUY = 2 THEN
        V_TU_NGAY  := TO_DATE('01/04/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
        V_DEN_NGAY := TO_DATE('30/06/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
      ELSIF P_QUY = 3 THEN
        V_TU_NGAY  := TO_DATE('01/07/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
        V_DEN_NGAY := TO_DATE('30/09/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
      ELSIF P_QUY = 4 THEN
        V_TU_NGAY  := TO_DATE('01/10/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
        V_DEN_NGAY := TO_DATE('31/12/' || P_NAM || ' 00:00:00',
                              'DD/MM/YYYY HH24:MI:SS');
      END IF;
    ELSIF P_LOAI_CBX = 2 THEN
      -- xem dữ liệu theo tháng
      IF P_THANG = 0 THEN
        V_TU_NGAY  := TO_DATE('01/01/' || P_NAM || ' 00:00:00',
                            'DD/MM/YYYY HH24:MI:SS');
                            V_DEN_NGAY := TO_DATE('31/12/' || P_NAM || ' 00:00:00',
                            'DD/MM/YYYY HH24:MI:SS');
      ELSE
        V_TU_NGAY  := TO_DATE('01/' || P_THANG || '/' || P_NAM || ' 00:00:00',
                            'DD/MM/YYYY HH24:MI:SS');
                            V_DEN_NGAY := ADD_MONTHS(V_TU_NGAY, 1);
      END IF;      
    ELSE
      -- xem dữ liệu theo ngày
      V_TU_NGAY  := TO_DATE(P_TUNGAY || ' 00:00:00',
                            'YYYY-MM-DD HH24:MI:SS');
      V_DEN_NGAY := TO_DATE(P_DENNGAY || ' 00:00:00',
                            'YYYY-MM-DD HH24:MI:SS');
    END IF;
    V_DEN_NGAY := V_DEN_NGAY + 1;    
    
    V_SQL := 'SELECT COUNT(1) TONG_SL_KHAMBENH, DV.TEN_HIEN_THI TEN_DON_VI
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB, CSDLYTE_DANHMUC.DM_DONVI DV
                        WHERE KCB.MA_CSKCB = DV.MA_DON_VI AND
                         KCB.NGAY_VAO_DATE BETWEEN ''' || V_TU_NGAY || ''' AND ''' || V_DEN_NGAY || ''' AND KCB.MA_LOAI_KCB = ''1''
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        GROUP BY DV.TEN_HIEN_THI ORDER BY TONG_SL_KHAMBENH DESC';
                    OPEN P_RS FOR V_SQL;
  
    /*IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := '
                    SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                        SELECT DV.TEN_HIEN_THI  TEN_DON_VI, COUNT(1) SL
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                  P_NAM || ')       
                        AND KCB.MA_LOAI_KCB = ''1''
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        GROUP BY DV.TEN_HIEN_THI 
                    ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                    
           ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := ' 
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '      
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        
                    ';
      ELSE
        V_IF_CBX := ' 
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '   
                           AND TO_CHAR(NGAY_VAO_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        


                    ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := ' 
                    SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                        SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_NAM || ')       
                        AND KCB.MA_LOAI_KCB = ''1''
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        GROUP BY DV.TEN_HIEN_THI 
                    ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                    
                ';
      ELSE
        V_IF_CBX := '
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_NAM || ') 
                            AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_THANG || ')   
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        
                 ';
      END IF;
    ELSE
      V_IF_CBX := '
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            (TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD''))
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        

            ';
    END IF;
  
    V_SQL := '' || V_IF_CBX || '';
    OPEN P_RS FOR V_SQL;*/
  END;

  PROCEDURE PROC_SL_KCB(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                        P_TUNGAY   NVARCHAR2,
                        P_DENNGAY  NVARCHAR2,
                        P_NAM      NUMBER,
                        P_THANG    NUMBER,
                        P_QUY      NUMBER,
                        P_HUYEN    NUMBER,
                        P_LOAIHINH VARCHAR2,
                        P_COSOKB   NVARCHAR2,
                        P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := '
                    SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                        SELECT DV.TEN_HIEN_THI  TEN_DON_VI, COUNT(1) SL
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                  P_NAM || ')       
                        AND KCB.MA_LOAI_KCB = ''1''
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        GROUP BY DV.TEN_HIEN_THI 
                    ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                    
           ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := ' 
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '      
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        
                    ';
      ELSE
        V_IF_CBX := ' 
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '   
                           AND TO_CHAR(NGAY_VAO_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        


                    ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := ' 
                    SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                        SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                        FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                        JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                        WHERE 
                        (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_NAM || ')       
                        AND KCB.MA_LOAI_KCB = ''1''
                        ' || V_HUYEN || '
                        ' || V_LOAIHINH || '
                        ' || V_COSOKB || '
                        GROUP BY DV.TEN_HIEN_THI 
                    ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                    
                ';
      ELSE
        V_IF_CBX := '
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            (EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_NAM || ') 
                            AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_THANG || ')   
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        
                 ';
      END IF;
    ELSE
      V_IF_CBX := '
                        SELECT TEN_DON_VI , SUM(SL) AS TONG_SL_KHAMBENH FROM (
                            SELECT DV.TEN_HIEN_THI TEN_DON_VI, COUNT(1) SL
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 
                            (TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD''))
                            AND KCB.MA_LOAI_KCB = ''1''
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            GROUP BY DV.TEN_HIEN_THI 
                        ) GROUP BY TEN_DON_VI ORDER BY TONG_SL_KHAMBENH DESC
                        

            ';
    END IF;
  
    V_SQL := '
               ' || V_IF_CBX || '
            ';
    --        DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;

  PROCEDURE PROC_SL_LUOT_VAO_VIEN(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                  P_TUNGAY   NVARCHAR2,
                                  P_DENNGAY  NVARCHAR2,
                                  P_NAM      NUMBER,
                                  P_THANG    NUMBER,
                                  P_QUY      NUMBER,
                                  P_HUYEN    NUMBER,
                                  P_LOAIHINH VARCHAR2,
                                  P_COSOKB   NVARCHAR2,
                                  P_RS       OUT SYS_REFCURSOR) IS
    V_SQL       VARCHAR2(10000);
    V_IF_CBX    VARCHAR2(2000);
    V_IF_CBX_CK VARCHAR2(2000);
    V_HUYEN     VARCHAR2(2000);
    V_LOAIHINH  VARCHAR2(200);
    V_COSOKB    VARCHAR2(2000);
    V_TUNGAY    VARCHAR2(200);
    V_DENNGAY   VARCHAR2(200);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSIF (P_QUY = 1) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-01-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/01/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-01-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/03/2023
      ELSIF (P_QUY = 2) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-04-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/04/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-04-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/06/2023
      ELSIF (P_QUY = 3) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-07-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/07/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-07-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/09/2023
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-10-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/10/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-10-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/12/2023
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSE
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-' || P_THANG ||
                     '-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM || '-' || P_THANG ||
                     ''', ''YYYY-MM'')) ';
      END IF;
    
    ELSE
      V_TUNGAY  := 'TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'') ';
    END IF;
    V_SQL := '
                    SELECT SUM(SOLUONG_CK) as SO_LUONG_VAO
                    FROM(
                            SELECT COUNT(1) SOLUONG_CK
                            FROM CSDLYTE_4210.CHECK_IN_OUT KCB  
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI --963
                            WHERE 1 = 1
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                            AND trunc(KCB.CHECK_IN_DATE) >= ' ||
             V_TUNGAY || ' 
                            AND trunc(KCB.CHECK_IN_DATE) <= ' ||
             V_DENNGAY || '
                            AND nvl(KCB.CHECK_OUT, 0) = 0
                            AND KCB.MA_LOAI_KCB IN (2) 

                        UNION ALL
                            SELECT COUNT(1) AS SOLUONG_CK 
                            FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB  KCB
                            JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                            WHERE 1 = 1
                            ' || V_HUYEN || '
                            ' || V_LOAIHINH || '
                            ' || V_COSOKB || '
                             AND KCB.MA_LOAI_KCB in (3)
                             AND trunc(KCB.NGAY_VAO_DATE) >= ' ||
             V_TUNGAY || ' 
                             AND trunc(KCB.NGAY_VAO_DATE) <= ' ||
             V_DENNGAY || '

                    )         
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    --OPEN P_RS FOR select V_SQL as SQL_STORE from dual; 
    OPEN P_RS FOR V_SQL;
  END;
  PROCEDURE PROC_SL_LUOT_RA_VIEN(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                 P_TUNGAY   NVARCHAR2,
                                 P_DENNGAY  NVARCHAR2,
                                 P_NAM      NUMBER,
                                 P_THANG    NUMBER,
                                 P_QUY      NUMBER,
                                 P_HUYEN    NUMBER,
                                 P_LOAIHINH VARCHAR2,
                                 P_COSOKB   NVARCHAR2,
                                 P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := 'AND TO_CHAR(KCB.NGAY_RA_DATE, ''YYYY'') = ' || P_NAM || ' ';
      ELSE
        V_IF_CBX := ' 
                         AND TO_CHAR(KCB.NGAY_RA_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                         AND TO_CHAR(KCB.NGAY_RA_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM || ') ';
      ELSE
        V_IF_CBX := 'AND (EXTRACT(YEAR FROM KCB.NGAY_RA_DATE) = ' || P_NAM ||
                    ') AND (EXTRACT(MONTH FROM KCB.NGAY_RA_DATE) = ' ||
                    P_THANG || ') ';
      END IF;
    
    ELSE
      V_IF_CBX := 'AND (TRUNC(KCB.NGAY_RA_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD'')) ';
    END IF;
  
    V_SQL := '
                --SELECT SUM(SL) SO_LUONG_RA FROM(      
                    SELECT --KCB.MA_CSKCB,
                    COUNT(1) AS SO_LUONG_RA
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                    WHERE 1 = 1 
                    ' || V_IF_CBX || '
                    ' || V_HUYEN || '
                    ' || V_LOAIHINH || '
                    ' || V_COSOKB || '
                    AND KCB.MA_LOAI_KCB IN (3) 
                    --GROUP BY KCB.MA_CSKCB
                --)
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;

  PROCEDURE PROC_TK_NOI_TRU_NGOAI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                      P_TUNGAY   NVARCHAR2,
                                      P_DENNGAY  NVARCHAR2,
                                      P_NAM      NUMBER,
                                      P_THANG    NUMBER,
                                      P_QUY      NUMBER,
                                      P_HUYEN    NUMBER,
                                      P_LOAIHINH VARCHAR2,
                                      P_COSOKB   NVARCHAR2,
                                      P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
    V_TUNGAY   VARCHAR2(200);
    V_DENNGAY  VARCHAR2(200);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSIF (P_QUY = 1) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-01-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/01/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-01-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/03/2023
      ELSIF (P_QUY = 2) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-04-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/04/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-04-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/06/2023
      ELSIF (P_QUY = 3) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-07-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/07/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-07-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/09/2023
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-10-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/10/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-10-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/12/2023
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY''), ''MM'') ';
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY'')) ';
      END IF;
    
    ELSE
      V_TUNGAY  := 'TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'') ';
    END IF;
  
    V_SQL := '
                SELECT TEN_DON_VI, SUM(SL_NGOAI_TRU) SL_NGOAI_TRU, SUM(SL_NOI_TRU) SL_NOI_TRU
                  FROM(
                    SELECT TEN_DON_VI, SUM(SL_NGOAI_TRU) AS SL_NGOAI_TRU, 0 SL_NOI_TRU
                    FROM (
                        SELECT  c.TEN_DON_VI,  CASE WHEN ( c.MA_LOAI_KCB = 2) THEN SONGAY ELSE 0 END AS SL_NGOAI_TRU
                         FROM ( 
                            SELECT b.*, (NGAY_RA_DATE - NGAY_VAO_DATE + 1) SONGAY 
                            FROM ( 
                                SELECT  DV.TEN_HIEN_THI TEN_DON_VI, KCB.ID_BENH_NHAN, KCB.HO_TEN, KCB.MA_LOAI_KCB,
                                        (CASE WHEN TRUNC(KCB.NGAY_VAO_DATE) < ' ||
             V_TUNGAY ||
             '   --v_tungay
                                              THEN ' ||
             V_TUNGAY ||
             '  --v_tungay
                                              ELSE TRUNC(KCB.NGAY_VAO_DATE) END) AS NGAY_VAO_DATE,
                                         (CASE WHEN TRUNC(KCB.NGAY_RA_DATE) > ' ||
             V_DENNGAY ||
             '   --v_denngay
                                              THEN ' ||
             V_DENNGAY || '   --v_denngay
                                              ELSE TRUNC(KCB.NGAY_RA_DATE) END) AS NGAY_RA_DATE
                                  FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB 
                                  JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                                  WHERE KCB.MA_LOAI_KCB IN (2,3) 
                                 ' || V_LOAIHINH || '
                                 ' || V_HUYEN || '
                                 ' || V_COSOKB || '
                                ) b where NGAY_VAO_DATE <  ' ||
             V_DENNGAY ||
             '  +1 --v_denngay+1
                                    and NGAY_RA_DATE >= ' ||
             V_TUNGAY || '  --v_tungay
                        ) c 
                                
                     ) GROUP BY TEN_DON_VI
                    UNION  
                        SELECT TEN_DON_VI, 0 SL_NGOAI_TRU, COUNT(MA_LK) as SL_NOI_TRU
                        FROM (
                            SELECT TEN_DON_VI, MA_LK
                            FROM (
                                SELECT MA_LK, TEN_HIEN_THI TEN_DON_VI
                                FROM CSDLYTE_4210.CHECK_IN_OUT t
                                INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                                WHERE 1 = 1
                                ' || V_LOAIHINH || '
                                ' || V_HUYEN || '
                                ' || V_COSOKB || '
                                AND nvl(t.CHECK_OUT,0) = 0
                                AND t.MA_LOAI_KCB in (2)
                                AND TRUNC(t.CHECK_IN_DATE) <= ' ||
             V_DENNGAY || '
                                UNION
                                SELECT MA_LK, TEN_HIEN_THI TEN_DON_VI
                                FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB t 
                                INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                                WHERE 1 = 1 AND t.MA_LOAI_KCB in (3)
                                ' || V_LOAIHINH || '
                                ' || V_HUYEN || '
                                ' || V_COSOKB || '
                                AND trunc(t.NGAY_RA_DATE) > ' ||
             V_DENNGAY || '
                                AND TRUNC(t.NGAY_VAO_DATE) <= ' ||
             V_DENNGAY || '
                                UNION ALL
                                SELECT MA_LK, TEN_HIEN_THI TEN_DON_VI
                                FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB t 
                                INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                                WHERE 1 = 1 
                                ' || V_LOAIHINH || '
                                 ' || V_HUYEN || '
                                 ' || V_COSOKB || '
                                AND t.MA_LOAI_KCB in (3)
                                AND TRUNC(t.NGAY_RA_DATE) BETWEEN ' ||
             V_TUNGAY || ' AND ' || V_DENNGAY || '
                            )
                        ) GROUP BY TEN_DON_VI
                    ) GROUP BY TEN_DON_VI
                    ORDER BY SL_NOI_TRU DESC
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;
  PROCEDURE PROC_SL_CA_NANG_XIN_VE(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                   P_TUNGAY   NVARCHAR2,
                                   P_DENNGAY  NVARCHAR2,
                                   P_NAM      NUMBER,
                                   P_THANG    NUMBER,
                                   P_QUY      NUMBER,
                                   P_HUYEN    NUMBER,
                                   P_LOAIHINH VARCHAR2,
                                   P_COSOKB   NVARCHAR2,
                                   P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM || ') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := ' TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' || P_NAM || ' ';
      ELSE
        V_IF_CBX := ' 
                         TO_CHAR(NGAY_VAO_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                         AND TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM || ') ';
      ELSE
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM ||
                    ') AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_THANG || ') ';
      END IF;
    
    ELSE
      V_IF_CBX := '(TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD'')) ';
    END IF;
  
    V_SQL := '
                SELECT SUM(SL) SL_BENH_NANG_XIN_VE FROM(      
                    SELECT KCB.MA_CSKCB, COUNT(KCB.MA_LK) AS SL
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                    WHERE 
                   ' || V_IF_CBX || '
                   ' || V_HUYEN || '
                    ' || V_LOAIHINH || '
                    ' || V_COSOKB || '
                    AND KET_QUA_DTRI = 4
                    AND TINH_TRANG_RV = 4
                    GROUP BY KCB.MA_CSKCB
                )
            ';
    --            DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  
  END;
  PROCEDURE PROC_SL_CHUYEN_VIEN(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                P_TUNGAY   NVARCHAR2,
                                P_DENNGAY  NVARCHAR2,
                                P_NAM      NUMBER,
                                P_THANG    NUMBER,
                                P_QUY      NUMBER,
                                P_HUYEN    NUMBER,
                                P_LOAIHINH VARCHAR2,
                                P_COSOKB   NVARCHAR2,
                                P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM || ') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_IF_CBX := ' TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' || P_NAM || ' ';
      ELSE
        V_IF_CBX := ' 
                         TO_CHAR(NGAY_VAO_DATE, ''Q'') = ' ||
                    P_QUY || ' 
                         AND TO_CHAR(NGAY_VAO_DATE, ''YYYY'') = ' ||
                    P_NAM || '
                ';
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM || ') ';
      ELSE
        V_IF_CBX := '(EXTRACT(YEAR FROM KCB.NGAY_VAO_DATE) = ' || P_NAM ||
                    ') AND (EXTRACT(MONTH FROM KCB.NGAY_VAO_DATE) = ' ||
                    P_THANG || ') ';
      END IF;
    
    ELSE
      V_IF_CBX := '(TRUNC(KCB.NGAY_VAO_DATE) BETWEEN TO_DATE(''' ||
                  P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' ||
                  P_DENNGAY || ''', ''YYYY-MM-DD'')) ';
    END IF;
  
    V_SQL := '
                SELECT SUM(SL) SL_CHUYEN_VIEN FROM(      
                    SELECT KCB.MA_CSKCB, COUNT(KCB.MA_LK) AS SL
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                    JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                    WHERE 
                   ' || V_IF_CBX || '
                   ' || V_HUYEN || '
                    ' || V_LOAIHINH || '
                    ' || V_COSOKB || '
                    AND TINH_TRANG_RV = 2 
                    GROUP BY KCB.MA_CSKCB
                )
            ';
    --            DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  
  END;
  PROCEDURE PROC_SL_NOI_TRU_NGOAI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                      P_TUNGAY   NVARCHAR2,
                                      P_DENNGAY  NVARCHAR2,
                                      P_NAM      NUMBER,
                                      P_THANG    NUMBER,
                                      P_QUY      NUMBER,
                                      P_HUYEN    NUMBER,
                                      P_LOAIHINH VARCHAR2,
                                      P_COSOKB   NVARCHAR2,
                                      P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
    V_TUNGAY   VARCHAR2(200);
    V_DENNGAY  VARCHAR2(200);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSIF (P_QUY = 1) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-01-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/01/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-01-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/03/2023
      ELSIF (P_QUY = 2) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-04-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/04/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-04-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/06/2023
      ELSIF (P_QUY = 3) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-07-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/07/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-07-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/09/2023
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-10-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/10/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-10-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/12/2023
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY''), ''MM'') ';
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY'')) ';
      END IF;
    
    ELSE
      V_TUNGAY  := 'TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'') ';
    END IF;
  
    V_SQL := '
                SELECT  SUM(SL_NGOAI_TRU) AS SL_NGOAI_TRU, SUM(SL_NOI_TRU) AS SL_NOI_TRU, 
                            SUM(SL_NGOAI_TRU_TRUNGUONG) SL_NGOAI_TRU_TRUNGUONG, SUM(SL_NOI_TRU_TRUNGUONG) SL_NOI_TRU_TRUNGUONG,
                            SUM(SL_NGOAI_TRU_CAPTINH) AS SL_NGOAI_TRU_CAPTINH, SUM(SL_NOI_TRU_CAPTINH) AS SL_NOI_TRU_CAPTINH,
                            SUM(SL_NGOAI_TRU_CAPHUYEN) AS SL_NGOAI_TRU_CAPHUYEN, SUM(SL_NOI_TRU_CAPHUYEN) AS SL_NOI_TRU_CAPHUYEN,
                            SUM(SL_NGOAI_TRU_CAPXA) AS SL_NGOAI_TRU_CAPXA, SUM(SL_NOI_TRU_CAPXA) AS SL_NOI_TRU_CAPXA 
                   FROM (
                    SELECT   CASE WHEN ( c.MA_LOAI_KCB = 2) THEN songay ELSE 0 END AS SL_NGOAI_TRU,         
                             CASE WHEN ( c.MA_LOAI_KCB = 3) THEN songay ELSE 0 END AS SL_NOI_TRU,
                             
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 1) THEN songay ELSE 0 END AS SL_NGOAI_TRU_TRUNGUONG,         
                             CASE WHEN ( c.MA_LOAI_KCB = 3 AND CAP = 1) THEN songay ELSE 0 END AS SL_NOI_TRU_TRUNGUONG,
                             
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 2) THEN songay ELSE 0 END AS SL_NGOAI_TRU_CAPTINH,         
                             CASE WHEN ( c.MA_LOAI_KCB = 3 AND CAP = 2) THEN songay ELSE 0 END AS SL_NOI_TRU_CAPTINH,
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 3) THEN songay ELSE 0 END AS SL_NGOAI_TRU_CAPHUYEN,         
                             CASE WHEN ( c.MA_LOAI_KCB = 3 AND CAP = 3) THEN songay ELSE 0 END AS SL_NOI_TRU_CAPHUYEN,
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 4) THEN songay ELSE 0 END AS SL_NGOAI_TRU_CAPXA,         
                             CASE WHEN ( c.MA_LOAI_KCB = 3 AND CAP = 4) THEN songay ELSE 0 END AS SL_NOI_TRU_CAPXA
                             from ( 
                             
                            SELECT b.*, (NGAY_RA_DATE - NGAY_VAO_DATE + 1) songay from ( 
                           
                                SELECT  KCB.ID_BENH_NHAN, KCB.HO_TEN, KCB.MA_LOAI_KCB,DV.CAP,
                                (CASE WHEN TRUNC(KCB.NGAY_VAO_DATE) < ' ||
             V_TUNGAY ||
             '  --v_tungay
                                                                    THEN ' ||
             V_TUNGAY ||
             ' --v_tungay
                                                                    ELSE TRUNC(KCB.NGAY_VAO_DATE) END) AS NGAY_VAO_DATE,
                                                             (CASE WHEN TRUNC(KCB.NGAY_RA_DATE) > ' ||
             V_DENNGAY ||
             '  --v_denngay
                                                                    THEN ' ||
             V_DENNGAY ||
             '  --v_denngay
                                                                    ELSE TRUNC(KCB.NGAY_RA_DATE) END) AS NGAY_RA_DATE
                                                          
                                                      FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                                                      JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                                                      WHERE KCB.MA_LOAI_KCB IN (2,3) 
                                                      ' ||
             V_HUYEN || '
                                                     ' ||
             V_LOAIHINH || '
                                                     ' ||
             V_COSOKB || '
                                   
                                    ) b where NGAY_VAO_DATE <  ' ||
             V_DENNGAY ||
             ' +1  --v_denngay+1
                                        and NGAY_RA_DATE >= ' ||
             V_TUNGAY || ' --v_tungay
                         ) c
                                
                     )
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;

  PROCEDURE PROC_SL_NOI_TRU_CUOI_KY(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                                    P_TUNGAY   NVARCHAR2,
                                    P_DENNGAY  NVARCHAR2,
                                    P_NAM      NUMBER,
                                    P_THANG    NUMBER,
                                    P_QUY      NUMBER,
                                    P_HUYEN    NUMBER,
                                    P_LOAIHINH VARCHAR2,
                                    P_COSOKB   NVARCHAR2,
                                    P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
    V_TUNGAY   VARCHAR2(200);
    V_DENNGAY  VARCHAR2(200);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSIF (P_QUY = 1) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-01-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/01/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-01-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/03/2023
      ELSIF (P_QUY = 2) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-04-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/04/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-04-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/06/2023
      ELSIF (P_QUY = 3) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-07-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/07/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-07-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/09/2023
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-10-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/10/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-10-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/12/2023
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSE
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-' || P_THANG ||
                     '-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM || '-' || P_THANG ||
                     ''', ''YYYY-MM'')) ';
      END IF;
    
    ELSE
      V_TUNGAY  := 'TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'') ';
    END IF;
  
    V_SQL := '
            
                SELECT SUM(SL) as SL_NOI_TRU_CUOIKY
                FROM (
                    SELECT COUNT(1) as SL
                    FROM CSDLYTE_4210.CHECK_IN_OUT t
                    INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                    WHERE 1 = 1
                    AND nvl(t.CHECK_OUT,0) = 0
                    AND t.MA_LOAI_KCB in (2)
                    ' || V_HUYEN || '
                    ' || V_COSOKB || '
                    ' || V_LOAIHINH || '
                    AND TRUNC(t.CHECK_IN_DATE) <= ' ||
             V_DENNGAY || '
                    UNION ALL
                    SELECT COUNT(1) as SL  
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB t 
                    INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                    WHERE 1 = 1 AND t.MA_LOAI_KCB in (3)
                    ' || V_HUYEN || '
                    ' || V_COSOKB || '
                    ' || V_LOAIHINH || '
                    AND trunc(t.NGAY_RA_DATE) > ' || V_DENNGAY || '
                    AND TRUNC(t.NGAY_VAO_DATE) <= ' ||
             V_DENNGAY || '
                )
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;

  PROCEDURE PROC_SL_NOI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                            P_TUNGAY   NVARCHAR2,
                            P_DENNGAY  NVARCHAR2,
                            P_NAM      NUMBER,
                            P_THANG    NUMBER,
                            P_QUY      NUMBER,
                            P_HUYEN    NUMBER,
                            P_LOAIHINH VARCHAR2,
                            P_COSOKB   NVARCHAR2,
                            P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
    V_TUNGAY   VARCHAR2(200);
    V_DENNGAY  VARCHAR2(200);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSIF (P_QUY = 1) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-01-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/01/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-01-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/03/2023
      ELSIF (P_QUY = 2) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-04-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/04/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-04-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/06/2023
      ELSIF (P_QUY = 3) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-07-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/07/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-07-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/09/2023
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-10-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/10/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-10-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/12/2023
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSE
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-' || P_THANG ||
                     '-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM || '-' || P_THANG ||
                     ''', ''YYYY-MM'')) ';
      END IF;
    
    ELSE
      V_TUNGAY  := 'TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'') ';
    END IF;
  
    V_SQL := '
                SELECT SUM(MA_LK) as SL_NOI_TRU, 
                       SUM(CASE WHEN CAP=1 THEN MA_LK ELSE 0 END) SL_NOI_TRU_TRUNGUONG,
                       SUM(CASE WHEN CAP=2 THEN MA_LK ELSE 0 END) SL_NOI_TRU_CAPTINH,
                       SUM(CASE WHEN CAP in (3,4) THEN MA_LK ELSE 0 END) SL_NOI_TRU_CAPHUYEN,
                       SUM(CASE WHEN CAP=4 THEN MA_LK ELSE 0 END) SL_NOI_TRU_CAPXA
                FROM (
                    SELECT COUNT(1) as MA_LK, CAP
                    FROM CSDLYTE_4210.CHECK_IN_OUT t
                    INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                    WHERE 1 = 1
                    AND nvl(t.CHECK_OUT,0) = 0
                    AND t.MA_LOAI_KCB in (2)
                    ' || V_HUYEN || '
                    ' || V_COSOKB || '
                    ' || V_LOAIHINH || '
                    AND TRUNC(t.CHECK_IN_DATE) <= ' ||
             V_DENNGAY || '
                    GROUP BY CAP
                    UNION
                    SELECT COUNT(1) as MA_LK, CAP
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB t 
                    INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                    WHERE 1 = 1 AND t.MA_LOAI_KCB in (3)
                    ' || V_HUYEN || '
                    ' || V_COSOKB || '
                    ' || V_LOAIHINH || '
                    AND trunc(t.NGAY_RA_DATE) > ' || V_DENNGAY || '
                    AND TRUNC(t.NGAY_VAO_DATE) <= ' ||
             V_DENNGAY || '
                    GROUP BY CAP
                    UNION ALL
                    SELECT COUNT(1) as MA_LK, CAP
                    FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB t 
                    INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON t.MA_CSKCB = dv.MA_DON_VI
                    WHERE 1 = 1 
                    AND t.MA_LOAI_KCB in (3)
                    ' || V_HUYEN || '
                    ' || V_COSOKB || '
                    ' || V_LOAIHINH || '
                    AND TRUNC(t.NGAY_RA_DATE) BETWEEN ' ||
             V_TUNGAY || ' AND ' || V_DENNGAY || '
                    GROUP BY CAP
                )
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;
  PROCEDURE PROC_SL_NGOAI_TRU(P_LOAI_CBX NUMBER, -- 0 NĂM , 1 Qúy, 2 tháng, 3 Ngày
                              P_TUNGAY   NVARCHAR2,
                              P_DENNGAY  NVARCHAR2,
                              P_NAM      NUMBER,
                              P_THANG    NUMBER,
                              P_QUY      NUMBER,
                              P_HUYEN    NUMBER,
                              P_LOAIHINH VARCHAR2,
                              P_COSOKB   NVARCHAR2,
                              P_RS       OUT SYS_REFCURSOR) IS
    V_SQL      VARCHAR2(10000);
    V_IF_CBX   VARCHAR2(2000);
    V_HUYEN    VARCHAR2(2000);
    V_LOAIHINH VARCHAR2(200);
    V_COSOKB   VARCHAR2(2000);
    V_TUNGAY   VARCHAR2(200);
    V_DENNGAY  VARCHAR2(200);
  BEGIN
    IF (P_HUYEN = 0) THEN
      V_HUYEN := '';
    ELSE
      V_HUYEN := 'AND DV.MA_HUYEN = ' || P_HUYEN || '';
    END IF;
    IF (P_LOAIHINH = '2') THEN
      V_LOAIHINH := '';
    ELSE
      V_LOAIHINH := 'AND DV.TU_NHAN = ''' || P_LOAIHINH || '''';
    END IF;
    IF (P_COSOKB = '0') THEN
      V_COSOKB := '';
    ELSE
      V_COSOKB := 'AND DV.MA_DON_VI IN( ' || P_COSOKB || ')';
    END IF;
    IF (P_LOAI_CBX = 0) THEN
      V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
    ELSIF (P_LOAI_CBX = 1) THEN
      IF (P_QUY = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSIF (P_QUY = 1) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-01-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/01/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-01-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/03/2023
      ELSIF (P_QUY = 2) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-04-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/04/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-04-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/06/2023
      ELSIF (P_QUY = 3) THEN
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-07-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/07/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-07-30'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 30/09/2023
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_NAM ||
                     '-10-01'', ''YYYY-MM-DD''), ''Q'') '; --NGÀY 01/10/2023
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_NAM ||
                     '-10-31'',''YYYY-MM-DD'') + INTERVAL ''2'' MONTH) '; --NGÀY 31/12/2023
      END IF;
    ELSIF (P_LOAI_CBX = 2) THEN
      IF (P_THANG = 0) THEN
        V_TUNGAY  := 'TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') ';
        V_DENNGAY := 'TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'') ';
      ELSE
        V_TUNGAY  := 'TRUNC(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY''), ''MM'') ';
        V_DENNGAY := 'LAST_DAY(TO_DATE(''' || P_THANG || '/' || P_NAM ||
                     ''', ''MM/YYYY'')) ';
      END IF;
    
    ELSE
      V_TUNGAY  := 'TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') ';
      V_DENNGAY := 'TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'') ';
    END IF;
  
    V_SQL := '
                SELECT  SUM(SL_NGOAI_TRU) AS SL_NGOAI_TRU,
                            SUM(SL_NGOAI_TRU_TRUNGUONG) SL_NGOAI_TRU_TRUNGUONG,
                            SUM(SL_NGOAI_TRU_CAPTINH) AS SL_NGOAI_TRU_CAPTINH,
                            SUM(SL_NGOAI_TRU_CAPHUYEN) AS SL_NGOAI_TRU_CAPHUYEN,
                            SUM(SL_NGOAI_TRU_CAPXA) AS SL_NGOAI_TRU_CAPXA
                   FROM (
                    SELECT   CASE WHEN ( c.MA_LOAI_KCB = 2) THEN songay ELSE 0 END AS SL_NGOAI_TRU,       
                             
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 1) THEN songay ELSE 0 END AS SL_NGOAI_TRU_TRUNGUONG,         
                             
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 2) THEN songay ELSE 0 END AS SL_NGOAI_TRU_CAPTINH,       
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 3) THEN songay ELSE 0 END AS SL_NGOAI_TRU_CAPHUYEN,        
                             CASE WHEN ( c.MA_LOAI_KCB = 2 AND CAP = 4) THEN songay ELSE 0 END AS SL_NGOAI_TRU_CAPXA    
                             from ( 
                             
                            SELECT b.*, (NGAY_RA_DATE - NGAY_VAO_DATE + 1) songay from ( 
                           
                                SELECT  KCB.ID_BENH_NHAN, KCB.HO_TEN, KCB.MA_LOAI_KCB,DV.CAP,
                                (CASE WHEN TRUNC(KCB.NGAY_VAO_DATE) < ' ||
             V_TUNGAY ||
             '  --v_tungay
                                                                    THEN ' ||
             V_TUNGAY ||
             ' --v_tungay
                                                                    ELSE TRUNC(KCB.NGAY_VAO_DATE) END) AS NGAY_VAO_DATE,
                                                             (CASE WHEN TRUNC(KCB.NGAY_RA_DATE) > ' ||
             V_DENNGAY ||
             '  --v_denngay
                                                                    THEN ' ||
             V_DENNGAY ||
             '  --v_denngay
                                                                    ELSE TRUNC(KCB.NGAY_RA_DATE) END) AS NGAY_RA_DATE
                                                          
                                                      FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB KCB
                                                      JOIN CSDLYTE_DANHMUC.DM_DONVI DV ON KCB.MA_CSKCB = DV.MA_DON_VI
                                                      WHERE KCB.MA_LOAI_KCB IN (2,3) 
                                                      ' ||
             V_HUYEN || '
                                                     ' ||
             V_LOAIHINH || '
                                                     ' ||
             V_COSOKB || '
                                   
                                    ) b where NGAY_VAO_DATE <  ' ||
             V_DENNGAY ||
             ' +1  --v_denngay+1
                                        and NGAY_RA_DATE >= ' ||
             V_TUNGAY || ' --v_tungay
                         ) c
                                
                     )
            ';
    DBMS_OUTPUT.PUT_LINE(V_SQL);
    OPEN P_RS FOR V_SQL;
  END;
END PKG_IOC_THONGTIN_CHUNG_1;
/
