﻿CREATE OR REPLACE PACKAGE                                                 CSDLYTE_SYT.PKG_BENH_TRUYEN_NHIEM AS 

  ----------------------------- PROC_SL_BENH_TIEU_CHAY -----------------------------
  PROCEDURE PROC_SL_BENH_TIEU_CHAY (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  );

END PKG_BENH_TRUYEN_NHIEM;
/
create or replace PACKAGE BODY                                                                         PKG_BENH_TRUYEN_NHIEM AS

  PROCEDURE PROC_SL_BENH_TIEU_CHAY (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_SL_BENH_TIEU_CHAY
    OPEN P_RS FOR
    SELECT H.ma_huyen, H.ten_huyen, SUM(A.SL_TIEU_CHAY) SL_TIEU_CHAY
        FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_TIEU_CHAY A
        LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON A.HUYEN = H.MA_HUYEN
        WHERE   (P_HUYEN = 0 OR A.HUYEN = P_HUYEN)
                  AND NAM = P_NAM
                  AND (
                    P_LOAI_CBX = 0
                    OR (P_LOAI_CBX = 1 AND (P_QUY  IS NULL OR P_QUY = 0))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND THANG IN (1, 2, 3))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND THANG IN (4, 5, 6))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND THANG IN (7, 8, 9))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND THANG IN (10, 11, 12))
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND THANG = P_THANG)
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NULL)
                  )
      GROUP BY H.ma_huyen, H.ten_huyen;
  END PROC_SL_BENH_TIEU_CHAY;

END PKG_BENH_TRUYEN_NHIEM;
/
