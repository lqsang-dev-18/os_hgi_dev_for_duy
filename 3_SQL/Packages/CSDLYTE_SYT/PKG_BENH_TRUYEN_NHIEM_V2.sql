CREATE OR REPLACE PACKAGE                                                                                                                                                                                                 PKG_BENH_TRUYEN_NHIEM AS 

  ----------------------------- PROC_SL_BENH_TIEU_CHAY -----------------------------
  PROCEDURE PROC_SL_BENH_TIEU_CHAY (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  );
  ----------------------------- PROC_TK_SL_HUYET_THANH_HOC -----------------------------
  PROCEDURE PROC_TK_SL_HUYET_THANH_HOC (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER DEFAULT NULL,
    P_RS            OUT SYS_REFCURSOR
  );
  
  ----------------------------- PROC_CT_PC_SOT_XUAT_HUYET -----------------------------
  PROCEDURE PROC_CT_PC_SOT_XUAT_HUYET (
    P_NAM           IN NUMBER,
    P_TUTHANG       IN NUMBER,
    P_DENTHANG      IN NUMBER,
    P_HUYEN         IN NUMBER DEFAULT 0,
    P_RS            OUT SYS_REFCURSOR
  );
  
  ----------------------------- PROC_SL_VIEM_GAN_C_DO_VIRUS -----------------------------
  PROCEDURE PROC_SL_VIEM_GAN_C_DO_VIRUS (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  );
   ----------------------------- PROC_SL_QUAI_BI -----------------------------
  PROCEDURE PROC_SL_QUAI_BI (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN VARCHAR2 DEFAULT 0,
    P_RS            OUT SYS_REFCURSOR
  );
  ----------------------------- PROC_SL_RUBELLA -----------------------------
  PROCEDURE PROC_SL_RUBELLA (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN VARCHAR2 DEFAULT 0,
    P_RS            OUT SYS_REFCURSOR
  );
  ----------------------------- PROC_SL_BENH_MERS_COV -----------------------------
  PROCEDURE PROC_SL_BENH_MERS_COV (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN VARCHAR2 DEFAULT 0,
    P_RS            OUT SYS_REFCURSOR
  );
  ----------------------------- PROC_SL_BENH_MERS_COV -----------------------------
  PROCEDURE CDC_BENH_TRUYEN_NHIEM_SOT_PHATBAN_NGHISOI (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER DEFAULT 0,
    P_RS            OUT SYS_REFCURSOR
  );
  ----------------------------- PROC_SL_BENH_COVID -----------------------------
  PROCEDURE PROC_SL_BENH_COVID (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER DEFAULT 0,
    P_RS            OUT SYS_REFCURSOR
  );
END PKG_BENH_TRUYEN_NHIEM;
/


CREATE OR REPLACE PACKAGE BODY                                                                                                                                                                                                                                                                                                                                                                         PKG_BENH_TRUYEN_NHIEM AS

  PROCEDURE PROC_SL_BENH_TIEU_CHAY (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_SL_BENH_TIEU_CHAY
    OPEN P_RS FOR
    SELECT H.ma_huyen, H.ten_huyen, A.sl_tieu_chay
        FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_TIEU_CHAY A
        LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON A.HUYEN = H.MA_HUYEN
        WHERE   (P_HUYEN = 0 OR A.HUYEN = P_HUYEN)
                  AND NAM = P_NAM
                  AND (
                    (P_LOAI_CBX = 0)
                    OR (P_LOAI_CBX = 1 AND P_QUY IS NULL)
                    OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND THANG IN (1, 2, 3))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND THANG IN (4, 5, 6))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND THANG IN (7, 8, 9))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND THANG IN (10, 11, 12))
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND THANG = P_THANG)
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NULL)
                  )
      order by A.THANG;
  END PROC_SL_BENH_TIEU_CHAY;
  ----------------------------- PROC_TK_SL_HUYET_THANH_HOC -----------------------------
PROCEDURE PROC_TK_SL_HUYET_THANH_HOC (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER DEFAULT NULL,
    P_RS            OUT SYS_REFCURSOR
) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_TK_SL_HUYET_THANH_HOC
    OPEN P_RS FOR
    SELECT ND.TEN_LOAI_XN, NULLIF(0, TH.THUC_HIEN) as THUC_HIEN, (SELECT NULLIF(0,SUM(THUC_HIEN)) FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_XET_NGHIEM_KHAC WHERE THUC_HIEN = TH.THUC_HIEN) CONG_DON
        FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_XET_NGHIEM_KHAC TH
        LEFT JOIN CSDLYTE_SYT.DM_CDC_BENH_TRUYEN_NHIEM_LOAI_XET_NGHIEM_KHAC ND ON ND.ID_LOAI_XN = TH.ID_LOAI_XN
        WHERE   NAM = P_NAM
                AND (ND.ID_LOAI_XN = 1)
                AND (
                    (P_LOAI_CBX = 0)
                    OR (P_LOAI_CBX = 1 AND P_QUY IS NULL)
                    OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND THANG IN (1, 2, 3))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND THANG IN (4, 5, 6))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND THANG IN (7, 8, 9))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND THANG IN (10, 11, 12))
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND THANG = P_THANG)
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NULL)
                  )
        order by TH.THANG ;
  END PROC_TK_SL_HUYET_THANH_HOC;
  
    ----------------------------- PROC_CT_PC_SOT_XUAT_HUYET -----------------------------
PROCEDURE PROC_CT_PC_SOT_XUAT_HUYET (
    P_NAM           IN NUMBER,
    P_TUTHANG       IN NUMBER,
    P_DENTHANG      IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_CT_PC_SOT_XUAT_HUYET
    OPEN P_RS FOR
    SELECT H.TEN_HUYEN, ND.TEN_NOI_DUNG AS noi_dung,CT.CHI_TIEU,SUM(TH.THUC_HIEN) AS thuc_hien, CD.CONG_DON,
    ROUND((CD.CONG_DON / CT.CHI_TIEU) * 100, 2) as ty_le_dat
    FROM CSDLYTE_SYT.CDC_THUC_HIEN_PHONG_CHONG_SXH TH
    LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON H.MA_HUYEN = TH.HUYEN
    LEFT JOIN CSDLYTE_SYT.DM_CHI_TIEU_PHONG_CHONG_SXH ND ON ND.ID_NOI_DUNG = TH.ID_NOI_DUNG
    
    LEFT JOIN (
    SELECT a.ID as idx, a.CHI_TIEU
    FROM CSDLYTE_SYT.CDC_CHI_TIEU_BENH_TRUYEN_NHIEM_SXH a
    WHERE a.NAM = P_NAM
    ) CT ON CT.idx = ND.ID_NOI_DUNG
    
    LEFT JOIN (
    SELECT SUM(b.THUC_HIEN) as CONG_DON, b.HUYEN, b.ID_NOI_DUNG
    FROM CSDLYTE_SYT.CDC_THUC_HIEN_PHONG_CHONG_SXH b
    WHERE b.NAM = P_NAM 
    AND b.THANG >= 1
    AND(P_DENTHANG IS NULL OR (P_DENTHANG IS NOT NULL AND b.THANG <= P_DENTHANG))
    GROUP BY b.HUYEN, b.ID_NOI_DUNG
    ) CD ON CD.HUYEN = TH.HUYEN and CD.ID_NOI_DUNG = TH.ID_NOI_DUNG
    
    WHERE (P_HUYEN = 0 OR (P_HUYEN <> 0 AND H.MA_HUYEN = P_HUYEN))
    AND TH.NAM = P_NAM
    AND (P_TUTHANG IS NULL OR (P_TUTHANG IS NOT NULL AND TH.THANG >= P_TUTHANG))
    AND (P_DENTHANG IS NULL OR (P_DENTHANG IS NOT NULL AND TH.THANG <= P_DENTHANG))
    GROUP BY H.TEN_HUYEN, ND.TEN_NOI_DUNG,CT.CHI_TIEU,CD.CONG_DON ;
  END PROC_CT_PC_SOT_XUAT_HUYET;
  
  ----------------------------- PROC_SL_VIEM_GAN_C_DO_VIRUS -----------------------------
  PROCEDURE PROC_SL_VIEM_GAN_C_DO_VIRUS (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_SL_BENH_TIEU_CHAY
    OPEN P_RS FOR
    SELECT H.MA_HUYEN AS ma_huyen, H.TEN_HUYEN AS ten_huyen, SUM(A.SO_CA_MAC) AS sl_mac
        FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_VIEM_GAN_C_DO_VIRUS A
        LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON A.MA_HUYEN = H.MA_HUYEN
        WHERE   (P_HUYEN = 0 OR A.MA_HUYEN = P_HUYEN)
                  AND NAM = P_NAM
                  AND (
                    (P_LOAI_CBX = 0)
                    OR (P_LOAI_CBX = 1 AND P_QUY IS NULL)
                    OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND THANG IN (1, 2, 3))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND THANG IN (4, 5, 6))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND THANG IN (7, 8, 9))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND THANG IN (10, 11, 12))
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND THANG = P_THANG)
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NULL)
                  )
        GROUP BY H.MA_HUYEN, H.TEN_HUYEN
        ORDER BY H.MA_HUYEN;
  END PROC_SL_VIEM_GAN_C_DO_VIRUS;

  ----------------------------- PROC_SL_QUAI_BI -----------------------------
  PROCEDURE PROC_SL_QUAI_BI (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN VARCHAR2,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_SL_QUAI_BI
    OPEN P_RS FOR
    SELECT H.MA_HUYEN, H.TEN_HUYEN, SUM(a.SO_CA_MAC) as sl_mac
    FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_QUAI_BI A
    LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON H.MA_HUYEN = A.MA_HUYEN
    WHERE (P_HUYEN = 0 OR (P_HUYEN <> 0 AND H.MA_HUYEN = P_HUYEN))
    AND (
    (P_LOAI_CBX = 0 AND A.NAM = P_NAM) OR
    (P_LOAI_CBX = 2 AND (P_THANG IS NULL OR(P_THANG IS NOT NULL AND A.THANG = P_THANG))) OR
    (P_LOAI_CBX = 1 AND P_QUY = 0) OR
    (P_LOAI_CBX = 1 AND P_QUY = 1 AND A.THANG IN (1,2,3)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 2 AND A.THANG IN (4,5,6)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 3 AND A.THANG IN (7,8,9)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 4 AND A.THANG IN (10,11,12))
    )
    GROUP BY H.MA_HUYEN, H.TEN_HUYEN;
  END PROC_SL_QUAI_BI;
  
  ----------------------------- PROC_SL_RUBELLA -----------------------------
  PROCEDURE PROC_SL_RUBELLA (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN VARCHAR2,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_SL_RUBELLA
    OPEN P_RS FOR
    SELECT H.MA_HUYEN, H.TEN_HUYEN, SUM(a.SO_CA_MAC) as sl_mac
    FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_RUBELLA A
    LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON H.MA_HUYEN = A.MA_HUYEN
    WHERE (P_HUYEN = 0 OR (P_HUYEN <> 0 AND H.MA_HUYEN = P_HUYEN))
    AND (
    (P_LOAI_CBX = 0 AND A.NAM = P_NAM) OR
    (P_LOAI_CBX = 2 AND (P_THANG IS NULL OR(P_THANG IS NOT NULL AND A.THANG = P_THANG))) OR
    (P_LOAI_CBX = 1 AND P_QUY = 0) OR
    (P_LOAI_CBX = 1 AND P_QUY = 1 AND A.THANG IN (1,2,3)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 2 AND A.THANG IN (4,5,6)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 3 AND A.THANG IN (7,8,9)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 4 AND A.THANG IN (10,11,12))
    )
    GROUP BY H.MA_HUYEN, H.TEN_HUYEN;
  END PROC_SL_RUBELLA;
   ----------------------------- PROC_SL_BENH_MERS_COV -----------------------------
  PROCEDURE PROC_SL_BENH_MERS_COV (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN VARCHAR2,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_SL_BENH_MERS_COV
    OPEN P_RS FOR
    SELECT H.MA_HUYEN, H.TEN_HUYEN, SUM(a.SL_MERS_COV) as sl_mac
    FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_MERS_COV A
    LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON H.MA_HUYEN = A.HUYEN
    WHERE (P_HUYEN = 0 OR (P_HUYEN <> 0 AND H.MA_HUYEN = P_HUYEN))
    AND (
    (P_LOAI_CBX = 0 AND A.NAM = P_NAM) OR
    (P_LOAI_CBX = 2 AND (P_THANG IS NULL OR(P_THANG IS NOT NULL AND A.THANG = P_THANG))) OR
    (P_LOAI_CBX = 1 AND P_QUY = 0) OR
    (P_LOAI_CBX = 1 AND P_QUY = 1 AND A.THANG IN (1,2,3)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 2 AND A.THANG IN (4,5,6)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 3 AND A.THANG IN (7,8,9)) OR
    (P_LOAI_CBX = 1 AND P_QUY = 4 AND A.THANG IN (10,11,12))
    )
    GROUP BY H.MA_HUYEN, H.TEN_HUYEN;
  END PROC_SL_BENH_MERS_COV;
  ----------------------------- CDC_BENH_TRUYEN_NHIEM_SOT_PHATBAN_NGHISOI -----------------------------
  PROCEDURE CDC_BENH_TRUYEN_NHIEM_SOT_PHATBAN_NGHISOI (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    OPEN P_RS FOR
    
    SELECT H.MA_HUYEN AS ma_huyen, H.TEN_HUYEN AS ten_huyen, SUM(A.SL_SOT_PHATBAN_NGHISOI) AS sl_sot_phatban_nghisoi
        FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_SOT_PHATBAN_NGHISOI A
        LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON A.HUYEN = H.MA_HUYEN
        WHERE   (P_HUYEN = 0 OR A.HUYEN = P_HUYEN)
                  AND NAM = P_NAM
                  AND (
                    (P_LOAI_CBX = 0)
                    OR (P_LOAI_CBX = 1 AND P_QUY IS NULL)
                    OR (P_LOAI_CBX = 1 AND P_QUY = 0 AND THANG IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND THANG IN (1, 2, 3))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND THANG IN (4, 5, 6))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND THANG IN (7, 8, 9))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND THANG IN (10, 11, 12))
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND THANG = P_THANG)
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NULL)
                  )
        GROUP BY H.MA_HUYEN, H.TEN_HUYEN
        ORDER BY H.MA_HUYEN;
  END CDC_BENH_TRUYEN_NHIEM_SOT_PHATBAN_NGHISOI;
   ----------------------------- PROC_SL_BENH_COVID -----------------------------
  PROCEDURE PROC_SL_BENH_COVID (
    P_LOAI_CBX      IN NUMBER,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
    -- TODO: Implementation required for PROCEDURE PKG_BENH_TRUYEN_NHIEM.PROC_SL_BENH_TIEU_CHAY
    OPEN P_RS FOR
    SELECT H.MA_HUYEN AS ma_huyen, H.TEN_HUYEN AS ten_huyen, SUM(A.SL_MAC) AS sl_mac, SUM(A.SL_TU_VONG) AS sl_tu_vong
        FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_COVID A
        LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON A.HUYEN = H.MA_HUYEN
        WHERE   (P_HUYEN = 0 OR A.HUYEN = P_HUYEN)
                  AND NAM = P_NAM
                  AND (
                    (P_LOAI_CBX = 0)
                    OR (P_LOAI_CBX = 1 AND P_QUY IS NULL)
                    OR (P_LOAI_CBX = 1 AND P_QUY = 0 AND THANG IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND THANG IN (1, 2, 3))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND THANG IN (4, 5, 6))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND THANG IN (7, 8, 9))
                    OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND THANG IN (10, 11, 12))
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND THANG = P_THANG)
                    OR (P_LOAI_CBX = 2 AND P_THANG IS NULL)
                  )
        GROUP BY H.MA_HUYEN, H.TEN_HUYEN
        ORDER BY H.MA_HUYEN;
  END PROC_SL_BENH_COVID;
  
END PKG_BENH_TRUYEN_NHIEM;
/
