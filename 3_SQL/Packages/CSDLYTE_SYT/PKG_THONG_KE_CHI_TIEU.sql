CREATE OR REPLACE PACKAGE             PKG_THONG_KE_CHI_TIEU AS 

  ----------------------------- PROC_TK_CHI_TIEU_PC_TIEU_CHAY -----------------------------
  PROCEDURE PROC_TK_CHI_TIEU_PC_TIEU_CHAY (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  );

END PKG_THONG_KE_CHI_TIEU;
/


CREATE OR REPLACE PACKAGE BODY                         PKG_THONG_KE_CHI_TIEU AS

  PROCEDURE PROC_TK_CHI_TIEU_PC_TIEU_CHAY (
    P_LOAI_CBX      IN NUMBER DEFAULT 0,
    P_NAM           IN NUMBER,
    P_QUY           IN NUMBER DEFAULT 0,
    P_THANG         IN NUMBER,
    P_HUYEN         IN NUMBER,
    P_RS            OUT SYS_REFCURSOR
  ) AS
  BEGIN
  
  OPEN P_RS FOR
    SELECT H.TEN_HUYEN, ND.NOI_DUNG AS noi_dung, CT.CHI_TIEU, SUM(TH.THUC_HIEN) AS thuc_hien, CD.CONG_DON,
    ROUND((CD.CONG_DON / CT.CHI_TIEU) * 100, 2) as ty_le_dat
    FROM CSDLYTE_SYT.CDC_THUC_HIEN_PHONG_CHONG_TIEU_CHAY TH
    LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H ON H.MA_HUYEN = TH.HUYEN
    LEFT JOIN CSDLYTE_SYT.DM_CDC_BENH_TRUYEN_NHIEM_PC_TIEU_CHAY ND ON ND.ID = TH.ID_NOI_DUNG
    
    LEFT JOIN (
    SELECT a.ID_NOI_DUNG as idx, a.CHI_TIEU
    FROM CSDLYTE_SYT.CDC_CHI_TIEU_PHONG_CHONG_TIEU_CHAY a
    WHERE a.NAM = P_NAM
    ) CT ON CT.idx = ND.ID
    
    LEFT JOIN (
    SELECT SUM(b.THUC_HIEN) as CONG_DON, b.HUYEN, b.ID_NOI_DUNG
    FROM CSDLYTE_SYT.CDC_THUC_HIEN_PHONG_CHONG_TIEU_CHAY b
    WHERE b.NAM = P_NAM
    AND b.THANG >= 1
    AND ((P_LOAI_CBX = 0) 
            OR (P_LOAI_CBX = 1 AND P_QUY IS NULL)
            OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND b.THANG IN (1, 2, 3))
            OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND b.THANG IN (1, 2, 3, 4, 5, 6))
            OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND b.THANG IN (1, 2, 3, 4, 5, 6, 7, 8, 9))
            OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND b.THANG IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
            OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND b.THANG <= P_THANG)
            OR (P_LOAI_CBX = 2 AND P_THANG IS NULL))
    GROUP BY b.HUYEN, b.ID_NOI_DUNG
    ) CD ON CD.HUYEN = TH.HUYEN and CD.ID_NOI_DUNG = TH.ID_NOI_DUNG
    
    WHERE H.MA_HUYEN = P_HUYEN
    AND TH.NAM = P_NAM
    AND ((P_LOAI_CBX = 0) 
            OR (P_LOAI_CBX = 1 AND P_QUY IS NULL)
            OR (P_LOAI_CBX = 1 AND P_QUY = 1 AND TH.THANG IN (1, 2, 3))
            OR (P_LOAI_CBX = 1 AND P_QUY = 2 AND TH.THANG IN (1, 2, 3, 4, 5, 6))
            OR (P_LOAI_CBX = 1 AND P_QUY = 3 AND TH.THANG IN (1, 2, 3, 4, 5, 6, 7, 8, 9))
            OR (P_LOAI_CBX = 1 AND P_QUY = 4 AND TH.THANG IN (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))
            OR (P_LOAI_CBX = 2 AND P_THANG IS NOT NULL AND TH.THANG <= P_THANG)
            OR (P_LOAI_CBX = 2 AND P_THANG IS NULL))
    GROUP BY H.TEN_HUYEN, ND.NOI_DUNG,CT.CHI_TIEU,CD.CONG_DON
    ORDER BY H.TEN_HUYEN;
    
  END PROC_TK_CHI_TIEU_PC_TIEU_CHAY;

END PKG_THONG_KE_CHI_TIEU;
/
