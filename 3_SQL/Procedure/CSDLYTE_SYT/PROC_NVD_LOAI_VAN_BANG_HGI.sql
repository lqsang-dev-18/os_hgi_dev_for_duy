create or replace PROCEDURE                         PROC_NVD_LOAI_VAN_BANG_HGI (
  P_MA_TINH IN NUMBER 
, P_MA_HUYEN IN NUMBER 
, P_MA_XA IN NUMBER 
, P_LOAI_CBX IN NUMBER 
, P_NAM IN NUMBER 
, P_QUY IN NUMBER 
, P_THANG IN NUMBER 
, P_TUNGAY IN VARCHAR2 
, P_DENNGAY IN VARCHAR2 
, P_RS OUT SYS_REFCURSOR
) AS 
BEGIN
    OPEN P_RS FOR

    SELECT 
        CASE T.LOAI_VAN_BANG WHEN '1' THEN 'D??c s? ??i h?c'
                            WHEN '2' THEN 'D??c s? cao ??ng'
                            WHEN '3' THEN 'D??c s? trung h?c'
                            WHEN '4' THEN 'C? nh�n th?c h�nh d??c'
                            WHEN '5' THEN 'YHCT'
                            ELSE 'Kh�c' END as LOAI_VAN_BANG,
        COUNT(1) AS SO_LUONG
    FROM CSDLYTE_SYT.NVD_QUANLY_CAP_CCHN_HGI T
    WHERE T.MA_TINH_CO_SO = P_MA_TINH
         AND (P_MA_HUYEN is null OR (T.MA_HUYEN_CO_SO is not null AND T.MA_HUYEN_CO_SO = P_MA_HUYEN)) 
         AND (P_MA_XA is null OR (T.MA_XA_CO_SO is not null AND T.MA_XA_CO_SO = P_MA_XA)) 
         AND
         (
            (P_LOAI_CBX = 0 AND TO_NUMBER(SUBSTR(NGAY_CAP_CCHN, 7, 4)) = P_NAM) OR
            (P_LOAI_CBX = 1 AND TO_NUMBER(SUBSTR(NGAY_CAP_CCHN, 7, 4)) = P_NAM AND TO_NUMBER(SUBSTR(NGAY_CAP_CCHN, 4, 2)) = P_THANG) OR
            (P_LOAI_CBX = 2 AND P_QUY = 1 AND TO_NUMBER(SUBSTR(NGAY_CAP_CCHN, 4, 2)) IN (1, 2, 3)) OR
            (P_LOAI_CBX = 2 AND P_QUY = 2 AND TO_NUMBER(SUBSTR(NGAY_CAP_CCHN, 4, 2)) IN (4, 5, 6)) OR
            (P_LOAI_CBX = 2 AND P_QUY = 3 AND TO_NUMBER(SUBSTR(NGAY_CAP_CCHN, 4, 2)) IN (7, 8, 9)) OR
            (P_LOAI_CBX = 2 AND P_QUY = 4 AND TO_NUMBER(SUBSTR(NGAY_CAP_CCHN, 4, 2)) IN (10, 11, 12)) OR
            (P_LOAI_CBX = 3 AND T.NGAY_CAP_CCHN BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_DENNGAY,'dd-mm-yyyy'))
         )
    GROUP BY T.LOAI_VAN_BANG
    ORDER BY SO_LUONG DESC;

END PROC_NVD_LOAI_VAN_BANG_HGI;