create or replace PROCEDURE             PROC_NVD_DANHSACH_CAP_GCN_HGI(
  P_MA_TINH IN NUMBER 
, P_MA_HUYEN IN NUMBER 
, P_MA_XA IN NUMBER 
, P_LOAI_CBX IN NUMBER 
, P_NAM IN NUMBER 
, P_QUY IN NUMBER 
, P_THANG IN NUMBER 
, P_TUNGAY IN VARCHAR2 
, P_DENNGAY IN VARCHAR2 
, P_RS OUT SYS_REFCURSOR
)AS 
BEGIN
        OPEN P_RS FOR
        SELECT UPPER(T.TEN_CO_SO) TEN_CO_SO,
               (CASE T.HINH_THUC_TO_CHUC
                 WHEN 1 THEN
                  'Qu?y thu?c'
                 WHEN 2 THEN
                  'Nh� thu?c'
                 WHEN 3 THEN
                  'Doanh nghi?p thu?c'
                 WHEN 4 THEN
                  'C? s? chuy�n b�n l? thu?c d??c li?u, c? truy?n'
                 WHEN 5 THEN
                  'T? thu?c tr?m y t?'
                 ELSE
                  ''
               END) HINH_THUC_TO_CHUC,
               T.SO_GCN_DKKD,
               T.SO_GIAYPHEP_GPP,
               T.HO_TEN,
               (CASE T.LOAI_CAP_GCN
                 WHEN 'M' THEN
                  'C?p m?i'
                 WHEN 'L1' THEN
                  'C?p l?i l?n 1'
                 WHEN 'L2' THEN
                  'C?p l?i l?n 2'
                 WHEN 'L3' THEN
                  'C?p l?i l?n 3'
                 WHEN 'L4' THEN
                  'C?p l?i l?n 4'
                 ELSE
                  ''
               END) LOAI_CAP_GCN,
               TO_CHAR(T.NGAY_CAP_QD, 'DD/MM/YYYY') NGAY_CAP_QD
          FROM CSDLYTE_SYT.NVD_QUANLY_CAP_GCN_HGI T
         WHERE T.MA_TINH_CO_SO = P_MA_TINH
             AND (P_MA_HUYEN is null OR (T.MA_HUYEN_CO_SO is not null AND T.MA_HUYEN_CO_SO = P_MA_HUYEN)) 
             AND (P_MA_XA is null OR (T.MA_XA_CO_SO is not null AND T.MA_XA_CO_SO = P_MA_XA)) 
             AND
             (
                (P_LOAI_CBX = 0 AND EXTRACT(YEAR FROM T.NGAY_CAP_QD) = P_NAM) OR
                (P_LOAI_CBX = 1 AND EXTRACT(YEAR FROM T.NGAY_CAP_QD) = P_NAM AND EXTRACT(MONTH FROM NGAY_CAP_QD) = P_THANG) OR
                (P_LOAI_CBX = 2 AND P_QUY = 1 AND EXTRACT(MONTH FROM T.NGAY_CAP_QD) IN (1, 2, 3)) OR
                (P_LOAI_CBX = 2 AND P_QUY = 2 AND EXTRACT(MONTH FROM T.NGAY_CAP_QD) IN (4, 5, 6)) OR
                (P_LOAI_CBX = 2 AND P_QUY = 3 AND EXTRACT(MONTH FROM T.NGAY_CAP_QD) IN (7, 8, 9)) OR
                (P_LOAI_CBX = 2 AND P_QUY = 4 AND EXTRACT(MONTH FROM T.NGAY_CAP_QD) IN (10, 11, 12)) OR
                (P_LOAI_CBX = 3 AND T.NGAY_CAP_QD BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_TUNGAY,'dd-mm-yyyy'))
             );
END PROC_NVD_DANHSACH_CAP_GCN_HGI;