create or replace PROCEDURE             PROC_GET_TTR_LINHVUC_TK 
(
  P_MA_TINH IN NUMBER 
, P_MA_HUYEN IN NUMBER 
, P_MA_XA IN NUMBER 
, P_LOAI_CBX IN NUMBER 
, P_NAM IN NUMBER 
, P_QUY IN NUMBER 
, P_THANG IN NUMBER 
, P_TUNGAY IN VARCHAR2 
, P_DENNGAY IN VARCHAR2 
, P_RS OUT SYS_REFCURSOR
) AS
BEGIN
  OPEN P_RS FOR
  
  SELECT 
    P_LOAI_CBX AS LOAI_CUNG_KY,
    CASE P_LOAI_CBX
            WHEN 0 THEN 'N?m'
            WHEN 1 THEN 'Th�ng'
            WHEN 2 THEN 'Qu�'
            ELSE P_TUNGAY || '-' || P_DENNGAY
        END AS KY,
    SL_KY.KY,
    SL_KY.Y_VIPHAM AS Y,
    SL_KY.DUOC_VIPHAM AS DUOC,
    SL_CUNGKY.CUNGKY,
    SL_CUNGKY.Y_VIPHAM AS Y_CUNGKY,
    SL_CUNGKY.DUOC_VIPHAM AS DUOC_CUNGKY
  FROM (SELECT 
            CASE P_LOAI_CBX
                    WHEN 0 THEN 'N?m ' || P_NAM
                    WHEN 1 THEN 'Th�ng ' || P_THANG|| '/' || P_NAM
                    WHEN 2 THEN 'Qu� ' || P_QUY  || '/' || P_NAM
                ELSE P_TUNGAY || '-' || P_DENNGAY
                END AS KY,
            SUM(CASE 
                    WHEN K.CO_SO_NGANH_Y_VI_PHAM = 0 THEN 0  
                    ELSE K.CO_SO_NGANH_Y_VI_PHAM 
                END) AS Y_VIPHAM,
            SUM(CASE 
                    WHEN K.CO_SO_NGANH_DUOC_VI_PHAM= 0 THEN 0  
                    ELSE K.CO_SO_NGANH_DUOC_VI_PHAM
                END) AS DUOC_VIPHAM
        FROM CSDLYTE_SYT.TT_BAO_CAO_BIEU_06_TTR_HGI K
        WHERE K.MA_TINH_CO_SO = P_MA_TINH 
            AND (P_MA_HUYEN is null OR (K.MA_HUYEN_CO_SO is not null AND K.MA_HUYEN_CO_SO = P_MA_HUYEN)) 
            AND (P_MA_XA is null OR (K.MA_XA_CO_SO is not null AND K.MA_XA_CO_SO = P_MA_XA)) 
            AND 
            (
                (P_LOAI_CBX = 0 AND K.NAM_BC = P_NAM) OR
                (P_LOAI_CBX = 1 AND K.NAM_BC = P_NAM AND K.THANG_BAO_CAO = P_THANG) OR
                (P_LOAI_CBX = 2 AND K.NAM_BC = P_NAM AND K.QUY_BC = P_QUY) OR
                (P_LOAI_CBX = 3 AND K.NGAY_BAO_CAO BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_DENNGAY,'dd-mm-yyyy'))
            )) SL_KY
    CROSS JOIN (SELECT 
                CASE P_LOAI_CBX
                        WHEN 0 THEN 'N?m ' || (P_NAM - 1)
                        WHEN 1 THEN 'Th�ng ' || P_THANG|| '/' || (P_NAM - 1)
                        WHEN 2 THEN 'Qu� ' || P_QUY  || '/' || (P_NAM - 1)
                        ELSE TO_CHAR(ADD_MONTHS(TO_DATE(P_TUNGAY,'dd-mm-yyyy'), -12), 'dd-mm-yyyy') || '-' || TO_CHAR(ADD_MONTHS(TO_DATE(P_DENNGAY,'dd-mm-yyyy'), -12), 'dd-mm-yyyy') 
                    END AS CUNGKY,
                SUM(CASE 
                        WHEN CK.CO_SO_NGANH_Y_VI_PHAM = 0 THEN 0  
                        ELSE CK.CO_SO_NGANH_Y_VI_PHAM 
                    END) AS Y_VIPHAM,
                SUM(CASE 
                        WHEN CK.CO_SO_NGANH_DUOC_VI_PHAM= 0 THEN 0  
                        ELSE CK.CO_SO_NGANH_DUOC_VI_PHAM
                    END) AS DUOC_VIPHAM
            FROM CSDLYTE_SYT.TT_BAO_CAO_BIEU_06_TTR_HGI CK
            WHERE CK.MA_TINH_CO_SO = P_MA_TINH 
                AND (P_MA_HUYEN is null OR (CK.MA_HUYEN_CO_SO is not null AND CK.MA_HUYEN_CO_SO = P_MA_HUYEN)) 
                AND (P_MA_XA is null OR (CK.MA_XA_CO_SO is not null AND CK.MA_XA_CO_SO = P_MA_XA)) 
                AND 
                (
                    (P_LOAI_CBX = 0 AND CK.NAM_BC = (P_NAM - 1)) OR
                    (P_LOAI_CBX = 1 AND CK.NAM_BC = (P_NAM - 1) AND CK.THANG_BAO_CAO = P_THANG) OR
                    (P_LOAI_CBX = 2 AND CK.NAM_BC = (P_NAM - 1) AND CK.QUY_BC = P_QUY) OR
                    (P_LOAI_CBX = 3 AND CK.NGAY_BAO_CAO BETWEEN ADD_MONTHS(TO_DATE(P_TUNGAY,'dd-mm-yyyy'), -12) AND ADD_MONTHS(TO_DATE(P_DENNGAY,'dd-mm-yyyy'), -12))
                )) SL_CUNGKY;
  
END PROC_GET_TTR_LINHVUC_TK;