CREATE OR REPLACE PROCEDURE PROC_NVD_TILE_HINHTHUC_HGI(
  P_MA_TINH IN NUMBER 
, P_MA_HUYEN IN NUMBER 
, P_MA_XA IN NUMBER 
, P_LOAI_CBX IN NUMBER 
, P_NAM IN NUMBER 
, P_QUY IN NUMBER 
, P_THANG IN NUMBER 
, P_TUNGAY IN VARCHAR2 
, P_DENNGAY IN VARCHAR2 
, P_RS OUT SYS_REFCURSOR
)AS 
  V_TONGSO NUMBER;
BEGIN
  
  SELECT COUNT(*) INTO V_TONGSO 
  FROM CSDLYTE_SYT.NVD_QUANLY_CAP_GCN_HGI
  WHERE NGAY_THU_HOI IS NULL
     AND NGAY_CAP_QD IS NOT NULL
     AND MA_TINH_CO_SO = P_MA_TINH
     AND (P_MA_HUYEN is null OR (MA_HUYEN_CO_SO is not null AND MA_HUYEN_CO_SO = P_MA_HUYEN)) 
     AND (P_MA_XA is null OR (MA_XA_CO_SO is not null AND MA_XA_CO_SO = P_MA_XA)) 
     AND
     (
        (P_LOAI_CBX = 0 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM) OR
        (P_LOAI_CBX = 1 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM AND EXTRACT(MONTH FROM NGAY_CAP_QD) = P_THANG) OR
        (P_LOAI_CBX = 2 AND P_QUY = 1 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (1, 2, 3)) OR
        (P_LOAI_CBX = 2 AND P_QUY = 2 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (4, 5, 6)) OR
        (P_LOAI_CBX = 2 AND P_QUY = 3 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (7, 8, 9)) OR
        (P_LOAI_CBX = 2 AND P_QUY = 4 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (10, 11, 12)) OR
        (P_LOAI_CBX = 3 AND NGAY_CAP_QD BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_TUNGAY,'dd-mm-yyyy'))
     );
  
  OPEN P_RS FOR
  
  SELECT 
           A.HINH_THUC_TO_CHUC, 
           ROUND(((A.TONGLOAI * 100) / V_TONGSO), 2) TILE
      FROM (
           SELECT 'Qu?y thu?c' HINH_THUC_TO_CHUC, COUNT(*) TONGLOAI
           FROM CSDLYTE_SYT.NVD_QUANLY_CAP_GCN_HGI
           WHERE NGAY_THU_HOI IS NULL
              AND NGAY_CAP_QD IS NOT NULL
              AND MA_TINH_CO_SO = P_MA_TINH
              AND (P_MA_HUYEN is null OR (MA_HUYEN_CO_SO is not null AND MA_HUYEN_CO_SO = P_MA_HUYEN)) 
              AND (P_MA_XA is null OR (MA_XA_CO_SO is not null AND MA_XA_CO_SO = P_MA_XA)) 
              AND
                 (
                    (P_LOAI_CBX = 0 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM) OR
                    (P_LOAI_CBX = 1 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM AND EXTRACT(MONTH FROM NGAY_CAP_QD) = P_THANG) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 1 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (1, 2, 3)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 2 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (4, 5, 6)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 3 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (7, 8, 9)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 4 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (10, 11, 12)) OR
                    (P_LOAI_CBX = 3 AND NGAY_CAP_QD BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_TUNGAY,'dd-mm-yyyy'))
                 )
              AND HINH_THUC_TO_CHUC = 1
           
           UNION
           
           SELECT 'Nh� thu?c' HINH_THUC_TO_CHUC, COUNT(*) TONGLOAI
              FROM CSDLYTE_SYT.NVD_QUANLY_CAP_GCN_HGI
             WHERE NGAY_THU_HOI IS NULL
              AND NGAY_CAP_QD IS NOT NULL
              AND MA_TINH_CO_SO = P_MA_TINH
              AND (P_MA_HUYEN is null OR (MA_HUYEN_CO_SO is not null AND MA_HUYEN_CO_SO = P_MA_HUYEN)) 
              AND (P_MA_XA is null OR (MA_XA_CO_SO is not null AND MA_XA_CO_SO = P_MA_XA)) 
              AND
                 (
                    (P_LOAI_CBX = 0 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM) OR
                    (P_LOAI_CBX = 1 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM AND EXTRACT(MONTH FROM NGAY_CAP_QD) = P_THANG) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 1 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (1, 2, 3)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 2 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (4, 5, 6)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 3 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (7, 8, 9)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 4 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (10, 11, 12)) OR
                    (P_LOAI_CBX = 3 AND NGAY_CAP_QD BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_TUNGAY,'dd-mm-yyyy'))
                 )
              AND HINH_THUC_TO_CHUC = 2
           
           UNION
           
           SELECT 'Doanh nghi?p thu?c' HINH_THUC_TO_CHUC, COUNT(*) TONGLOAI
              FROM CSDLYTE_SYT.NVD_QUANLY_CAP_GCN_HGI
             WHERE NGAY_THU_HOI IS NULL
              AND NGAY_CAP_QD IS NOT NULL
              AND MA_TINH_CO_SO = P_MA_TINH
              AND (P_MA_HUYEN is null OR (MA_HUYEN_CO_SO is not null AND MA_HUYEN_CO_SO = P_MA_HUYEN)) 
              AND (P_MA_XA is null OR (MA_XA_CO_SO is not null AND MA_XA_CO_SO = P_MA_XA)) 
              AND
                 (
                    (P_LOAI_CBX = 0 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM) OR
                    (P_LOAI_CBX = 1 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM AND EXTRACT(MONTH FROM NGAY_CAP_QD) = P_THANG) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 1 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (1, 2, 3)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 2 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (4, 5, 6)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 3 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (7, 8, 9)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 4 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (10, 11, 12)) OR
                    (P_LOAI_CBX = 3 AND NGAY_CAP_QD BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_TUNGAY,'dd-mm-yyyy'))
                 )
              AND HINH_THUC_TO_CHUC = 3
           
           UNION
           
           SELECT 'CS b�n l? thu?c d??c li?u, c? truy?n' HINH_THUC_TO_CHUC, COUNT(*) TONGLOAI
              FROM CSDLYTE_SYT.NVD_QUANLY_CAP_GCN_HGI
             WHERE NGAY_THU_HOI IS NULL
              AND NGAY_CAP_QD IS NOT NULL
              AND MA_TINH_CO_SO = P_MA_TINH
              AND (P_MA_HUYEN is null OR (MA_HUYEN_CO_SO is not null AND MA_HUYEN_CO_SO = P_MA_HUYEN)) 
              AND (P_MA_XA is null OR (MA_XA_CO_SO is not null AND MA_XA_CO_SO = P_MA_XA)) 
              AND
                 (
                    (P_LOAI_CBX = 0 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM) OR
                    (P_LOAI_CBX = 1 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM AND EXTRACT(MONTH FROM NGAY_CAP_QD) = P_THANG) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 1 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (1, 2, 3)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 2 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (4, 5, 6)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 3 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (7, 8, 9)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 4 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (10, 11, 12)) OR
                    (P_LOAI_CBX = 3 AND NGAY_CAP_QD BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_TUNGAY,'dd-mm-yyyy'))
                 )
              AND HINH_THUC_TO_CHUC = 4
           
           UNION
           
           SELECT 'T? thu?c tr?m Y t?' HINH_THUC_TO_CHUC, COUNT(*) TONGLOAI
              FROM CSDLYTE_SYT.NVD_QUANLY_CAP_GCN_HGI
             WHERE NGAY_THU_HOI IS NULL
              AND NGAY_CAP_QD IS NOT NULL
              AND MA_TINH_CO_SO = P_MA_TINH
              AND (P_MA_HUYEN is null OR (MA_HUYEN_CO_SO is not null AND MA_HUYEN_CO_SO = P_MA_HUYEN)) 
              AND (P_MA_XA is null OR (MA_XA_CO_SO is not null AND MA_XA_CO_SO = P_MA_XA)) 
              AND
                 (
                    (P_LOAI_CBX = 0 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM) OR
                    (P_LOAI_CBX = 1 AND EXTRACT(YEAR FROM NGAY_CAP_QD) = P_NAM AND EXTRACT(MONTH FROM NGAY_CAP_QD) = P_THANG) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 1 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (1, 2, 3)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 2 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (4, 5, 6)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 3 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (7, 8, 9)) OR
                    (P_LOAI_CBX = 2 AND P_QUY = 4 AND EXTRACT(MONTH FROM NGAY_CAP_QD) IN (10, 11, 12)) OR
                    (P_LOAI_CBX = 3 AND NGAY_CAP_QD BETWEEN TO_DATE(P_TUNGAY,'dd-mm-yyyy') AND TO_DATE(P_TUNGAY,'dd-mm-yyyy'))
                 )
              AND HINH_THUC_TO_CHUC = 5  
      ) A;
  
END PROC_NVD_TILE_HINHTHUC_HGI;