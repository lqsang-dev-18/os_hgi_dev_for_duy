create or replace procedure PROC_TY_CANBO_THEO_GIOITINH(P_NAM       NUMBER,
                                                        P_COSOKCB VARCHAR2,
                                                        P_RS        OUT SYS_REFCURSOR) is
begin
  OPEN P_RS FOR
    SELECT COUNT(*) AS TONG_SO,
           COUNT(CASE
                   WHEN GIOI_TINH = 1 THEN
                    1
                 END) SL_NAM,
           COUNT(CASE
                   WHEN GIOI_TINH = 2 THEN
                    1
                 END) SL_NU,
           CASE
             WHEN COUNT(*) > 0 THEN
              ROUND(COUNT(CASE
                            WHEN GIOI_TINH = 1 THEN
                             1 
                          END) / COUNT(*),
                    3)
             ELSE
              0
           END AS TY_LE_NAM,
           CASE
             WHEN COUNT(*) > 0 THEN
              ROUND(COUNT(CASE
                            WHEN GIOI_TINH = 2 THEN
                             1 
                          END) / COUNT(*),
                    3)
             ELSE
              0
           END AS TY_LE_NU
      FROM CSDLYTE_SYT.TCCB_HOSO_CAN_BO A
     WHERE A.NAM_BAO_CAO = P_NAM
       AND ( A.DON_VI = P_COSOKCB OR P_COSOKCB IS NULL) ;
end PROC_TY_CANBO_THEO_GIOITINH;
/
