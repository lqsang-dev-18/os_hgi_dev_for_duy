create or replace procedure PROC_TK_SL_HO_GA(P_NAM      NUMBER,
                                             P_THANG    NUMBER,
                                             P_MA_HUYEN VARCHAR2,
                                             P_RS       OUT SYS_REFCURSOR) is
begin
  OPEN P_RS FOR
    SELECT HG.SO_CA_MAC, DM.TEN_HUYEN
      FROM CSDLYTE_SYT.CDC_BENH_TRUYEN_NHIEM_HO_GA HG
      LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN DM
        ON HG.MA_HUYEN = DM.MA_HUYEN
     WHERE HG.NAM = P_NAM
       AND HG.THANG = P_THANG
       AND HG.MA_HUYEN = NVL(P_MA_HUYEN, HG.MA_HUYEN);
end PROC_TK_SL_HO_GA;
/
