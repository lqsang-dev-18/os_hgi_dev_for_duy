create or replace PROCEDURE                         PROC_TK_CT_PC_TANG_HUYET_AP 
(
  P_NAM NUMBER 
, P_THANG NUMBER 
, P_HUYEN VARCHAR2 
, P_RS    OUT SYS_REFCURSOR
) IS
BEGIN

  OPEN P_RS FOR
  SELECT X.NOI_DUNG,
           X.CHI_TIEU,
           X.THUC_HIEN,
           X.CONG_DON,
           ROUND(X.CONG_DON / X.CHI_TIEU,3) TY_LE_DAT
      FROM (SELECT H.TEN_HUYEN,
                   DM.NOI_DUNG,
                   CT.CHI_TIEU,
                   TH.THUC_HIEN,
                   (SELECT SUM(A.THUC_HIEN) CONG_DON
                      FROM CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_TANG_HUYET_AP A
                     WHERE A.NAM = P_NAM
                       AND A.THANG <= P_THANG
                       AND DM.ID = A.ID
                       AND A.MA_HUYEN = TH.MA_HUYEN
                     GROUP BY A.NAM, A.ID) AS CONG_DON
              FROM CSDLYTE_SYT.CDC_DM_CT_PC_TANG_HUYET_AP DM
              LEFT JOIN CSDLYTE_SYT.CDC_CHI_TIEU_PC_TANG_HUYET_AP CT
                ON DM.ID = CT.ID
              LEFT JOIN CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_TANG_HUYET_AP TH
                ON DM.ID = TH.ID
               AND TH.NAM = CT.NAM
              LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H
                ON TH.MA_HUYEN = H.MA_HUYEN
             WHERE TH.NAM = P_NAM
               AND (P_HUYEN IS NULL OR TH.MA_HUYEN = P_HUYEN)
               AND TH.THANG = P_THANG) X;
               
END PROC_TK_CT_PC_TANG_HUYET_AP;