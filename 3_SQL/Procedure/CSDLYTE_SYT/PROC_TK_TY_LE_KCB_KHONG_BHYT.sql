create or replace procedure PROC_TK_TY_LE_KCB_KHONG_BHYT(P_TUNGAY    DATE,
                                                         P_DENNGAY   DATE,
                                                         P_MA_HUYEN  VARCHAR2,
                                                         P_MA_DON_VI VARCHAR2,
                                                         P_LOAI_HINH NUMBER,
                                                         P_RS        OUT SYS_REFCURSOR) is
  V_SEPARATOR VARCHAR2(10) := '[^,]+';
begin
  OPEN P_RS FOR
    select COUNT(s1.MA_LK) TONG_SO,
           SUM(CASE
                 WHEN (s1.MA_THE IS NULL) THEN
                  1
                 ELSE
                  0
               END) KHONG_BAO_HIEM,
           CASE
             WHEN COUNT(*) > 0 THEN
              ROUND(COUNT(CASE
                            WHEN s1.MA_THE IS NULL THEN
                             1
                          END) / COUNT(*),
                    3)
             ELSE
              0
           END AS TY_LE
      from CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB s1
      LEFT JOIN CSDLYTE_DANHMUC.DM_DONVI s2
        ON s1.MA_CSKCB = s2.MA_DON_VI
     WHERE s1.NGAY_RA_DATE BETWEEN P_TUNGAY AND P_DENNGAY
       AND (P_MA_HUYEN IS NULL OR
           (s2.MA_HUYEN = P_MA_HUYEN AND s2.MA_DON_VI = s1.MA_CSKCB))
       AND (P_LOAI_HINH IS NULL OR s2.TU_NHAN = P_LOAI_HINH)
       AND (P_MA_DON_VI IS NULL OR
           s1.MA_CSKCB IN
           (SELECT regexp_substr(P_MA_DON_VI, V_SEPARATOR, 1, level)
               FROM dual
             CONNECT BY regexp_substr(P_MA_DON_VI, V_SEPARATOR, 1, level) IS NOT NULL));
end PROC_TK_TY_LE_KCB_KHONG_BHYT;
/
