create or replace procedure PROC_GET_PAGE_LIST_DS_BENH_NHAN_HOAN_THANH_DHTL(P_PAGE    NUMBER DEFAULT 0,
                                                                        P_SIZE    NUMBER DEFAULT 0,
                                                                        P_TUNGAY  VARCHAR2,
                                                                        P_DENNGAY VARCHAR2,
                                                                        P_KEYWORD VARCHAR2,
                                                                        P_QUY     VARCHAR2,
                                                                        P_NAM     VARCHAR2,
                                                                        P_THANG   VARCHAR2,
                                                                        P_RS      OUT SYS_REFCURSOR) is
                                                                        vsql      CLOB;
  pzsql     CLOB;
  total_row VARCHAR2(50) := ', COUNT(1) OVER () TOTAL_ROW';
  v_cond    VARCHAR2(200);
begin
  IF P_TUNGAY IS NOT NULL and P_DENNGAY IS NOT NULL THEN
    v_cond := ' and A.NGAY BETWEEN to_date(''' || P_TUNGAY ||
              ''',''dd/mm/yyyy'') AND to_date(''' || P_DENNGAY ||
              ''',''dd/mm/yyyy'')';
  ELSIF P_THANG IS NOT NULL AND P_NAM IS NOT NULL THEN
    v_cond := ' and A.NGAY <= LAST_DAY(TO_DATE(''' || '01' || '/' ||
              P_THANG || '/' || P_NAM || ''',''dd/mm/yyyy''))';
  ELSIF P_QUY IS NOT NULL AND P_NAM IS NOT NULL THEN
    v_cond := ' and A.NGAY <= LAST_DAY(TO_DATE(''' || '01' || '/' ||
              ((P_QUY - 1) * 3 + 3) || '/' || P_NAM ||
              ''',''dd/mm/yyyy''))';
  ELSE
    v_cond := ' and A.NGAY <= TO_DATE(''31/12/' || P_NAM ||
              ''',''dd/mm/yyyy'')';
  END IF;
vsql  := '
  SELECT 
    A.HO_TEN
   ,TO_CHAR(A.NGAY,''DD/MM/YYYY'') NGAY 
   ,A.DIA_CHI
    ' || total_row || '
  FROM CSDLYTE_SYT.KHAM_PHATHIEN_DIEUTRI_PHONG  A
  WHERE A.DHTL = 1 AND A.HIENTRANG_DHTL = 1 AND A.TRANG_THAI = 1
  ' || v_cond || '
  ORDER BY A.NGAY DESC
  ';
  pzsql := '
  SELECT *
    FROM (SELECT TEMP.*, ROWNUM R
            FROM (
            ' || vsql || '
            ) TEMP
            WHERE ROWNUM < ((' || P_PAGE || ' * ' || P_SIZE ||
           ') + 1)
            )
    WHERE R >= (((' || P_PAGE || ' - 1) * ' || P_SIZE || ') + 1)
  ';
  --OPEN cur FOR select vsql as SQL_STORE from dual; 

  IF P_PAGE = 0 THEN
    OPEN P_RS FOR vsql;
  ELSE
    OPEN P_RS FOR pzsql;
  END IF;
end PROC_GET_PAGE_LIST_DS_BENH_NHAN_HOAN_THANH_DHTL;
/
