CREATE OR REPLACE PROCEDURE "PROC_RP_HOATDONG_DIEUTRI_B03" (
    P_PAGE          NUMBER, 
    P_SIZE          NUMBER,
    P_TUNGAY        IN VARCHAR2,
    P_DENNGAY       IN VARCHAR2,
    P_MA_HUYEN       VARCHAR2,
    P_MA_XA          VARCHAR2,
    P_MA_CSYT        VARCHAR2,
    P_NAM VARCHAR2,
    P_THANG VARCHAR2,
    P_RS OUT SYS_REFCURSOR 
) IS
    vsql CLOB;
    pzsql CLOB;
    total_row VARCHAR2(50) := ' COUNT(1) OVER () TOTAL_ROW';

    v_tungay VARCHAR2(50) := P_TUNGAY || ' 00:00:00';
    v_denngay VARCHAR2(50) := P_DENNGAY || ' 23:59:59';
    v_mahuyen VARCHAR2(200);
    v_maxa VARCHAR2(200);
    v_macsyt VARCHAR2(200);
    v_nam VARCHAR2(200);
    v_thang VARCHAR2(200);
BEGIN
    if P_MA_HUYEN is not null then
        v_mahuyen := 'and dv.MA_HUYEN in ('|| P_MA_HUYEN ||')';
    end if;

    if P_MA_XA is not null then
        v_maxa := '';
    end if;
    
    if P_NAM is not null then
        v_nam := 'and EXTRACT(YEAR FROM b1.NGAY_RA_DATE) = ' || P_NAM;
    end if;
    
    if P_THANG is not null then
        v_thang := 'and EXTRACT(MONTH FROM b1.NGAY_RA_DATE) = ' || P_THANG;
    end if;
    
    if P_MA_CSYT is not null then
        v_macsyt := 'and b1.MA_CSKCB in ('|| P_MA_CSYT ||')';
    end if;

  vsql:= '
       SELECT  dv.CAP, 
               dv.MA_DON_VI, 
               dv.TEN_DON_VI as TEN_CSYT,
               SUM(1) as TONG_SO,
               SUM(case when GIOI_TINH=2 then 1 else 0 end) as TONG_SL_NU,
               SUM(case when (EXTRACT(YEAR FROM NGAY_VAO_DATE) - EXTRACT(YEAR FROM NGAY_SINH_DATE) < 15) then 1 else 0 end ) as TONG_TREMEM,
               SUM(case when ((EXTRACT(YEAR FROM NGAY_VAO_DATE) - EXTRACT(YEAR FROM NGAY_SINH_DATE) < 15) and GIOI_TINH=2) then 1 else 0 end )as TONG_TREMEM_NU,
               SUM(case when MA_LYDO_VVIEN=2 then 1 else 0 end ) as SL_CAPCUU,
               SUM(case when (MA_LYDO_VVIEN=2 and GIOI_TINH=2) then 1 else 0 end) as SL_CAPCUU_NU,
               SUM(SO_NGAY_DTRI) as TONG_SO_NGAY_DTRI,
               SUM(case when KET_QUA_DTRI=5 then 1 else 0 end )as SL_TUVONG,
               SUM(case when (GIOI_TINH=2 and KET_QUA_DTRI=5) then 1 else 0 end )as TUVONG_NU,
               SUM(case when ((EXTRACT(YEAR FROM NGAY_VAO_DATE) - EXTRACT(YEAR FROM NGAY_SINH_DATE) < 15) and KET_QUA_DTRI=5) then 1 else 0 end ) as TONG_TREMEM_TUVONG,
               SUM(case when ((EXTRACT(YEAR FROM NGAY_VAO_DATE) - EXTRACT(YEAR FROM NGAY_SINH_DATE) < 15) and KET_QUA_DTRI=5 and GIOI_TINH=2) then 1 else 0 end )as TONG_TREMEMNU_TUVONG,
               SUM(case when (KET_QUA_DTRI=5 AND SO_NGAY_DTRI < 1)then 1 else 0 end ) as SL_TUVONG_24H,
               SUm(case when (KET_QUA_DTRI=5 AND SO_NGAY_DTRI < 1 and GIOI_TINH=2)then 1 else 0 end) as SL_TUVONG_24H_NU,
               SUM(case when MA_THE is not NULL then 1 else 0 end ) as Co_BHYT,
               ' || total_row || '
        from  CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB b1
        INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON b1.MA_CSKCB = dv.MA_DON_VI
        WHERE b1.MA_LOAI_KCB=3
        ' || v_mahuyen || '
        ' || v_maxa || '
        ' || v_macsyt || '
        ' || v_thang || '
        ' || v_nam || '
        and trunc(NGAY_RA_DATE) between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
        GROUP BY dv.CAP, dv.MA_DON_VI, dv.TEN_DON_VI
        ORDER BY CAP, MA_DON_VI
        ';
   pzsql :='
                SELECT *
                FROM (SELECT TEMP.*, ROWNUM R
                FROM (
                        '|| vsql ||'
                        ) TEMP
                        WHERE ROWNUM < (('||P_PAGE||' * '||P_SIZE||') + 1)
                        )
                WHERE R >= ((('||P_PAGE||' - 1) * '||P_SIZE||') + 1) ';
  --OPEN cur FOR select vsql as SQL_STORE from dual; 
  IF P_PAGE = 0 THEN
    OPEN P_RS FOR vsql;
  ELSE
    OPEN P_RS FOR pzsql;
  END IF;
END;
/
