CREATE OR REPLACE PROCEDURE "PROC_RP_HOATDONG_CLS_B06" (
    P_PAGE NUMBER, P_SIZE NUMBER,
    P_TUNGAY        IN VARCHAR2,
    P_DENNGAY       IN VARCHAR2,
    P_MA_HUYEN       VARCHAR2,
    P_MA_XA          VARCHAR2,
    P_MA_CSYT        VARCHAR2,
    P_NAM VARCHAR2,
    P_THANG VARCHAR2,   
    P_RS OUT SYS_REFCURSOR 
) IS
    vsql CLOB;
    pzsql CLOB;
    total_row VARCHAR2(50) := ', COUNT(1) OVER () TOTAL_ROW';

    v_tungay VARCHAR2(50) := P_TUNGAY || ' 00:00:00';
    v_denngay VARCHAR2(50) := P_DENNGAY || ' 23:59:59';
    v_mahuyen VARCHAR2(200);
    v_maxa VARCHAR2(200);
    v_macsyt VARCHAR2(200);
    v_nam VARCHAR2(200);
    v_thang VARCHAR2(200);
BEGIN
  if P_MA_HUYEN is not null then
        v_mahuyen := 'and dv.MA_HUYEN in ('|| P_MA_HUYEN ||')';
    end if;
    
    if P_NAM is not null then
        v_nam := 'and EXTRACT(YEAR FROM c.NGAY_RA_DATE) = ' || P_NAM;
    end if;
    
    if P_THANG is not null then
        v_thang := 'and EXTRACT(MONTH FROM c.NGAY_RA_DATE) = ' || P_THANG;
    end if;
    
    if P_MA_XA is not null then
        v_maxa := '';
    end if;

    if P_MA_CSYT is not null then
        v_macsyt := 'and a.MACSKCB in ('|| P_MA_CSYT ||')';
    end if;
    vsql:='SELECT dv.CAP as CAP ,dv.MA_DON_VI as MA_DON_VI ,dv.TEN_DON_VI as TEN_CSYT, 
                SUM(CLS.TONG_XN) as TONG_XN, SUM(CLS.XN_NGOAITRU) as XN_NGOAITRU, SUM(CLS.XN_NOITRU) as XN_NOITRU,
                SUM(CLS.TONG_CDHA) as TONG_CDHA, SUM(CLS.CDHA_NGOAITRU) as CDHA_NGOAITRU, SUM(CLS.CDHA_NOITRU) as CDHA_NOITRU,
                SUM(CLS.TONG_TDCN) as TONG_TDCN, SUM(CLS.TDCN_NGOAITRU) as TDCN_NGOAITRU, SUM(CLS.TDCN_NOITRU) as TDCN_NOITRU' 
    || total_row || '
            FROM (

                SELECT MACSKCB, SUM(XN_NGOAITRU) + SUM(XN_NOITRU) as TONG_XN, SUM(XN_NGOAITRU) as XN_NGOAITRU, SUM(XN_NOITRU) as XN_NOITRU, 
                        SUM(CDHA_NGOAITRU) + SUM(CDHA_NOITRU) as TONG_CDHA, SUM(CDHA_NGOAITRU) as CDHA_NGOAITRU, SUM(CDHA_NOITRU) as CDHA_NOITRU,
                        SUM(TDCN_NGOAITRU) + SUM(TDCN_NOITRU) as TONG_TDCN, SUM(TDCN_NGOAITRU) as TDCN_NGOAITRU, SUM(TDCN_NOITRU) as TDCN_NOITRU

                FROM (
                SELECT a.MACSKCB, a.SO_LUONG as XN_NGOAITRU, 0 as XN_NOITRU, 0 as CDHA_NGOAITRU, 0 as CDHA_NOITRU, 0 as TDCN_NGOAITRU, 0 as TDCN_NOITRU
                from CSDLYTE_4210.B3_CHITIEUCLS a
                INNER JOIN CSDLYTE_4210.B6_DM_NHOMCHIPHI b ON a.MA_NHOM = b.ma_nhom
                INNER JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB c ON c.MA_LK = a.MA_LK
                WHERE b.MA_NHOM in (1)
                      and c.MA_LOAI_KCB=1
                      and trunc(a.NGAY_YL_DATE) between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
                      ' || v_thang || '
                      ' || v_nam || '
                union all
                SELECT a.MACSKCB, 0 as XN_NGOAITRU, a.SO_LUONG as XN_NOITRU, 0 as CDHA_NGOAITRU, 0 as CDHA_NOITRU, 0 as TDCN_NGOAITRU, 0 as TDCN_NOITRU
                from CSDLYTE_4210.B3_CHITIEUCLS a
                INNER JOIN CSDLYTE_4210.B6_DM_NHOMCHIPHI b ON a.MA_NHOM = b.ma_nhom
                INNER JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB c ON c.MA_LK = a.MA_LK
                WHERE b.MA_NHOM = 1
                      and c.MA_LOAI_KCB in (2, 3)
                      and trunc(a.NGAY_YL_DATE) between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
                      ' || v_thang || '
                      ' || v_nam || '
                union all
                SELECT a.MACSKCB, 0 as XN_NGOAITRU, 0 as XN_NOITRU, a.SO_LUONG as CDHA_NGOAITRU, 0 as CDHA_NOITRU, 0 as TDCN_NGOAITRU, 0 as TDCN_NOITRU
                from CSDLYTE_4210.B3_CHITIEUCLS a
                INNER JOIN CSDLYTE_4210.B6_DM_NHOMCHIPHI b ON a.MA_NHOM = b.ma_nhom
                INNER JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB c ON c.MA_LK = a.MA_LK
                WHERE b.MA_NHOM = 2
                    and c.MA_LOAI_KCB=1
                    and trunc(a.NGAY_YL_DATE) between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
                    ' || v_thang || '
                    ' || v_nam || '
                union all
                SELECT a.MACSKCB, 0 as XN_NGOAITRU, 0 as XN_NOITRU, 0 as CDHA_NGOAITRU, a.SO_LUONG as CDHA_NOITRU, 0 as TDCN_NGOAITRU, 0 as TDCN_NOITRU
                from CSDLYTE_4210.B3_CHITIEUCLS a
                INNER JOIN CSDLYTE_4210.B6_DM_NHOMCHIPHI b ON a.MA_NHOM = b.ma_nhom
                INNER JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB c ON c.MA_LK = a.MA_LK
                WHERE b.MA_NHOM = 2
                    and c.MA_LOAI_KCB in (2, 3)
                    and trunc(a.NGAY_YL_DATE) between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
                    ' || v_thang || '
                    ' || v_nam || '
                union all 
                SELECT a.MACSKCB, 0 as XN_NGOAITRU, 0 as XN_NOITRU, 0 as CDHA_NGOAITRU, 0 as CDHA_NOITRU, a.SO_LUONG as TDCN_NGOAITRU, 0 as TDCN_NOITRU
                from CSDLYTE_4210.B3_CHITIEUCLS a
                INNER JOIN CSDLYTE_4210.B6_DM_NHOMCHIPHI b ON a.MA_NHOM = b.ma_nhom
                INNER JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB c ON c.MA_LK = a.MA_LK
                WHERE b.MA_NHOM = 3
                    and c.MA_LOAI_KCB = 1
                    and trunc(a.NGAY_YL_DATE) between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
                    ' || v_thang || '
                    ' || v_nam || '
                union all
                SELECT a.MACSKCB, 0 as XN_NGOAITRU, 0 as XN_NOITRU, 0 as CDHA_NGOAITRU, 0 as CDHA_NOITRU,  0 as TDCN_NGOAITRU, a.SO_LUONG as TDCN_NOITRU
                from CSDLYTE_4210.B3_CHITIEUCLS a
                INNER JOIN CSDLYTE_4210.B6_DM_NHOMCHIPHI b ON a.MA_NHOM = b.ma_nhom
                INNER JOIN CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB c ON c.MA_LK = a.MA_LK
                WHERE b.MA_NHOM = 3
                    and c.MA_LOAI_KCB in (2, 3)
                    and trunc(a.NGAY_YL_DATE) between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
                    ' || v_thang || '
                    ' || v_nam || '
                  )
                  GROUP BY MACSKCB ) CLS
            INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON CLS.MACSKCB = dv.MA_DON_VI
            WHERE 1=1  ' || v_mahuyen || '
                       ' || v_maxa || '
                       ' || v_macsyt || '
                      
            GROUP BY dv.CAP ,dv.MA_DON_VI, dv.TEN_DON_VI
            ORDER BY CAP, MA_DON_VI';

      pzsql :='
                SELECT *
                FROM (SELECT TEMP.*, ROWNUM R
                FROM (
                        '|| vsql ||'
                        ) TEMP
                        WHERE ROWNUM < (('||P_PAGE||' * '||P_SIZE||') + 1)
                        )
                WHERE R >= ((('||P_PAGE||' - 1) * '||P_SIZE||') + 1) ';
       --OPEN cur FOR select vsql as SQL_STORE from dual; 
  IF P_PAGE = 0 THEN
    OPEN P_RS FOR vsql;
  ELSE
    OPEN P_RS FOR pzsql;
  END IF;
END;
/
