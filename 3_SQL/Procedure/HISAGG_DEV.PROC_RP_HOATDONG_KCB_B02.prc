CREATE OR REPLACE PROCEDURE "PROC_RP_HOATDONG_KCB_B02" (
    P_PAGE NUMBER DEFAULT 0, P_SIZE NUMBER DEFAULT 0,
    P_TUNGAY        VARCHAR2,
    P_DENNGAY       VARCHAR2,
    P_MA_HUYEN       VARCHAR2,
    P_MA_XA          VARCHAR2,
    P_MA_CSYT        VARCHAR2,
    P_NAM VARCHAR2,
    P_THANG VARCHAR2,
    P_RS OUT SYS_REFCURSOR
) IS
  vsql CLOB;
  pzsql CLOB;
  total_row VARCHAR2(50) := ', COUNT(1) OVER () TOTAL_ROW';

  v_tungay VARCHAR2(50) := P_TUNGAY || ' 00:00:00';
  v_denngay VARCHAR2(50) := P_DENNGAY || ' 23:59:59';
  v_mahuyen VARCHAR2(200);
  v_maxa VARCHAR2(200);
  v_macsyt VARCHAR2(200);
  v_nam VARCHAR2(200);
  v_thang VARCHAR2(200);
BEGIN

    if P_MA_HUYEN is not null then
        v_mahuyen := 'and dv.MA_HUYEN in ('|| P_MA_HUYEN ||')';
    end if;

    if P_NAM is not null then
        v_nam := 'and EXTRACT(YEAR FROM b1.NGAY_RA_DATE) = ' || P_NAM;
    end if;
    
    if P_THANG is not null then
        v_thang := 'and EXTRACT(MONTH FROM b1.NGAY_RA_DATE) = ' || P_THANG;
    end if;
    
    if P_MA_XA is not null then
        v_maxa := '';
    end if;

    if P_MA_CSYT is not null then
        v_macsyt := 'and b1.MA_CSKCB in ('|| P_MA_CSYT ||')';
    end if;

  vsql:= '
  SELECT 
    dv.CAP as CAP
    ,dv.MA_DON_VI as MA_DON_VI
    ,dv.TEN_DON_VI as TEN_CSYT
    ,SUM(CASE WHEN b1.MA_LOAI_KCB = 1 THEN 1 ELSE 0 END) as TONGSO
    ,SUM(CASE WHEN b1.MA_LOAI_KCB = 1 AND b1.GIOI_TINH = 2 THEN 1 ELSE 0 END) as NU
    ,SUM(CASE WHEN b1.MA_LOAI_KCB = 1 AND b1.MA_THE is not null THEN 1 ELSE 0 END) as BHYT
    ,SUM(CASE WHEN b1.MA_LOAI_KCB = 1 AND b1.MA_THE is null THEN 1 ELSE 0 END) as VIENPHI
    ,0 as YHCT
    ,0 as TE_15T
    ,SUM(CASE WHEN b1.MA_LYDO_VVIEN = 2 THEN 1 ELSE 0 END) as CAPCUU
    ,0 as VAOVIEN
    ,SUM(CASE WHEN b1.MA_LOAI_KCB = 1 AND b1.TINH_TRANG_RV = 2 THEN 1 ELSE 0 END) as CHUYENVIEN
    ,SUM(CASE WHEN b1.MA_LOAI_KCB = 2 THEN 1 ELSE 0 END) as BANT_SONGUOI
    ,SUM(CASE WHEN b1.MA_LOAI_KCB = 2 THEN b1.SO_NGAY_DTRI ELSE 0 END) as BANT_SONGAY
    '|| total_row ||'
  FROM CSDLYTE_4210.B1_CHITIEUTONGHOP_KCB b1
  INNER JOIN CSDLYTE_DANHMUC.DM_DONVI dv ON b1.MA_CSKCB = dv.MA_DON_VI
  WHERE 
  b1.NGAY_RA_DATE between to_date(''' || v_tungay || ''', ''dd/mm/yyyy hh24:mi:ss'') and to_date(''' || v_denngay || ''', ''dd/mm/yyyy hh24:mi:ss'')
  AND b1.MA_LOAI_KCB in (1, 2)
  AND nvl(b1.TRANG_THAI_XOA,0) = 0
  ' || v_mahuyen || '
  ' || v_maxa || '
  ' || v_thang || '
  ' || v_nam || '
  ' || v_macsyt || '
  GROUP BY dv.CAP
            ,dv.MA_DON_VI
            ,dv.TEN_DON_VI
  ORDER BY CAP, MA_DON_VI
  ';
  pzsql :='
  SELECT *
    FROM (SELECT TEMP.*, ROWNUM R
            FROM (
            '|| vsql ||'
            ) TEMP
            WHERE ROWNUM < (('||P_PAGE||' * '||P_SIZE||') + 1)
            )
    WHERE R >= ((('||P_PAGE||' - 1) * '||P_SIZE||') + 1)
  ';
  --OPEN cur FOR select vsql as SQL_STORE from dual; 
  IF P_PAGE = 0 THEN
    OPEN P_RS FOR vsql;
  ELSE
    OPEN P_RS FOR pzsql;
  END IF;

END;
/
