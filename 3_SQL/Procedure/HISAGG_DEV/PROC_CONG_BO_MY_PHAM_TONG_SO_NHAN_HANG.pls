create or replace PROCEDURE PROC_CONG_BO_MY_PHAM_TONG_SO_NHAN_HANG(
        P_LOAI_CBX NUMBER,
        P_NAM VARCHAR2,
        P_QUY NVARCHAR2, 
        P_THANG NVARCHAR2, 
        P_TUNGAY NVARCHAR2,
        P_DENNGAY NVARCHAR2,
        P_RS OUT SYS_REFCURSOR
    ) IS 
    vsql VARCHAR2(5000);
    v_luachon VARCHAR2(1000);
    BEGIN
        if (P_LOAI_CBX = 0) then
            v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-01-01'', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_NAM||'-12-31'', ''YYYY-MM-DD''))';
        elsif (P_LOAI_CBX = 1) then
            if(P_THANG = 0) then
                v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-01-01'', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_NAM||'-12-31'', ''YYYY-MM-DD''))';
            else
                v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-'||P_THANG||'-01'', ''YYYY-MM-DD'') AND LAST_DAY(TO_DATE('''||P_NAM||'-'||P_THANG||''', ''YYYY-MM''))';
            end if;
        elsif (P_LOAI_CBX = 2) then
            IF (P_QUY = 1) THEN
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-03-31'', ''YYYY-MM-DD'')';
            ELSIF (P_QUY = 2) THEN
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-04-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-06-30'', ''YYYY-MM-DD'')';
            ELSIF (P_QUY = 3) THEN
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-07-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-09-30'', ''YYYY-MM-DD'')';
            ELSE
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-10-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'')';
            END IF;
        else
                v_luachon := 'BETWEEN TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'')';
        end if;
    
--    if (P_LOAI_CBX = 0) then --
--        if(P_THANG = 0) then
--            vthang := 'BETWEEN TO_DATE('''||P_NAM||'-01-01'', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_NAM||'-12-31'', ''YYYY-MM-DD''))';
--        else
--            vthang := 'BETWEEN TO_DATE('''||P_NAM||'-'||P_THANG||'-01'', ''YYYY-MM-DD'') AND LAST_DAY(TO_DATE('''||P_NAM||'-'||P_THANG||''', ''YYYY-MM''))';
--        end if;
--    else
--        vthang := 'BETWEEN TO_DATE('''||P_TUNGAY||''', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_DENNGAY||''', ''YYYY-MM-DD''))';
--    end if;
         vsql:= ' 
           SELECT SUM(SL_NHAN_HANG) SL_NHAN_HANG, SUM(SL_NHAN_HANG_MOI_TRONG_THANG) SL_NHAN_HANG_MOI_TRONG_THANG
                    ,SUM(TONG_MA_CTY_SANXUAT) TONG_MA_CTY_SANXUAT, SUM(TONG_SO_SAN_MOI_TRONG_THANG) TONG_SO_SAN_MOI_TRONG_THANG
                    ,SUM(TONG_SO_SP) TONG_SO_SP, SUM(TONG_SO_SP_MOI_TRONG_THANG) TONG_SO_SP_MOI_TRONG_THANG
            FROM(
                SELECT COUNT(NHAN_HANG) SL_NHAN_HANG
                       ,SUM(CASE WHEN NGAY_CAP '||v_luachon||' THEN 1 ELSE 0 END ) SL_NHAN_HANG_MOI_TRONG_THANG
                       , 0 TONG_MA_CTY_SANXUAT, 0 TONG_SO_SAN_MOI_TRONG_THANG
                       , 0 TONG_SO_SP, 0 TONG_SO_SP_MOI_TRONG_THANG       
                FROM(
                    SELECT NHAN_HANG, NGAY_CAP
                    FROM(
                        SELECT ROW_NUMBER() OVER(PARTITION BY NHAN_HANG ORDER BY NHAN_HANG DESC) RN, NHAN_HANG, NGAY_CAP
                        FROM CSDLYTE_SYT.NVD_CONGBO_MYPHAM
                    ) 
                    WHERE RN=1
                )
                UNION 
                SELECT 0 SL_NHAN_HANG, 0 SL_NHAN_HANG_MOI_TRONG_THANG 
                       ,COUNT(MA_CTY_SANXUAT) TONG_MA_CTY_SANXUAT
                       ,SUM(CASE WHEN NGAY_CAP '||v_luachon||' THEN 1 ELSE 0 END) TONG_SO_SAN_MOI_TRONG_THANG
                       , 0 TONG_SO_SP, 0 TONG_SO_SP_MOI_TRONG_THANG    
                FROM(
                    SELECT MA_CTY_SANXUAT, NGAY_CAP, TEN_CTY_SANXUAT
                    FROM(
                        SELECT DISTINCT MA_CTY_SANXUAT, NGAY_CAP, TEN_CTY_SANXUAT
                        FROM CSDLYTE_SYT.NVD_CONGBO_MYPHAM
                    ) 
                )
                UNION 
                SELECT  0 SL_NHAN_HANG, 0 SL_NHAN_HANG_MOI_TRONG_THANG 
                        ,0 TONG_MA_CTY_SANXUAT, 0 TONG_SO_SAN_MOI_TRONG_THANG    
                       ,COUNT(TEN_SP) TONG_SO_SP
                       ,SUM(CASE WHEN NGAY_CAP '||v_luachon||' THEN 1 ELSE 0 END) TONG_SO_SP_MOI_TRONG_THANG
                      
                FROM(
                    SELECT TEN_SP, NGAY_CAP
                    FROM(
                        SELECT DISTINCT TEN_SP, NGAY_CAP
                        FROM CSDLYTE_SYT.NVD_CONGBO_MYPHAM
                    ) 
                ))' ;
                DBMS_OUTPUT.PUT_LINE(vsql);
     OPEN P_RS FOR vsql;
END PROC_CONG_BO_MY_PHAM_TONG_SO_NHAN_HANG;