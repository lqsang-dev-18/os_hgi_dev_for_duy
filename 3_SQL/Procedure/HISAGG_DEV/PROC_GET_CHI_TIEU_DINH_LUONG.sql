CREATE OR REPLACE PROCEDURE PROC_GET_CHI_TIEU_DINH_LUONG (P_NAM NUMBER, P_THANG VARCHAR2, P_TUNGAY VARCHAR2, P_DENNGAY VARCHAR2, P_RS OUT SYS_REFCURSOR) AS 

vsql CLOB;
thang VARCHAR2(200);
nam VARCHAR2(200);
ngay VARCHAR2(200);
 
BEGIN
if P_THANG is not null then
        thang := 'AND EXTRACT(MONTH FROM NGAY_BAO_CAO) in ('|| P_THANG ||')';
    end if;
    if P_TUNGAY is not null AND P_DENNGAY is not null then
        ngay := 'AND (TRUNC(NGAY_BAO_CAO) BETWEEN TO_DATE('''||P_TUNGAY||''', ''DD/MM/YYYY'') AND TO_DATE('''||P_DENNGAY||''', ''DD/MM/YYYY''))';
    end if;  
    if P_NAM is not null then
        nam := 'AND NAM in ('|| P_NAM ||')';
    end if;
 vsql:= '
    SELECT  --bieu do 3
        NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 10 THEN CHI_TIEU end),0) as ctDLUVVIS
        ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 11 THEN CHI_TIEU end),0) as ctDLHPLC
        ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 12 THEN CHI_TIEU end),0) as ctChuando
                
    FROM CSDLYTE_SYT.DPMP_CHI_TIEU_PP_KIEM_NGHIEM t
--      INNER JOIN CSDLYTE_SYT.phong_dalieu_dm_don_vi dv ON t.MA_DON_VI = dv.MA_DON_VI
    WHERE 1=1
                '|| nam ||'
                '|| thang ||'
                '|| ngay ||'
    ';
--    DBMS_OUTPUT.PUT_LINE(vsql);
    OPEN P_RS FOR vsql;
  NULL;
END PROC_GET_CHI_TIEU_DINH_LUONG;