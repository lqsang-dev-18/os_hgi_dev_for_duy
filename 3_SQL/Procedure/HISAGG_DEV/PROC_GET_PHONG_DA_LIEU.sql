create or replace PROCEDURE PROC_GET_PHONG_DA_LIEU(P_NAM NUMBER, P_THANG VARCHAR2, P_TUNGAY VARCHAR2, P_DENNGAY VARCHAR2, P_RS OUT SYS_REFCURSOR) AS 

vsql CLOB;
thang VARCHAR2(200);
nam VARCHAR2(200);
ngay VARCHAR2(200);

BEGIN
    if P_THANG is not null then
        thang := 'AND EXTRACT(MONTH FROM NGAY) in ('|| P_THANG ||')';
    end if;
    if P_TUNGAY is not null AND P_DENNGAY is not null then
        ngay := 'AND (TRUNC(NGAY) BETWEEN TO_DATE('''||P_TUNGAY||''', ''DD/MM/YYYY'') AND TO_DATE('''||P_DENNGAY||''', ''DD/MM/YYYY''))';
    end if;  
    if P_NAM is not null then
        nam := 'AND NAM in ('|| P_NAM ||')';
    end if;
   
    vsql:= '
    SELECT NVL(SUM(CASE WHEN t.MA_TIEU_CHI = 1 THEN thuc_hien end),0) as DHTL
          ,NVL(SUM(CASE WHEN t.MA_TIEU_CHI = 2 THEN thuc_hien end),0) as HOANTHANH_DHTL
          ,NVL(SUM(CASE WHEN t.MA_TIEU_CHI = 3 THEN thuc_hien end),0) as GIAM_SAT
          ,NVL(SUM(CASE WHEN t.MA_TIEU_CHI = 4 THEN thuc_hien end),0) as TIEPXUC
          ,NVL(SUM(CASE WHEN t.MA_TIEU_CHI = 5 THEN thuc_hien end),0) as STI
          ,NVL(SUM(CASE WHEN t.MA_TIEU_CHI = 6 THEN thuc_hien end),0) as GIANGMAI
          ,NVL(SUM(CASE WHEN t.MA_TIEU_CHI = 7 THEN thuc_hien end),0) as LAU
    FROM CSDLYTE_SYT.phong_dalieu_chitieu t
      INNER JOIN CSDLYTE_SYT.phong_dalieu_dm_don_vi dv ON t.MA_DON_VI = dv.MA_DON_VI
    WHERE dv.MA_DON_VI <> ''KH''
                '|| nam ||'
                '|| thang ||'
                '|| ngay ||'
    ';
--    DBMS_OUTPUT.PUT_LINE(vsql);
    OPEN P_RS FOR vsql;
  NULL;
END PROC_GET_PHONG_DA_LIEU;