create or replace PROCEDURE              "PROC_BENH_KHONG_LAY_DAITHAODUONG_HUYEN_HGI" (
    P_NAM           VARCHAR2,
    P_THANG         VARCHAR2,
    P_TUNGAY        VARCHAR2,
    P_DENNGAY       VARCHAR2,
    P_RS OUT SYS_REFCURSOR
) IS
 vsql CLOB;
thang VARCHAR2(200);
nam VARCHAR2(200);
ngay VARCHAR2(200);
v_order VARCHAR2(200) := 'ORDER BY TEN_HUYEN asc';

BEGIN
    if P_THANG is not null then
        thang := 'AND THANG in ('|| P_THANG ||')';
    end if;
    if P_TUNGAY is not null AND P_DENNGAY is not null then
        ngay := 'AND (TRUNC(NGAY) BETWEEN TO_DATE('''||P_TUNGAY||''', ''DD/MM/YYYY'') AND TO_DATE('''||P_DENNGAY||''', ''DD/MM/YYYY''))';
    end if;  
    if P_NAM is not null then
        nam := 'AND NAM in ('|| P_NAM ||')';
    end if;
  vsql:= '
  SELECT
    dm.TEN_HUYEN
    ,ROUND((SUM(SO_BN_PHAT_HIEN))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH))*100, 2) as BN_PHAT_HIEN
    ,ROUND((SUM(SO_BN_QUAN_LY_DIEU_TRI))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH))*100, 2) as BN_DIEU_TRI
    FROM csdlyte_syt.CDC_BENH_KHONG_LAY_DAI_THAO_DUONG_HGI t
    INNER JOIN csdlyte_danhmuc.DM_HUYEN dm ON t.MA_HUYEN = dm.MA_HUYEN
    WHERE 1 = 1
    '|| nam ||'
    '|| thang ||'
    '|| ngay ||'
    GROUP BY dm.TEN_HUYEN
    '|| v_order ||'
  ';

  --OPEN P_RS FOR select vsql as SQL_STORE from dual; 
  DBMS_OUTPUT.PUT_LINE(vsql);
    OPEN P_RS FOR vsql;

END;