create or replace PROCEDURE  "PROC_KCB_TYLE_KCB_COBHYT_HGI" (
    P_LOAI_SS   NUMBER, -- 0: Cung ky ,    1: Lien ke
    P_NAM       VARCHAR2,
    P_QUI       VARCHAR2,
    P_THANG     VARCHAR2,
    P_MA_HUYEN  VARCHAR2,
    P_LOAI_HINH NUMBER,  -- 0: cong lap,   1: tu nhan
    P_MA_CSKCB  VARCHAR2,
    P_TUNGAY       VARCHAR2,
    P_DENNGAY      VARCHAR2,
    P_LOAI NUMBER,
    P_RS        OUT  SYS_REFCURSOR
) IS
  vsql CLOB;
  r_label_truoc VARCHAR2(100);
  r_label_hien_tai VARCHAR2(100);
  v_nam VARCHAR2(200);
  v_nam_truoc VARCHAR2(200);
  v_qui VARCHAR2(200);
  v_qui_truoc VARCHAR2(200);
  v_thang VARCHAR2(200);
  v_thang_truoc VARCHAR2(200);
  v_thang_qui VARCHAR2(200);
  v_thang_qui_truoc VARCHAR2(200);
  v_ma_huyen CLOB;
  v_loai_hinh CLOB;
  v_ma_cskcb CLOB;
  v_list_ma_cskcb CLOB;
  v_songay VARCHAR2(200);
  i_nam number(4,0):= to_number(P_NAM);

  v_ngay VARCHAR2(200);
  v_ngay_truoc VARCHAR2(200);
  v_ngay_hientai VARCHAR2(200);

  r_nam_truoc VARCHAR2(200);
  r_nam_ht VARCHAR2(200);

  r_qui_truoc VARCHAR2(200);
  r_qui_ht VARCHAR2(200);

  r_thang_truoc VARCHAR2(200);
  r_thang_ht VARCHAR2(200);

  Type arr_number_qui IS VARRAY(4) OF INTEGER;
  Type arr_number_thang IS VARRAY(12) OF INTEGER;
  arr_qui arr_number_qui;
  arr_thang arr_number_thang;
  index_thang integer :=0;
  index_qui integer :=0;
  
BEGIN
    v_songay := '(SELECT TO_DATE('''||P_TUNGAY||''', ''yyyy-mm-dd'') - TO_DATE('''||P_TUNGAY||''', ''yyyy-mm-dd'') + 1 AS SoNgay FROM dual)';
    if P_MA_HUYEN is not null then
        select LISTAGG(to_char(dv.MA_DON_VI), ',') as LIST_DON_VI into v_list_ma_cskcb
        from CSDLYTE_DANHMUC.DM_DONVI dv
        WHERE 1 = 1
        AND dv.MA_HUYEN in (P_MA_HUYEN);

        v_ma_huyen := 'AND MA_CSKCB in ('|| v_list_ma_cskcb ||')';
    end if;

    if P_LOAI_HINH is not null then
        select LISTAGG(to_char(dv.MA_DON_VI), ',') as LIST_DON_VI into v_list_ma_cskcb
        from CSDLYTE_DANHMUC.DM_DONVI dv
        WHERE 1 = 1
        AND dv.TU_NHAN in (P_LOAI_HINH);

        v_loai_hinh := 'AND MA_CSKCB in ('|| v_list_ma_cskcb ||')';
    end if;

    if P_MA_CSKCB is not null then
        v_ma_cskcb := 'AND MA_CSKCB in ('|| P_MA_CSKCB ||')';
    end if;


    if P_LOAI_SS = 0 then 
      IF(P_LOAI !=3) THEN

        if P_NAM is not null then
            v_nam := 'AND EXTRACT(YEAR FROM NGAY_RA_DATE) in ('|| P_NAM ||', '||to_char(i_nam - 1)||')';
            r_nam_truoc:='NAM = '||to_char(i_nam - 1);
            r_nam_ht:='NAM = '||to_char(i_nam);

            r_label_truoc := 'Năm '||to_char(i_nam - 1);
            r_label_hien_tai := 'Năm '||to_char(i_nam);

        end if;
        if P_QUI is not null then
            SELECT LISTAGG(case COLUMN_VALUE when '1' then '1,2,3' 
                             when '2' then '4,5,6'
                             when '3' then '7,8,9'
                             when '4' then '10,11,12'
                             else '0'
                            end, ',') as thang into v_thang_qui
            from table(hpg_split_string(P_QUI));
            v_qui := 'AND extract(month from NGAY_RA_DATE) in ('|| v_thang_qui ||')';
            r_qui_ht := 'AND extract(month from NGAY_RA_DATE) in ('|| v_thang_qui ||')';

            r_label_truoc := 'Quý '||P_QUI||' năm '||to_char(i_nam - 1) ;
            r_label_hien_tai := 'Quý '||P_QUI||' năm '||to_char(i_nam) ;
       end if;
        if
            P_THANG is not null then
            v_thang := 'AND extract(month from NGAY_RA_DATE) in ('|| P_THANG ||')';
            r_thang_ht := 'AND extract(month from NGAY_RA_DATE) in ('|| P_THANG ||')';

            r_label_truoc := 'Tháng '||P_THANG||' năm '||to_char(i_nam - 1) ;
            r_label_hien_tai := 'Tháng '||P_THANG||' năm '||to_char(i_nam) ;
       end if;

        vsql:= '
        SELECT case when TT_BHYT = ''BHYT'' then ''Có BHYT'' else ''Không BHYT'' end as TT_BHYT
            ,SUM(case when '||r_nam_truoc||' then SO_LUONG else 0 end) as TG_TRUOC
            ,SUM(case when '||r_nam_ht||' then SO_LUONG else 0 end) as TG_HIEN_TAI
            ,'''||r_label_truoc||''' as LABEL_TRUOC
            ,'''||r_label_hien_tai||''' as LABEL_HIEN_TAI
        FROM (
        select EXTRACT(YEAR FROM NGAY_RA_DATE) as NAM
        ,CASE WHEN NVL(MA_THE, ''KBHYT'') != ''KBHYT'' THEN ''BHYT'' ELSE ''K_BHYT'' END  as TT_BHYT
        ,COUNT(1) as SO_LUONG
        from csdlyte_4210.B1_CHITIEUTONGHOP_KCB 
        WHERE 1 = 1
        '||v_nam||'
        '||v_qui||'
        '||v_thang||'
        '||v_ma_huyen||'
        '||v_loai_hinh||'
        '||v_ma_cskcb||'
        GROUP BY EXTRACT(YEAR FROM NGAY_RA_DATE), NVL(MA_THE, ''KBHYT'')
        )
        GROUP BY TT_BHYT ORDER BY CASE WHEN TT_BHYT=''Có BHYT'' THEN 1  else 2 end
      ';
    else
          vsql:= '
              WITH NGAY_HIENTAI AS(
               SELECT case when TT_BHYT = ''BHYT'' then ''Có BHYT'' else ''Không BHYT'' end as TT_BHYT
                    ,SUM( SO_LUONG ) as TG_HIEN_TAI
                    ,''Ngày hiện tại'' as LABEL_HIEN_TAI
                FROM (
                    select EXTRACT(YEAR FROM NGAY_RA_DATE) as NAM
                        ,CASE WHEN NVL(MA_THE, ''KBHYT'') != ''KBHYT'' THEN ''BHYT'' ELSE ''K_BHYT'' END  as TT_BHYT
                        ,COUNT(1) as SO_LUONG
                        from csdlyte_4210.B1_CHITIEUTONGHOP_KCB 
                        WHERE 1 = 1
                        '||v_ma_huyen||'
                        '||v_loai_hinh||'
                        '||v_ma_cskcb||'
                        AND NGAY_RA_DATE BETWEEN TO_DATE('''||P_TUNGAY||''', ''yyyy-mm-dd'') AND  TO_DATE('''||P_DENNGAY||''', ''yyyy-mm-dd'')
                        GROUP BY EXTRACT(YEAR FROM NGAY_RA_DATE), NVL(MA_THE, ''KBHYT'')
                    )
                GROUP BY TT_BHYT ORDER BY CASE WHEN TT_BHYT=''Có BHYT'' THEN 1  else 2 end
           ),
           NGAY_NAM_1 AS(
               SELECT case when TT_BHYT = ''BHYT'' then ''Có BHYT'' else ''Không BHYT'' end as TT_BHYT
                    ,SUM( SO_LUONG ) as TG_TRUOC
                    ,''Ngày cùng kỳ'' as LABEL_TRUOC
                FROM (
                    select EXTRACT(YEAR FROM NGAY_RA_DATE) as NAM
                        ,CASE WHEN NVL(MA_THE, ''KBHYT'') != ''KBHYT'' THEN ''BHYT'' ELSE ''K_BHYT'' END  as TT_BHYT
                        ,COUNT(1) as SO_LUONG
                        from csdlyte_4210.B1_CHITIEUTONGHOP_KCB 
                        WHERE 1 = 1
                        '||v_ma_huyen||'
                        '||v_loai_hinh||'
                        '||v_ma_cskcb||'
                        AND NGAY_RA_DATE BETWEEN TO_DATE('''||P_TUNGAY||''', ''yyyy-mm-dd'') - INTERVAL ''1'' YEAR 
                        AND TO_DATE('''||P_DENNGAY||''', ''yyyy-mm-dd'') - INTERVAL ''1'' YEAR
                        GROUP BY EXTRACT(YEAR FROM NGAY_RA_DATE), NVL(MA_THE, ''KBHYT'')
                    )
                GROUP BY TT_BHYT ORDER BY CASE WHEN TT_BHYT=''Có BHYT'' THEN 1  else 2 end
           )
           SELECT A.TT_BHYT, CASE WHEN B.LABEL_TRUOC !=NULL THEN ''Ngày cùng kỳ'' ELSE ''Ngày cùng kỳ'' END LABEL_TRUOC
           ,  nvl(B.TG_TRUOC,0) TG_TRUOC, 
           A.LABEL_HIEN_TAI, A.TG_HIEN_TAI
           FROM NGAY_HIENTAI A FULL JOIN NGAY_NAM_1 B ON A.TT_BHYT= B.TT_BHYT
        ';
      end if;
    else
      IF(P_LOAI !=3) THEN
        if P_NAM is not null then
            v_nam := 'AND EXTRACT(YEAR FROM NGAY_RA_DATE) in ('|| P_NAM ||')';
            v_nam_truoc := 'AND EXTRACT(YEAR FROM NGAY_RA_DATE) in ('|| to_char(i_nam - 1) ||')';
            r_nam_truoc:='NAM = '||to_char(i_nam - 1);
            r_nam_ht:='NAM = '||to_char(i_nam);

            r_label_truoc := 'Năm '||to_char(i_nam - 1);
            r_label_hien_tai := 'Năm '||to_char(i_nam);
        end if;

        if P_QUI is not null AND P_THANG is null then

            arr_qui := arr_number_qui(1,2,3,4);
            index_qui := MOD(to_number(P_QUI)-1 + 4, 4);
            if index_qui = 0 then
                index_qui := 4;

                r_label_truoc := 'Quý '||to_char(arr_qui(index_qui))||' năm '||to_char(i_nam - 1);
            else 
                v_nam_truoc := 'AND EXTRACT(YEAR FROM NGAY_RA_DATE) in ('|| P_NAM ||')';

                r_label_truoc := 'Quý '||to_char(arr_qui(index_qui))||' năm '||to_char(i_nam);
            end if;
            r_qui_truoc := to_char(arr_qui(index_qui));
            SELECT LISTAGG(case COLUMN_VALUE when '1' then '1,2,3' 
                             when '2' then '4,5,6'
                             when '3' then '7,8,9'
                             when '4' then '10,11,12'
                             else '0'
                            end, ',') as thang into v_thang_qui_truoc
            from table(hpg_split_string(r_qui_truoc));
            v_qui_truoc := 'AND extract(month from NGAY_RA_DATE) in ('|| v_thang_qui_truoc ||')';

            SELECT LISTAGG(case COLUMN_VALUE when '1' then '1,2,3' 
                             when '2' then '4,5,6'
                             when '3' then '7,8,9'
                             when '4' then '10,11,12'
                             else '0'
                            end, ',') as thang into v_thang_qui
            from table(hpg_split_string(P_QUI));
            v_qui := 'AND extract(month from NGAY_RA_DATE) in ('|| v_thang_qui ||')';

            r_label_hien_tai := 'Quý '||P_QUI||' năm '||to_char(i_nam);
            --r_qui_ht := 'AND extract(month from NGAY_RA_DATE) in ('|| v_thang_qui ||')';
        end if;

        if P_THANG is not null then
            arr_thang := arr_number_thang(1,2,3,4,5,6,7,8,9,10,11,12);
            index_thang := MOD(to_number(P_THANG)-1 + 12, 12);

            if index_thang = 0 then
                index_thang := 12;
                r_label_truoc := 'Tháng '||to_char(arr_thang(index_thang))||' năm '||to_char(i_nam - 1);
            else 
                v_nam_truoc := 'AND EXTRACT(YEAR FROM NGAY_RA_DATE) in ('|| P_NAM ||')';
                r_label_truoc := 'Tháng '||to_char(arr_thang(index_thang))||' năm '||to_char(i_nam);
            end if;

            r_thang_truoc := to_char(arr_thang(index_thang));

            v_thang := 'AND extract(month from NGAY_RA_DATE) in ('|| P_THANG ||')';
            v_thang_truoc := 'AND extract(month from NGAY_RA_DATE) in ('|| r_thang_truoc ||')';
            --r_thang_ht := 'AND extract(month from NGAY_RA_DATE) in ('|| P_THANG ||')';
            r_label_hien_tai := 'Tháng '||P_THANG||' năm '||to_char(i_nam);

        end if;

        vsql:= '
        SELECT TT_BHYT as TT_BHYT
        ,SUM(TG_TRUOC) as TG_TRUOC
        ,SUM(TG_HIEN_TAI) as TG_HIEN_TAI
        ,'''||r_label_truoc||''' as LABEL_TRUOC
        ,'''||r_label_hien_tai||''' as LABEL_HIEN_TAI
        FROM (
            SELECT case when TT_BHYT = ''BHYT'' then ''Có BHYT'' else ''Không BHYT'' end as TT_BHYT
                ,0 as TG_TRUOC
                ,SUM(SO_LUONG) as TG_HIEN_TAI
            FROM (
            select EXTRACT(YEAR FROM NGAY_RA_DATE) as NAM
            ,CASE WHEN NVL(MA_THE, ''KBHYT'') != ''KBHYT'' THEN ''BHYT'' ELSE ''K_BHYT'' END  as TT_BHYT
            ,COUNT(1) as SO_LUONG
            from csdlyte_4210.B1_CHITIEUTONGHOP_KCB 
            WHERE 1 = 1
            '||v_nam||'
            '||v_qui||'
            '||v_thang||'
            '||v_ma_huyen||'
            '||v_loai_hinh||'
            '||v_ma_cskcb||'
            GROUP BY EXTRACT(YEAR FROM NGAY_RA_DATE), NVL(MA_THE, ''KBHYT'')
            )
            GROUP BY TT_BHYT

            UNION ALL

            SELECT case when TT_BHYT = ''BHYT'' then ''Có BHYT'' else ''Không BHYT'' end as TT_BHYT
                ,SUM(SO_LUONG) as TG_TRUOC
                ,0 as TG_HIEN_TAI
            FROM (
            select EXTRACT(YEAR FROM NGAY_RA_DATE) as NAM
            ,CASE WHEN NVL(MA_THE, ''KBHYT'') != ''KBHYT'' THEN ''BHYT'' ELSE ''K_BHYT'' END  as TT_BHYT
            ,COUNT(1) as SO_LUONG
            from csdlyte_4210.B1_CHITIEUTONGHOP_KCB 
            WHERE 1 = 1
            '||v_nam_truoc||'
            '||v_qui_truoc||'
            '||v_thang_truoc||'
            '||v_ma_huyen||'
            '||v_loai_hinh||'
            '||v_ma_cskcb||'
            GROUP BY EXTRACT(YEAR FROM NGAY_RA_DATE), NVL(MA_THE, ''KBHYT'')
            )
            GROUP BY TT_BHYT
        )
        GROUP BY TT_BHYT 
        ORDER BY CASE WHEN TT_BHYT=''Có BHYT'' THEN 1  else 2 end
        ';
    else
        vsql:= '
            SELECT TT_BHYT as TT_BHYT
                ,SUM(TG_TRUOC) as TG_TRUOC
                ,SUM(TG_HIEN_TAI) as TG_HIEN_TAI
                ,''Ngày liền kề'' as LABEL_TRUOC
                ,''Ngày hiện tại'' as LABEL_HIEN_TAI
                FROM (
                    SELECT case when TT_BHYT = ''BHYT'' then ''Có BHYT'' else ''Không BHYT'' end as TT_BHYT
                        ,0 as TG_TRUOC
                        ,SUM(SO_LUONG) as TG_HIEN_TAI
                    FROM (
                    select EXTRACT(YEAR FROM NGAY_RA_DATE) as NAM
                    ,CASE WHEN NVL(MA_THE, ''KBHYT'') != ''KBHYT'' THEN ''BHYT'' ELSE ''K_BHYT'' END  as TT_BHYT
                    ,COUNT(1) as SO_LUONG
                    from csdlyte_4210.B1_CHITIEUTONGHOP_KCB 
                    WHERE 1 = 1
                    '||v_ma_huyen||'
                    '||v_loai_hinh||'
                    '||v_ma_cskcb||'
                        AND NGAY_RA_DATE BETWEEN TO_DATE('''||P_TUNGAY||''', ''yyyy-mm-dd'') AND TO_DATE('''||P_DENNGAY||''', ''yyyy-mm-dd'')
                    GROUP BY EXTRACT(YEAR FROM NGAY_RA_DATE), NVL(MA_THE, ''KBHYT'')
                    )
                    GROUP BY TT_BHYT
        
                    UNION ALL
        
                    SELECT case when TT_BHYT = ''BHYT'' then ''Có BHYT'' else ''Không BHYT'' end as TT_BHYT
                        ,SUM(SO_LUONG) as TG_TRUOC
                        ,0 as TG_HIEN_TAI
                    FROM (
                    select EXTRACT(YEAR FROM NGAY_RA_DATE) as NAM
                    ,CASE WHEN NVL(MA_THE, ''KBHYT'') != ''KBHYT'' THEN ''BHYT'' ELSE ''K_BHYT'' END  as TT_BHYT
                    ,COUNT(1) as SO_LUONG
                    from csdlyte_4210.B1_CHITIEUTONGHOP_KCB 
                    WHERE 1 = 1
                    '||v_ma_huyen||'
                    '||v_loai_hinh||'
                    '||v_ma_cskcb||'
                    AND NGAY_RA_DATE BETWEEN TO_DATE('''||P_TUNGAY||''', ''yyyy-mm-dd'')- '||v_songay||' AND TO_DATE('''||P_TUNGAY||''', ''yyyy-mm-dd'') 
        
                    
                    GROUP BY EXTRACT(YEAR FROM NGAY_RA_DATE), NVL(MA_THE, ''KBHYT'')
                    )
                    GROUP BY TT_BHYT
                )
                GROUP BY TT_BHYT 
                ORDER BY CASE WHEN TT_BHYT=''Có BHYT'' THEN 1  else 2 end
        ';
      end if;
    end if;
    --OPEN P_RS FOR select vsql as SQL_STORE from dual; 
    --OPEN P_RS FOR select r_thang_truoc as SQL_STORE from dual;
    DBMS_OUTPUT.PUT_LINE(vsql);
    OPEN P_RS FOR vsql;
END;