create or replace PROCEDURE PROC_CONG_BO_MY_PHAM_CTY_THEO_HUYEN(
     P_LOAI_CBX NUMBER,
     P_NAM VARCHAR2,
     P_QUY NVARCHAR2, 
     P_THANG NVARCHAR2,
     P_TUNGAY NVARCHAR2,
     P_DENNGAY NVARCHAR2,
     P_RS OUT SYS_REFCURSOR
 ) IS 
    vsql VARCHAR2(5000);
    v_luachon VARCHAR2(1000);
    BEGIN
        if (P_LOAI_CBX = 0) then
            v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-01-01'', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_NAM||'-12-31'', ''YYYY-MM-DD''))';
        elsif (P_LOAI_CBX = 1) then
            if(P_THANG = 0) then
                v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-01-01'', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_NAM||'-12-31'', ''YYYY-MM-DD''))';
            else
                v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-'||P_THANG||'-01'', ''YYYY-MM-DD'') AND LAST_DAY(TO_DATE('''||P_NAM||'-'||P_THANG||''', ''YYYY-MM''))';
            end if;
        elsif (P_LOAI_CBX = 2) then
            IF (P_QUY = 1) THEN
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-01-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-03-31'', ''YYYY-MM-DD'')';
            ELSIF (P_QUY = 2) THEN
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-04-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-06-30'', ''YYYY-MM-DD'')';
            ELSIF (P_QUY = 3) THEN
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-07-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-09-30'', ''YYYY-MM-DD'')';
            ELSE
                v_luachon := 'BETWEEN TO_DATE(''' || P_NAM || '-10-01'', ''YYYY-MM-DD'') AND TO_DATE(''' || P_NAM || '-12-31'', ''YYYY-MM-DD'')';
            END IF;
        else
                v_luachon := 'BETWEEN TO_DATE(''' || P_TUNGAY || ''', ''YYYY-MM-DD'') AND TO_DATE(''' || P_DENNGAY || ''', ''YYYY-MM-DD'')';
        end if;
--    if (P_LOAI_CBX = 0) then
--        if(P_THANG = 0) then
--            v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-01-01'', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_NAM||'-12-31'', ''YYYY-MM-DD''))';
--        else
--            v_luachon := 'BETWEEN TO_DATE('''||P_NAM||'-'||P_THANG||'-01'', ''YYYY-MM-DD'') AND LAST_DAY(TO_DATE('''||P_NAM||'-'||P_THANG||''', ''YYYY-MM''))';
--        end if;
--    else
--        v_luachon := 'BETWEEN TO_DATE('''||P_TUNGAY||''', ''YYYY-MM-DD'') AND TO_DATE(TO_DATE('''||P_DENNGAY||''', ''YYYY-MM-DD''))';
--    end if;
         vsql:= ' 
           SELECT COUNT(TEN_CTY_SANXUAT) SL_CONGTY, DV.TEN_HUYEN
        FROM(
            SELECT MA_CTY_SANXUAT, NGAY_CAP, TEN_CTY_SANXUAT,MA_HUYEN_CTY_SX
            FROM(
                SELECT ROW_NUMBER() OVER(PARTITION BY MA_CTY_SANXUAT ORDER BY MA_CTY_SANXUAT DESC) RN, NGAY_CAP, MA_CTY_SANXUAT, TEN_CTY_SANXUAT, MA_HUYEN_CTY_SX 
                FROM CSDLYTE_SYT.NVD_CONGBO_MYPHAM
                WHERE NGAY_CAP '||v_luachon||'
            ) 
            WHERE RN=1
        ) A JOIN CSDLYTE_DANHMUC.DM_HUYEN DV ON A.MA_HUYEN_CTY_SX = DV.MA_HUYEN
        GROUP BY DV.TEN_HUYEN ORDER BY TEN_HUYEN DESC
        ' ;
    
     OPEN P_RS FOR vsql;
--    
--    P_NAM VARCHAR2,
--    P_THANG NVARCHAR2, 
--    P_RS OUT SYS_REFCURSOR
--) AS 
--BEGIN
--    OPEN P_RS FOR
--        SELECT COUNT(TEN_CTY_SANXUAT) SL_CONGTY, DV.TEN_HUYEN
--        FROM(
--            SELECT MA_CTY_SANXUAT, NGAY_CAP, TEN_CTY_SANXUAT,MA_HUYEN_CTY_SX
--            FROM(
--                SELECT ROW_NUMBER() OVER(PARTITION BY MA_CTY_SANXUAT ORDER BY MA_CTY_SANXUAT DESC) RN, NGAY_CAP, MA_CTY_SANXUAT, TEN_CTY_SANXUAT, MA_HUYEN_CTY_SX 
--                FROM CSDLYTE_SYT.NVD_CONGBO_MYPHAM
--                
--            ) 
--            WHERE RN=1
--        ) A JOIN CSDLYTE_DANHMUC.DM_HUYEN DV ON A.MA_HUYEN_CTY_SX = DV.MA_HUYEN
--        GROUP BY DV.TEN_HUYEN ORDER BY TEN_HUYEN DESC;
END PROC_CONG_BO_MY_PHAM_CTY_THEO_HUYEN;