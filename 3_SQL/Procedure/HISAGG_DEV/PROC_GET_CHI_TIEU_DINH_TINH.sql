CREATE OR REPLACE PROCEDURE PROC_GET_CHI_TIEU_DINH_TINH(P_NAM NUMBER, P_THANG VARCHAR2, P_TUNGAY VARCHAR2, P_DENNGAY VARCHAR2, P_RS OUT SYS_REFCURSOR) AS 

vsql CLOB;
thang VARCHAR2(200);
nam VARCHAR2(200);
ngay VARCHAR2(200);
 
BEGIN
if P_THANG is not null then
        thang := 'AND EXTRACT(MONTH FROM NGAY_BAO_CAO) in ('|| P_THANG ||')';
    end if;
    if P_TUNGAY is not null AND P_DENNGAY is not null then
        ngay := 'AND (TRUNC(NGAY_BAO_CAO) BETWEEN TO_DATE('''||P_TUNGAY||''', ''DD/MM/YYYY'') AND TO_DATE('''||P_DENNGAY||''', ''DD/MM/YYYY''))';
    end if;  
    if P_NAM is not null then
        nam := 'AND NAM in ('|| P_NAM ||')';
    end if;
 vsql:= '
    SELECT  --bieu do 2
        NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 4 THEN CHI_TIEU end),0) as ctSoibot
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 5 THEN CHI_TIEU end),0) as ctDTUVVIS
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 6 THEN CHI_TIEU end),0) as ctDTHPLC
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 7 THEN CHI_TIEU end),0) as ctHoahoc
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 8 THEN CHI_TIEU end),0) as ctIR
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 9 THEN CHI_TIEU end),0) as ctSacky
                
    FROM CSDLYTE_SYT.DPMP_CHI_TIEU_PP_KIEM_NGHIEM t
--      INNER JOIN CSDLYTE_SYT.phong_dalieu_dm_don_vi dv ON t.MA_DON_VI = dv.MA_DON_VI
    WHERE 1=1
                '|| nam ||'
                '|| thang ||'
                '|| ngay ||'
    ';
--    DBMS_OUTPUT.PUT_LINE(vsql);
    OPEN P_RS FOR vsql;
  NULL;
END PROC_GET_CHI_TIEU_DINH_TINH;