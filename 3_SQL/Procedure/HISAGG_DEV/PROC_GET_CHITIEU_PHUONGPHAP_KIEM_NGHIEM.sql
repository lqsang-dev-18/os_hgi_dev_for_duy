create or replace PROCEDURE PROC_GET_CHITIEU_PHUONGPHAP_KIEM_NGHIEM(P_NAM NUMBER, P_THANG VARCHAR2, P_TUNGAY VARCHAR2, P_DENNGAY VARCHAR2, P_RS OUT SYS_REFCURSOR) AS 

vsql CLOB;
thang VARCHAR2(200);
nam VARCHAR2(200);
ngay VARCHAR2(200);

BEGIN
    if P_THANG is not null then
        thang := 'AND EXTRACT(MONTH FROM NGAY_BAO_CAO) in ('|| P_THANG ||')';
    end if;
    if P_TUNGAY is not null AND P_DENNGAY is not null then
        ngay := 'AND (TRUNC(NGAY_BAO_CAO) BETWEEN TO_DATE('''||P_TUNGAY||''', ''DD/MM/YYYY'') AND TO_DATE('''||P_DENNGAY||''', ''DD/MM/YYYY''))';
    end if;  
    if P_NAM is not null then
        nam := 'AND NAM in ('|| P_NAM ||')';
    end if;

    vsql:= '
    SELECT --6 the tong chi tieu
        NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 1 THEN CHI_TIEU end),0) as ctTinhchatMota
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 2 THEN CHI_TIEU end),0) as ctDodongdeu
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 3 THEN CHI_TIEU end),0) as ctDoraHoatan
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 13 THEN CHI_TIEU end),0) as ctDopH
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 14 THEN CHI_TIEU end),0) as ctTapchat
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 15 THEN CHI_TIEU end),0) as ctKhac 
          --bieu do 1 
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 1 THEN THUC_HIEN end),0) as thTinhchatMota
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 2 THEN THUC_HIEN end),0) as thDodongdeu
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 3 THEN THUC_HIEN end),0) as thDoraHoatan
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 13 THEN THUC_HIEN end),0) as thDopH
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 14 THEN THUC_HIEN end),0) as thTapchat
          ,NVL(SUM(CASE WHEN t.MA_PHUONG_PHAP = 15 THEN THUC_HIEN end),0) as thKhac
    
    FROM CSDLYTE_SYT.DPMP_CHI_TIEU_PP_KIEM_NGHIEM t
--      INNER JOIN CSDLYTE_SYT.phong_dalieu_dm_don_vi dv ON t.MA_DON_VI = dv.MA_DON_VI
    WHERE 1=1
                '|| nam ||'
                '|| thang ||'
                '|| ngay ||'
    ';
--    DBMS_OUTPUT.PUT_LINE(vsql);
    OPEN P_RS FOR vsql;
  NULL;
END PROC_GET_CHITIEU_PHUONGPHAP_KIEM_NGHIEM;