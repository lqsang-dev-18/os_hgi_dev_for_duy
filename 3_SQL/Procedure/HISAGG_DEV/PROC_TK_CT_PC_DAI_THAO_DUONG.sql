create or replace procedure PROC_TK_CT_PC_DAI_THAO_DUONG(P_NAM      NUMBER,
                                                         P_THANG    NUMBER,
                                                         P_MA_HUYEN VARCHAR2,
                                                         P_RS       OUT SYS_REFCURSOR) is
begin
  OPEN P_RS FOR
    SELECT X.TEN_HUYEN,
           X.NOI_DUNG,
           X.CHI_TIEU,
           X.THUC_HIEN,
           X.CONG_DON,
           ROUND(X.CONG_DON / X.CHI_TIEU, 3) TY_LE_DAT
      FROM (SELECT H.TEN_HUYEN,
                   DM.NOIDUNG NOI_DUNG,
                   CT.CHI_TIEU,
                   TH.THUC_HIEN,
                   (SELECT SUM(A.THUC_HIEN) CONG_DON
                      FROM CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG A
                     WHERE A.NAM = P_NAM
                       AND A.THANG <= P_THANG
                       AND DM.ID = A.ID
                       AND A.MA_HUYEN = TH.MA_HUYEN
                     GROUP BY A.NAM, A.ID) AS CONG_DON
              FROM CSDLYTE_SYT.CDC_DM_CT_PC_DAI_THAO_DUONG DM
              LEFT JOIN CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_DAI_THAO_DUONG CT
                ON DM.ID = CT.ID
              LEFT JOIN CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG TH
                ON DM.ID = TH.ID
               AND TH.NAM = CT.NAM
              LEFT JOIN CSDLYTE_DANHMUC.DM_HUYEN H
                ON TH.MA_HUYEN = H.MA_HUYEN
             WHERE TH.NAM = P_NAM
               AND (P_MA_HUYEN IS NULL OR TH.MA_HUYEN = P_MA_HUYEN)
               AND TH.THANG = P_THANG) X;

end PROC_TK_CT_PC_DAI_THAO_DUONG;
/
