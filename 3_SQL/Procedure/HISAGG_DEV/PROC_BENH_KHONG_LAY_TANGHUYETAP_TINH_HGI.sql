create or replace PROCEDURE              "PROC_BENH_KHONG_LAY_TANGHUYETAP_TINH_HGI" (
   P_NAM           VARCHAR2,
    P_THANG         VARCHAR2,
    P_TUNGAY        VARCHAR2,
    P_DENNGAY       VARCHAR2,
    P_RS OUT SYS_REFCURSOR
) IS
 vsql CLOB;
thang VARCHAR2(200);
nam VARCHAR2(200);
ngay VARCHAR2(200);
v_order VARCHAR2(200) := '';

BEGIN

    if P_THANG is not null then
        thang := 'AND THANG in ('|| P_THANG ||')';
    end if;
    if P_TUNGAY is not null AND P_DENNGAY is not null then
        ngay := 'AND (TRUNC(NGAY) BETWEEN TO_DATE('''||P_TUNGAY||''', ''DD/MM/YYYY'') AND TO_DATE('''||P_DENNGAY||''', ''DD/MM/YYYY''))';
    end if;  
    if P_NAM is not null then
        nam := 'AND NAM in ('|| P_NAM ||')';
    end if;


  vsql:= '
  SELECT
    ROUND((100*SUM(SO_BN_PHAT_HIEN))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) as BN_PHAT_HIEN
    ,ROUND((100*SUM(SO_BN_QUAN_LY_DIEU_TRI))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) as BN_QUAN_LY_DIEU_TRI
    ,100 - ROUND((100*SUM(SO_BN_PHAT_HIEN))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) - ROUND((100*SUM(SO_BN_QUAN_LY_DIEU_TRI))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) as CHUA_BN_PHAT_HIEN
    FROM csdlyte_syt.CDC_BENH_KHONG_LAY_TANG_HUYET_AP_HGI t
    WHERE 1 = 1
    '|| nam ||'
    '|| thang ||'
    '|| ngay ||'

    '|| v_order ||'
  ';

  --OPEN P_RS FOR select vsql as SQL_STORE from dual; 
    OPEN P_RS FOR vsql;

END;

--create or replace PROCEDURE              "PROC_BENH_KHONG_LAY_TANGHUYETAP_TINH" (
--    P_NAM           VARCHAR2,
--    P_QUI           VARCHAR2,
--    P_THANG         VARCHAR2,
--    P_RS OUT SYS_REFCURSOR
--) IS
--  vsql CLOB;
--  v_nam VARCHAR2(200);
--  v_qui VARCHAR2(200);
--  v_thang VARCHAR2(200);
--  v_thang_qui VARCHAR2(200);
--  v_thang_nam_hientai NUMBER(3):= 0;
--  v_count_thang NUMBER(3):= 12;
--  v_order VARCHAR2(200) := '';
--
--BEGIN
--
--    if P_NAM is not null then
--        v_nam := 'AND t.NAM in ('|| P_NAM ||')';
--    end if;
--
--    if P_QUI is not null then
--        SELECT LISTAGG(case COLUMN_VALUE when '1' then '1,2,3' 
--                         when '2' then '4,5,6'
--                         when '3' then '7,8,9'
--                         when '4' then '10,11,12'
--                         else '0'
--                        end, ',') as thang into v_thang_qui
--        from table(hpg_split_string(P_QUI));
--        v_qui := 'AND t.THANG in ('|| v_thang_qui ||')';
--    end if;
--
--    if P_THANG is not null then
--        v_thang := 'AND t.THANG in ('|| P_THANG ||')';
--    else 
--        v_thang := 'AND t.THANG in (12)';
--    end if;
--
--  vsql:= '
--  SELECT
--    ROUND((100*SUM(SO_BN_PHAT_HIEN))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) as BN_PHAT_HIEN
--    ,ROUND((100*SUM(SO_BN_QUAN_LY_DIEU_TRI))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) as BN_QUAN_LY_DIEU_TRI
--    ,100 - ROUND((100*SUM(SO_BN_PHAT_HIEN))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) - ROUND((100*SUM(SO_BN_QUAN_LY_DIEU_TRI))/(SUM(TONG_DAN_SO) * SUM(TY_LE_UOC_TINH)/100), 1) as CHUA_BN_PHAT_HIEN
--    FROM csdlyte_syt.CDC_BENH_KHONG_LAY_TANG_HUYET_AP t
--    WHERE 1 = 1
--    '|| v_nam ||'
--    '|| v_qui ||'
--    '|| v_thang ||'
--
--    '|| v_order ||'
--  ';
--
--  --OPEN P_RS FOR select vsql as SQL_STORE from dual; 
--    OPEN P_RS FOR vsql;
--
--END;