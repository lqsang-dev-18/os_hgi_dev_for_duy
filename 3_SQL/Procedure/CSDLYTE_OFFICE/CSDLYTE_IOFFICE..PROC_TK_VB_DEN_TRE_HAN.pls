create or replace PROCEDURE                 PROC_TK_VB_DEN_TRE_HAN 
(
  P_NAM IN VARCHAR2 
, P_THANG IN VARCHAR2 
, P_MADONVI IN VARCHAR2 
, P_RS OUT SYS_REFCURSOR 
) AS 
  vsql CLOB;
  v_nam VARCHAR2(200);
  v_thang VARCHAR2(200);
  v_madonvi VARCHAR2(200);
BEGIN
  if P_NAM is not null then
        v_nam := 'AND TO_CHAR(t.NGAY_CAP_NHAT, ''YYYY'') in ('|| P_NAM ||')';
    end if;

    if P_THANG is not null then
        v_thang := 'AND t.GIA_TRI_THANG in ('|| P_THANG ||')';
    end if;
    
    if P_MADONVI is not null then
        v_madonvi := 'AND t.MA_DON_VI_KC in ('|| P_MADONVI ||')';
    end if;

  vsql:= '
  SELECT 
         t.MA_DON_VI_KC AS MA_DON_VI
         , t.TEN_DON_VI AS TEN_DON_VI
         , t.TS_VBDE_DA_XU_LY_TRE_HAN AS TS_VBDE_DA_XU_LY_TRE_HAN
         , t.TS_VBDE_CHUA_XU_LY_TRE_HAN AS TS_VBDE_CHUA_XU_LY_TRE_HAN
         , t.TS_VBDE_CHUA_XU_LY_SAP_DEN_HAN AS TS_VBDE_CHUA_XU_LY_SAP_DEN_HAN
    FROM CSDLYTE_IOFFICE.IOFFICE_V4_API_TK_VBDE_CHUA_XU_LY t
    WHERE 1 = 1
        '|| v_nam ||'
        '|| v_thang ||'
        '|| v_madonvi ||'
  ';

  --OPEN cur FOR select vsql as SQL_STORE from dual; 
    OPEN P_RS FOR vsql;
END PROC_TK_VB_DEN_TRE_HAN;