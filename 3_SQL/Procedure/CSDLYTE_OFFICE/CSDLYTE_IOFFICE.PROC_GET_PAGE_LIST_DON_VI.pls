create or replace PROCEDURE                                                                                 PROC_GET_PAGE_LIST_DON_VI 
(
  P_PAGE              IN NUMBER,
  P_SIZE              IN NUMBER,
  P_RS OUT SYS_REFCURSOR 
) AS 
BEGIN
  OPEN P_RS FOR 
    SELECT *
    FROM (SELECT R.*,
                 ROWNUM RNUM, COUNT(1) OVER () TOTAL_ROW FROM
                 --Main query
                 (SELECT a.MA_DON_VI_CON, a.TEN_DON_VI_CON
                        FROM CSDLYTE_IOFFICE.IOFFICE_V4_DANH_MUC_DON_VI a
                        WHERE a.TRANG_THAI_DON_VI = 1
                        ORDER BY a.MA_DON_VI_CON
                        ) R)
                 --End
    WHERE RNUM BETWEEN ((P_SIZE * (P_PAGE - 1)) + 1) AND (P_SIZE * P_PAGE);
END PROC_GET_PAGE_LIST_DON_VI;