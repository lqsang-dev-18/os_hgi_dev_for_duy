create or replace PROCEDURE                                                 PROC_TK_TINH_HINH_PHE_DUYET_VB_DEN 
(
  P_NAM IN VARCHAR2 
, P_THANG IN VARCHAR2 
, P_MADONVI IN VARCHAR2 
, P_RS OUT SYS_REFCURSOR 
) AS 
  vsql CLOB;
  v_nam VARCHAR2(200);
  v_thang VARCHAR2(200);
  v_madonvi VARCHAR2(200);
BEGIN
  if P_NAM is not null then
        v_nam := 'AND TO_CHAR(tk.NGAY_CAPNHAT, ''YYYY'') in ('|| P_NAM ||')';
    end if;

    if P_THANG is not null then
        v_thang := 'AND tk.THANG_BAOCAO in ('|| P_THANG ||')';
    end if;
    
    if P_MADONVI is not null then
        v_madonvi := 'AND tk.ID_DON_VI in ('|| P_MADONVI ||')';
    end if;
  vsql:= '
  SELECT DISTINCT thongke.ID_KY,
                ds_don_vi.MA_DON_VI_CON,
                ds_don_vi.TEN_DON_VI_CON,
                thongke.THANG_BAOCAO,
                thongke.KY_TU,
                thongke.KY_DEN,
                SUM(thongke.TONG_VB) over(PARTITION BY ds_don_vi.MA_DON_VI_CON, thongke.KY_TU, thongke.KY_DEN) AS TONG_VB,
                SUM(thongke.VB_CHUA_DUYET) over(PARTITION BY ds_don_vi.MA_DON_VI_CON, thongke.KY_TU, thongke.KY_DEN) AS VB_CHUA_DUYET,
                SUM(thongke.VB_DA_DUYET) over(PARTITION BY ds_don_vi.MA_DON_VI_CON, thongke.KY_TU, thongke.KY_DEN) AS VB_DA_DUYET
FROM
  (SELECT tk.*,
          ky.ID_KY
   FROM CSDLYTE_IOFFICE.ioffice_v4_api_tk_tinh_hinh_phe_duyet_van_ban tk,
        CSDLYTE_DANHMUC.DM_KY ky
   WHERE tk.TONG_VB IS NOT NULL
     AND ky.LOAI_KY = ''M''
     AND ky.HIEU_LUC_TU = tk.KY_TU
     AND ky.HIEU_LUC_DEN = tk.KY_DEN) thongke,
  (select b.ky_tu from (select distinct ky_tu from CSDLYTE_IOFFICE.ioffice_v4_api_tk_tinh_hinh_phe_duyet_van_ban WHERE extract(year from to_date(ky_tu)) = extract(year from (SYSDATE)) AND extract(month from to_date(ky_tu)) = (extract(month from (SYSDATE)))) b) max_ky_tu, 
  (SELECT tkdv.ma,
          dv.MA_DON_VI_CON,
          dv.TEN_DON_VI_CON
   FROM CSDLYTE_IOFFICE.ioffice_v4_danh_muc_don_vi dv,
     (SELECT DISTINCT tk.ma,
                      tk.cha,
                      tk.TEN
      FROM CSDLYTE_IOFFICE.ioffice_v4_api_tk_tinh_hinh_phe_duyet_van_ban tk
      WHERE tk.TONG_VB IS NULL
        AND cha <> ''DV''
        '|| v_nam ||'
        '|| v_thang ||'
        '|| v_madonvi ||') tkdv
   WHERE concat(''DV'', dv.ma_don_vi_con) = tkdv.cha) ds_don_vi
WHERE thongke.cha = ds_don_vi.MA AND thongke.KY_TU = max_ky_tu.ky_tu
ORDER BY TONG_VB DESC, ds_don_vi.MA_DON_VI_CON, KY_TU
';
--OPEN cur FOR select vsql as SQL_STORE from dual; 
    OPEN P_RS FOR vsql;
END PROC_TK_TINH_HINH_PHE_DUYET_VB_DEN;