create or replace PROCEDURE                                 PROC_TK_2_CHI_TIEU 
(
  P_NAM IN VARCHAR2 
, P_THANG IN VARCHAR2 
, P_MADONVI IN VARCHAR2 
, P_RS OUT SYS_REFCURSOR 
) AS 
  vsql CLOB;
  v_nam VARCHAR2(200);
  v_thang VARCHAR2(200);
  v_madonvi VARCHAR2(200);
BEGIN
  if P_NAM is not null then
        v_nam := 'AND TO_CHAR(t.NGAY_CAPNHAT, ''YYYY'') in ('|| P_NAM ||')';
    end if;

    if P_THANG is not null then
        v_thang := 'AND t.THANG_BAOCAO in ('|| P_THANG ||')';
    end if;
    
    if P_MADONVI is not null then
        v_madonvi := 'AND t.MA_DON_VI in ('|| P_MADONVI ||')';
    end if;

  vsql:= '
  SELECT
         COUNT(s.MA_DON_VI) AS TONG_SO_DON_VI
         , SUM(s.TONG_SO_DI) AS TONG_SO_DI
         , SUM(s.TONG_SO_DEN) AS TONG_SO_DEN
  FROM
        (SELECT 
                 t.MA_DON_VI AS MA_DON_VI
                 , SUM(t.TONG_SO_DI) AS TONG_SO_DI
                 , SUM(t.TONG_SO_DEN) AS TONG_SO_DEN
                 , TO_CHAR(t.NGAY_CAPNHAT, ''YYYY'') AS NAM
            FROM CSDLYTE_IOFFICE.IOFFICE_V4_API_TK_VAN_BAN_DI_DEN_TOAN_TINH t
            WHERE 1 = 1
                '|| v_nam ||'
                '|| v_thang ||'
                '|| v_madonvi ||'
            GROUP BY t.MA_DON_VI, t.NGAY_CAPNHAT) s
  ';

  --OPEN cur FOR select vsql as SQL_STORE from dual; 
    OPEN P_RS FOR vsql;
END PROC_TK_2_CHI_TIEU;