-- Create table
create table CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_ROI_LOAN_DO_THIEU_IOT
(
  nam       NUMBER not null,
  thang     NUMBER not null,
  thuc_hien NUMBER not null,
  id        NUMBER not null,
  ma_huyen  VARCHAR2(20) not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_ROI_LOAN_DO_THIEU_IOT
  add constraint CDC_THUC_HIEN_CT_PC_ROI_LOAN_DO_THIEU_IOT_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_CT_PC_ROI_LOAN_DO_THIEU_IOT (ID);
-- Create table
create table CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG
(
  nam       NUMBER not null,
  thang     NUMBER not null,
  thuc_hien NUMBER not null,
  id        NUMBER not null,
  ma_huyen  VARCHAR2(20)
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG.nam
  is 'Năm';
comment on column CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG.thang
  is 'Tháng';
comment on column CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG.thuc_hien
  is 'Số lượng thực hiện';
comment on column CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG.id
  is 'ID công tác phog chống đái tháo đường';
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG
  add constraint CDC_THUC_HIEN_CT_PC_DAI_THAO_DUONG_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_CT_PC_DAI_THAO_DUONG (ID);
-- Create table
create table CSDLYTE_SYT.CDC_THUC_HIEN_CONG_TAC_TIEM_CHUNG_MR
(
  nam       NUMBER not null,
  thang     NUMBER not null,
  thuc_hien NUMBER not null,
  id        NUMBER not null,
  ma_huyen  VARCHAR2(20)
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CSDLYTE_SYT.CDC_THUC_HIEN_CONG_TAC_TIEM_CHUNG_MR.nam
  is 'Năm';
comment on column CSDLYTE_SYT.CDC_THUC_HIEN_CONG_TAC_TIEM_CHUNG_MR.thang
  is 'Tháng';
comment on column CSDLYTE_SYT.CDC_THUC_HIEN_CONG_TAC_TIEM_CHUNG_MR.thuc_hien
  is 'Số lượng thực hiện';
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_THUC_HIEN_CONG_TAC_TIEM_CHUNG_MR
  add constraint CDC_THUC_HIEN_CONG_TAC_TIEM_CHUNG_MR_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_CONG_TAC_TIEM_CHUNG_MR (ID);
-- Create table
create table CSDLYTE_SYT.CDC_DM_CT_PC_ROI_LOAN_DO_THIEU_IOT
(
  id      NUMBER not null,
  noidung VARCHAR2(500) not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_DM_CT_PC_ROI_LOAN_DO_THIEU_IOT
  add constraint CDC_DM_CT_PC_ROI_LOAN_DO_THIEU_IOT_PK primary key (ID)
  using index 
  tablespace HIS_AGG
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create table
create table CSDLYTE_SYT.CDC_DM_CT_PC_DAI_THAO_DUONG
(
  id      NUMBER not null,
  noidung VARCHAR2(500) not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_DM_CT_PC_DAI_THAO_DUONG
  add constraint CDC_DM_CT_PC_DAI_THAO_DUONG_PK primary key (ID)
  using index 
  tablespace HIS_AGG
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create table
create table CSDLYTE_SYT.CDC_DM_CONG_TAC_TIEM_CHUNG_MR
(
  id      NUMBER not null,
  noidung VARCHAR2(200) not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_DM_CONG_TAC_TIEM_CHUNG_MR
  add constraint CDC_DM_CONG_TAC_TIEM_CHUNG_MR_PK primary key (ID)
  using index 
  tablespace HIS_AGG
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create table
create table CSDLYTE_SYT.CDC_CHI_TIEU_PHONG_CHONG_HEN_PHE_QUAN
(
  nam      NUMBER not null,
  chi_tieu NUMBER not null,
  id       NUMBER not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_CHI_TIEU_PHONG_CHONG_HEN_PHE_QUAN
  add constraint CDC_CHI_TIEU_PHONG_CHONG_HEN_PHE_QUAN_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_PHONG_CHONG_HEN_PHE_QUAN (ID);
-- Create table
create table CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_ROI_LOAN_DO_THIEU_IOT
(
  nam      NUMBER not null,
  chi_tieu NUMBER not null,
  id       NUMBER not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_ROI_LOAN_DO_THIEU_IOT
  add constraint CDC_CHI_TIEU_CT_PC_ROI_LOAN_DO_THIEU_IOT_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_CT_PC_ROI_LOAN_DO_THIEU_IOT (ID);
-- Create table
create table CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_DAI_THAO_DUONG
(
  nam      NUMBER not null,
  chi_tieu NUMBER not null,
  id       NUMBER not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Add comments to the columns 
comment on column CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_DAI_THAO_DUONG.nam
  is 'Năm';
comment on column CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_DAI_THAO_DUONG.chi_tieu
  is 'Chỉ tiêu';
comment on column CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_DAI_THAO_DUONG.id
  is 'ID công tác phòng chog đái tháo đường';
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_CHI_TIEU_CT_PC_DAI_THAO_DUONG
  add constraint CDC_CHI_TIEU_CT_PC_DAI_THAO_DUONG_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_CT_PC_DAI_THAO_DUONG (ID);
-- Create table
create table CSDLYTE_SYT.CDC_CHI_TIEU_CONG_TAC_TIEM_CHUNG_MR
(
  nam               NUMBER not null,
  id                NUMBER not null,
  chi_tieu          NUMBER not null,
  chi_tieu_hien_thi VARCHAR2(200) not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_CHI_TIEU_CONG_TAC_TIEM_CHUNG_MR
  add constraint CDC_CHI_TIEU_CONG_TAC_TIEM_CHUNG_MR_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_CONG_TAC_TIEM_CHUNG_MR (ID);
-- Create table
create table CSDLYTE_SYT.CDC_THUC_HIEN_PHONG_CHONG_HEN_PHE_QUAN
(
  nam       NUMBER not null,
  thang     NUMBER not null,
  thuc_hien NUMBER not null,
  ma_huyen  VARCHAR2(20) not null,
  id        NUMBER not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_THUC_HIEN_PHONG_CHONG_HEN_PHE_QUAN
  add constraint CDC_THUC_HIEN_PHONG_CHONG_HEN_PHE_QUAN_FK1 foreign key (ID)
  references CSDLYTE_SYT.CDC_DM_PHONG_CHONG_HEN_PHE_QUAN (ID);
-- Create table
create table CSDLYTE_SYT.CDC_DM_PHONG_CHONG_HEN_PHE_QUAN
(
  id      NUMBER not null,
  noidung VARCHAR2(500) not null
)
tablespace HIS_AGG
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table CSDLYTE_SYT.CDC_DM_PHONG_CHONG_HEN_PHE_QUAN
  add constraint CDC_DM_PHONG_CHONG_HEN_PHE_QUAN_PK primary key (ID)
  using index 
  tablespace HIS_AGG
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    next 1M
    minextents 1
    maxextents unlimited
  );
